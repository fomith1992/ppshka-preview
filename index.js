import { AppRegistry } from 'react-native';
import App from './src/app';

import { name as appName } from './app.json';

import 'react-native-get-random-values';
import { LogBox } from 'react-native';

LogBox.ignoreAllLogs(true);

AppRegistry.registerComponent(appName, () => App);
