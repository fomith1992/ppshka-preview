import { Text, View } from 'react-native';

import { color } from '@shared/config/color';
import { styled } from '@shared/config/styled';

import { getScaleSize } from 'src/utils/dimensions';

import { font14m, font20m } from '../../../commonStyles/fonts';

export const TitleContainer = styled(View, {
  flexDirection: 'row',
  alignItems: 'flex-end',
  justifyContent: 'space-between',
  paddingHorizontal: getScaleSize(15),
  marginBottom: getScaleSize(12),
  marginTop: getScaleSize(20),
});

export const MainTitle = styled(Text, {
  ...font20m,
  color: color('gray10'),
});

export const DetailsText = styled(Text, {
  ...font14m,
  color: color('gray6'),
});

export const Separator = styled(View, {
  marginHorizontal: getScaleSize(8),
});
