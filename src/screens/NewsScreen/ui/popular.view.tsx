import React from 'react';
import { TouchableOpacity, View } from 'react-native';
import { FlatList } from 'react-native-gesture-handler';

import { NewsSmallView } from 'src/components/news-small/news-small.view';
import { getScaleSize } from 'src/utils/dimensions';
import { hitSlopParams } from 'src/utils/hit-slop-params';

import * as UI from './popular.view.style';

const mockPhotos = Array.from({ length: 5 }, (_, i) => `https://picsum.photos/id/${i + 1}/200/300`);

export const PopularNewsView = () => {
  return (
    <View>
      <UI.TitleContainer>
        <UI.MainTitle>Популярные новости</UI.MainTitle>
        <TouchableOpacity hitSlop={hitSlopParams('M')}>
          <UI.DetailsText>Подробнее</UI.DetailsText>
        </TouchableOpacity>
      </UI.TitleContainer>
      <FlatList
        horizontal={true}
        showsHorizontalScrollIndicator={false}
        contentContainerStyle={{ paddingHorizontal: getScaleSize(15) }}
        data={mockPhotos}
        renderItem={({ item }) => (
          <TouchableOpacity onPress={() => undefined}>
            <NewsSmallView photo={item} />
          </TouchableOpacity>
        )}
        keyExtractor={(_, idx) => String(idx)}
        ItemSeparatorComponent={() => <UI.Separator />}
      />
    </View>
  );
};
