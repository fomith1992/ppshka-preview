import { StyleSheet } from 'react-native';

import { font20m } from 'src/commonStyles/fonts';

import { bottomTabsHeight, getScaleSize, statusBarHeight } from '../../utils/dimensions';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f5f5f5',
  },
  header: {
    paddingTop: statusBarHeight + getScaleSize(6),
  },
  containerScroll: {
    paddingBottom: getScaleSize(35) + bottomTabsHeight,
  },
  titlePopular: {
    ...font20m,
    color: '#262626',
    marginTop: getScaleSize(20),
    marginBottom: getScaleSize(16),
    paddingHorizontal: getScaleSize(15),
  },
});
