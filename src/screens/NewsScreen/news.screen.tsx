import React from 'react';
import { FlatList, Text, View } from 'react-native';

import { HeaderLight } from 'src/components/header-light/header-light.view';
import { PostCard } from 'src/components/post-card/post-card.container';
import ScreenLayout from 'src/components/ScreenLayout';
import { bottomTabsHeight, getScaleSize } from 'src/utils/dimensions';

import { styles } from './styles';
import { PopularNewsView } from './ui/popular.view';

export const NewsScreen = () => {
  return (
    <ScreenLayout wrapperStyle={styles.container} edges={['bottom', 'left', 'right']}>
      <HeaderLight style={styles.header} />
      <FlatList
        data={['1', '2', '3']}
        showsVerticalScrollIndicator={false}
        ListHeaderComponent={
          <>
            <PopularNewsView />
            <Text style={styles.titlePopular}>Новости</Text>
          </>
        }
        renderItem={() => (
          <View style={{ marginHorizontal: getScaleSize(20) }}>
            <PostCard />
          </View>
        )}
        keyExtractor={(_, idx) => idx.toString()}
        contentContainerStyle={{ paddingBottom: getScaleSize(20) + bottomTabsHeight }}
      />
    </ScreenLayout>
  );
};
