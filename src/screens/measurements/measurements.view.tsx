import React from 'react';
import {
  ButtonContainer,
  HeaderContainer,
  HeaderDescription,
  HeaderTitle,
  ScrollContainer,
  styles,
} from './measurements.style';
import { MeasurementsCard } from './measurements-card/measurements-card.container';
import { GradientLayoutHeaderView } from '@shared/ui/gradient-layout/gradient-layout-header.view';
import { Image, KeyboardAvoidingView, Platform, View } from 'react-native';

import logo from '../../shared/ui/assets/logo.png';
import Button from 'src/components/Button';
import { MeasurementsParams, measurementsData } from './mock-data/mock-data';
import ScreenLayout from 'src/components/ScreenLayout';

interface MeasurementsViewProps {
  measurementsParams: MeasurementsParams;
  onSetMeasurementsParams: (item: MeasurementsParams) => void;
  onSubmit: () => void;
}

export const MeasurementsView = ({
  measurementsParams,
  onSetMeasurementsParams,
  onSubmit,
}: MeasurementsViewProps): React.ReactElement => {
  return (
    <ScreenLayout
      safeArea
      style={{
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
      }}
      edges={['bottom']}
    >
      <KeyboardAvoidingView
        style={{ flex: 1 }}
        behavior={Platform.OS === 'ios' ? 'padding' : 'padding'}
      >
        <View style={{ flex: 1 }}>
          <GradientLayoutHeaderView>
            <HeaderContainer>
              <Image style={{ width: 60, height: 60 }} source={logo} />
              <HeaderTitle>Замеры</HeaderTitle>
              <HeaderDescription>Введите пожалуйста свои параметры</HeaderDescription>
            </HeaderContainer>
          </GradientLayoutHeaderView>
          <ScrollContainer contentContainerStyle={styles.scrollContainer}>
            {measurementsData.map((item, idx) => (
              <MeasurementsCard
                key={idx + item.cardName}
                data={item.data.map((x) => ({
                  value: measurementsParams[x.type],
                  onChange: (text) =>
                    onSetMeasurementsParams({ ...measurementsParams, [x.type]: text }),
                  placeholder: x.placeholder,
                }))}
                cardName={item.cardName}
              />
            ))}
            <ButtonContainer>
              <Button uppercase onPress={onSubmit} content="ввод" />
            </ButtonContainer>
          </ScrollContainer>
        </View>
      </KeyboardAvoidingView>
    </ScreenLayout>
  );
};
