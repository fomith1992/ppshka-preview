export type InputTypes =
  | 'l_shoulder'
  | 'r_shoulder'
  | 'stomach'
  | 'buttocks'
  | 'breast'
  | 'l_hip'
  | 'r_hip'
  | 'l_shin'
  | 'r_shin'
  | 'waist';
export type MeasurementsParams = Record<InputTypes, string>;

export interface TData {
  type: InputTypes;
  placeholder: string;
}

export const measurementsData: { data: TData[]; cardName: string }[] = [
  {
    cardName: 'Руки',
    data: [
      {
        placeholder: 'Плечо левое*',
        type: 'l_shoulder',
      },
      {
        placeholder: 'Плечо правое*',
        type: 'r_shoulder',
      },
    ],
  },
  {
    cardName: 'Туловище',
    data: [
      {
        placeholder: 'Живот*',
        type: 'stomach',
      },
      {
        placeholder: 'Талия*',
        type: 'waist',
      },
      {
        placeholder: 'Ягодицы*',
        type: 'buttocks',
      },
      {
        placeholder: 'Грудь*',
        type: 'breast',
      },
    ],
  },
  {
    cardName: 'Ноги',
    data: [
      {
        placeholder: 'Бедро левое*',
        type: 'l_hip',
      },
      {
        placeholder: 'Бедро правое*',
        type: 'r_hip',
      },
      {
        placeholder: 'Голень левая*',
        type: 'l_shin',
      },
      {
        placeholder: 'Голень правая*',
        type: 'r_shin',
      },
    ],
  },
];

export const mockData: MeasurementsParams = {
  l_shoulder: '',
  r_shoulder: '',
  stomach: '',
  buttocks: '',
  breast: '',
  l_hip: '',
  r_hip: '',
  l_shin: '',
  r_shin: '',
  waist: '',
};
