import { useNavigation } from '@react-navigation/core';
import React, { useState } from 'react';
import { Alert } from 'react-native';
import { useSelector } from 'react-redux';
import useAsyncFn from 'react-use/lib/useAsyncFn';
import { updateMeasurementsEvent } from 'src/firebase/analytics/analytics-user/update-measurements';
import { measurementsAPI } from 'src/api';
import { AppState } from 'src/store';
import { MeasurementsView } from './measurements.view';
import { MeasurementsParams, mockData } from './mock-data/mock-data';

export const Measurements = (): React.ReactElement => {
  const [measurementsParams, setMeasurementsParams] = useState<MeasurementsParams>(mockData);
  const access_token = useSelector((state: AppState) => state.session.tokens.accessToken);
  const userId = useSelector((state: AppState) => state.session.user.id);
  const navigation = useNavigation();

  const [_, setToMeasurements] = useAsyncFn(async () => {
    return await measurementsAPI
      .setMeasurements({ access_token, ...measurementsParams })
      .then(async () => {
        updateMeasurementsEvent(userId);
        navigation.navigate('AddParamPhotos');
      })
      .catch((err) => {
        console.log('setToMeasurements err: ', err);
        Alert.alert('Произошла ошибка');
      });
  }, [measurementsParams, access_token]);

  return (
    <MeasurementsView
      measurementsParams={measurementsParams}
      onSetMeasurementsParams={setMeasurementsParams}
      onSubmit={setToMeasurements}
    />
  );
};
