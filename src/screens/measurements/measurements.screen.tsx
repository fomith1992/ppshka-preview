import React from 'react';
import { Measurements } from './measurements.container';

export const MeasurementsScreen = () => {
  return <Measurements />;
};
