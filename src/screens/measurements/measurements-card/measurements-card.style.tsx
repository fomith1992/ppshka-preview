import { Row } from '@screens/recipes/ui/header.style';
import { color } from '@shared/config/color';
import { styled } from '@shared/config/styled';
import { font } from '@shared/config/text';
import { container } from '@shared/config/view';
import { Platform, Text, TextInput, View } from 'react-native';
import { getScaleSize, windowWidth } from 'src/utils/dimensions';

export const Container = styled(View, {
  ...container(),
  ...container('padding'),
  marginTop: getScaleSize(16),
  paddingTop: getScaleSize(15),
  paddingBottom: getScaleSize(35),
  backgroundColor: color('white'),
  borderRadius: getScaleSize(15),
  ...Platform.select({
    android: {
      elevation: 1,
    },
    ios: {
      shadowColor: color('black'),
      shadowOffset: {
        width: 0,
        height: 1,
      },
      shadowOpacity: 0.06,
      shadowRadius: 1,
    },
  }),
});

export const TitileContainer = styled(View, {
  flexDirection: 'row',
  alignItems: 'center',
});

export const TitleRow = styled(Row, {
  paddingRight: getScaleSize(20),
  paddingLeft: getScaleSize(17),
  alignItems: 'flex-start',
});

export const Title = styled(Text, {
  ...font({ type: 'h2', weight: 'normal' }),
  marginLeft: getScaleSize(4),
});

export const CurrentWeight = styled(Text, {
  ...font({ type: 'text1', weight: 'normal' }),
  color: color('tertiary'),
});

export const CurrentWeightInfo = styled(Text, {
  ...font({ type: 'text1', weight: 'normal' }),
  color: color('gray10'),
});

export const CurrentWeightDescription = styled(Text, {
  ...font({ type: 'text4', weight: 'normal' }),
  color: color('gray7'),
});

export const CurrentWeightTitle = styled(Text, {
  ...font({ type: 'text3', weight: 'normal' }),
  color: color('gray10'),
  marginTop: getScaleSize(12),
});

export const LeftContainer = styled(View, {
  marginTop: getScaleSize(8),
  marginLeft: getScaleSize(15),
});

export const ProgressChartContainer = styled(View, {});

export const InfoContainer = styled(View, {
  position: 'absolute',
  top: 0,
  bottom: 0,
  left: 0,
  right: 0,
  justifyContent: 'center',
  alignItems: 'center',
});

export const ChartItemContainer = styled(View, {});

export const ChartItemView = styled(
  View,
  (props: { height: number; active: boolean }) =>
    ({
      width: windowWidth / 8,
      height: getScaleSize(props.height),
      backgroundColor: color(props.active ? 'primary' : 'white'),
      borderRadius: 8,
      borderWidth: props.active ? undefined : 1,
      borderColor: color(props.active ? 'white' : 'gray4'),
      justifyContent: 'center',
      alignItems: 'center',
    } as const),
);

export const ChartItemDescription = styled(Text, {
  ...font({ type: 'text3', weight: 'normal' }),
  color: color('gray7'),
  marginTop: getScaleSize(12),
  textAlign: 'center',
});

export const ChartTextTitle = styled(
  Text,
  (props: { active: boolean }) =>
    ({
      ...font({ type: 'text4', weight: 'normal' }),
      color: color(props.active ? 'white' : 'gray10'),
    } as const),
);

export const ChartTextDescription = styled(Text, {
  ...font({ type: 'text4', weight: 'normal' }),
  color: color('white'),
});

export const Input = styled(TextInput, {
  flex: 1,
  height: 35,
  ...font({ type: 'text2' }),
  lineHeight: getScaleSize(16),
  paddingHorizontal: getScaleSize(8),
  paddingTop: 0,
  paddingBottom: 0,
  textAlignVertical: 'center',
  /* asd */
});

export const InputContainer = styled(View, {
  flex: 1,
  flexDirection: 'row',
  alignItems: 'center',
  backgroundColor: color('white'),
  margin: 1,
  borderRadius: 5,
  overflow: 'hidden',
});

export const CardContainer = styled(View, {
  height: 37, // don`t give need effect, if use getScaleSize
  alignItems: 'center',
  marginTop: getScaleSize(6),
  borderRadius: 5,
  overflow: 'hidden',
});

export const CardContainerPlaceHolder = styled(View, {
  marginTop: getScaleSize(15),
});

export const PlaceholderText = styled(Text, {
  ...font({ type: 'text2' }),
  color: color('gray7'),
  position: 'absolute',
  marginLeft: getScaleSize(8),
});

export const ChartContainer = styled(View, {
  marginTop: getScaleSize(5),
});
