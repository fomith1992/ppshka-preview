import React from 'react';
import { CardItemProps, MeasurementsCardView } from './measurements-card.view';

interface MeasurementsCardProps {
  data: CardItemProps[];
  cardName: string;
}

export const MeasurementsCard = ({ data, cardName }: MeasurementsCardProps): React.ReactElement => {
  return <MeasurementsCardView data={data} cardName={cardName} />;
};
