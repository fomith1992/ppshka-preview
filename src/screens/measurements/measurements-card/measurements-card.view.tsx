import React from 'react';
import {
  Container,
  Title,
  CardContainer,
  ChartContainer,
  Input,
  InputContainer,
  CardContainerPlaceHolder,
  PlaceholderText,
} from './measurements-card.style';
import { GradientLayoutView } from '@shared/ui/gradient-layout/gradient-layout.view';
import { Platform } from 'react-native';

export interface CardItemProps {
  placeholder: string;
  value: string;
  onChange: (value: string) => void;
}

interface MeasurementsCardViewProps {
  data: CardItemProps[];
  cardName: string;
}

const CardItem = ({ onChange, placeholder, value }: CardItemProps): React.ReactElement => {
  return (
    <CardContainerPlaceHolder>
      <CardContainer>
        <GradientLayoutView horizontal>
          <InputContainer>
            {!value && <PlaceholderText>{placeholder}</PlaceholderText>}
            <Input
              placeholder={Platform.OS === 'android' ? placeholder : undefined}
              value={value}
              onChangeText={onChange}
              keyboardType="numeric"
              maxLength={3}
            />
          </InputContainer>
        </GradientLayoutView>
      </CardContainer>
    </CardContainerPlaceHolder>
  );
};

export const MeasurementsCardView = ({
  data,
  cardName,
}: MeasurementsCardViewProps): React.ReactElement => {
  return (
    <Container>
      <Title>{cardName}</Title>
      <ChartContainer>
        {data.map((item, idx) => (
          <CardItem
            key={idx + item.placeholder}
            value={item.value}
            onChange={item.onChange}
            placeholder={item.placeholder}
          />
        ))}
      </ChartContainer>
    </Container>
  );
};
