import { useHeaderHeight } from '@react-navigation/stack';
import React from 'react';
import { KeyboardAvoidingView, Platform } from 'react-native';
import SafeAreaView from 'react-native-safe-area-view';

export function ScreenContentView({
  children,
}: React.PropsWithChildren<Record<string, unknown>>): React.ReactElement {
  const headerHeight = useHeaderHeight();

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <KeyboardAvoidingView
        style={{ flex: 1 }}
        behavior={Platform.OS === 'ios' ? 'padding' : undefined}
        keyboardVerticalOffset={headerHeight}
      >
        {children}
      </KeyboardAvoidingView>
    </SafeAreaView>
  );
}
