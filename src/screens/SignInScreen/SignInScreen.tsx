import React, { useState } from 'react';
import { Alert, Text, TouchableOpacity, View } from 'react-native';

import { useNavigation } from '@react-navigation/native';
import * as storage from 'src/utils/storage/storage';

/* import Icon from 'src/components/Icon';
import { onLogin } from 'src/api/auth0'; */
import Button from 'src/components/Button';
import Input from 'src/components/Input';
import ScreenLayout from 'src/components/ScreenLayout';

import { ContentContainer, Description, Logo, styles, Title, TitleContainer } from './styles';
import useAsyncFn from 'react-use/lib/useAsyncFn';
import { auth } from 'src/api';
import { useDispatch } from 'react-redux';
import { LoaderModal } from 'src/components/ui/Loader/loading-indicator.modal';

import * as yup from 'yup';
import { GradientLayoutHeaderView } from '@shared/ui/gradient-layout/gradient-layout-header.view';
import { signInEvent } from 'src/firebase/analytics/analytics-auth/signin-event';
import { sessionSetTokens } from 'src/store/session/actions.types';

const MIN_LENGTH = 6;
const MAX_LENGTH = 20;
const ERROR_MIN_MESSAGE = 'Слишком короткий пароль';
const ERROR_MAX_MESSAGE =
  'Максимальная длина пароля - 20 символов. Пожалуйста, сократите ваш пароль. ';

const loginShema = yup
  .object({
    email: yup
      .string()
      .required()
      .matches(/.+@.+\..+/i, 'Проверьте правильность введенного email'),
    password: yup
      .string()
      .required(ERROR_MIN_MESSAGE)
      .matches(
        /^[a-zA-Z0-9!@#$%^&-]+$/,
        'Пароль может содержать только латинские буквы, цифры и символы !@#$%^&-',
      )
      .min(MIN_LENGTH, ERROR_MIN_MESSAGE)
      .max(MAX_LENGTH, ERROR_MAX_MESSAGE),
  })
  .required();

const SignInScreen = () => {
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState<{ path?: string; msg: string } | null>(null);

  const onGoSignUp = () => {
    navigation.navigate('SignUp');
  };

  const [signInState, onSignIn] = useAsyncFn(async () => {
    return await auth
      .authByEmailPass({
        email,
        password,
      })
      .then(async ({ data }) => {
        if (data.status === 'ok') {
          await storage.setRefreshToken({
            refresh_token: data.data.refresh_token,
            type: 'email',
          });
          dispatch(
            sessionSetTokens({
              accessToken: data.data.access_token,
              refreshToken: data.data.refresh_token ?? null,
            }),
          );
          signInEvent('email');
        } else {
          if (data.error_code === 0) Alert.alert('Ошибка', 'password и email не введены');
          if (data.error_code === 1) Alert.alert('Ошибка', 'email не введен');
          if (data.error_code === 2) Alert.alert('Ошибка', 'password не введен');
          if (data.error_code === 3) Alert.alert('Ошибка', 'email не прошел валидацию');
          if (data.error_code === 4)
            Alert.alert('Ошибка', 'Пароль не прошел валидацию(не должен быть меньше 6 символов)');
          if (data.error_code === 6) Alert.alert('Ошибка', 'Не верный пароль');
          if (data.error_code === 7) Alert.alert('Ошибка', 'Пользователь заблокирован');
        }
      });
  }, [email, password]);

  const onSubmit = () => {
    setError(null);
    loginShema
      .validate({ email, password })
      .then(() => {
        onSignIn();
      })
      .catch((e: yup.ValidationError) => setError({ path: e.path, msg: e.message }));
  };

  return (
    <ScreenLayout edges={['left', 'right', 'bottom']}>
      <GradientLayoutHeaderView>
        <TitleContainer>
          <Logo source={require('../../../assets/logo.png')} />
          <Title>Вход</Title>
          <Description>
            Добро пожаловать! Войдите в личный аккаунт,{'\n'}чтобы продолжить
          </Description>
        </TitleContainer>
      </GradientLayoutHeaderView>
      <ContentContainer>
        <Input
          value={email}
          error={error?.path === 'email' ? error?.msg : undefined}
          onChange={setEmail}
          placeholder={'Введите ваш e-mail'}
        />
        <Input
          value={password}
          error={error?.path === 'password' ? error?.msg : undefined}
          onChange={setPassword}
          style={styles.marginBottom24}
          isPassword={true}
          placeholder={'Введите пароль'}
        />
        <Button uppercase onPress={onSubmit} content="Войти" />
        <TouchableOpacity
          style={styles.containerButtonForgetPassword}
          onPress={() => navigation.navigate('EnterEmail')}
        >
          <Text style={styles.textButtonForgetPassword}>Забыли пароль?</Text>
        </TouchableOpacity>
        {/* <View style={styles.containerSeparator}>
          <View style={styles.partSeparator} />
          <Text style={styles.textSeparator}>Или</Text>
          <View style={styles.partSeparator} />
        </View>

        <TouchableOpacity
          style={[
            styles.containerButton,
            {
              borderWidth: 1,
              borderColor: '#F0F0F0',
            },
          ]}
          onPress={() => onLogin('google')}
        >
          <View style={styles.containerIcon}>
            <Icon name="google" />
          </View>
          <Text style={styles.textLoginMethod}>Войти с помощью Google</Text>
        </TouchableOpacity>
        {Platform.OS === 'ios' && (
          <TouchableOpacity style={[styles.containerButton, { backgroundColor: '#0B0B0A' }]}>
            <View style={styles.containerIcon}>
              <Icon name="apple" />
            </View>
            <Text style={[styles.textLoginMethod, { color: 'white' }]}>
              Войти с помощью Apple ID
            </Text>
          </TouchableOpacity>
        )} */}
        <View style={styles.containerLoginBottom}>
          <Text style={styles.textLogin}>{'У меня нет аккаунта. '}</Text>
          <TouchableOpacity style={styles.containerTextLoginBold} onPress={onGoSignUp}>
            <Text style={styles.textLoginBold}>Регистрация</Text>
          </TouchableOpacity>
        </View>
      </ContentContainer>
      <LoaderModal visible={signInState.loading} />
    </ScreenLayout>
  );
};

export default SignInScreen;
