import { color } from '@shared/config/color';
import { styled } from '@shared/config/styled';
import { font } from '@shared/config/text';
import { container } from '@shared/config/view';
import { StyleSheet, Text, View } from 'react-native';
/* import TextInputMask from 'react-native-text-input-mask'; */

import { purpleColor } from 'src/commonStyles/colors';
import { font12m, font14r, font16r, font30m } from 'src/commonStyles/fonts';
import { getScaleSize } from 'src/utils/dimensions';

export const styles = StyleSheet.create({
  container: {
    paddingTop: getScaleSize(10),
    flexGrow: 1,
    justifyContent: 'space-between',
    paddingBottom: getScaleSize(70),
  },
  paddingHorizontal: {
    paddingHorizontal: getScaleSize(15),
  },
  containerTabs: {
    marginBottom: getScaleSize(81),
  },
  textTitle: {
    ...font30m,
    lineHeight: getScaleSize(37),
    color: '#262626',
    marginBottom: getScaleSize(12),
    textAlign: 'center',
    marginHorizontal: getScaleSize(10),
  },
  textSubTitle: {
    ...font14r,
    lineHeight: getScaleSize(21),
    color: '#8C8C8C',
    marginBottom: getScaleSize(40),
    marginHorizontal: getScaleSize(25),
    textAlign: 'center',
  },
  buttonBack: {
    paddingVertical: getScaleSize(18),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  textButtonBack: {
    ...font12m,
    color: purpleColor,
    marginLeft: getScaleSize(8),
  },
  containerChoice: {
    marginHorizontal: getScaleSize(15),
    borderRadius: getScaleSize(15),
    borderWidth: 1,
    borderColor: '#D9D9D9',
    marginBottom: getScaleSize(12),
    justifyContent: 'center',
    alignItems: 'center',
    height: getScaleSize(65),
  },
  textChoice: {
    ...font16r,
    lineHeight: 24,
    color: '#595959',
  },
  selectedContainerChoice: {
    borderColor: '#6034A8',
  },
  containerIconTick: {
    position: 'absolute',
    left: getScaleSize(30),
  },
});

export const BirthdayPickersContainer = styled(View, {
  ...container('margin'),
  marginTop: getScaleSize(5),
  flexDirection: 'row',
});

export const CustomizePickerContainer = styled(View, {
  borderRadius: getScaleSize(10),
  borderColor: color('primary'),
  borderWidth: getScaleSize(2),
  flex: 1,
});

export const Spring = styled(View, {
  width: getScaleSize(10),
});

export const Container = styled(View, {
  paddingTop: getScaleSize(10),
  flexGrow: 1,
  justifyContent: 'space-between',
  paddingBottom: getScaleSize(70),
  backgroundColor: color('white'),
});

export const PickerLabel = styled(Text, {
  ...font({ type: 'text3' }),
  zIndex: 100,
  flex: 1,
  paddingLeft: getScaleSize(5),
});
