import React, { useState } from 'react';
import { KeyboardAvoidingView, Text, TouchableOpacity, View } from 'react-native';

import { useNavigation } from '@react-navigation/native';
import Button from 'src/components/Button';
import Icon from 'src/components/Icon';
import OnboardingTabs from 'src/components/OnboardingTabs';
import ScreenLayout from 'src/components/ScreenLayout';

import {
  styles,
  BirthdayPickersContainer,
  PickerLabel,
  CustomizePickerContainer,
  Spring,
} from './styles';
import { StackScreenProps } from '@react-navigation/stack';
import { RootStackParamList } from 'src/navigation/RootNavigator';
import { Picker } from '@react-native-picker/picker';
import { daysInMonth } from 'src/utils/date';
import { encodeMonth } from 'src/utils/i18n';

export const OnboardingUserBirthdayScreen = ({
  route,
}: StackScreenProps<RootStackParamList, 'OnboardingUserBirthday'>) => {
  const initDate = new Date(2001, 0, 1);
  const navigation = useNavigation();
  const [day, setDay] = useState<number>(initDate.getDate());
  const [month, setMonth] = useState<number>(initDate.getMonth());
  const [year, setYear] = useState<number>(initDate.getFullYear());

  const onContinue = () => {
    navigation.navigate('OnboardingWeight', {
      ...route.params,
      birthday: new Date(year, month, day),
    });
  };

  return (
    <ScreenLayout>
      <KeyboardAvoidingView style={{ flex: 1 }} behavior="padding">
        <View style={styles.container}>
          <View>
            <OnboardingTabs activeValue={3} totalValues={7} style={styles.containerTabs} />
            <Text style={styles.textTitle}>Укажите дату {'\n'} рождения</Text>
            <Text style={styles.textSubTitle}>Пожалуйста, укажите вашу дату рождения</Text>
            <BirthdayPickersContainer>
              <PickerLabel>Месяц</PickerLabel>
              <PickerLabel>День</PickerLabel>
              <PickerLabel>Год</PickerLabel>
            </BirthdayPickersContainer>
            <BirthdayPickersContainer>
              <CustomizePickerContainer>
                <Picker selectedValue={month} onValueChange={(itemValue) => setMonth(itemValue)}>
                  {Array.from({ length: 12 }, (_, i) => i).map((value, index) => (
                    <Picker.Item key={index} label={encodeMonth[value]} value={value} />
                  ))}
                </Picker>
              </CustomizePickerContainer>
              <Spring />
              <CustomizePickerContainer>
                <Picker selectedValue={day} onValueChange={(itemValue) => setDay(itemValue)}>
                  {Array.from({ length: daysInMonth(month + 1, year) + 1 }, (_, i) => i)
                    .slice(1)
                    .map((value, index) => (
                      <Picker.Item key={index} label={value.toString()} value={value} />
                    ))}
                </Picker>
              </CustomizePickerContainer>
              <Spring />
              <CustomizePickerContainer>
                <Picker
                  selectedValue={year}
                  onValueChange={(itemValue) => setYear(itemValue)}
                  style={{
                    width: '100%',
                  }}
                >
                  {Array.from({ length: initDate.getFullYear() + 20 }, (_, i) => i)
                    .slice(1900)
                    .reverse()
                    .map((value, index) => (
                      <Picker.Item key={index} label={`${value} г`} value={value} />
                    ))}
                </Picker>
              </CustomizePickerContainer>
            </BirthdayPickersContainer>
          </View>
          <View style={styles.paddingHorizontal}>
            <Button uppercase content="Продолжить" onPress={onContinue} />
            <TouchableOpacity style={styles.buttonBack} onPress={navigation.goBack}>
              <Icon name="arrowBack" size={12} color="primary" />
              <Text style={styles.textButtonBack}>Назад</Text>
            </TouchableOpacity>
          </View>
        </View>
      </KeyboardAvoidingView>
    </ScreenLayout>
  );
};
