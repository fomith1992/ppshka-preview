import React, { useState } from 'react';
import { Text, TouchableOpacity, View } from 'react-native';

import { useNavigation } from '@react-navigation/native';

import { purpleColor } from 'src/commonStyles/colors';
import Button from 'src/components/Button';
import Icon from 'src/components/Icon';
import OnboardingTabs from 'src/components/OnboardingTabs';
import ScreenLayout from 'src/components/ScreenLayout';

import { styles } from './styles';
import { StackScreenProps } from '@react-navigation/stack';
import { RootStackParamList } from 'src/navigation/RootNavigator';
import { Bottom } from '@screens/recipes/ui/header.style';

type SexType = 'male' | 'female';

const choices = [
  { label: 'Женский', id: 'female' },
  { label: 'Мужской', id: 'male' },
];

export const OnboardingUserSexScreen = ({
  route,
}: StackScreenProps<RootStackParamList, 'OnboardingQuestionActivity'>) => {
  const navigation = useNavigation();
  const [sex, setSex] = useState<SexType>('female');

  const onContinue = () => {
    navigation.navigate('OnboardingUserBirthday', {
      ...route.params,
      sex,
    });
  };

  return (
    <ScreenLayout>
      <View style={styles.container}>
        <View>
          <OnboardingTabs activeValue={2} totalValues={7} style={styles.containerTabs} />
          <Text style={styles.textTitle}>Укажите ваш пол</Text>
          <Bottom mb={48} />
          {choices.map((choice, index) => (
            <TouchableOpacity
              key={index}
              onPress={() => setSex(choice.id as SexType)}
              style={[
                styles.containerChoice,
                sex === choice.id ? styles.selectedContainerChoice : null,
              ]}
            >
              {sex === choice.id ? (
                <Icon name="tick" style={styles.containerIconTick} color="primary" />
              ) : null}
              <Text style={[styles.textChoice, sex === choice.id ? { color: purpleColor } : null]}>
                {choice.label}
              </Text>
            </TouchableOpacity>
          ))}
        </View>
        <View style={styles.paddingHorizontal}>
          <Button uppercase content="Продолжить" onPress={onContinue} />
          <TouchableOpacity style={styles.buttonBack} onPress={navigation.goBack}>
            <Icon name="arrowBack" size={12} color="primary" />
            <Text style={styles.textButtonBack}>Назад</Text>
          </TouchableOpacity>
        </View>
      </View>
    </ScreenLayout>
  );
};
