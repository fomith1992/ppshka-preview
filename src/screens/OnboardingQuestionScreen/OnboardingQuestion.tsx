import React, { useState } from 'react';
import { Text, TouchableOpacity, View } from 'react-native';

import { useNavigation } from '@react-navigation/native';

import { purpleColor } from 'src/commonStyles/colors';
import Button from 'src/components/Button';
import Icon from 'src/components/Icon';
import OnboardingTabs from 'src/components/OnboardingTabs';
import ScreenLayout from 'src/components/ScreenLayout';

import { styles } from './styles';
import { StackScreenProps } from '@react-navigation/stack';
import { RootStackParamList } from 'src/navigation/RootNavigator';

export const choices = ['Сбросить вес', 'Сохранить вес'];

const OnboardingQuestionScreen = ({
  route,
}: StackScreenProps<RootStackParamList, 'OnboardingQuestionActivity'>) => {
  const navigation = useNavigation();
  const [selectedChoiceIndex, setSelectedChoiceIndex] = useState(0);

  const onContinue = () => {
    navigation.navigate('OnboardingQuestionGoal', {
      ...route.params,
      activity: selectedChoiceIndex,
    });
  };

  return (
    <ScreenLayout>
      <View style={styles.container}>
        <View>
          <OnboardingTabs activeValue={6} totalValues={7} style={styles.containerTabs} />
          <Text style={styles.textTitle}>Какая у Вас цель?</Text>
          <Text style={styles.textSubTitle}>
            Пожалуйста, ответьте на данный вопрос. Мы подберём специальные рецепты для Вас
          </Text>
          {choices.map((choice, index) => (
            <TouchableOpacity
              key={index}
              onPress={() => setSelectedChoiceIndex(index)}
              style={[
                styles.containerChoice,
                selectedChoiceIndex === index ? styles.selectedContainerChoice : null,
              ]}
            >
              {selectedChoiceIndex === index ? (
                <Icon name="tick" style={styles.containerIconTick} color="primary" />
              ) : null}
              <Text
                style={[
                  styles.textChoice,
                  selectedChoiceIndex === index ? { color: purpleColor } : null,
                ]}
              >
                {choice}
              </Text>
            </TouchableOpacity>
          ))}
        </View>
        <View style={styles.paddingHorizontal}>
          <Button uppercase content="Продолжить" onPress={onContinue} />
          <TouchableOpacity style={styles.buttonBack} onPress={() => navigation.goBack()}>
            <Icon name="arrowBack" size={12} color="primary" />
            <Text style={styles.textButtonBack}>Назад</Text>
          </TouchableOpacity>
        </View>
      </View>
    </ScreenLayout>
  );
};

export default OnboardingQuestionScreen;
