import React, { useState } from 'react';
import { Alert, Text, TouchableOpacity, View } from 'react-native';

import { purpleColor } from 'src/commonStyles/colors';
import Button from 'src/components/Button';
import Icon from 'src/components/Icon';
import OnboardingTabs from 'src/components/OnboardingTabs';
import ScreenLayout from 'src/components/ScreenLayout';

import { styles } from './styles';
import { StackScreenProps } from '@react-navigation/stack';
import { RootStackParamList } from 'src/navigation/RootNavigator';

import useAsyncFn from 'react-use/lib/useAsyncFn';
import { auth, location } from 'src/api';
import { useDispatch } from 'react-redux';
import { useNavigation } from '@react-navigation/native';
import { LoaderModal } from 'src/components/ui/Loader/loading-indicator.modal';
import { format } from 'date-fns';
import { storage } from 'src/utils/storage';
import { sessionSetTokens } from 'src/store/session/actions.types';

const choices = ['Низкая', 'Средняя', 'Высокая'];

const OnboardingQuestionGoalScreen = ({
  route,
}: StackScreenProps<RootStackParamList, 'OnboardingQuestionGoal'>) => {
  const dispatch = useDispatch();
  const [selectedChoiceIndex, setSelectedChoiceIndex] = useState(0);
  const navigation = useNavigation();

  const [registerState, onSignUp] = useAsyncFn(async () => {
    return await auth
      .registerSetUserParams({
        access_token: route.params.access_token,
        activity: route.params.activity,
        goal: selectedChoiceIndex,
        height: route.params.height,
        weight: route.params.weight,
        sex: route.params.sex,
        b_day: format(route.params.birthday, 'yyyy-MM-dd'),
      })
      .then(async () => {
        if (route.params.refresh_token) {
          await storage.setRefreshToken({
            refresh_token: route.params.refresh_token,
            type: 'email',
          });
        }
        const timezone = format(new Date(), 'xx');
        await location.setTimeZone({ access_token: route.params.access_token, timezone });
        storage.setUpdateLocationDate();
        dispatch(
          sessionSetTokens({
            accessToken: route.params.access_token,
            refreshToken: route.params.refresh_token ?? null,
          }),
        );
      })
      .catch((e) => {
        console.log(e);
        Alert.alert('Произошла ошибка');
      });
  }, [selectedChoiceIndex]);

  return (
    <ScreenLayout>
      <View style={styles.container}>
        <View>
          <OnboardingTabs activeValue={7} totalValues={7} style={styles.containerTabs} />
          <Text style={styles.textTitle}>Какая у Вас активность?</Text>
          <Text style={styles.textSubTitle}>
            Мы собираем эти данные, что бы персонализировать для Вас план питания и тренировок
          </Text>
          {choices.map((choice, index) => (
            <TouchableOpacity
              key={index}
              onPress={() => setSelectedChoiceIndex(index)}
              style={[
                styles.containerChoice,
                selectedChoiceIndex === index ? styles.selectedContainerChoice : null,
              ]}
            >
              {selectedChoiceIndex === index ? (
                <Icon name="tick" style={styles.containerIconTick} color="primary" />
              ) : null}
              <Text
                style={[
                  styles.textChoice,
                  selectedChoiceIndex === index ? { color: purpleColor } : null,
                ]}
              >
                {choice}
              </Text>
            </TouchableOpacity>
          ))}
        </View>
        <View style={styles.paddingHorizontal}>
          <Button uppercase content="Продолжить" onPress={onSignUp} />
          <TouchableOpacity style={styles.buttonBack} onPress={navigation.goBack}>
            <Icon name="arrowBack" size={12} color="primary" />
            <Text style={styles.textButtonBack}>Назад</Text>
          </TouchableOpacity>
        </View>
      </View>
      <LoaderModal visible={registerState.loading} />
    </ScreenLayout>
  );
};

export default OnboardingQuestionGoalScreen;
