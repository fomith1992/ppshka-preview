import React from 'react';
import { Container, Description, Title } from './invalid-version.style';
import ScreenLayout from 'src/components/ScreenLayout';

import icon from '../../shared/ui/assets/invalid-screen.png';
import { Image, Linking, Platform, View } from 'react-native';
import { getScaleSize, windowWidth } from 'src/utils/dimensions';
import Button from 'src/components/Button';

const urlApple = 'https://apps.apple.com/ru/app/ппшка/id1585426844';
const urlGoogle = 'https://play.google.com/store/apps/details?id=com.ppshka';

export const InvalidVersionScreen = (): React.ReactElement => {
  return (
    <ScreenLayout safeArea edges={['top']}>
      <Container>
        <Image
          style={{ width: windowWidth - getScaleSize(64), height: windowWidth }}
          source={icon}
        />
        <Title>Обновите приложение</Title>
        <Description>
          Эта версия приложения устарела. Обновите приложение, чтобы продолжить пользоваться
          серивисом.
        </Description>
        <View
          style={{
            flexDirection: 'row',
            marginHorizontal: getScaleSize(15),
            marginTop: getScaleSize(15),
          }}
        >
          <Button
            uppercase
            onPress={() => Linking.openURL(Platform.OS === 'android' ? urlGoogle : urlApple)}
            content="Обновить приложение"
          />
        </View>
      </Container>
    </ScreenLayout>
  );
};
