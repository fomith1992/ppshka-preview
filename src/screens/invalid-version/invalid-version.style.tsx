import { color } from '@shared/config/color';
import { styled } from '@shared/config/styled';
import { font } from '@shared/config/text';
import { Text, View } from 'react-native';
import { getScaleSize } from 'src/utils/dimensions';

export const Container = styled(View, {
  flex: 1,
  justifyContent: 'center',
  alignItems: 'center',
  backgroundColor: color('white'),
});

export const Title = styled(Text, {
  ...font({ type: 'h2', weight: 'bold' }),
  marginTop: getScaleSize(30),
  textAlign: 'center',
});

export const Description = styled(Text, {
  ...font({ type: 'text2' }),
  color: color('gray7'),
  marginTop: getScaleSize(15),
  textAlign: 'center',
});
