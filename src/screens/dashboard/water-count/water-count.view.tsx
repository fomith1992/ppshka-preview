import { theme } from '@shared/config/theme';
import { CloseIcon8 } from '@shared/ui/icons/close.icon-8';
import { EditIcon14 } from '@shared/ui/icons/edit.icon-14';
import React, { useEffect, useRef } from 'react';
import { FlatList, TouchableOpacity, View } from 'react-native';
import { ProgressChart } from 'react-native-chart-kit';
import {
  useAnimatedStyle,
  useSharedValue,
  withRepeat,
  withSequence,
  withTiming,
} from 'react-native-reanimated';
import Icon from 'src/components/Icon';
import { hitSlopParams } from 'src/utils/hit-slop-params';
import {
  Container,
  CurrentWeight,
  TitileContainer,
  Title,
  RowTitle,
  Description,
  ArrowButton,
  DescriptionBold,
  YourDestinationText,
  WaterCountTouch,
  WaterCountTouchText,
  WaterCountTabsRow,
  WaterCountTouchEmpty,
  WaterCountTouchTextEmpty,
  ProgressChartContainer,
  InfoContainer,
  CurrentWeightInfo,
  CurrentWeightDescription,
  LinearGradientBlock,
} from './water-count.style';

interface WaterCountViewProps {
  waterCountNeed: number;
  waterCount: { value: number | null; id: number | null }[];
  onAddWater: () => void;
  setWaterModalEditvisible: (value: boolean) => void;
  waterModalEditvisible: boolean;
  setWaterDeletedItem: (value: number | null) => void;
  waterDeletedItem: number | null;
}

const mockAddTabs = [
  { value: null, id: null },
  { value: null, id: null },
];

interface WaterCountTabsProps {
  onAddWater: () => void;
  waterCount: { value: number | null; id: number | null }[];
  editable: boolean;
  setWaterDeletedItem: (value: number | null) => void;
}

const AnimatedItem = (props: { editable: boolean; item: string; onPress: () => void }) => {
  const rotation = useSharedValue(0);

  const startShake = () => {
    rotation.value = withSequence(
      withTiming(-6, { duration: 50 }),
      withRepeat(withTiming(6, { duration: 100 }), 6, true),
      withTiming(6, { duration: 100 }),
      withTiming(0, { duration: 50 }),
    );
  };
  useEffect(() => {
    if (props.editable) {
      const interval = setInterval(startShake, 800);
      return () => clearInterval(interval);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props.editable]);

  const animatedStyle = useAnimatedStyle(() => {
    return {
      transform: [{ rotateZ: `${rotation.value}deg` }],
    };
  });

  return (
    <TouchableOpacity onPress={props.onPress} disabled={!props.editable}>
      <WaterCountTouch editable={props.editable} style={animatedStyle}>
        <WaterCountTouchText>{props.item}</WaterCountTouchText>
      </WaterCountTouch>
    </TouchableOpacity>
  );
};

const WaterCountTabs = ({
  onAddWater,
  waterCount,
  editable,
  setWaterDeletedItem,
}: WaterCountTabsProps) => {
  const flatListRef = useRef<FlatList>(null);

  return (
    <WaterCountTabsRow>
      <FlatList<{ value: number | null; id: number | null }>
        horizontal
        showsHorizontalScrollIndicator={false}
        ref={flatListRef}
        onContentSizeChange={() => flatListRef.current?.scrollToEnd()}
        onLayout={() => flatListRef.current?.scrollToEnd()}
        data={[...waterCount, ...mockAddTabs]}
        keyExtractor={(item, idx) => `${item}+${idx}`}
        renderItem={({ item, index }) =>
          item.id ? (
            <AnimatedItem
              onPress={() => setWaterDeletedItem(item.id)}
              editable={editable}
              item={item.value?.toString() ?? ''}
            />
          ) : (
            <WaterCountTouchEmpty
              disabled={index !== waterCount.length}
              key={`${index}-empty`}
              hitSlop={hitSlopParams('L')}
              onPress={onAddWater}
            >
              <WaterCountTouchTextEmpty>
                {index === waterCount.length ? '+' : null}
              </WaterCountTouchTextEmpty>
            </WaterCountTouchEmpty>
          )
        }
      />
      {waterCount.length > 3 && !editable && (
        <LinearGradientBlock
          colors={['rgba(255, 255, 255, 1)', 'rgba(255, 255, 255, 0)']}
          start={{ x: 0, y: 1 }}
          end={{ x: 1, y: 1 }}
        />
      )}
    </WaterCountTabsRow>
  );
};

export const WaterCountView = ({
  waterCountNeed,
  waterCount,
  onAddWater,

  setWaterModalEditvisible,
  waterModalEditvisible,
  setWaterDeletedItem,
}: WaterCountViewProps): React.ReactElement => {
  const currentWaterSum =
    waterCount.reduce((partial_sum, a) => partial_sum + (a.value ?? 0), 0) / 1000;
  const currentWaterSumFix = currentWaterSum > 0 ? currentWaterSum : 1;
  const waterCountNeedFix = waterCountNeed > 0 ? waterCountNeed : 1;
  const currentWaterPercent =
    currentWaterSum === 0 || waterCountNeed === 0 ? 0 : currentWaterSumFix / waterCountNeedFix;
  const currentWaterPercentShort = currentWaterPercent.toFixed(2);

  useEffect(() => {
    waterCount.length === 0 && setWaterModalEditvisible(false);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [waterCount]);

  return (
    <Container>
      <RowTitle>
        <TitileContainer>
          <Icon name="water" />
          <View style={{ flexShrink: 1 }}>
            <Title>Количество выпитой воды:</Title>
            <Description>
              Рекомендуемое количество воды{'\n'}на ваш вес:{' '}
              <DescriptionBold>{waterCountNeed} л/день</DescriptionBold>
            </Description>
            <RowTitle>
              <View style={{ flexGrow: 1 }}>
                <YourDestinationText>Ваша цель:</YourDestinationText>
                <CurrentWeight>
                  {currentWaterSum} л / {waterCountNeed} л
                </CurrentWeight>
                <WaterCountTabs
                  setWaterDeletedItem={setWaterDeletedItem}
                  editable={waterModalEditvisible}
                  waterCount={waterCount}
                  onAddWater={onAddWater}
                />
              </View>
              <ProgressChartContainer>
                <ProgressChart
                  data={[+currentWaterPercent < 1 ? +currentWaterPercentShort : 1]}
                  width={96}
                  height={96}
                  strokeWidth={8}
                  radius={42}
                  chartConfig={{
                    backgroundGradientFrom: '#fff',
                    backgroundGradientTo: '#fff',
                    color: (opacity = 1) => `rgba(57, 183, 223, ${opacity})`,
                    labelColor: (opacity = 1) => `rgba(57, 183, 223, ${opacity})`,
                    style: {
                      borderRadius: 8,
                      marginRight: 0,
                    },
                  }}
                  hideLegend={true}
                />
                <InfoContainer>
                  <CurrentWeightInfo>{currentWaterSum} л</CurrentWeightInfo>
                  <CurrentWeightDescription>
                    {Math.floor(+currentWaterPercentShort * 100)} %
                  </CurrentWeightDescription>
                </InfoContainer>
              </ProgressChartContainer>
            </RowTitle>
          </View>
        </TitileContainer>
        <ArrowButton
          disabled={waterCount.length === 0}
          onPress={() => setWaterModalEditvisible(!waterModalEditvisible)}
          hitSlop={hitSlopParams('L')}
        >
          {waterModalEditvisible ? (
            <CloseIcon8 size={14} color={theme.colors.secondary} />
          ) : (
            <EditIcon14 />
          )}
        </ArrowButton>
      </RowTitle>
    </Container>
  );
};
