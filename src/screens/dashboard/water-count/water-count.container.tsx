import React, { useState } from 'react';
import { WaterCountView } from './water-count.view';
import { useFocusEffect } from '@react-navigation/core';
import { Alert } from 'react-native';
import { useSelector } from 'react-redux';
import { userAPI } from 'src/api';
import { AppState } from 'src/store';

interface WaterCountProps {
  onSetWaterModalvisible: () => void;
  waterModalvisible: boolean;
  setWaterModalEditvisible: (value: boolean) => void;
  waterModalEditvisible: boolean;
  setWaterDeletedItem: (value: number | null) => void;
  waterDeletedItem: number | null;
}

export const WaterCount = ({
  onSetWaterModalvisible,
  waterModalvisible,
  setWaterModalEditvisible,
  waterModalEditvisible,
  waterDeletedItem,
  setWaterDeletedItem,
}: WaterCountProps): React.ReactElement => {
  const access_token = useSelector((state: AppState) => state.session.tokens.accessToken);
  const [waterCount, setWaterCount] = useState<{ value: number; id: number }[]>([]);
  const [waterCountNeed, setWaterCountNeed] = useState(0);

  useFocusEffect(
    React.useCallback(() => {
      userAPI
        .getWaterContent({ access_token })
        .then((x) => {
          setWaterCountNeed(x.data.data.water ?? []);
          setWaterCount(x.data.data.data.map((x) => ({ value: x.col_water, id: x.id })) ?? []);
        })
        .catch((e) => {
          console.log('meteringDataStatus: ', e);
          Alert.alert('Произошла ошибка');
        });
      return;
    }, [waterModalvisible, waterDeletedItem]), // eslint-disable-line react-hooks/exhaustive-deps
  );

  return (
    <WaterCountView
      waterCountNeed={waterCountNeed > 0 ? waterCountNeed / 1000 : 0}
      waterCount={waterCount}
      onAddWater={onSetWaterModalvisible}
      waterModalEditvisible={waterModalEditvisible}
      setWaterModalEditvisible={setWaterModalEditvisible}
      waterDeletedItem={waterDeletedItem}
      setWaterDeletedItem={setWaterDeletedItem}
    />
  );
};
