import { color } from '@shared/config/color';
import { styled } from '@shared/config/styled';
import { font } from '@shared/config/text';
import { container } from '@shared/config/view';
import { Platform, Text, TouchableOpacity, View } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Animated from 'react-native-reanimated';
import { getScaleSize } from 'src/utils/dimensions';

export const Container = styled(View, {
  ...container('padding'),
  marginTop: getScaleSize(16),
  marginHorizontal: getScaleSize(15),
  paddingVertical: getScaleSize(16),
  backgroundColor: color('white'),
  borderRadius: getScaleSize(15),
  ...Platform.select({
    android: {
      elevation: 1,
    },
    ios: {
      shadowColor: color('black'),
      shadowOffset: {
        width: 0,
        height: 1,
      },
      shadowOpacity: 0.06,
      shadowRadius: 1,
    },
  }),
});

export const TitileContainer = styled(View, {
  flexDirection: 'row',
  alignItems: 'flex-start',
});

export const Title = styled(Text, {
  ...font({ type: 'text1', weight: 'normal' }),
  lineHeight: getScaleSize(16),
  marginLeft: getScaleSize(4),
});

export const Description = styled(Text, {
  ...font({ type: 'text3' }),
  color: color('gray8'),
  marginLeft: getScaleSize(4),
  marginTop: getScaleSize(4),
});

export const DescriptionBold = styled(Text, {
  ...font({ type: 'text3', weight: 'normal' }),
  color: color('gray10'),
  marginLeft: getScaleSize(4),
  marginTop: getScaleSize(4),
});

export const YourDestinationText = styled(Text, {
  ...font({ type: 'text2', weight: 'normal' }),
  color: color('gray10'),
  marginTop: getScaleSize(16),
});

export const CurrentWeight = styled(Text, {
  ...font({ type: 'text1', weight: 'normal' }),
  color: color('tertiary'),
});

export const RowTitle = styled(View, {
  flex: 1,
  flexDirection: 'row',
  alignItems: 'flex-start',
  justifyContent: 'space-between',
  marginTop: getScaleSize(4),
});

export const WaterCountTabsRow = styled(View, {
  flexDirection: 'row',
  alignItems: 'center',
  marginTop: getScaleSize(12),
  width: (getScaleSize(28) + getScaleSize(5)) * 6,
});

export const ArrowButton = styled(TouchableOpacity, {
  width: getScaleSize(16),
  height: getScaleSize(16),
});

export const WaterCountTouch = styled(
  Animated.View,
  (props: { editable: boolean }) =>
    ({
      width: getScaleSize(28),
      height: getScaleSize(28),
      alignItems: 'center',
      justifyContent: 'center',
      borderRadius: getScaleSize(5),
      borderWidth: 1,
      borderColor: color('gray4'),
      backgroundColor: color(props.editable ? 'secondary' : 'tertiary'),
      marginRight: getScaleSize(5),
    } as const),
);

export const WaterCountTouchEmpty = styled(TouchableOpacity, {
  width: getScaleSize(28),
  height: getScaleSize(28),
  alignItems: 'center',
  justifyContent: 'center',
  borderRadius: getScaleSize(5),
  borderColor: color('gray4'),
  borderWidth: 1,
  backgroundColor: color('white'),
  marginRight: getScaleSize(5),
});

export const WaterCountTouchText = styled(Text, {
  ...font({ type: 'text4', weight: 'normal' }),
  color: color('white'),
});

export const WaterCountTouchTextEmpty = styled(Text, {
  ...font({ type: 'h2', weight: 'normal' }),
  color: color('black'),
});

export const CurrentWeightInfo = styled(Text, {
  ...font({ type: 'text1', weight: 'normal' }),
  color: color('gray10'),
});

export const ProgressChartContainer = styled(View, {
  marginLeft: 'auto',
});

export const InfoContainer = styled(View, {
  position: 'absolute',
  top: 0,
  bottom: 0,
  left: 0,
  right: 0,
  justifyContent: 'center',
  alignItems: 'center',
});

export const CurrentWeightDescription = styled(Text, {
  ...font({ type: 'text4', weight: 'normal' }),
  color: color('gray7'),
});

export const LinearGradientBlock = styled(LinearGradient, {
  width: (getScaleSize(28) + getScaleSize(5)) * 0.8,
  height: getScaleSize(28),
  position: 'absolute',
  /* left: -5, */
});
