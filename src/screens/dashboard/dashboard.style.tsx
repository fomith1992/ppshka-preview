import { color } from '@shared/config/color';
import { styled } from '@shared/config/styled';
import { font } from '@shared/config/text';
import { container } from '@shared/config/view';
import { blockShadow } from '@shared/ui/block-shadow/block-shadow';
import { Platform, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { bottomTabsHeight, getScaleSize, screenWidth } from 'src/utils/dimensions';

export const HeaderContainer = styled(View, {
  paddingTop: getScaleSize(6),
  paddingBottom: getScaleSize(24),
  ...blockShadow(),
});

export const ScrollViewBlock = styled(ScrollView, {
  paddingTop: getScaleSize(4),
});

export const HeaderTitle = styled(Text, {
  ...font({ type: 'h1' }),
  color: color('white'),
  marginHorizontal: getScaleSize(15),
  marginBottom: getScaleSize(12),
});

export const styles = StyleSheet.create({
  scrollContainer: {
    ...Platform.select({
      android: {
        paddingBottom: getScaleSize(30) + bottomTabsHeight,
      },
      ios: {
        paddingBottom: getScaleSize(45) + bottomTabsHeight,
      },
    }),
  },
});
export const ScrollContainer = styled(ScrollView, {
  flex: 1,
  marginTop: -getScaleSize(45),
});

export const WaterModal = styled(View, {
  position: 'absolute',
  top: 0,
  bottom: 0,
  left: 0,
  right: 0,
  backgroundColor: color('grayOpacity50'),
  alignItems: 'center',
  justifyContent: 'center',
});

export const PopUp = styled(View, {
  ...container('padding'),
  width: ({ layout }) => screenWidth - getScaleSize(layout.margin) * 2,
  backgroundColor: color('white'),
  borderRadius: getScaleSize(12),
  paddingBottom: getScaleSize(12),
  ...blockShadow(),
});

export const PopUpTitle = styled(Text, {
  ...font({ type: 'text1' }),
  color: color('gray10'),
  marginHorizontal: getScaleSize(15),
  marginTop: getScaleSize(12),
  marginBottom: getScaleSize(12),
  textAlign: 'center',
});

export const PopUpButton = styled(TouchableOpacity, {
  width: getScaleSize(66),
  height: getScaleSize(44),
  alignItems: 'center',
  justifyContent: 'center',
  borderRadius: getScaleSize(8),
  borderWidth: 1,
  borderColor: color('gray4'),
  backgroundColor: color('tertiary'),
  marginRight: getScaleSize(5),
  marginBottom: getScaleSize(12),
});

export const PopUpButtonText = styled(Text, {
  ...font({ type: 'text1' }),
  color: color('white'),
  textAlign: 'center',
});

export const ButtonBack = styled(TouchableOpacity, {
  paddingVertical: getScaleSize(18),
  flexDirection: 'row',
  alignItems: 'center',
  justifyContent: 'center',
});

export const ButtonBackText = styled(Text, {
  ...font({ type: 'text3', weight: 'normal' }),
  color: color('primary'),
  marginLeft: getScaleSize(8),
});
