import { Row } from '@screens/recipes/ui/header.style';
import { color } from '@shared/config/color';
import { styled } from '@shared/config/styled';
import { font } from '@shared/config/text';
import { container } from '@shared/config/view';
import { blockShadow } from '@shared/ui/block-shadow/block-shadow';
import { Text, View } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { getScaleSize, windowWidth } from 'src/utils/dimensions';

export const Container = styled(View, {
  marginTop: getScaleSize(16),
  marginHorizontal: getScaleSize(15),
  paddingVertical: getScaleSize(16),
  backgroundColor: color('white'),
  borderRadius: getScaleSize(15),
  ...blockShadow(),
});

export const TitileContainer = styled(View, {
  flexDirection: 'row',
  alignItems: 'center',
});

export const TitleRow = styled(Row, {
  paddingRight: getScaleSize(20),
  paddingLeft: getScaleSize(17),
  alignItems: 'flex-start',
});

export const Title = styled(Text, {
  ...font({ type: 'text1', weight: 'normal' }),
  marginLeft: getScaleSize(4),
});

export const CurrentWeight = styled(
  Text,
  (props: { color: 'green' | 'yellow' | 'orange' | 'red' }) =>
    ({
      ...font({ type: 'text1', weight: 'normal' }),
      color: color(
        props.color === 'red'
          ? 'secondary'
          : props.color === 'green'
          ? 'tertiary'
          : props.color === 'orange'
          ? 'weightColorOrange'
          : 'weightColorYellow',
      ),
    } as const),
);

export const CurrentWeightInfo = styled(Text, {
  ...font({ type: 'text1', weight: 'normal' }),
  color: color('gray10'),
});

export const CurrentWeightDescription = styled(Text, {
  ...font({ type: 'text4', weight: 'normal' }),
  color: color('gray7'),
});

export const CurrentWeightTitle = styled(Text, {
  ...font({ type: 'text3', weight: 'normal' }),
  color: color('gray10'),
  marginTop: getScaleSize(12),
});

export const LeftContainer = styled(View, {
  marginTop: getScaleSize(8),
  marginLeft: getScaleSize(15),
});

export const BottomInfoCOntainer = styled(View, {
  ...container(),
  paddingLeft: getScaleSize(15),
});

export const ProgressChartContainer = styled(View, {});

export const InfoContainer = styled(View, {
  position: 'absolute',
  top: 0,
  bottom: 0,
  left: 0,
  right: 0,
  justifyContent: 'center',
  alignItems: 'center',
});

export const ChartContainer = styled(View, {
  marginTop: getScaleSize(12),
  flexDirection: 'row',
  justifyContent: 'flex-start',
  alignItems: 'flex-end',
  marginHorizontal: getScaleSize(30),
});

export const Separator = styled(View, {
  width: ((windowWidth - 90) / 7) * 0.5,
});

export const ChartItemContainer = styled(View, {});

export const ChartItemView = styled(
  View,
  (props: { height: number; active: boolean }) =>
    ({
      width: (windowWidth - 90) / 7,
      height: getScaleSize(props.height),
      backgroundColor: color(props.active ? 'primary' : 'white'),
      borderRadius: 8,
      borderWidth: props.active ? undefined : 1,
      borderColor: color(props.active ? 'white' : 'gray4'),
      justifyContent: 'center',
      alignItems: 'center',
    } as const),
);

export const ChartItemDescription = styled(Text, {
  ...font({ type: 'text3', weight: 'normal' }),
  color: color('gray7'),
  marginTop: getScaleSize(12),
  textAlign: 'center',
});

export const ChartTextTitle = styled(
  Text,
  (props: { active: boolean }) =>
    ({
      ...font({ type: 'text4', weight: 'normal' }),
      color: color(props.active ? 'white' : 'gray10'),
      textAlign: 'center',
    } as const),
);

export const ChartTextDescription = styled(Text, {
  ...font({ type: 'text4', weight: 'normal' }),
  color: color('white'),
});

export const PickLabelWeight = styled(
  LinearGradient,
  (props: { x: number; y: number }) =>
    ({
      position: 'absolute',
      top: props.y,
      left: props.x,
      flexDirection: 'row',
      alignItems: 'center',
      padding: 3,
      borderRadius: getScaleSize(7),
    } as const),
);

export const PickLabelText = styled(Text, {
  ...font({ type: 'text4' }),
  color: color('white'),
});
