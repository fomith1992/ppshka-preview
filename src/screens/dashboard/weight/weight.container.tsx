import { useNavigation } from '@react-navigation/core';
import React from 'react';
import { Alert } from 'react-native';
import { useSelector } from 'react-redux';
import useAsync from 'react-use/lib/useAsync';
import { userAPI } from 'src/api';
import { AppState } from 'src/store';
import { WeightView } from './weight.view';

export const Weight = (): React.ReactElement => {
  const is_pro = useSelector((state: AppState) => state.session.user.is_pro);

  const navigation = useNavigation();
  const access_token = useSelector((state: AppState) => state.session.tokens.accessToken);

  const meelDataStatus = useAsync(async () => {
    return await userAPI.getUserWeightHistory(access_token).catch((e) => {
      console.log('meelData: ', e);
      Alert.alert('Произошла ошибка');
    });
  }, []);

  const weightTarget = useAsync(async () => {
    return await userAPI.getUserWeightTarget(access_token).catch((e) => {
      console.log('meelData: ', e);
      Alert.alert('Произошла ошибка');
    });
  }, []);

  return (
    <WeightView
      data={meelDataStatus.value?.data.data ?? []}
      weightTarget={weightTarget.value?.data.data}
      onGoToSetWeight={() => navigation.navigate('SetWeight')}
      is_pro={is_pro}
      onOpenPromo={() => navigation.navigate('Promo')}
    />
  );
};
