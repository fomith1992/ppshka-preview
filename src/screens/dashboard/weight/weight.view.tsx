import { Row } from '@screens/recipes/ui/header.style';
import React, { useState } from 'react';
import { ScrollView, TouchableOpacity, View } from 'react-native';
import Icon from 'src/components/Icon';
import { hitSlopParams } from 'src/utils/hit-slop-params';
import {
  Container,
  CurrentWeight,
  TitileContainer,
  LeftContainer,
  Title,
  TitleRow,
  CurrentWeightDescription,
  CurrentWeightTitle,
  PickLabelWeight,
  PickLabelText,
  BottomInfoCOntainer,
  /* ProgressChartContainer,
  InfoContainer,
  CurrentWeightInfo, */
} from './weight.style';
import { TUserWeightHistory } from 'src/api/user';
import {
  ButtonBack,
  ButtonBackText,
  ButtonsContainer,
  ModalBlock,
  OverflowContainer,
  PopUpDescription,
  PopUpTitle,
} from 'src/components/calendar/calendar.style';
import Button from 'src/components/Button';
import { Modal } from '@shared/ui/modal';
import { LineChart } from 'react-native-chart-kit';
import { gradientLayoutColors } from '@shared/ui/gradient-layout/constant';
import { theme } from '@shared/config/theme';
import { screenWidth } from 'src/utils/dimensions';

// const ChartItem = (props: {
//   weight: number;
//   weightLoss?: number;
//   date: string;
// }): React.ReactElement => {
//   return (
//     <ChartItemContainer>
//       <ChartItemView
//         active={props.weightLoss != null && props.weightLoss > 0}
//         height={props.weight}
//       >
//         <ChartTextTitle active={props.weightLoss != null && props.weightLoss > 0}>
//           {props.weight} кг
//         </ChartTextTitle>
//         {props.weightLoss != null && props.weightLoss > 0 ? (
//           <ChartTextDescription>{`-${props.weightLoss} кг`}</ChartTextDescription>
//         ) : undefined}
//       </ChartItemView>
//       <ChartItemDescription>{props.date}</ChartItemDescription>
//     </ChartItemContainer>
//   );
// };

interface WeightViewProps {
  data: TUserWeightHistory[];
  onGoToSetWeight: () => void;
  onOpenPromo: () => void;
  is_pro: boolean;
  weightTarget?: string;
}

const chartConfig = {
  backgroundGradientFromOpacity: 0,
  backgroundGradientToOpacity: 0,
  color: (opacity = 1) => `rgba(96, 52, 168, ${opacity})`, // (opacity = 1) => `rgba(26, 255, 146, ${opacity})`,
  strokeWidth: 3,
  verticalLabelsHeightPercentage: 10,
  propsForDots: {
    r: '8',
    // strokeWidth: "2",
    // stroke: "#ffa726"
  },
};

function calculateWidthChart(countItem: number) {
  const WIDTH_STEP = 80;
  const max_width = screenWidth - theme.layout.margin * 2 - 16;
  return countItem * WIDTH_STEP < max_width ? max_width : countItem * WIDTH_STEP;
}

const leftToTheGoalColor = (num: number): 'green' | 'yellow' | 'orange' | 'red' => {
  switch (num) {
    case 0:
    case 1:
    case -1:
      return 'green';
    case 2:
    case -2:
      return 'yellow';
    case 3:
    case -3:
    case 4:
    case -4:
      return 'orange';
    default:
      return 'red';
  }
};

export const WeightView = ({
  data,
  onGoToSetWeight,
  is_pro,
  onOpenPromo,
  weightTarget,
}: WeightViewProps): React.ReactElement => {
  const [popUpVisible, setPopUpVisible] = useState(false);
  const [pickIndex, setPickIndex] = useState<number | null>(null);

  const canAddParams = () => {
    if (!is_pro && data.length > 5) {
      setPopUpVisible(true);
    } else {
      onGoToSetWeight();
    }
  };

  const sortedData = data
    .sort(function (a, b) {
      if (a.full_date > b.full_date) {
        return 1;
      }
      if (a.full_date < b.full_date) {
        return -1;
      }
      // a должно быть равным b
      return 0;
    })
    .reverse();

  const leftToTheGoal =
    +(weightTarget ?? 0) -
    ((sortedData[sortedData.length - 1] && sortedData[sortedData.length - 1].weight) ?? 0);

  return (
    <Container>
      <TitleRow alignItems spaceBeetween>
        <View>
          <TitileContainer>
            <Icon name="workout" size={16} color="tertiary" />
            <Title>Ваш вес</Title>
          </TitileContainer>
          <LeftContainer>
            <CurrentWeight color="green">
              {(sortedData[0] && sortedData[0].weight) ?? 0} кг
            </CurrentWeight>
            <CurrentWeightDescription>Текущий вес</CurrentWeightDescription>
            <CurrentWeightTitle>График замеров</CurrentWeightTitle>
          </LeftContainer>
        </View>
        <Row>
          <TouchableOpacity onPress={canAddParams} hitSlop={hitSlopParams('L')}>
            <Icon color="gray6" style={{ transform: [{ rotate: '180deg' }] }} name="arrowBack" />
          </TouchableOpacity>
        </Row>
      </TitleRow>
      {sortedData.length > 0 && (
        <ScrollView
          horizontal={true}
          contentOffset={{ x: 35, y: 0 }}
          showsHorizontalScrollIndicator={false}
        >
          <LineChart
            data={{
              labels: sortedData.map((item) => item.data),
              datasets: [
                {
                  data: sortedData.map((item) => item.weight),
                },
              ],
            }}
            width={calculateWidthChart(sortedData.length)}
            height={100}
            fromZero={true}
            chartConfig={chartConfig}
            bezier
            yAxisLabel="$"
            yAxisSuffix="k"
            yAxisInterval={1}
            withVerticalLines={false}
            withHorizontalLines={false}
            withHorizontalLabels={false}
            style={{
              flex: 1,
              marginTop: 8,
              paddingBottom: 16,
              marginHorizontal: 8,
              borderRadius: 16,
            }}
            onDataPointClick={({ index }) => {
              setPickIndex(index);
            }}
            renderDotContent={({ x, y, index, indexData }) =>
              pickIndex === index && (
                <PickLabelWeight colors={gradientLayoutColors} x={x} y={y} key={`${x}-${y}`}>
                  <PickLabelText>{`${indexData} кг`}</PickLabelText>
                </PickLabelWeight>
              )
            }
          />
        </ScrollView>
      )}
      {weightTarget && (
        <BottomInfoCOntainer>
          <CurrentWeightTitle>
            До достижения цели осталось:{' '}
            <CurrentWeight color={leftToTheGoalColor(leftToTheGoal)}>
              {leftToTheGoal} кг
            </CurrentWeight>
          </CurrentWeightTitle>
        </BottomInfoCOntainer>
      )}
      <Modal visible={popUpVisible}>
        <OverflowContainer>
          <ModalBlock>
            <PopUpTitle>Доступно в Pro</PopUpTitle>
            <PopUpDescription>
              Добавление более двух замеров доступно только при подписке на один из наших тарфиных
              планов
            </PopUpDescription>
            <ButtonsContainer>
              <Button
                uppercase
                content="Выбрать тариф"
                onPress={() => {
                  setPopUpVisible(false);
                  onOpenPromo();
                }}
              />
              <ButtonBack onPress={() => setPopUpVisible(false)}>
                <Icon name="arrowBack" size={12} color="primary" />
                <ButtonBackText>Назад</ButtonBackText>
              </ButtonBack>
            </ButtonsContainer>
          </ModalBlock>
        </OverflowContainer>
      </Modal>
    </Container>
  );
};
