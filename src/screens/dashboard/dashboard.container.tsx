import React, { useState } from 'react';
import { Alert } from 'react-native';
import { useSelector } from 'react-redux';
import useAsyncFn from 'react-use/lib/useAsyncFn';
import { userAPI } from 'src/api';
import { AppState } from 'src/store';
import { DashboardView } from './dashboard.view';

interface DashboardProps {
  date?: Date;
}

export const Dashboard = ({ date }: DashboardProps): React.ReactElement => {
  const [waterModalvisible, setWaterModalvisible] = useState(false);
  const [waterModalEditvisible, setWaterModalEditvisible] = useState<boolean>(false);
  const [waterDeletedItem, setWaterDeletedItem] = useState<number | null>(null);
  const access_token = useSelector((state: AppState) => state.session.tokens.accessToken);
  const [selectedDate, setSelectedDate] = useState(date ?? new Date());

  const [_, onAddWater] = useAsyncFn(async (col_water: number) => {
    userAPI
      .addWaterContent({ access_token, col_water })
      .then(() => setWaterModalvisible(false))
      .catch((e) => {
        console.log('getWaterContent: ', e);
        Alert.alert('Произошла ошибка');
      });
  }, []);

  const [, onDelWaterItem] = useAsyncFn(async () => {
    if (waterDeletedItem) {
      userAPI
        .delWaterContent({ access_token, water_id: waterDeletedItem })
        .then(() => {
          setWaterDeletedItem(null);
        })
        .catch((e) => {
          console.log('getWaterContent: ', e);
          Alert.alert('Произошла ошибка');
        });
    }
  }, [waterDeletedItem]);

  return (
    <DashboardView
      selectedDate={selectedDate}
      setSelectedDate={setSelectedDate}
      onAddWater={onAddWater}
      setWaterModalvisible={setWaterModalvisible}
      waterModalvisible={waterModalvisible}
      waterModalEditvisible={waterModalEditvisible}
      setWaterModalEditvisible={setWaterModalEditvisible}
      waterDeletedItem={waterDeletedItem}
      setWaterDeletedItem={setWaterDeletedItem}
      onDelWaterItem={onDelWaterItem}
    />
  );
};
