import React from 'react';
import { TrainingsView } from './trainings.view';

export const Trainings = (): React.ReactElement => {
  return <TrainingsView />;
};
