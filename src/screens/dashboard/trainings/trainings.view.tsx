import React from 'react';
import { TouchableOpacity } from 'react-native';
import Icon from 'src/components/Icon';
import { hitSlopParams } from 'src/utils/hit-slop-params';
import {
  Container,
  /* ContainerStyleSheet,
  Slider, */
  TitileContainer,
  Title,
  TitleRow,
  GymSvg,
  Description,
} from './trainings.style';
/* import { Row } from '@screens/recipes/ui/header.style';
import { Spring } from '@screens/workout/styles';
import { useMaterialized } from '@shared/config/styled';
import WorkoutSmallCard from 'src/components/workout-card-small'; */

export const TrainingsView = (): React.ReactElement => {
  /* const workoutListData: string[] = Array.from({ length: 10 }, (_, idx) => `${idx}`);
  const containerStyle = useMaterialized(ContainerStyleSheet); */
  return (
    <Container>
      <TitleRow alignItems spaceBeetween>
        <TitileContainer>
          <GymSvg />
          <Title>Предстоящая тренировка</Title>
        </TitileContainer>
        <TouchableOpacity hitSlop={hitSlopParams('L')}>
          <Icon color="gray6" style={{ transform: [{ rotate: '180deg' }] }} name="arrowBack" />
        </TouchableOpacity>
      </TitleRow>
      {/* <Slider
        contentContainerStyle={containerStyle}
        showsHorizontalScrollIndicator={false}
        horizontal={true}
      >
        {workoutListData.map((item, idx) => (
          <Row key={idx}>
            <WorkoutSmallCard id={item} key={item} />
            <Spring />
          </Row>
        ))}
      </Slider> */}
      <Description>Недоступно в Beta-версии</Description>
    </Container>
  );
};
