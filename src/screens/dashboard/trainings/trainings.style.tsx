import { Row } from '@screens/recipes/ui/header.style';
import { color } from '@shared/config/color';
import { lift, styled } from '@shared/config/styled';
import { font } from '@shared/config/text';
import { blockShadow } from '@shared/ui/block-shadow/block-shadow';
import { GymIcon30 } from '@shared/ui/icons/gym.icon-16';
import { ScrollView, Text, View } from 'react-native';
import { getScaleSize } from 'src/utils/dimensions';

export const Container = styled(View, {
  marginTop: getScaleSize(16),
  marginHorizontal: getScaleSize(15),
  paddingVertical: getScaleSize(16),
  backgroundColor: color('white'),
  borderRadius: getScaleSize(15),
  ...blockShadow(),
});

export const TitileContainer = styled(View, {
  flexDirection: 'row',
  alignItems: 'center',
});

export const TitleRow = styled(Row, {
  paddingRight: getScaleSize(20),
  paddingLeft: getScaleSize(17),
});

export const Title = styled(Text, {
  ...font({ type: 'text1', weight: 'normal' }),
  marginLeft: getScaleSize(4),
});

export const Slider = styled(ScrollView, {
  paddingHorizontal: getScaleSize(15),
  marginTop: getScaleSize(12),
});

export const GymSvg = styled(GymIcon30, {
  color: color('tertiary'),
});

export const ContainerStyleSheet = lift({
  paddingRight: getScaleSize(15),
});

export const Description = styled(Text, {
  ...font({ type: 'text3' }),
  color: color('secondary'),
  textAlign: 'center',
  marginVertical: getScaleSize(16),
});
