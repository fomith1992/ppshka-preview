import React from 'react';
import { Alert } from 'react-native';

import { CommonActions, useNavigation } from '@react-navigation/native';

import useAsyncFn from 'react-use/lib/useAsyncFn';
import { auth } from 'src/api';
import { useSelector } from 'react-redux';
import { AppState } from 'src/store';
import { updateWeightEvent } from 'src/firebase/analytics/analytics-user/update-weight';
import { SetWeightWeightView } from './set-weight.view';

export const SetWeightScreen = () => {
  const access_token = useSelector((state: AppState) => state.session.tokens.accessToken);
  const userId = useSelector((state: AppState) => state.session.user.id);

  const navigation = useNavigation();

  const [changeWeightState, onChangeWeight] = useAsyncFn(async (value: string) => {
    return await auth
      .registerSetUserParams({
        access_token,
        weight: value,
      })
      .then(async () => {
        updateWeightEvent(userId);
        navigation.dispatch(
          CommonActions.reset({
            index: 0,
            routes: [
              {
                name: 'TabNavigator',
                params: {
                  screen: 'Dashboard',
                  initial: false,
                  params: {
                    initial: false,
                    screen: 'Dashboard',
                    params: {},
                  },
                },
              },
            ],
          }),
        );
      })
      .catch((err) => {
        console.log('onChangeWeight err: ', err);
        Alert.alert('Произошла ошибка');
      });
  }, []);

  return (
    <SetWeightWeightView
      title="Укажите ваш вес"
      description="Это необходимо для того, чтобы наши алгоритмы правильно подобрали программу тренировок и питания под ваши особенности"
      loading={changeWeightState.loading}
      textButton={'Добавить в историю'}
      onPressSaveWidthButton={onChangeWeight}
      onPressBackButton={navigation.goBack}
    />
  );
};
