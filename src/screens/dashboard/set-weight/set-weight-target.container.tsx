import React from 'react';
import { Alert } from 'react-native';

import { CommonActions, useNavigation } from '@react-navigation/native';

import useAsyncFn from 'react-use/lib/useAsyncFn';
import { userAPI } from 'src/api';
import { useSelector } from 'react-redux';
import { AppState } from 'src/store';
import { updateWeightTargetEvent } from 'src/firebase/analytics/analytics-user/update-weight';
import { SetWeightWeightView } from './set-weight.view';

export const SetWeightTargetScreen = () => {
  const access_token = useSelector((state: AppState) => state.session.tokens.accessToken);
  const userId = useSelector((state: AppState) => state.session.user.id);

  const navigation = useNavigation();

  const [changeWeightState, onChangeWeight] = useAsyncFn(async (value: string) => {
    return await userAPI
      .setUserWeightTarget(access_token, value)
      .then(async () => {
        updateWeightTargetEvent(userId);
        navigation.dispatch(
          CommonActions.reset({
            index: 0,
            routes: [
              {
                name: 'TabNavigator',
                params: {
                  screen: 'Dashboard',
                  initial: false,
                  params: {
                    initial: false,
                    screen: 'Dashboard',
                    params: {},
                  },
                },
              },
            ],
          }),
        );
      })
      .catch((err) => {
        console.log('onChangeWeight err: ', err);
        Alert.alert('Произошла ошибка');
      });
  }, []);

  return (
    <SetWeightWeightView
      title="Укажите желаемый вес"
      description="Цель поможет нам выбрать оптимальную программу и программу питания."
      loading={changeWeightState.loading}
      textButton={'Добавить новую цель'}
      onPressSaveWidthButton={onChangeWeight}
      onPressBackButton={navigation.goBack}
    />
  );
};
