import { color } from '@shared/config/color';
import { styled } from '@shared/config/styled';
import { font } from '@shared/config/text';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { getScaleSize } from 'src/utils/dimensions';
import { container } from '@shared/config/view';

export const WeightContainer = styled(View, {
  flex: 1,
  justifyContent: 'center',
  alignItems: 'center',
});

export const TextTitle = styled(Text, {
  ...font({ type: 'h1', weight: 'normal' }),
  lineHeight: getScaleSize(37),
  color: color('gray10'),
  marginBottom: getScaleSize(12),
  textAlign: 'center',
  marginHorizontal: getScaleSize(10),
});

export const TextSubTitle = styled(Text, {
  ...font({ type: 'text2' }),
  lineHeight: getScaleSize(21),
  color: color('gray7'),
  marginBottom: getScaleSize(40),
  marginHorizontal: getScaleSize(54),
  textAlign: 'center',
});

export const TextButtonBack = styled(Text, {
  ...font({ type: 'text3', weight: 'normal' }),
  color: color('primary'),
  marginLeft: getScaleSize(8),
});

export const BottomContainer = styled(View, {
  paddingHorizontal: getScaleSize(15),
});

export const ButtonBack = styled(TouchableOpacity, {
  paddingVertical: getScaleSize(18),
  flexDirection: 'row',
  alignItems: 'center',
  justifyContent: 'center',
});

export const styles = StyleSheet.create({
  container: {
    paddingTop: getScaleSize(10),
    flexGrow: 1,
    justifyContent: 'space-between',
    paddingBottom: getScaleSize(70),
  },
});

export const Spring = styled(View, {
  width: getScaleSize(10),
});

export const WeightPickersContainer = styled(View, {
  ...container('margin'),
  flexDirection: 'row',
});

export const CustomizePickerContainer = styled(View, {
  borderRadius: getScaleSize(10),
  borderColor: color('primary'),
  borderWidth: getScaleSize(2),
  flex: 1,
});
