import { Row } from '@screens/recipes/ui/header.style';
import { color } from '@shared/config/color';
import { styled } from '@shared/config/styled';
import { font } from '@shared/config/text';
import { blockShadow } from '@shared/ui/block-shadow/block-shadow';
import { Text, View } from 'react-native';
import { getScaleSize } from 'src/utils/dimensions';

export const Container = styled(View, {
  marginTop: getScaleSize(16),
  marginHorizontal: getScaleSize(15),
  paddingVertical: getScaleSize(16),
  backgroundColor: color('white'),
  borderRadius: getScaleSize(15),
  ...blockShadow(),
});

export const TitileContainer = styled(View, {
  flexDirection: 'row',
  alignItems: 'center',
});

export const TitleRow = styled(Row, {
  paddingRight: getScaleSize(20),
  paddingLeft: getScaleSize(17),
  alignItems: 'flex-start',
});

export const Title = styled(Text, {
  ...font({ type: 'text1', weight: 'normal' }),
  marginLeft: getScaleSize(4),
});

export const CurrentWeight = styled(Text, {
  ...font({ type: 'h2', weight: 'normal' }),
  color: color('tertiary'),
});

export const LeftContainer = styled(View, {
  marginTop: getScaleSize(8),
  marginLeft: getScaleSize(15),
});
