import React from 'react';
import { Row } from '@screens/recipes/ui/header.style';
import { TouchableOpacity, View } from 'react-native';
import Icon from 'src/components/Icon';
import { hitSlopParams } from 'src/utils/hit-slop-params';
import {
  Container,
  CurrentWeight,
  TitileContainer,
  LeftContainer,
  Title,
  TitleRow,
} from './weight-target.style';

interface WeightViewProps {
  weight: string;
  onGoToSetWeightTarget: () => void;
}

export const WeightView = ({
  weight,
  onGoToSetWeightTarget,
}: WeightViewProps): React.ReactElement => {
  return (
    <Container>
      <TitleRow alignItems spaceBeetween>
        <View>
          <TitileContainer>
            <Icon name="workout" size={16} color="tertiary" />
            <Title>Ваша цель</Title>
          </TitileContainer>
          <LeftContainer>
            <CurrentWeight>{`${weight === '' ? 'Нет цели' : `${weight} кг`}`}</CurrentWeight>
          </LeftContainer>
        </View>
        <Row>
          <TouchableOpacity onPress={onGoToSetWeightTarget} hitSlop={hitSlopParams('L')}>
            <Icon color="gray6" style={{ transform: [{ rotate: '180deg' }] }} name="arrowBack" />
          </TouchableOpacity>
        </Row>
      </TitleRow>
    </Container>
  );
};
