import { useNavigation } from '@react-navigation/core';
import React from 'react';
import { Alert } from 'react-native';
import { useSelector } from 'react-redux';
import useAsync from 'react-use/lib/useAsync';
import { userAPI } from 'src/api';
import { AppState } from 'src/store';
import { WeightView } from './weight-target.view';

export const WeightTarget = (): React.ReactElement => {
  const navigation = useNavigation();
  const access_token = useSelector((state: AppState) => state.session.tokens.accessToken);

  const weightTarget = useAsync(async () => {
    return await userAPI.getUserWeightTarget(access_token).catch((e) => {
      console.log('meelData: ', e);
      Alert.alert('Произошла ошибка');
    });
  }, []);

  return (
    <WeightView
      weight={weightTarget.value?.data.data ?? ''}
      onGoToSetWeightTarget={() => navigation.navigate('SetWeightTarget')}
    />
  );
};
