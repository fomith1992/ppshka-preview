import React from 'react';
import { Calendar } from 'src/components/calendar/calendar.view';
import {
  ButtonBack,
  ButtonBackText,
  PopUp,
  PopUpButton,
  PopUpButtonText,
  PopUpTitle,
  ScrollContainer,
  styles,
  WaterModal,
} from './dashboard.style';
import { Meal } from './meal/meal.container';
import { Metering } from './metering/metering.container';
import { СaloricСontent } from './caloric-content/caloric-content.container';
/* import { Trainings } from './trainings/trainings.container'; */
import { Weight } from './weight/weight.container';
import ScreenLayout from 'src/components/ScreenLayout';
import { GradientLayoutHeaderView } from '@shared/ui/gradient-layout/gradient-layout-header.view';
import { View } from 'react-native';
import { Header } from 'src/components/header/header.view';
import { StepsСontent } from './steps-content/steps-content.container';
import { WeightTarget } from './weight-target/weight-target.container';
import { WaterCount } from './water-count/water-count.container';
import Icon from 'src/components/Icon';
import Button from 'src/components/Button';

interface DashboardViewProps {
  selectedDate: Date;
  setSelectedDate: (item: Date) => void;
  onAddWater: (item: number) => void;
  setWaterModalvisible: (value: boolean) => void;
  waterModalvisible: boolean;
  setWaterModalEditvisible: (value: boolean) => void;
  waterModalEditvisible: boolean;
  setWaterDeletedItem: (value: number | null) => void;
  waterDeletedItem: number | null;
  onDelWaterItem: () => void;
}

export const DashboardView = ({
  selectedDate,
  waterModalvisible,
  onAddWater,
  setSelectedDate,
  setWaterModalvisible,
  setWaterModalEditvisible,
  waterModalEditvisible,
  waterDeletedItem,
  setWaterDeletedItem,
  onDelWaterItem,
}: DashboardViewProps): React.ReactElement => {
  return (
    <ScreenLayout safeArea edges={['left', 'right']}>
      <View style={{ flexGrow: 1 }}>
        <GradientLayoutHeaderView>
          <Header title="Дашборд">
            <Calendar selectedDate={selectedDate} setSelectedDate={setSelectedDate} />
          </Header>
        </GradientLayoutHeaderView>
        <ScrollContainer contentContainerStyle={styles.scrollContainer}>
          <WaterCount
            waterDeletedItem={waterDeletedItem}
            setWaterDeletedItem={setWaterDeletedItem}
            waterModalEditvisible={waterModalEditvisible}
            setWaterModalEditvisible={setWaterModalEditvisible}
            waterModalvisible={waterModalvisible}
            onSetWaterModalvisible={() => setWaterModalvisible(true)}
          />
          <StepsСontent />
          <Weight />
          <WeightTarget />
          <Metering />
          <СaloricСontent />
          <Meal selectedDate={selectedDate} />
          {/* <Trainings /> */}
        </ScrollContainer>
        {waterModalvisible && (
          <WaterModal>
            <PopUp>
              <PopUpTitle>Укажите количество, выпитой к этому времени, воды</PopUpTitle>
              <View
                style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}
              >
                <PopUpButton onPress={() => onAddWater(150)}>
                  <PopUpButtonText>150мл</PopUpButtonText>
                </PopUpButton>
                <PopUpButton onPress={() => onAddWater(250)}>
                  <PopUpButtonText>250мл</PopUpButtonText>
                </PopUpButton>
                <PopUpButton onPress={() => onAddWater(500)}>
                  <PopUpButtonText>500мл</PopUpButtonText>
                </PopUpButton>
              </View>
              <View
                style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}
              >
                <PopUpButton onPress={() => onAddWater(1000)}>
                  <PopUpButtonText>1л</PopUpButtonText>
                </PopUpButton>
                <PopUpButton onPress={() => onAddWater(1500)}>
                  <PopUpButtonText>1.5л</PopUpButtonText>
                </PopUpButton>
              </View>

              <View
                style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}
              >
                <ButtonBack
                  hitSlop={{ top: 0, bottom: 0, left: 48, right: 48 }}
                  onPress={() => setWaterModalvisible(false)}
                >
                  <Icon name="arrowBack" size={12} color="primary" />
                  <ButtonBackText>Назад</ButtonBackText>
                </ButtonBack>
              </View>
            </PopUp>
          </WaterModal>
        )}
        {waterDeletedItem && (
          <WaterModal>
            <PopUp>
              <PopUpTitle>Вы уверены, что хотите удалить выбранный элемент?</PopUpTitle>
              <View style={{ justifyContent: 'center' }}>
                <Button uppercase content="Удалить" onPress={onDelWaterItem} />
                <ButtonBack
                  hitSlop={{ top: 0, bottom: 0, left: 48, right: 48 }}
                  onPress={() => setWaterDeletedItem(null)}
                >
                  <Icon name="arrowBack" size={12} color="primary" />
                  <ButtonBackText>Назад</ButtonBackText>
                </ButtonBack>
              </View>
            </PopUp>
          </WaterModal>
        )}
      </View>
    </ScreenLayout>
  );
};
