import React from 'react';
import { View } from 'react-native';
import Icon from 'src/components/Icon';
import {
  Container,
  CurrentWeight,
  TitileContainer,
  LeftContainer,
  Title,
  TitleRow,
  NotAccessText,
} from './steps-content.style';

interface StepsСontentViewProps {
  stepsCount: number;
  isAccess: boolean;
}

export const StepsСontentView = ({
  stepsCount,
  isAccess,
}: StepsСontentViewProps): React.ReactElement => {
  return (
    <Container>
      <TitleRow alignItems spaceBeetween>
        <View style={{ flex: 1 }}>
          <TitileContainer>
            <Icon name="ccal" />
            <Title>Количество шагов за день</Title>
          </TitileContainer>
          <LeftContainer>
            {isAccess && <CurrentWeight>{stepsCount} шагов</CurrentWeight>}
            {!isAccess && (
              <NotAccessText>Не получено разрешение от приложения "Здоровье"</NotAccessText>
            )}
          </LeftContainer>
        </View>
      </TitleRow>
    </Container>
  );
};
