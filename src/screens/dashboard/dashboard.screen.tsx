import { StackScreenProps } from '@react-navigation/stack';
import React from 'react';
import { DashboardStackParamList } from 'src/navigation/DashboardNavigator/DashboardNavigator';
import { Dashboard } from './dashboard.container';

export const DashboardScreen = ({
  route: { params },
}: StackScreenProps<DashboardStackParamList, 'Dashboard'>) => {
  return <Dashboard date={params ? params.date : undefined} />;
};
