import { useNavigation } from '@react-navigation/core';
import React from 'react';
import { Alert } from 'react-native';
import { useSelector } from 'react-redux';
import useAsync from 'react-use/lib/useAsync';
import { measurementsAPI } from 'src/api';
import { AppState } from 'src/store';
import { MeteringView } from './metering.view';

export const Metering = (): React.ReactElement => {
  const is_pro = useSelector((state: AppState) => state.session.user.is_pro);
  const navigation = useNavigation();
  const access_token = useSelector((state: AppState) => state.session.tokens.accessToken);

  const meteringDataStatus = useAsync(async () => {
    return await measurementsAPI.getMeasurements({ access_token }).catch((e) => {
      console.log('meteringDataStatus: ', e);
      Alert.alert('Произошла ошибка');
    });
  }, []);

  return (
    <MeteringView
      data={meteringDataStatus.value?.data.data ?? []}
      onOpenMeasurements={() => navigation.navigate('Measurements')}
      onOpenPromo={() => navigation.navigate('Promo')}
      is_pro={is_pro}
    />
  );
};
