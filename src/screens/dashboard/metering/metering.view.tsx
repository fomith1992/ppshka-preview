import { Row } from '@screens/recipes/ui/header.style';
import { Modal } from '@shared/ui/modal';
import React, { useState } from 'react';
import { TouchableOpacity, View } from 'react-native';
import { TUserMeasurementsHistory } from 'src/api/measurements';
/* import { ProgressChart } from 'react-native-chart-kit'; */
import Icon from 'src/components/Icon';
import { hitSlopParams } from 'src/utils/hit-slop-params';
import {
  Container,
  CurrentWeight,
  TitileContainer,
  LeftContainer,
  Title,
  TitleRow,
  CurrentWeightDescription,
  CurrentWeightTitle,
  ChartContainer,
  ChartItemContainer,
  ChartItemView,
  ChartItemDescription,
  ChartTextTitle,
  ChartTextDescription,
  Separator,
} from './metering.style';
import {
  ButtonBack,
  ButtonBackText,
  ButtonsContainer,
  ModalBlock,
  OverflowContainer,
  PopUpDescription,
  PopUpTitle,
} from 'src/components/calendar/calendar.style';
import Button from 'src/components/Button';

const ChartItem = (props: {
  weight: number;
  weightLoss?: number;
  date: string;
}): React.ReactElement => {
  return (
    <ChartItemContainer>
      <ChartItemView
        active={props.weightLoss != null && props.weightLoss > 0}
        height={props.weight / 10}
      >
        <ChartTextTitle active={props.weightLoss != null && props.weightLoss > 0}>
          {props.weight}
        </ChartTextTitle>
        {props.weightLoss != null && props.weightLoss > 0 ? (
          <ChartTextDescription>{`-${props.weightLoss}`}</ChartTextDescription>
        ) : undefined}
      </ChartItemView>
      <ChartItemDescription>{props.date}</ChartItemDescription>
    </ChartItemContainer>
  );
};

interface MeteringViewProps {
  data: TUserMeasurementsHistory[];
  is_pro: boolean;
  onOpenMeasurements: () => void;
  onOpenPromo: () => void;
}

export const MeteringView = ({
  data,
  is_pro,
  onOpenMeasurements,
  onOpenPromo,
}: MeteringViewProps): React.ReactElement => {
  const [popUpVisible, setPopUpVisible] = useState(false);

  const canAddParams = () => {
    if (!is_pro && data.length > 5) {
      setPopUpVisible(true);
    } else {
      onOpenMeasurements();
    }
  };

  const sortedData = data.sort(function (a, b) {
    if (a.data > b.data) {
      return -1;
    }
    if (a.data < b.data) {
      return 1;
    }
    // a должно быть равным b
    return 0;
  });

  return (
    <Container>
      <TitleRow alignItems spaceBeetween>
        <View>
          <TitileContainer>
            <Icon name="metering" />
            <Title>Последние замеры</Title>
          </TitileContainer>
          <LeftContainer>
            <CurrentWeight>{(sortedData[0] && sortedData[0].weight) ?? 0}</CurrentWeight>
            <CurrentWeightDescription>Текущий индекс</CurrentWeightDescription>
            <CurrentWeightTitle>График замеров</CurrentWeightTitle>
          </LeftContainer>
        </View>
        <Row>
          <TouchableOpacity onPress={canAddParams} hitSlop={hitSlopParams('L')}>
            <Icon color="gray6" style={{ transform: [{ rotate: '180deg' }] }} name="arrowBack" />
          </TouchableOpacity>
        </Row>
      </TitleRow>
      <ChartContainer>
        {sortedData.map((item, idx) => (
          <View style={{ flexDirection: 'row' }} key={idx}>
            <ChartItem
              weightLoss={sortedData[idx + 1] && sortedData[idx + 1].weight - item.weight}
              weight={item.weight}
              date={item.data}
            />
            {idx !== 4 && <Separator />}
          </View>
        ))}
      </ChartContainer>
      <Modal visible={popUpVisible}>
        <OverflowContainer>
          <ModalBlock>
            <PopUpTitle>Доступно в Pro</PopUpTitle>
            <PopUpDescription>
              Добавление более двух замеров доступно только при подписке на один из наших тарфиных
              планов
            </PopUpDescription>
            <ButtonsContainer>
              <Button
                uppercase
                content="Выбрать тариф"
                onPress={() => {
                  setPopUpVisible(false);
                  onOpenPromo();
                }}
              />
              <ButtonBack onPress={() => setPopUpVisible(false)}>
                <Icon name="arrowBack" size={12} color="primary" />
                <ButtonBackText>Назад</ButtonBackText>
              </ButtonBack>
            </ButtonsContainer>
          </ModalBlock>
        </OverflowContainer>
      </Modal>
    </Container>
  );
};
