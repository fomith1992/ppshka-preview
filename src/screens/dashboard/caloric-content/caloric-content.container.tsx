import { useFocusEffect } from '@react-navigation/core';
import React, { useState } from 'react';
import { Alert } from 'react-native';
import { useSelector } from 'react-redux';
import { userAPI } from 'src/api';
import { AppState } from 'src/store';
import { СaloricСontentView } from './caloric-content.view';

export const СaloricСontent = (): React.ReactElement => {
  const access_token = useSelector((state: AppState) => state.session.tokens.accessToken);
  const [caloriesCount, setCaloriesCount] = useState(0);

  useFocusEffect(
    React.useCallback(() => {
      userAPI
        .getCaloricContent({ access_token })
        .then((x) => setCaloriesCount(x.data.data ?? 0))
        .catch((e) => {
          console.log('meteringDataStatus: ', e);
          Alert.alert('Произошла ошибка');
        });
      return;
    }, []), // eslint-disable-line react-hooks/exhaustive-deps
  );

  return <СaloricСontentView ccalCount={caloriesCount} />;
};
