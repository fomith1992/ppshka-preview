import React from 'react';
import { View } from 'react-native';
import Icon from 'src/components/Icon';
import {
  Container,
  CurrentWeight,
  TitileContainer,
  LeftContainer,
  Title,
  TitleRow,
} from './caloric-content.style';

interface MeteringViewProps {
  ccalCount: number;
}

export const СaloricСontentView = ({ ccalCount }: MeteringViewProps): React.ReactElement => {
  return (
    <Container>
      <TitleRow alignItems spaceBeetween>
        <View>
          <TitileContainer>
            <Icon name="ccal" />
            <Title>Ваша суточная калорийность:</Title>
          </TitileContainer>
          <LeftContainer>
            <CurrentWeight>{ccalCount} ккал</CurrentWeight>
          </LeftContainer>
        </View>
      </TitleRow>
    </Container>
  );
};
