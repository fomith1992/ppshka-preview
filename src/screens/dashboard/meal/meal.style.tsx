import { color } from '@shared/config/color';
import { styled } from '@shared/config/styled';
import { font } from '@shared/config/text';
import { blockShadow } from '@shared/ui/block-shadow/block-shadow';
import { Text, TouchableOpacity, View } from 'react-native';
import { getScaleSize } from 'src/utils/dimensions';

export const Container = styled(View, {
  marginTop: getScaleSize(16),
  marginHorizontal: getScaleSize(15),
  paddingRight: getScaleSize(20),
  paddingLeft: getScaleSize(17),
  paddingTop: getScaleSize(16),
  paddingBottom: getScaleSize(35),
  backgroundColor: color('white'),
  borderRadius: getScaleSize(15),
  overflow: 'hidden',
  ...blockShadow(),
});

export const TitileContainer = styled(View, {
  flexDirection: 'row',
  alignItems: 'center',
});

export const WeightItemContainer = styled(View, {
  marginRight: getScaleSize(24),
});

export const Title = styled(Text, {
  ...font({ type: 'text1', weight: 'normal' }),
  marginLeft: getScaleSize(4),
});

export const MealItemTitle = styled(Text, {
  ...font({ type: 'text2', weight: 'normal' }),
  marginTop: getScaleSize(10),
  marginLeft: getScaleSize(16),
});

export const FoodCardContainer = styled(TouchableOpacity, {
  paddingRight: getScaleSize(16),
  backgroundColor: color('white'),
  borderRadius: getScaleSize(15),
  marginTop: getScaleSize(10),
  zIndex: 991,
  ...blockShadow(),
});

/* export const MealItemContainer = styled(View, {
  marginTop: getScaleSize(16),
  backgroundColor: '#f2f',
}); */

export const MealItemContainer = styled(View, {
  marginTop: getScaleSize(15),
  backgroundColor: color('gray3'),
  borderRadius: getScaleSize(15),
  ...blockShadow(),
});

export const EmptyCard = styled(View, {
  justifyContent: 'center',
  alignItems: 'center',
});

export const EmptyCardDescription = styled(Text, {
  ...font({ type: 'text3' }),
  lineHeight: getScaleSize(21),
  color: color('gray7'),
  marginTop: getScaleSize(4),
});

export const EmptyCardTitle = styled(Text, {
  ...font({ type: 'text3', weight: 'normal' }),
  lineHeight: getScaleSize(21),
  color: color('black'),
});

export const EmptyCardButton = styled(TouchableOpacity, {
  flexDirection: 'row',
});

export const EmptyCardText = styled(Text, {
  ...font({ type: 'text3', weight: 'normal' }),
  color: color('primary'),
  marginHorizontal: getScaleSize(12),
  marginBottom: getScaleSize(12),
});
