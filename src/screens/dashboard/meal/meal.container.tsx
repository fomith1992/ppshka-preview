import { useFocusEffect, useNavigation } from '@react-navigation/core';
import { format } from 'date-fns';
import React, { useState } from 'react';
import { Alert } from 'react-native';
import { useSelector } from 'react-redux';
import useAsyncFn from 'react-use/lib/useAsyncFn';
import { meelAPI } from 'src/api';
import { TMeetItem } from 'src/api/meal-plan';
import { AppState } from 'src/store';
import { MealView } from './meal.view';

interface MealProps {
  selectedDate: Date;
}

export const Meal = ({ selectedDate }: MealProps): React.ReactElement => {
  const access_token = useSelector((state: AppState) => state.session.tokens.accessToken);
  const categories = useSelector((state: AppState) => state.session.planCategories);
  const navigation = useNavigation();
  const [mealData, setMealData] = useState<TMeetItem[]>([]);
  const parseDate = format(selectedDate, 'y-MM-dd');

  const [meelDataStatus, onGetMeel] = useAsyncFn(async () => {
    return await meelAPI
      .getMeelData(access_token, parseDate)
      .then(({ data: { data } }) => {
        setMealData(data);
      })
      .catch((e) => {
        console.log('meelData: ', e);
        Alert.alert('Произошла ошибка');
      });
  }, [parseDate]);

  useFocusEffect(
    React.useCallback(() => {
      onGetMeel();
      return;
    }, [parseDate]), // eslint-disable-line react-hooks/exhaustive-deps
  );

  const [deleteMeelStatus, onDeleteMeel] = useAsyncFn(
    async (id: number) => {
      return await meelAPI
        .delMeelData(access_token, id)
        .then(async () => {
          await meelAPI
            .getMeelData(access_token, parseDate)
            .then(({ data }) => {
              setMealData(data.data);
            })
            .catch((e) => {
              console.log('meelData: ', e);
              Alert.alert('Произошла ошибка');
            });
        })
        .catch(() => Alert.alert('Произошла ошибка'));
    },
    [selectedDate],
  );

  const [, onChangeColServing] = useAsyncFn(
    async (data: { col: number; meal_id: number }) => {
      return await meelAPI
        .changeColServing({ access_token, col: data.col, meal_id: data.meal_id })
        .then(async () => {
          onGetMeel();
        })
        .catch((e) => {
          console.log('onChangeColServing', e);
          Alert.alert('Произошла ошибка');
        });
    },
    [selectedDate],
  );

  const data = categories.map((x) => ({
    title: x.name,
    meal_type_id: x.id,
    data: mealData.find((y) => y.time_category === x.system_key),
  }));

  return (
    <MealView
      setCount={onChangeColServing}
      onDeleteMeel={onDeleteMeel}
      data={data}
      loadingData={meelDataStatus.loading || deleteMeelStatus.loading}
      onGoSelectMeel={(type) =>
        navigation.navigate('SelectMeel', { type, date: selectedDate, returnScreen: 'Dashboard' })
      }
    />
  );
};
