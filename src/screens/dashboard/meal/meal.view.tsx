import { useNavigation } from '@react-navigation/core';
import { FoodCard } from '@screens/recipes-plan/recipes-plan.view';
import { Bottom, Row } from '@screens/recipes/ui/header.style';
import React from 'react';
import { useSelector } from 'react-redux';
import { TMeetItem } from 'src/api/meal-plan';
import Icon from 'src/components/Icon';
import { AppState } from 'src/store';
import { expectDefined } from 'src/utils/expect-defined';
import { hitSlopParams } from 'src/utils/hit-slop-params';
import {
  Container,
  TitileContainer,
  Title,
  MealItemContainer,
  MealItemTitle,
  EmptyCard,
  EmptyCardDescription,
  EmptyCardButton,
  EmptyCardText,
  EmptyCardTitle,
  FoodCardContainer,
} from './meal.style';

interface WeightItemProps {
  setCount: (data: { col: number; meal_id: number }) => void;
  description: string;
  data?: TMeetItem;
  onGoSelectMeel: () => void;
  onDeleteMeel: (id: number) => void;
}

const MealItem = ({
  setCount,
  data,
  description,
  onGoSelectMeel,
  onDeleteMeel,
}: WeightItemProps): React.ReactElement => {
  const categories = useSelector((state: AppState) => state.session.categories);
  const navigation = useNavigation();
  return (
    <MealItemContainer>
      <MealItemTitle>{description}</MealItemTitle>
      {data != null ? (
        <FoodCardContainer
          onPress={() =>
            navigation.navigate('RecipeFull', {
              id: data?.recipe_id ?? 1,
              category: expectDefined(categories.find((x) => x.system_key === data.time_category)),
            })
          }
        >
          <FoodCard setCount={setCount} onDeleteMeel={onDeleteMeel} data={data} small />
        </FoodCardContainer>
      ) : (
        <EmptyCard>
          <EmptyCardTitle>Нет добавленных рецептов</EmptyCardTitle>
          <EmptyCardDescription>
            Добавьте один из рецептов, чтобы {'\n'} придерживаться плана питания
          </EmptyCardDescription>
          <Bottom mb={12} />
          <EmptyCardButton onPress={onGoSelectMeel} hitSlop={hitSlopParams('L')}>
            <EmptyCardText>Добавить {description.toLowerCase()}</EmptyCardText>
          </EmptyCardButton>
        </EmptyCard>
      )}
    </MealItemContainer>
  );
};

interface MealViewProps {
  setCount: (data: { col: number; meal_id: number }) => void;
  onGoSelectMeel: (item: number) => void;
  data: {
    title: string;
    meal_type_id: number;
    data?: TMeetItem;
  }[];
  loadingData: boolean;
  onDeleteMeel: (id: number) => void;
}

export const MealView = ({
  setCount,
  onGoSelectMeel,
  onDeleteMeel,
  data,
}: MealViewProps): React.ReactElement => {
  return (
    <Container>
      <Row alignItems spaceBeetween>
        <TitileContainer>
          <Icon name="food" color="primary" size={18} />
          <Title>Питание</Title>
        </TitileContainer>
        {/* <TouchableOpacity
          onPress={() => onGoSelectMeel('Все рецепты')}
          hitSlop={hitSlopParams('L')}
        >
          <Icon color="gray6" style={{ transform: [{ rotate: '180deg' }] }} name="arrowBack" />
        </TouchableOpacity> */}
      </Row>
      {data.map((item, idx) => (
        <MealItem
          key={idx}
          setCount={setCount}
          onDeleteMeel={onDeleteMeel}
          data={item.data}
          description={item.title}
          onGoSelectMeel={() => onGoSelectMeel(item.meal_type_id)}
        />
      ))}
    </Container>
  );
};
