import { color } from '@shared/config/color';
import { styled } from '@shared/config/styled';
import { container } from '@shared/config/view';
import { StyleSheet, View } from 'react-native';

import { purpleColor } from 'src/commonStyles/colors';
import { font12m, font14r, font20r, font30m, font50r } from 'src/commonStyles/fonts';
import { getScaleSize } from 'src/utils/dimensions';

export const styles = StyleSheet.create({
  container: {
    paddingTop: getScaleSize(10),
    flexGrow: 1,
    justifyContent: 'space-between',
    paddingBottom: getScaleSize(70),
  },
  paddingHorizontal: {
    paddingHorizontal: getScaleSize(15),
  },
  containerTabs: {
    marginBottom: getScaleSize(81),
  },
  textTitle: {
    ...font30m,
    lineHeight: getScaleSize(37),
    color: '#262626',
    marginBottom: getScaleSize(12),
    textAlign: 'center',
    marginHorizontal: getScaleSize(10),
  },
  containerChoose: {
    alignItems: 'center',
  },
  smallDash: {
    width: getScaleSize(2),
    height: getScaleSize(15),
    backgroundColor: '#D9D9D9',
  },
  dash: {
    width: getScaleSize(2),
    height: getScaleSize(30),
    backgroundColor: '#D9D9D9',
  },
  mainDash: {
    width: getScaleSize(3),
    height: getScaleSize(67),
    backgroundColor: purpleColor,
  },
  textDash: {
    ...font20r,
    color: '#262626',
    marginBottom: getScaleSize(11),
  },
  containerChoices: {
    marginTop: getScaleSize(25),
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-end',
  },
  textChoice: {
    ...font50r,
    color: '#434343',
  },
  textSubTitle: {
    ...font14r,
    lineHeight: getScaleSize(21),
    color: '#8C8C8C',
    marginBottom: getScaleSize(40),
    marginHorizontal: getScaleSize(54),
    textAlign: 'center',
  },
  buttonBack: {
    paddingVertical: getScaleSize(18),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  textButtonBack: {
    ...font12m,
    color: purpleColor,
    marginLeft: getScaleSize(8),
  },
  containerIconTick: {
    position: 'absolute',
    left: getScaleSize(30),
  },
});

export const Spring = styled(View, {
  width: getScaleSize(10),
});

export const WeightPickersContainer = styled(View, {
  ...container('margin'),
  flexDirection: 'row',
});

export const CustomizePickerContainer = styled(View, {
  borderRadius: getScaleSize(10),
  borderColor: color('primary'),
  borderWidth: getScaleSize(2),
  flex: 1,
});
