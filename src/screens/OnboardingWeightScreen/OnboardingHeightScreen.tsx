import React, { useState } from 'react';
import { Text, TouchableOpacity, View } from 'react-native';

import { useNavigation } from '@react-navigation/native';

import Button from 'src/components/Button';
import Icon from 'src/components/Icon';
import OnboardingTabs from 'src/components/OnboardingTabs';
import ScreenLayout from 'src/components/ScreenLayout';

import { CustomizePickerContainer, Spring, styles, WeightPickersContainer } from './styles';
import { StackScreenProps } from '@react-navigation/stack';
import { RootStackParamList } from 'src/navigation/RootNavigator';
import { Picker } from '@react-native-picker/picker';

const MAX_HEIGHT = 220;
const MIN_HEIGHT = 90;
const INITIAL_HEIGHT = 160;

const OnboardingHeightScreen = ({
  route,
}: StackScreenProps<RootStackParamList, 'OnboardingHeight'>) => {
  const [currentHeight, setCurrentWeightKGramms] = useState<number>(INITIAL_HEIGHT);
  const [currentWeightGramms, setCurrentWeightGramms] = useState<number>(0);
  const navigation = useNavigation();

  const onBack = () => {
    navigation.goBack();
  };

  return (
    <ScreenLayout style={styles.container}>
      <View>
        <OnboardingTabs activeValue={5} totalValues={7} style={styles.containerTabs} />
        <Text style={styles.textTitle}>Укажите ваш рост</Text>
        <Text style={styles.textSubTitle}>
          Это необходимо для того, чтобы наши алгоритмы правильно подобрали программу тренировок и
          питания под ваши особенности
        </Text>
        <WeightPickersContainer>
          <CustomizePickerContainer>
            <Picker
              selectedValue={currentHeight}
              onValueChange={(itemValue) => setCurrentWeightKGramms(itemValue)}
              style={{
                width: '100%',
              }}
            >
              {Array.from({ length: MAX_HEIGHT }, (_, i) => i)
                .slice(MIN_HEIGHT)
                .map((value, index) => (
                  <Picker.Item key={index} label={`${value} см`} value={value} />
                ))}
            </Picker>
          </CustomizePickerContainer>
          <Spring />
          <CustomizePickerContainer>
            <Picker
              selectedValue={currentWeightGramms}
              onValueChange={(itemValue) => setCurrentWeightGramms(itemValue)}
            >
              {Array.from({ length: 10 }, (_, i) => i).map((value, index) => (
                <Picker.Item key={index} label={`${value * 1} мм`} value={value} />
              ))}
            </Picker>
          </CustomizePickerContainer>
        </WeightPickersContainer>
      </View>
      <View style={styles.paddingHorizontal}>
        <Button
          uppercase
          content="Продолжить"
          onPress={() =>
            navigation.navigate('OnboardingQuestionActivity', {
              ...route.params,
              height: currentHeight,
            })
          }
        />
        <TouchableOpacity style={styles.buttonBack} onPress={onBack}>
          <Icon name="arrowBack" size={12} color="primary" />
          <Text style={styles.textButtonBack}>Назад</Text>
        </TouchableOpacity>
      </View>
    </ScreenLayout>
  );
};

export default OnboardingHeightScreen;
