import React, { useState } from 'react';
import { Text, TouchableOpacity, View } from 'react-native';

import { useNavigation } from '@react-navigation/native';

import Button from 'src/components/Button';
import Icon from 'src/components/Icon';
import OnboardingTabs from 'src/components/OnboardingTabs';
import ScreenLayout from 'src/components/ScreenLayout';

import { CustomizePickerContainer, Spring, styles, WeightPickersContainer } from './styles';
import { StackScreenProps } from '@react-navigation/stack';
import { RootStackParamList } from 'src/navigation/RootNavigator';
import { Picker } from '@react-native-picker/picker';

const MAX_WEIGHT_KG = 201;
const MIN_WEIGHT_KG = 35;
const INITIAL_WEIGHT_KG = 70;

const OnboardingWeightScreen = ({
  route,
}: StackScreenProps<RootStackParamList, 'OnboardingWeight'>) => {
  const [currentWeightKGrumms, setCurrentWeightKGramms] = useState<number>(INITIAL_WEIGHT_KG);
  const [currentWeightGramms, setCurrentWeightGramms] = useState<number>(0);
  const navigation = useNavigation();

  const onBack = () => {
    navigation.goBack();
  };

  return (
    <ScreenLayout style={styles.container}>
      <View>
        <OnboardingTabs activeValue={4} totalValues={7} style={styles.containerTabs} />
        <Text style={styles.textTitle}>Укажите ваш вес</Text>
        <Text style={styles.textSubTitle}>
          Это необходимо для того, чтобы наши алгоритмы правильно подобрали программу тренировок и
          питания под ваши особенности
        </Text>
        <WeightPickersContainer>
          <CustomizePickerContainer>
            <Picker
              selectedValue={currentWeightKGrumms}
              onValueChange={(itemValue) => setCurrentWeightKGramms(itemValue)}
              style={{
                width: '100%',
              }}
            >
              {Array.from({ length: MAX_WEIGHT_KG }, (_, i) => i)
                .slice(MIN_WEIGHT_KG)
                .map((value, index) => (
                  <Picker.Item key={index} label={`${value} кг`} value={value} />
                ))}
            </Picker>
          </CustomizePickerContainer>
          <Spring />
          <CustomizePickerContainer>
            <Picker
              selectedValue={currentWeightGramms}
              onValueChange={(itemValue) => setCurrentWeightGramms(itemValue)}
            >
              {Array.from({ length: 9 }, (_, i) => i).map((value, index) => (
                <Picker.Item key={index} label={`${value * 100} гр`} value={value} />
              ))}
            </Picker>
          </CustomizePickerContainer>
        </WeightPickersContainer>
      </View>
      <View style={styles.paddingHorizontal}>
        <Button
          uppercase
          content="Продолжить"
          onPress={() =>
            navigation.navigate('OnboardingHeight', {
              ...route.params,
              weight: (currentWeightKGrumms + currentWeightGramms / 10).toString(),
            })
          }
        />
        <TouchableOpacity style={styles.buttonBack} onPress={onBack}>
          <Icon name="arrowBack" size={12} color="primary" />
          <Text style={styles.textButtonBack}>Назад</Text>
        </TouchableOpacity>
      </View>
    </ScreenLayout>
  );
};

export default OnboardingWeightScreen;
