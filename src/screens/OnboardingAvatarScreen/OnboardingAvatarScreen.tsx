import React, { useState } from 'react';
import { Alert, Text, TouchableOpacity, View } from 'react-native';
import ImageCropPicker from 'react-native-image-crop-picker';
import useAsyncFn from 'react-use/lib/useAsyncFn';

import { useNavigation } from '@react-navigation/native';
import { StackScreenProps } from '@react-navigation/stack';

import { auth } from 'src/api';
import Button from 'src/components/Button';
import Icon from 'src/components/Icon';
import OnboardingTabs from 'src/components/OnboardingTabs';
import PreLoadedImage from 'src/components/PreLoadedImage';
import ScreenLayout from 'src/components/ScreenLayout';
import { LoaderModal } from 'src/components/ui/Loader/loading-indicator.modal';
import { RootStackParamList } from 'src/navigation/RootNavigator';

import { styles } from './styles';

const OnboardingAvatarScreen = ({
  route,
}: StackScreenProps<RootStackParamList, 'OnboardingAvatar'>) => {
  const navigation = useNavigation();

  const [image, setImage] = useState<{ path: string; base64: string | null | undefined }>({
    path: '',
    base64: null,
  });

  const [updateUserPhotoStatus, onUpdateUserPhoto] = useAsyncFn(async () => {
    if (image.base64 == null) return navigation.navigate('OnboardingUserSex', { ...route.params });
    return await auth
      .updateUserPhoto({ file_path: image.base64, access_token: route.params.access_token })
      .then(async () => {
        navigation.navigate('OnboardingUserSex', { ...route.params });
      })
      .catch(() => Alert.alert('Произошла ошибка'));
  }, [image]);

  const onBack = () => {
    navigation.goBack();
  };

  const onUploadPhoto = () => {
    ImageCropPicker.openPicker({
      width: 400,
      height: 400,
      cropping: true,
      mediaType: 'photo',
      includeBase64: true,
    })
      .then(({ data, path }) => setImage({ path, base64: data }))
      .catch((e) => {
        console.log('Error set avatar', e);
      });
  };

  return (
    <ScreenLayout>
      <LoaderModal visible={updateUserPhotoStatus.loading} />
      <View style={styles.container}>
        <View>
          <OnboardingTabs activeValue={2} totalValues={8} style={styles.containerTabs} />
          <Text style={styles.textTitle}>Загрузите Ваш аватар</Text>
          <Text style={styles.textSubTitle}>
            Вы можете загрузить собственный аватар или же пропустить данный шаг и загрузить
            изображение позже
          </Text>
          <TouchableOpacity onPress={onUploadPhoto} style={styles.containerUploadPhoto}>
            {image.base64 ? (
              <PreLoadedImage source={{ uri: image.path }} style={styles.imagePhoto} />
            ) : (
              <Icon name="+" />
            )}
          </TouchableOpacity>
          <View style={styles.containerIcon}>
            <Icon name="fingerUp" />
            <Text style={styles.textUnderIcon}>Нажмите, чтобы загрузить Ваше фото</Text>
          </View>
        </View>
        <View style={styles.paddingHorizontal}>
          <Button uppercase content="Продолжить" onPress={onUpdateUserPhoto} />
          <TouchableOpacity style={styles.buttonBack} onPress={onBack}>
            <Icon name="arrowBack" size={12} color="primary" />
            <Text style={styles.textButtonBack}>Назад</Text>
          </TouchableOpacity>
        </View>
      </View>
    </ScreenLayout>
  );
};

export default OnboardingAvatarScreen;
