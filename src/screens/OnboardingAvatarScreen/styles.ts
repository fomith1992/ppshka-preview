import { theme } from '@shared/config/theme';
import { StyleSheet } from 'react-native';

import { purpleColor } from 'src/commonStyles/colors';
import { font12m, font12r, font14r, font30m } from 'src/commonStyles/fonts';
import { getScaleSize } from 'src/utils/dimensions';

export const styles = StyleSheet.create({
  container: {
    paddingTop: getScaleSize(10),
    flexGrow: 1,
    justifyContent: 'space-between',
    paddingBottom: getScaleSize(70),
  },
  containerTabs: {
    marginHorizontal: getScaleSize(7),
    marginBottom: getScaleSize(81),
  },
  paddingHorizontal: {
    paddingHorizontal: getScaleSize(15),
  },
  textTitle: {
    ...font30m,
    lineHeight: getScaleSize(37),
    color: '#262626',
    marginBottom: getScaleSize(12),
    textAlign: 'center',
    marginHorizontal: getScaleSize(10),
  },
  imagePhoto: {
    width: '100%',
    height: '100%',
    borderRadius: getScaleSize(12),
  },
  containerUploadPhoto: {
    alignSelf: 'center',
    width: getScaleSize(130),
    height: getScaleSize(130),
    borderRadius: getScaleSize(12),
    borderWidth: 1,
    borderColor: theme.colors.gray5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textSubTitle: {
    ...font14r,
    lineHeight: getScaleSize(21),
    color: '#8C8C8C',
    marginBottom: getScaleSize(54),
    textAlign: 'center',
    marginHorizontal: getScaleSize(24),
  },
  containerIcon: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: getScaleSize(16),
  },
  textUnderIcon: {
    marginTop: getScaleSize(8),
    ...font12r,
    color: '#8C8C8C',
    width: getScaleSize(150),
    textAlign: 'center',
  },
  buttonBack: {
    paddingVertical: getScaleSize(18),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  textButtonBack: {
    ...font12m,
    color: purpleColor,
    marginLeft: getScaleSize(8),
  },
});
