import React, { useState } from 'react';
import { KeyboardAvoidingView, Text, TouchableOpacity, View } from 'react-native';
import { Picker } from '@react-native-picker/picker';
import { format } from 'date-fns';

import Button from 'src/components/Button';
import Icon from 'src/components/Icon';

import {
  BirthdayPickersContainer,
  Container,
  CustomizePickerContainer,
  PickerLabel,
  Spring,
  styles,
} from './set-birthday-screen.style';
import useAsyncFn from 'react-use/lib/useAsyncFn';
import { Bottom } from '@screens/recipes/ui/header.style';
import { getScaleSize } from 'src/utils/dimensions';
import { daysInMonth } from 'src/utils/date';
import { encodeMonth } from 'src/utils/i18n';

export const SetBirthdayScreen = (props: {
  currentDate: Date;
  hideScreen: () => void;
  setBirthday: (date: string) => void;
}) => {
  const initDate = new Date(2001, 0, 1);
  const [day, setDay] = useState<number>(props.currentDate.getDate() ?? initDate.getDate());
  const [month, setMonth] = useState<number>(props.currentDate.getMonth() ?? initDate.getMonth());
  const [year, setYear] = useState<number>(
    props.currentDate.getFullYear() ?? initDate.getFullYear(),
  );

  const [_, onChangeBirthday] = useAsyncFn(async () => {
    props.setBirthday(format(new Date(year, month, day), 'yyyy-MM-dd'));
  }, [day, month, year]);

  return (
    <KeyboardAvoidingView style={{ flex: 1 }} behavior="padding">
      <Container>
        <View>
          <Bottom mb={getScaleSize(81)} />
          <Text style={styles.textTitle}>Укажите дату {'\n'} рождения</Text>
          <Text style={styles.textSubTitle}>Пожалуйста, укажите вашу дату рождения</Text>
          <BirthdayPickersContainer>
            <PickerLabel>Месяц</PickerLabel>
            <PickerLabel>День</PickerLabel>
            <PickerLabel>Год</PickerLabel>
          </BirthdayPickersContainer>
          <BirthdayPickersContainer>
            <CustomizePickerContainer>
              <Picker selectedValue={month} onValueChange={(itemValue) => setMonth(itemValue)}>
                {Array.from({ length: 12 }, (_, i) => i).map((value, index) => (
                  <Picker.Item key={index} label={encodeMonth[value]} value={value} />
                ))}
              </Picker>
            </CustomizePickerContainer>
            <Spring />
            <CustomizePickerContainer>
              <Picker selectedValue={day} onValueChange={(itemValue) => setDay(itemValue)}>
                {Array.from({ length: daysInMonth(month + 1, year) + 1 }, (_, i) => i)
                  .slice(1)
                  .map((value, index) => (
                    <Picker.Item key={index} label={value.toString()} value={value} />
                  ))}
              </Picker>
            </CustomizePickerContainer>
            <Spring />
            <CustomizePickerContainer>
              <Picker
                selectedValue={year}
                onValueChange={(itemValue) => setYear(itemValue)}
                style={{
                  width: '100%',
                }}
              >
                {Array.from({ length: initDate.getFullYear() + 20 }, (_, i) => i)
                  .slice(1900)
                  .reverse()
                  .map((value, index) => (
                    <Picker.Item key={index} label={`${value} г`} value={value} />
                  ))}
              </Picker>
            </CustomizePickerContainer>
          </BirthdayPickersContainer>
        </View>
        <View style={styles.paddingHorizontal}>
          <Button uppercase disabled={false} content="Сохранить" onPress={onChangeBirthday} />
          <TouchableOpacity style={styles.buttonBack} onPress={props.hideScreen}>
            <Icon name="arrowBack" size={12} color="primary" />
            <Text style={styles.textButtonBack}>Назад</Text>
          </TouchableOpacity>
        </View>
      </Container>
    </KeyboardAvoidingView>
  );
};
