import { StyleSheet, Text, View } from 'react-native';
import { getScaleSize } from 'src/utils/dimensions';
import { font12m, font14r, font30m } from 'src/commonStyles/fonts';
import { purpleColor } from 'src/commonStyles/colors';
import { styled } from '@shared/config/styled';
import { container } from '@shared/config/view';
import { color } from '@shared/config/color';
import { font } from '@shared/config/text';

export const BirthdayPickersContainer = styled(View, {
  ...container('margin'),
  marginTop: getScaleSize(5),
  flexDirection: 'row',
});

export const CustomizePickerContainer = styled(View, {
  borderRadius: getScaleSize(10),
  borderColor: color('primary'),
  borderWidth: getScaleSize(2),
  flex: 1,
});

export const Spring = styled(View, {
  width: getScaleSize(10),
});

export const Container = styled(View, {
  paddingTop: getScaleSize(10),
  flexGrow: 1,
  justifyContent: 'space-between',
  paddingBottom: getScaleSize(70),
  backgroundColor: color('white'),
});

export const PickerLabel = styled(Text, {
  ...font({ type: 'text3' }),
  zIndex: 100,
  flex: 1,
  paddingLeft: getScaleSize(5),
});

export const styles = StyleSheet.create({
  paddingHorizontal: {
    paddingHorizontal: getScaleSize(15),
  },
  textTitle: {
    ...font30m,
    lineHeight: getScaleSize(37),
    color: '#262626',
    marginBottom: getScaleSize(12),
    textAlign: 'center',
    marginHorizontal: getScaleSize(10),
    zIndex: 100,
  },
  textSubTitle: {
    ...font14r,
    lineHeight: getScaleSize(21),
    color: '#8C8C8C',
    marginBottom: getScaleSize(40),
    marginHorizontal: getScaleSize(25),
    textAlign: 'center',
    zIndex: 100,
  },
  buttonBack: {
    paddingVertical: getScaleSize(18),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  textButtonBack: {
    ...font12m,
    color: purpleColor,
    marginLeft: getScaleSize(8),
  },
});
