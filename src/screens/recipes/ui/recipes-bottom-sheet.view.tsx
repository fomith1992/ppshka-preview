import BottomSheet from '@gorhom/bottom-sheet';
import { GradientLayoutView } from '@shared/ui/gradient-layout/gradient-layout.view';
import React, { RefObject, useState } from 'react';
import { View } from 'react-native';
import Button from 'src/components/Button';
import {
  BetweenButtonsSpace,
  Bottom,
  HeaderButton,
  ButtonsContainer,
  ButtonText,
  PopUpContainer,
  Row,
  Subtitle,
} from './header.style';

interface RecipesBottomFilterProps {
  filter: {
    time: number | null;
    complexity: number | null;
  };
  sheetRef: RefObject<BottomSheet>;
  setIsIncludeFilter: (data: { time: number | null; complexity: number | null }) => void;
}

export const RecipesBottomFilter = ({
  filter,
  sheetRef,
  setIsIncludeFilter,
}: RecipesBottomFilterProps): React.ReactElement => {
  const [cookingTime, setCookingTime] = useState<number | null>(filter.time);
  const [cookingComplexity, setCookingComplexity] = useState<number | null>(filter.complexity);
  return (
    <PopUpContainer>
      <Subtitle>Время приготовления:</Subtitle>
      <ButtonsContainer>
        <HeaderButton
          active={cookingTime === 0}
          onPress={() => setCookingTime(cookingTime === 0 ? null : 0)}
        >
          {cookingTime === 0 && <GradientLayoutView />}
          <ButtonText>⩽ 15 минут</ButtonText>
        </HeaderButton>
        <HeaderButton
          active={cookingTime === 1}
          onPress={() => setCookingTime(cookingTime === 1 ? null : 1)}
        >
          {cookingTime === 1 && <GradientLayoutView />}
          <ButtonText>15-45 минут</ButtonText>
        </HeaderButton>
        <HeaderButton
          active={cookingTime === 2}
          onPress={() => setCookingTime(cookingTime === 2 ? null : 2)}
        >
          {cookingTime === 2 && <GradientLayoutView />}
          <ButtonText>⩾ 45 минут</ButtonText>
        </HeaderButton>
      </ButtonsContainer>
      <Subtitle>Сложность:</Subtitle>
      <ButtonsContainer>
        <HeaderButton
          active={cookingComplexity === 0}
          onPress={() => setCookingComplexity(cookingComplexity === 0 ? null : 0)}
        >
          {cookingComplexity === 0 && <GradientLayoutView />}
          <ButtonText>Лёгкие</ButtonText>
        </HeaderButton>
        <HeaderButton
          active={cookingComplexity === 1}
          onPress={() => setCookingComplexity(cookingComplexity === 1 ? null : 1)}
        >
          {cookingComplexity === 1 && <GradientLayoutView />}
          <ButtonText>Средние</ButtonText>
        </HeaderButton>
        <HeaderButton
          active={cookingComplexity === 2}
          onPress={() => setCookingComplexity(cookingComplexity === 2 ? null : 2)}
        >
          {cookingComplexity === 2 && <GradientLayoutView />}
          <ButtonText>Сложные</ButtonText>
        </HeaderButton>
      </ButtonsContainer>
      <Bottom mb={16} />
      <Row>
        {(filter.time !== null || filter.complexity !== null) && (
          <View style={{ flex: 1 }}>
            <Button
              onPress={() => {
                setIsIncludeFilter({ time: null, complexity: null });
                sheetRef.current?.snapTo(0);
              }}
              size="S"
              content="Показать все рецепты"
              borderGradient
            />
          </View>
        )}
        <BetweenButtonsSpace />
        <View style={{ flex: 1 }}>
          <Button
            disabled={cookingTime == null && cookingComplexity == null}
            uppercase
            onPress={() => {
              setIsIncludeFilter({
                time: cookingTime,
                complexity: cookingComplexity,
              });
              sheetRef.current?.snapTo(0);
            }}
            size="S"
            content="Применить"
          />
        </View>
      </Row>
      <Bottom mb={35} />
    </PopUpContainer>
  );
};
