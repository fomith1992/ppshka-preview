import React, { useState } from 'react';
import { Alert, View } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';

import RecipeSmall from 'src/components/RecipeSmall';

import { RecipeTypeHeaderText, styles } from '../styles';
import useAsync from 'react-use/lib/useAsync';
import { TRecipe } from 'src/api/recipes';
import { useNavigation } from '@react-navigation/native';
import { recipesAPI } from 'src/api';
import { useSelector } from 'react-redux';
import { AppState } from 'src/store';
import { expectDefined } from 'src/utils/expect-defined';

export const PopularRecipesView = () => {
  const [receipesData, setReceipes] = useState<TRecipe[]>([]);
  const navigation = useNavigation();
  const access_token = useSelector((state: AppState) => state.session.tokens.accessToken);

  useAsync(async () => {
    await recipesAPI
      .getNewRecipes({ access_token })
      .then(async ({ data }) => {
        setReceipes(data.data.data);
      })
      .catch((e) => {
        console.log('Popular recipes: ', e);
        Alert.alert('Произошла ошибка запроса новых рецептов');
      });
  }, []);

  if (receipesData.length === 0) {
    return <></>;
  }

  return (
    <View style={styles.containerScroll}>
      <View style={[styles.containerPopularRecipes]}>
        <View style={styles.containerPopularTitle}>
          <RecipeTypeHeaderText>Новые рецепты</RecipeTypeHeaderText>
          {/* <TouchableOpacity>
            <Text style={styles.buttonPopular}>Подробнее</Text>
          </TouchableOpacity> */}
        </View>
        <ScrollView
          horizontal={true}
          showsHorizontalScrollIndicator={false}
          contentContainerStyle={styles.containerPopularRecipesScroll}
        >
          {receipesData.map((item, idx) => (
            <RecipeSmall
              key={idx}
              onPress={() =>
                navigation.navigate('RecipeFull', {
                  id: item.id,
                  date: undefined,
                  returnScreen: undefined,
                  category: expectDefined(item.category_array.find((x) => x.default)),
                })
              }
              photoUri={item.url_image}
              name={item.name}
              burn={`${item.ccal} Ккал`}
              style={styles.containerSmallRecipe}
              publish_date={item.publish_date}
            />
          ))}
        </ScrollView>
      </View>
    </View>
  );
};
