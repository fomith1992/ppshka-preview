import React from 'react';
import { TouchableOpacity, View } from 'react-native';

import FilterIcon from 'src/components/FilterIcon';
import MenuSlider from 'src/components/MenuSlider';
import SearchInput from 'src/components/SearchInput';

import { ContainerHeaderTop, HeaderImage, styles } from '../styles';
import { PopularRecipesView } from './pupular-recipes.view';
import { Title } from './header.style';

import avatar from '../../../shared/ui/assets/avatar.png';
import { useSelector } from 'react-redux';
import { AppState } from 'src/store';

interface HeaderProps {
  search: string;
  menuSelectedValue?: number;
  setSearch: (value: string) => void;
  setMenuSelectedValue: (value?: number) => void;
  setFilterVisible: () => void;
}

export const Header = ({
  search,
  menuSelectedValue,
  setFilterVisible,
  setSearch,
  setMenuSelectedValue,
}: HeaderProps) => {
  const user = useSelector((state: AppState) => state.session.user);

  return (
    <>
      <View style={styles.containerHeader}>
        <ContainerHeaderTop>
          <HeaderImage source={user.user_photo ? { uri: user.user_photo } : avatar} />
          <SearchInput
            value={search}
            onChangeText={(text: string) => setSearch(text)}
            height={40}
          />
          <TouchableOpacity onPress={() => setFilterVisible()}>
            <FilterIcon style={styles.filterIcon} />
          </TouchableOpacity>
        </ContainerHeaderTop>
        <Title>Рецепты</Title>
        <MenuSlider selectedValue={menuSelectedValue} setSelectedValue={setMenuSelectedValue} />
      </View>
      <PopularRecipesView />
    </>
  );
};
