import { color } from '@shared/config/color';
import { styled } from '@shared/config/styled';
import { font } from '@shared/config/text';
import { container } from '@shared/config/view';
import { Text, TouchableOpacity } from 'react-native';
import { View } from 'react-native';
import { font14m, font16m } from 'src/commonStyles/fonts';
import { getScaleSize, screenWidth, statusBarHeight } from 'src/utils/dimensions';

export const PopUpContainer = styled(View, {
  ...container('padding'),
  paddingBottom: getScaleSize(60) + statusBarHeight,
  backgroundColor: color('white'),
});

export const TitleContainer = styled(View, {
  flexDirection: 'row',
  justifyContent: 'space-between',
  alignItems: 'center',
  marginBottom: getScaleSize(12),
});

export const ButtonsContainer = styled(View, {
  flexDirection: 'row',
  justifyContent: 'space-between',
  alignItems: 'center',
  marginBottom: getScaleSize(12),
  marginTop: getScaleSize(12),
});

export const Row = styled(
  View,
  (props: {
    flexDirection?: boolean;
    alignItems?: boolean;
    spaceBeetween?: boolean;
    ph?: number;
  }) =>
    ({
      flexDirection: 'row',
      paddingHorizontal: props.ph && getScaleSize(props.ph),
      alignItems: props.alignItems ? 'center' : undefined,
      justifyContent: props.flexDirection
        ? 'center'
        : props.spaceBeetween
        ? 'space-between'
        : undefined,
    } as const),
);

export const Bottom = styled(
  View,
  (props: { mb?: number }) =>
    ({
      width: '100%',
      marginBottom: props.mb ? getScaleSize(props.mb) : undefined,
    } as const),
);

export const BetweenButtonsSpace = styled(View, {
  height: getScaleSize(1),
  width: getScaleSize(12),
});

export const Title = styled(Text, {
  ...container(),
  ...font({ type: 'h1' }),
  color: color('white'),
  marginBottom: getScaleSize(20),
});

export const Subtitle = styled(Text, {
  ...font16m,
  color: color('gray8'),
  marginTop: getScaleSize(12),
});

export const ButtonText = styled(Text, {
  ...font14m,
  color: color('white'),
  margin: getScaleSize(8),
});

export const FilterTextButton = styled(TouchableOpacity, {
  marginRight: getScaleSize(15),
});

export const HeaderButton = styled(
  TouchableOpacity,
  (props: { active: boolean }) =>
    ({
      width: screenWidth / 4,
      backgroundColor: color(!props.active ? 'gray5' : 'primary'),
      justifyContent: 'center',
      alignItems: 'center',
      borderRadius: getScaleSize(5),
      overflow: 'hidden',
    } as const),
);

export const SubmitButton = styled(
  TouchableOpacity,
  (props: { error?: boolean }) =>
    ({
      flex: 1,
      flexDirection: 'row',
      height: getScaleSize(50),
      justifyContent: 'center',
      alignItems: 'center',
      borderRadius: getScaleSize(8),
      backgroundColor: color(props.error ? 'secondary' : 'primary'),
    } as const),
);

export const FiterTextButtonText = styled(Text, {
  ...font({ type: 'text2' }),
  color: color('gray6'),
  fontSize: getScaleSize(13),
});

export const EmptyComponentText = styled(Text, {
  ...font14m,
  color: color('gray8'),
  fontSize: getScaleSize(16),
});

export const HeaderLightText = styled(Text, {
  ...font({ type: 'text1', weight: 'normal' }),
  color: color('white'),
  /* fontSize: getScaleSize(16), */
  textAlign: 'center',
  marginHorizontal: getScaleSize(28),
  marginBottom: getScaleSize(24),
});

export const EmptyComponentContainer = styled(View, {
  flex: 1,
  justifyContent: 'center',
  alignItems: 'center',
});
