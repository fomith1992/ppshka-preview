import React from 'react';
import { StackScreenProps } from '@react-navigation/stack';
import { MealPlanParamList } from 'src/navigation/meal-plan-navigator/meal-plan-navigator';
import { Recipes } from './recipes.container';
import ScreenLayout from 'src/components/ScreenLayout';
import { LogBox } from 'react-native';

export type ReturnScreenRecipes = 'Dashboard' | 'MealPlan';

export const RecipesScreen = ({
  route: { params },
}: StackScreenProps<MealPlanParamList, 'SelectMeel'>) => {
  LogBox.ignoreLogs(['Non-serializable values were found in the navigation state']);
  return (
    <ScreenLayout edges={['left', 'right']}>
      <Recipes type={params.type} date={params.date} returnScreen={params.returnScreen} />
    </ScreenLayout>
  );
};
