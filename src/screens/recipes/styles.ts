import { color } from '@shared/config/color';
import { styled } from '@shared/config/styled';
import { font } from '@shared/config/text';
import { container } from '@shared/config/view';
import { Image, StyleSheet, Text, View } from 'react-native';

import { font14m, font14r, font20m } from 'src/commonStyles/fonts';

import { bottomTabsHeight, getScaleSize, statusBarHeight } from '../../utils/dimensions';
import { Row } from './ui/header.style';

export const RecipeContainer = styled(
  View,
  (props: { negativeIndentation: boolean }) =>
    ({
      ...container(),
      marginTop: props.negativeIndentation ? -getScaleSize(50) : undefined,
      marginBottom: getScaleSize(props.negativeIndentation ? 70 : 20),
    } as const),
);

export const ContainerHeaderTop = styled(View, {
  ...container(),
  alignItems: 'center',
  flexDirection: 'row',
  marginBottom: getScaleSize(25),
});

export const HeaderImage = styled(Image, {
  marginRight: getScaleSize(15),
  width: getScaleSize(40),
  height: getScaleSize(40),
  borderRadius: getScaleSize(8),
});

export const Title = styled(Text, {
  ...font({ type: 'h1' }),
  marginBottom: getScaleSize(15),
});

export const RecipeTypeHeader = styled(View, {
  marginBottom: getScaleSize(15),
  paddingHorizontal: getScaleSize(15),
});

export const RecipeTypeHeaderText = styled(Text, {
  ...font({ type: 'h2', weight: 'normal' }),
  color: color('white'),
});

export const HeaderBottomRow = styled(Row, {
  marginBottom: getScaleSize(15),
});

export const styles = StyleSheet.create({
  container: {
    backgroundColor: '#F5F5F5',
  },
  containerHeader: {
    paddingTop: statusBarHeight + getScaleSize(6),
  },
  containerHeaderTop: {},
  containerScroll: {
    paddingTop: getScaleSize(25),
    height: getScaleSize(150),
  },
  containerRecipe: {},
  profileIcon: {},
  filterIcon: {
    marginLeft: getScaleSize(15),
    width: getScaleSize(40),
    height: getScaleSize(40),
    borderRadius: getScaleSize(8),
  },
  containerPopularRecipes: {
    marginBottom: getScaleSize(25),
  },
  containerPopularTitle: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: getScaleSize(15),
    paddingHorizontal: getScaleSize(15),
  },
  containerPopularRecipesScroll: {
    paddingHorizontal: getScaleSize(10),
  },
  buttonPopular: {
    ...font14m,
    color: '#BFBFBF',
  },
  containerSmallRecipe: {
    marginHorizontal: getScaleSize(5),
  },
  tabsBottomSpace: {
    flexGrow: 1,
    paddingBottom: getScaleSize(15) + bottomTabsHeight,
  },
  textSecondTitle: {
    ...font20m,
    color: '#262626',
    marginHorizontal: getScaleSize(15),
  },
  textDescription: {
    ...font14r,
    lineHeight: getScaleSize(22),
    color: '#8C8C8C',
    marginHorizontal: getScaleSize(15),
  },
});
