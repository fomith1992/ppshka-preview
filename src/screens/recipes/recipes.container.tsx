import React, { useEffect, useRef, useState } from 'react';
import { Alert, FlatList, ActivityIndicator, View, Text, Platform } from 'react-native';

import { ViewToken } from '@shared/@types';
import { RecipeCard } from 'src/components/recipe-card/recipe-card.container';
import {
  HeaderBottomRow,
  RecipeContainer,
  RecipeTypeHeader,
  RecipeTypeHeaderText,
  styles,
} from './styles';
import { Header } from './ui/header.view';
import { useSelector } from 'react-redux';
import { AppState } from 'src/store';
import useAsync from 'react-use/lib/useAsync';
import { meelAPI, recipesAPI } from 'src/api';
import { LoaderModal } from 'src/components/ui/Loader/loading-indicator.modal';
import { TRecipe } from 'src/api/recipes';
import { favoritesAPI } from 'src/utils/storage';
import {
  Bottom,
  EmptyComponentContainer,
  EmptyComponentText,
  FilterTextButton,
  FiterTextButtonText,
  HeaderLightText,
  Row,
} from './ui/header.style';
import { ReturnScreenRecipes } from './recipes.screen';
import { GradientLayoutView } from '@shared/ui/gradient-layout/gradient-layout.view';
import ScreenLayout from 'src/components/ScreenLayout';
import { theme } from '@shared/config/theme';
import { format } from 'date-fns';
import { ru } from 'date-fns/locale';
import { hitSlopParams } from 'src/utils/hit-slop-params';
import { Spring } from 'src/components/post-card/post-card.style';
import { BottomSheetGorhom } from 'src/components/bottom-sheet/bottom-sheet';
import BottomSheet from '@gorhom/bottom-sheet';
import { RecipesBottomFilter } from './ui/recipes-bottom-sheet.view';
import { ChapterDescription } from 'src/components/chapter-description/chapter-description.view';
import { useDebouncedValue } from 'src/utils/hooks/use-debounced-value';
import useAsyncFn from 'react-use/lib/useAsyncFn';
import { RecipesBottomSheetView } from '@screens/recipe-full/bottom-sheet/recipes-bottom-sheet.view';
import { PopUpWindow } from 'src/components/pop-up-window/pop-up-window.view';
import Button from 'src/components/Button';
import { GradientLayoutHeaderView } from '@shared/ui/gradient-layout/gradient-layout-header.view';
import { expectDefined } from 'src/utils/expect-defined';

type TOnViewChanged = {
  viewableItems: Array<ViewToken<{ id: string }>>;
  changed: Array<ViewToken<{ id: string }>>;
};

interface RecipesProps {
  type?: number;
  date?: Date;
  returnScreen?: ReturnScreenRecipes;
}

export const Recipes = ({ type, date, returnScreen }: RecipesProps) => {
  const faves = useSelector((state: AppState) => state.ui.favorites);
  const categories = useSelector((state: AppState) => state.session.categories);
  const [activeItemId, setActiveItemId] = useState<string | null>(null);
  const access_token = useSelector((state: AppState) => state.session.tokens.accessToken);
  const sheetRefFilter = useRef<BottomSheet>(null);

  const [checkedRecipeId, setRecipeId] = useState<{
    recipe_id: number;
    category: number;
  } | null>(null);
  const sheetRefAddToPlan = useRef<BottomSheet>(null);

  const [serverCurrentPage, setServerCurrentPage] = useState(0);
  const [pagesCount, setPagesCount] = useState(1);

  const [isLoadNextPage, setIsLoadNextPage] = useState<boolean>(true);
  const [loading, setLoading] = useState<boolean>(false);

  const [recipesData, setRecipesData] = useState<TRecipe[]>([]);
  const [modalVisible, setModalVisible] = useState<boolean>(false);

  // header
  const [search, setSearch] = useState<string>('');
  const [menuSelectedValue, setMenuSelectedValue] = useState<number | undefined>(type);
  const [filter, setIsIncludeFilter] = useState<{
    time: number | null;
    complexity: number | null;
  }>({ complexity: null, time: null });

  const effectiveQuery = useDebouncedValue(search, 500);

  useEffect(() => {
    setServerCurrentPage(0);
    setIsLoadNextPage(true);
  }, [effectiveQuery]);

  const recipesApi = useAsync(async () => {
    if (isLoadNextPage && !loading) {
      setLoading(true);
      if (menuSelectedValue !== 99) {
        return await recipesAPI
          .getRecipes({
            page: serverCurrentPage + 1,
            access_token,
            filter,
            menuSelectedValue,
            search: effectiveQuery,
          })
          .then(async ({ data: { data } }) => {
            if (data.current_page === 1 || !data.current_page) setRecipesData(data.data);
            if (data.current_page > 1) setRecipesData(recipesData.concat(data.data));
            setIsLoadNextPage(false);
            setPagesCount(data.page_cnt);
            setServerCurrentPage(data.current_page ?? 1);
            await favoritesAPI.setPopularReceipes(data.fave_list);
          })
          .catch((e) => {
            console.log('Recipes: ', e);
            Alert.alert('Произошла ошибка');
          });
      } else {
        const favesData = await Promise.all(
          faves.map(
            async (item) =>
              await (
                await recipesAPI.getRecipe({ access_token, id: item })
              ).data.data.data,
          ),
        );
        setRecipesData(favesData);
      }
    }
    setLoading(false);
  }, [filter, menuSelectedValue, serverCurrentPage, isLoadNextPage, effectiveQuery]);

  const onViewRef = React.useRef(({ viewableItems }: TOnViewChanged) => {
    const item = viewableItems[viewableItems.length - 1];
    if (item?.item?.id) setActiveItemId(item.item.id);
  });

  const [_x, addToRecipesPlan] = useAsyncFn(
    async (data: { date: Date; recipe_id: number; category: number }) => {
      if (data.date != null) {
        return await meelAPI
          .addToMealPlan({
            access_token,
            recipe_id: data.recipe_id,
            date: format(data.date ?? new Date(), 'y-MM-dd'),
            category_id: data.category,
          })
          .then(async () => {
            setModalVisible(true);
          })
          .catch(() => Alert.alert('Произошла ошибка', 'Ошибка добавления рецепта в план питания'));
      }
    },
    [date, returnScreen],
  );

  const [_y, checkMealPlan] = useAsyncFn(
    async (data: { date: Date; recipe_id: number; category: number }) => {
      return await meelAPI
        .checkMealData({
          access_token,
          date: format(data.date, 'y-MM-dd'),
          category_id: data.category,
        })
        .then(async (response) => {
          if (response.data.data.exist) {
            Alert.alert(
              'Предупреждение!',
              'Вы уверены, что хотите заменить ранее выбранный рецепт?',
              [
                {
                  text: 'Отмена',
                  onPress: () => console.log('close window'),
                },
                {
                  text: 'Да',
                  onPress: () => {
                    addToRecipesPlan({
                      date: data.date,
                      recipe_id: data.recipe_id,
                      category: data.category,
                    });
                  },
                },
              ],
              { cancelable: false },
            );
          } else {
            addToRecipesPlan({
              date: data.date,
              recipe_id: data.recipe_id,
              category: data.category,
            });
          }
        })
        .catch((e) => {
          console.log(e);
          Alert.alert(
            'Произошла ошибка',
            'Произошла ошибка проверки наличия добавленого рецепта на выбранный день',
          );
        });
    },
    [date, returnScreen, menuSelectedValue],
  );

  const _setFilterVisible = () => sheetRefFilter.current?.snapTo(1);
  const _onEndReached = () =>
    pagesCount > serverCurrentPage ? setIsLoadNextPage(true) : undefined;
  const _keyExtractor = (item: TRecipe) => item.id.toString();
  const _renderItem = (data: { item: TRecipe }) => {
    return (
      <RecipeContainer key={data.item.id} negativeIndentation={!!returnScreen}>
        <RecipeCard
          category={expectDefined(categories.find((x) => x.id === menuSelectedValue))}
          openContextMenu={({ recipe_id, category }) => {
            setRecipeId({ recipe_id, category });
            sheetRefAddToPlan.current?.snapTo(1);
          }}
          data={data.item}
          fallback={activeItemId !== data.item.id.toString()}
          date={date}
          returnScreen={returnScreen}
        />
      </RecipeContainer>
    );
  };

  return (
    <ScreenLayout safeArea edges={['left', 'right']}>
      {!returnScreen && <GradientLayoutView />}
      {search.length === 0 && <LoaderModal visible={recipesApi.loading && !serverCurrentPage} />}
      <FlatList
        data={recipesData}
        renderItem={_renderItem}
        keyExtractor={_keyExtractor}
        onEndReached={_onEndReached}
        contentContainerStyle={styles.tabsBottomSpace}
        onViewableItemsChanged={onViewRef.current}
        viewabilityConfig={{ itemVisiblePercentThreshold: 85 }}
        ListHeaderComponent={
          returnScreen ? (
            <GradientLayoutHeaderView>
              <HeaderLightText>
                Выберите {categories.find((x) => x.id === type)?.name.toLowerCase()}, который{'\n'}
                вы хотите добавить в план{'\n'}питания на{' '}
                {date && format(date, 'd MMMM', { locale: ru })}
              </HeaderLightText>
              <HeaderBottomRow alignItems>
                <RecipeTypeHeader>
                  <RecipeTypeHeaderText>
                    {categories.find((x) => x.id === type)?.name}
                  </RecipeTypeHeaderText>
                </RecipeTypeHeader>
                <Spring />
                <FilterTextButton hitSlop={hitSlopParams('L')} onPress={_setFilterVisible}>
                  <FiterTextButtonText>Фильтры</FiterTextButtonText>
                </FilterTextButton>
              </HeaderBottomRow>
            </GradientLayoutHeaderView>
          ) : (
            <View>
              <Header
                search={search}
                setSearch={setSearch}
                menuSelectedValue={menuSelectedValue}
                setMenuSelectedValue={(x) => {
                  setServerCurrentPage(0);
                  setMenuSelectedValue(x);
                  setIsLoadNextPage(true);
                }}
                setFilterVisible={_setFilterVisible}
              />
              <Row alignItems>
                <RecipeTypeHeader>
                  <Bottom mb={15} />
                  <RecipeTypeHeaderText>Все рецепты</RecipeTypeHeaderText>
                </RecipeTypeHeader>
                <Spring />
                <FilterTextButton hitSlop={hitSlopParams('L')} onPress={_setFilterVisible}>
                  <FiterTextButtonText>Фильтры</FiterTextButtonText>
                </FilterTextButton>
              </Row>
              <Bottom mb={15} />
            </View>
          )
        }
        showsVerticalScrollIndicator={false}
        ListFooterComponent={
          recipesApi.loading && recipesData.length !== 0 ? (
            <ActivityIndicator size="large" color={theme.colors.black} />
          ) : (
            <></>
          )
        }
        ListEmptyComponent={
          !recipesApi.loading ? (
            <EmptyComponentContainer>
              <EmptyComponentText>Ничего не найдено</EmptyComponentText>
            </EmptyComponentContainer>
          ) : (
            <></>
          )
        }
        initialNumToRender={3}
        windowSize={3}
        removeClippedSubviews={Platform.OS === 'android'}
        maxToRenderPerBatch={10}
        updateCellsBatchingPeriod={50}
      />
      <ChapterDescription chapter="recipe" />
      <BottomSheetGorhom sheetRef={sheetRefFilter}>
        <RecipesBottomFilter
          filter={filter}
          setIsIncludeFilter={(x) => {
            setServerCurrentPage(0);
            setIsIncludeFilter(x);
            setIsLoadNextPage(true);
          }}
          sheetRef={sheetRefFilter}
        />
      </BottomSheetGorhom>
      <BottomSheetGorhom sheetRef={sheetRefAddToPlan}>
        <RecipesBottomSheetView
          onSubmit={(date) => {
            checkedRecipeId && checkMealPlan({ date, ...checkedRecipeId });
            sheetRefAddToPlan.current?.close();
          }}
        />
      </BottomSheetGorhom>
      <PopUpWindow modalVisible={modalVisible}>
        <View>
          <Text style={{ ...styles.textSecondTitle, textAlign: 'center' }}>
            Рецепт успешно добавлен
          </Text>
          <Text style={{ ...styles.textDescription, textAlign: 'center' }}>
            Для продолжения нажмите кнопку "Продолжить"
          </Text>
          <Bottom mb={24} />
          <Button uppercase onPress={() => setModalVisible(false)} content="Продолжить" />
        </View>
      </PopUpWindow>
    </ScreenLayout>
  );
};
