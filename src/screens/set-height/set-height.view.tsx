import React, { useState } from 'react';
import { Picker } from '@react-native-picker/picker';

import Button from 'src/components/Button';
import Icon from 'src/components/Icon';
import ScreenLayout from 'src/components/ScreenLayout';

import {
  BottomContainer,
  ButtonBack,
  CustomizePickerContainer,
  Spring,
  styles,
  TextButtonBack,
  TextSubTitle,
  TextTitle,
  WeightContainer,
  WeightPickersContainer,
} from './set-height.style';
import { LoaderModal } from 'src/components/ui/Loader/loading-indicator.modal';

const MAX_WEIGHT_KG = 220;
const MIN_WEIGHT_KG = 90;
const INITIAL_WEIGHT_KG = 160;

interface SetWeightWeightViewProps {
  loading: boolean;
  textButton: string;
  onPressSaveWidthButton: (value: string) => void;
  onPressBackButton: () => void;
  title: string;
  description: string;
}

export const SetWeightHeightView = ({
  loading,
  textButton,
  onPressSaveWidthButton,
  onPressBackButton,
  title,
  description,
}: SetWeightWeightViewProps) => {
  const [currentWeightKGrumms, setCurrentWeightKGramms] = useState<number>(INITIAL_WEIGHT_KG);
  const [currentWeightGramms, setCurrentWeightGramms] = useState<number>(0);
  return (
    <ScreenLayout style={styles.container}>
      <LoaderModal visible={loading} />
      <WeightContainer>
        <TextTitle>{title}</TextTitle>
        <TextSubTitle>{description}</TextSubTitle>
        <WeightPickersContainer>
          <CustomizePickerContainer>
            <Picker
              selectedValue={currentWeightKGrumms}
              onValueChange={(itemValue) => setCurrentWeightKGramms(itemValue)}
              style={{
                width: '100%',
              }}
            >
              {Array.from({ length: MAX_WEIGHT_KG }, (_, i) => i)
                .slice(MIN_WEIGHT_KG)
                .map((value, index) => (
                  <Picker.Item key={index} label={`${value} см`} value={value} />
                ))}
            </Picker>
          </CustomizePickerContainer>
          <Spring />
          <CustomizePickerContainer>
            <Picker
              selectedValue={currentWeightGramms}
              onValueChange={(itemValue) => setCurrentWeightGramms(itemValue)}
            >
              {Array.from({ length: 10 }, (_, i) => i).map((value, index) => (
                <Picker.Item key={index} label={`${value * 1} мм`} value={value} />
              ))}
            </Picker>
          </CustomizePickerContainer>
        </WeightPickersContainer>
      </WeightContainer>
      <BottomContainer>
        <Button
          uppercase
          content={textButton}
          onPress={() =>
            onPressSaveWidthButton((currentWeightKGrumms + currentWeightGramms / 10).toString())
          }
        />
        <ButtonBack onPress={onPressBackButton}>
          <Icon name="arrowBack" size={12} color="primary" />
          <TextButtonBack>Назад</TextButtonBack>
        </ButtonBack>
      </BottomContainer>
    </ScreenLayout>
  );
};
