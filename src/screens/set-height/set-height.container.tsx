import React from 'react';
import { Alert } from 'react-native';

import { CommonActions, useNavigation } from '@react-navigation/native';

import useAsyncFn from 'react-use/lib/useAsyncFn';
import { auth } from 'src/api';
import { useSelector } from 'react-redux';
import { AppState } from 'src/store';
import { updateWeightEvent } from 'src/firebase/analytics/analytics-user/update-weight';
import { SetWeightHeightView } from './set-height.view';

export const SetHeightScreen = () => {
  const access_token = useSelector((state: AppState) => state.session.tokens.accessToken);
  const userId = useSelector((state: AppState) => state.session.user.id);

  const navigation = useNavigation();

  const [changeWeightState, onChangeWeight] = useAsyncFn(async (value: string) => {
    return await auth
      .registerSetUserParams({
        access_token,
        height: value,
      })
      .then(async () => {
        updateWeightEvent(userId);
        navigation.dispatch(
          CommonActions.reset({
            index: 0,
            routes: [
              {
                name: 'TabNavigator',
                params: {
                  screen: 'Profile',
                  initial: false,
                  params: {
                    initial: false,
                    screen: 'Profile',
                    params: {},
                  },
                },
              },
            ],
          }),
        );
      })
      .catch((err) => {
        console.log('onChangeWeight err: ', err);
        Alert.alert('Произошла ошибка');
      });
  }, []);

  return (
    <SetWeightHeightView
      title="Укажите ваш рост"
      description="Это необходимо для того, чтобы наши алгоритмы правильно подобрали программу тренировок и питания под ваши особенности"
      loading={changeWeightState.loading}
      textButton={'Изменить рост'}
      onPressSaveWidthButton={onChangeWeight}
      onPressBackButton={navigation.goBack}
    />
  );
};
