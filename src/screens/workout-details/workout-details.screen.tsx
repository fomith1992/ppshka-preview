import React, { useRef } from 'react';

import { useNavigation } from '@react-navigation/native';

import Icon from 'src/components/Icon';
import ScreenLayout from 'src/components/ScreenLayout';

import {
  BackButtonContainer,
  BottomContainer,
  BottomDescription,
  BottomSpring,
  BubbleContainer,
  BubbleInfoRow,
  BubbleInfoRowTitle,
  BubbleInfoRowValue,
  BubbleInfoTitle,
  ButtonContainer,
  ContentContainer,
  Delimeter,
  DescriptionMain,
  LinkButtonContainer,
  LinkButtonText,
  // FavoriteButtonContainer,
  ScrollContainer,
  styles,
  // SubTitleMain,
  TabsInfoContainer,
  TitleMain,
  /* VideoDemo, */
  VideoDemoContainer,
} from './styles';
import Button from 'src/components/Button';
import { StackScreenProps } from '@react-navigation/stack';
import { WorkoutStackParamList } from 'src/navigation/WorkoutNavigator/WorkoutNavigator';
import useAsync from 'react-use/lib/useAsync';
import { userAPI, workoutAPI } from 'src/api';
import { useSelector } from 'react-redux';
import { AppState } from 'src/store';
import { Alert, Platform } from 'react-native';
import { LoadingIndicator } from 'src/components/ui/Loader/loading-indicator';
import { TabInfo } from 'src/components/tabs/info-tab.view copy';
import { TabComplexity } from 'src/components/tabs/complexity-tab.view';
import useAsyncFn from 'react-use/lib/useAsyncFn';
import { Bottom } from '@screens/recipes/ui/header.style';
import { BottomSheetGorhom } from 'src/components/bottom-sheet/bottom-sheet';
import BottomSheet from '@gorhom/bottom-sheet';
import { VideoPlayer } from 'src/components/video-player';
import { theme } from '@shared/config/theme';
import { getCacheContent } from 'src/utils/cache-content';

interface BubleInfoProps {
  title: string;
  data: Array<{
    title: string;
    value: string;
  }>;
}

function BubleInfo({ title, data }: BubleInfoProps) {
  return (
    <BubbleContainer>
      <BubbleInfoTitle>{title}</BubbleInfoTitle>
      <Delimeter />
      {data.map((value) => (
        <BubbleInfoRow key={`key-${value.title}-${value.value}`}>
          <BubbleInfoRowTitle>{value.title}</BubbleInfoRowTitle>
          <BubbleInfoRowValue>{value.value}</BubbleInfoRowValue>
        </BubbleInfoRow>
      ))}
    </BubbleContainer>
  );
}

const WorkoutDetailsScreen = ({
  route,
}: StackScreenProps<WorkoutStackParamList, 'WorkoutDetails'>) => {
  const { id: workoutId, dateStart: workoutDateStart, calendar_id } = route.params;
  const navigation = useNavigation();
  const sheetRef = useRef<BottomSheet>(null);
  const access_token = useSelector((state: AppState) => state.session.tokens.accessToken);
  const workoutApi = useAsync(async () => {
    return await workoutAPI
      .getWorkoutById({ id: workoutId, access_token })
      .then(async (data) => {
        return data;
      })
      .catch((e) => {
        console.log(e);
        Alert.alert('Произошла ошибка');
      });
  }, [workoutId]);
  function handlePressStartWorkoutButton(): void {
    navigation.navigate('WorkoutVideoView', { id: workoutId, dateStart: workoutDateStart });
  }
  const [_, userPostponementWorkout] = useAsyncFn(
    async (rebuild: boolean) => {
      return await userAPI
        .userPostponementWorkout(access_token, calendar_id, rebuild)
        .then((data) => {
          if (data.data.status === 'ok') {
            navigation.goBack();
          } else {
            Alert.alert('Ошибка переноса тренировки!');
          }
        })
        .catch(console.log);
    },
    [calendar_id],
  );
  function handlePostponementWorkout(): void {
    sheetRef.current?.snapTo(1);
    // Alert.alert('Внимание', 'Перенести 1 тренировку, или перестроить календарь тренировок?', [
    //   {
    //     style: 'cancel',
    //     text: 'Отмена',
    //   },
    //   {
    //     style: 'default',
    //     text: 'Перенести тренировку С перестроением графика тренировок',
    //     onPress: () => userPostponementWorkout(true),
    //   },
    //   {
    //     style: 'default',
    //     text: 'Перенести тренировку БЕЗ перестроения графика тренировок',
    //     onPress: () => userPostponementWorkout(false),
    //   },
    // ]);
  }

  const filePath = useAsync(async () => {
    return await getCacheContent({
      url: workoutApi.value?.data.data.preview_file_path,
      format: 'mp4',
    });
  }, [workoutApi.value?.data.data]);

  if (workoutApi.loading || workoutApi.value == null) return <LoadingIndicator visible />;
  const posterUri = workoutApi.value.data.data.url_image;
  const name = workoutApi.value.data.data.name;
  const description = workoutApi.value.data.data.description;
  const duration = workoutApi.value.data.data.duration;
  const inventory = workoutApi.value.data.data.inventory;
  const complexity = workoutApi.value.data.data.complexity;

  return (
    <ScreenLayout safeArea={false}>
      <ScrollContainer bounces={false} showsVerticalScrollIndicator={false}>
        <VideoDemoContainer>
          {/* <VideoDemo
            source={{
              uri: videoUri,
            }}
          /> */}
          <VideoPlayer
            source={{ uri: filePath.value }}
            poster={!filePath.loading || filePath.value ? undefined : posterUri}
            fullscreen={true}
            posterResizeMode="cover"
            resizeMode="cover"
            repeat={true}
            paused={false}
            disableBack={true}
            disableFullscreen={true}
            disableTimer={true}
            disablePlayPause={true}
            disableVolume={true}
            disableSeekbar={true}
            tapAnywhereToPause={true}
            seekColor={theme.colors.primary}
            style={styles.video}
          />
          <BackButtonContainer onPress={() => navigation.goBack()}>
            <Icon name="arrowBack" />
          </BackButtonContainer>
          {/* <FavoriteButtonContainer>
            <Icon name="favorites" color="gray9" size={16} />
          </FavoriteButtonContainer> */}
        </VideoDemoContainer>
        <ContentContainer>
          <BubbleContainer>
            <TitleMain>{name}</TitleMain>
            {/* <SubTitleMain>Упражнение для ног</SubTitleMain> */}
            <DescriptionMain>{description}</DescriptionMain>
            <ButtonContainer>
              <Button
                uppercase
                onPress={handlePressStartWorkoutButton}
                content="Приступить к тренировке"
              />
            </ButtonContainer>
            <LinkButtonContainer onPress={handlePostponementWorkout}>
              <LinkButtonText>Перенести тренировку</LinkButtonText>
            </LinkButtonContainer>
            <Delimeter />
            <TabsInfoContainer>
              <TabInfo title={`${duration} мин`} description="Время тренировки" />
              {/* <TabInfo title="450 ккал" description="Потраченных ккал" /> */}
              <TabComplexity current={complexity} total={7} description="Сложность упражнения" />
            </TabsInfoContainer>
          </BubbleContainer>
          <BubleInfo
            title="Необходимый инвентарь"
            data={inventory.map((item, _idx) => ({
              title: item.name,
              value: `${item.cnt} шт.`,
            }))}
          />
        </ContentContainer>
        <BottomSpring />
      </ScrollContainer>
      <BottomSheetGorhom sheetRef={sheetRef}>
        <BottomContainer>
          <BottomDescription>
            Перенести 1 тренировку, или перестроить календарь тренировок?
          </BottomDescription>
          <Bottom mb={24} />
          <Button
            uppercase={false}
            onPress={() => {
              userPostponementWorkout(true);
              sheetRef.current?.close();
            }}
            content="Перенести тренировку С перестроением графика тренировок"
          />
          <Bottom mb={12} />
          <Button
            uppercase={false}
            onPress={() => {
              userPostponementWorkout(false);
              sheetRef.current?.close();
            }}
            content="Перенести тренировку БЕЗ перестроения графика тренировок"
          />
          <Bottom mb={Platform.OS === 'ios' ? 70 : 0} />
        </BottomContainer>
      </BottomSheetGorhom>
    </ScreenLayout>
  );
};

export default WorkoutDetailsScreen;
