import { color } from '@shared/config/color';
import { styled } from '@shared/config/styled';
import { font } from '@shared/config/text';
import { container } from '@shared/config/view';
import { ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';

import PreLoadedImage from 'src/components/PreLoadedImage';
import { bottomTabsHeight, getScaleSize, screenWidth } from 'src/utils/dimensions';

export const ScrollContainer = styled(ScrollView, {
  flexGrow: 1,
});

export const VideoDemoContainer = View;
export const VideoDemo = styled(PreLoadedImage, {
  height: getScaleSize(222),
});

export const ButtonHeaderCommon = styled(TouchableOpacity, {
  position: 'absolute',
  width: getScaleSize(30),
  height: getScaleSize(30),
  borderRadius: getScaleSize(8),
  backgroundColor: color('white'),
  justifyContent: 'center',
  alignItems: 'center',
});

export const BackButtonContainer = styled(ButtonHeaderCommon, {
  top: getScaleSize(50),
  left: getScaleSize(15),
});

export const FavoriteButtonContainer = styled(ButtonHeaderCommon, {
  top: getScaleSize(50),
  right: getScaleSize(15),
});

export const ContentContainer = styled(View, {
  top: -getScaleSize(12),
});

export const BubbleContainer = styled(View, {
  ...container('padding'),
  borderRadius: getScaleSize(12),
  paddingVertical: getScaleSize(20),
  backgroundColor: color('white'),
  shadowColor: color('black'),
  shadowOffset: {
    width: 4,
    height: 0,
  },
  shadowOpacity: 0.05,
  shadowRadius: 50,
  elevation: 1,
  marginBottom: getScaleSize(16),
});

export const TitleMain = styled(Text, {
  ...font({ type: 'h1' }),
});

export const SubTitleMain = styled(Text, {
  ...font({ type: 'text3', weight: 'light' }),
  color: color('gray7'),
});

export const DescriptionMain = styled(Text, {
  ...font({ type: 'text2', weight: 'light' }),
  color: color('gray8'),
  paddingTop: getScaleSize(15),
});

export const ButtonContainer = styled(View, {
  paddingTop: getScaleSize(16),
  marginHorizontal: getScaleSize(38),
});

export const Delimeter = styled(View, {
  height: 1,
  width: '100%',
  backgroundColor: color('gray3'),
  marginVertical: getScaleSize(20),
});

export const TabsInfoContainer = styled(View, {
  flexDirection: 'row',
  justifyContent: 'space-between',
});

export const BubbleInfoTitle = styled(Text, {
  ...font({ type: 'h2' }),
});
export const BubbleInfoRow = styled(View, {
  marginBottom: getScaleSize(15),
  flexDirection: 'row',
  justifyContent: 'space-between',
});

export const BubbleInfoRowTitle = styled(Text, {
  ...font({ type: 'text2', weight: 'light' }),
  color: color('gray10'),
});

export const BubbleInfoRowValue = styled(Text, {
  ...font({ type: 'text2' }),
  color: color('gray10'),
});

export const BottomSpring = styled(View, {
  height: bottomTabsHeight,
  width: '100%',
});

export const LinkButtonContainer = styled(TouchableOpacity, {
  marginTop: getScaleSize(10),
});

export const LinkButtonText = styled(Text, {
  ...font({ type: 'text2' }),
  color: color('gray10'),
  textAlign: 'center',
});

export const BottomDescription = styled(Text, {
  ...font({ type: 'text1' }),
  color: color('gray10'),
});

export const BottomContainer = styled(View, {
  ...container('padding'),
  backgroundColor: color('white'),
  paddingBottom: getScaleSize(30),
});

export const Spring = styled(View, {
  height: getScaleSize(30),
});

export const styles = StyleSheet.create({
  video: {
    width: screenWidth,
    height: (screenWidth / 4) * 5,
  },
});
