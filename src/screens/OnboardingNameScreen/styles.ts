import { theme } from '@shared/config/theme';
import { StyleSheet } from 'react-native';

import { purpleColor } from 'src/commonStyles/colors';
import { font12r, font14r, font30m } from 'src/commonStyles/fonts';
import { getScaleSize } from 'src/utils/dimensions';

export const styles = StyleSheet.create({
  paddingHorizontal: {
    paddingHorizontal: getScaleSize(15),
  },
  container: {
    paddingTop: getScaleSize(10),
    paddingBottom: getScaleSize(60),
    justifyContent: 'space-between',
    flexGrow: 1,
  },
  containerTabs: {
    marginBottom: getScaleSize(81),
  },
  textTitle: {
    ...font30m,
    lineHeight: getScaleSize(37),
    color: '#262626',
    marginBottom: getScaleSize(12),
    textAlign: 'center',
  },
  textSubTitle: {
    ...font14r,
    lineHeight: getScaleSize(21),
    color: '#8C8C8C',
    marginBottom: getScaleSize(32),
    textAlign: 'center',
  },
  containerIcon: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: getScaleSize(16),
  },
  input: {
    height: getScaleSize(60),
    lineHeight: getScaleSize(27),
    color: purpleColor,
    borderWidth: 1,
    borderColor: theme.colors.gray5,
    borderRadius: getScaleSize(8),
    marginHorizontal: getScaleSize(15),
  },
  textUnderIcon: {
    marginTop: getScaleSize(8),
    ...font12r,
    color: '#8C8C8C',
    width: getScaleSize(150),
    textAlign: 'center',
  },
});
