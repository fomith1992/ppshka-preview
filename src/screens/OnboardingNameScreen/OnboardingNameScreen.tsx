import React, { useState } from 'react';
import { KeyboardAvoidingView, Text, TextInput, View } from 'react-native';

import { useNavigation } from '@react-navigation/native';

import { font20m, font20r } from 'src/commonStyles/fonts';
import Button from 'src/components/Button';
import Icon from 'src/components/Icon';
import OnboardingTabs from 'src/components/OnboardingTabs';
import ScreenLayout from 'src/components/ScreenLayout';

import { styles } from './styles';
import { StackScreenProps } from '@react-navigation/stack';
import { RootStackParamList } from 'src/navigation/RootNavigator';

const OnboardingNameScreen = ({
  route,
}: StackScreenProps<RootStackParamList, 'OnboardingName'>) => {
  const navigation = useNavigation();
  const [name, setName] = useState('');

  const onContinue = () => {
    navigation.navigate('OnboardingUserSex', { ...route.params, name });
  };

  return (
    <ScreenLayout>
      <KeyboardAvoidingView style={{ flex: 1 }} behavior="padding">
        <View style={styles.container}>
          <View>
            <OnboardingTabs activeValue={1} totalValues={7} style={styles.containerTabs} />
            <Text style={styles.textTitle}>Как вас зовут?</Text>
            <Text style={styles.textSubTitle}>Давайте познакомимся! Введите ваше имя ниже</Text>
            <TextInput
              style={[styles.input, name.length ? font20m : font20r]}
              value={name}
              onChangeText={setName}
              textAlign={'center'}
              placeholder={'Введите ваше имя'}
              placeholderTextColor={'#8C8C8C'}
            />
            <View style={styles.containerIcon}>
              <Icon name="fingerUp" />
              <Text style={styles.textUnderIcon}>Нажмите, для ручного ввода имени</Text>
            </View>
          </View>
          <View style={styles.paddingHorizontal}>
            <Button uppercase content="Продолжить" onPress={onContinue} />
          </View>
        </View>
      </KeyboardAvoidingView>
    </ScreenLayout>
  );
};

export default OnboardingNameScreen;
