import { Bottom } from '@screens/recipes/ui/header.style';
import React, { useState } from 'react';
import { Text, TouchableOpacity, View } from 'react-native';

import { purpleColor } from 'src/commonStyles/colors';
import Button from 'src/components/Button';
import Icon from 'src/components/Icon';
import { getScaleSize } from 'src/utils/dimensions';

import { styles } from './question.styles';
import { Container } from './question.styles';

const choices = ['Низкая', 'Средняя', 'Высокая'];

interface ActivityScreenProps {
  activity?: 0 | 1 | 2;
  setActivity: (activity: 0 | 1 | 2) => void;
  hideScreen: () => void;
}

export const ActivityScreen = ({ activity = 0, setActivity, hideScreen }: ActivityScreenProps) => {
  const [activityLocal, setActivityLocal] = useState(activity);
  return (
    <>
      <Container>
        <View>
          <Bottom mb={getScaleSize(81)} />
          <Text style={styles.textTitle}>Какая у Вас активность?</Text>
          <Text style={styles.textSubTitle}>
            Пожалуйста, ответьте на данный вопрос. Мы подберём специальные рецепты для Вас
          </Text>
          {choices.map((choice, index) => (
            <TouchableOpacity
              key={index}
              onPress={() => setActivityLocal(index as 0 | 1 | 2)}
              style={[
                styles.containerChoice,
                activityLocal === index ? styles.selectedContainerChoice : null,
              ]}
            >
              {activityLocal === index ? (
                <Icon name="tick" style={styles.containerIconTick} color="primary" />
              ) : null}
              <Text
                style={[styles.textChoice, activityLocal === index ? { color: purpleColor } : null]}
              >
                {choice}
              </Text>
            </TouchableOpacity>
          ))}
        </View>
        <View style={styles.paddingHorizontal}>
          <Button uppercase content="Продолжить" onPress={() => setActivity(activityLocal)} />
          <TouchableOpacity style={styles.buttonBack} onPress={hideScreen}>
            <Icon name="arrowBack" size={12} color="primary" />
            <Text style={styles.textButtonBack}>Назад</Text>
          </TouchableOpacity>
        </View>
      </Container>
    </>
  );
};
