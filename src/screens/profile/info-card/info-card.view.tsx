import { Row } from '@screens/recipes/ui/header.style';
import React from 'react';
import { TouchableOpacity, View } from 'react-native';
import Icon from 'src/components/Icon';
import { hitSlopParams } from 'src/utils/hit-slop-params';
import {
  Container,
  CurrentWeight,
  TitileContainer,
  LeftContainer,
  Title,
  TitleRow,
} from './info-card.style';

interface MeteringViewProps {
  title: string;
  currentValue: string;
  onEdit: () => void;
}

export const InfoCardView = ({
  currentValue,
  onEdit,
  title,
}: MeteringViewProps): React.ReactElement => {
  return (
    <Container>
      <TitleRow alignItems spaceBeetween>
        <View style={{ flex: 1 }}>
          <Row alignItems spaceBeetween>
            <TitileContainer>
              <Title>{title}:</Title>
            </TitileContainer>
            <TouchableOpacity onPress={onEdit} hitSlop={hitSlopParams('L')}>
              <Icon color="gray6" style={{ transform: [{ rotate: '180deg' }] }} name="arrowBack" />
            </TouchableOpacity>
          </Row>
          <LeftContainer>
            <CurrentWeight>{currentValue}</CurrentWeight>
          </LeftContainer>
        </View>
      </TitleRow>
    </Container>
  );
};
