import React, { useState } from 'react';
import { Alert } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import useAsyncFn from 'react-use/lib/useAsyncFn';
import useAsync from 'react-use/lib/useAsync';
import { auth, userAPI } from 'src/api';
import { LoadingIndicator } from 'src/components/ui/Loader/loading-indicator';
import { AppState } from 'src/store';
import { ProfileView } from './profile.view';
import ImageCropPicker from 'react-native-image-crop-picker';
import { getUserData } from 'src/store/session/actions';
import { updateBirthdayEvent } from 'src/firebase/analytics/analytics-user/update-birthday';
import { updateNameEvent } from 'src/firebase/analytics/analytics-user/update-name';

export const Profile = (): React.ReactElement => {
  const dispatch = useDispatch();
  const user = useSelector((state: AppState) => state.session.user);

  const access_token = useSelector((state: AppState) => state.session.tokens.accessToken);

  const [name, setName] = useState<string | undefined>(undefined);
  const [image, setImage] = useState<{ path: string; base64: string | null | undefined }>({
    path: '',
    base64: null,
  });

  const changeUserData = useAsync(async () => {
    if (name !== undefined && user.name !== name) {
      return await auth
        .registerSetUserParams({
          access_token,
          name,
        })
        .then(async () => {
          updateNameEvent(user.id?.toString() ?? null);
          setName(undefined);
          dispatch(getUserData(access_token));
        })
        .catch((e) => {
          console.log(e);
          Alert.alert('Произошла ошибка');
        });
    }
  }, [user, name]);

  const [_, sendBirthday] = useAsyncFn(
    async (birthday: string) => {
      return await userAPI
        .setUserBDay({
          access_token,
          b_day: birthday,
        })
        .then(async () => {
          updateBirthdayEvent(user.id?.toString() ?? null);
          dispatch(getUserData(access_token));
        })
        .catch((e) => {
          console.log(e);
          Alert.alert('Произошла ошибка');
        });
    },
    [access_token],
  );

  const [updateUserPhotoStatus, onUpdateUserPhoto] = useAsyncFn(
    async (file_path?: string | null) => {
      return await auth
        .updateUserPhoto({ file_path, access_token })
        .catch(() => Alert.alert('Произошла ошибка'));
    },
    [image],
  );

  const [__, onUpdateUserActivity] = useAsyncFn(async (activity: 0 | 1 | 2) => {
    return await userAPI
      .setUserActivity({ activity, access_token })
      .then(() => dispatch(getUserData(access_token)))
      .catch(() => Alert.alert('Произошла ошибка'));
  }, []);

  const [___, onUpdateUserGoal] = useAsyncFn(async (goal: 0 | 1 | 2) => {
    return await userAPI
      .setUserGoal({ goal, access_token })
      .then(() => dispatch(getUserData(access_token)))
      .catch(() => Alert.alert('Произошла ошибка'));
  }, []);

  const onUploadPhoto = () => {
    ImageCropPicker.openPicker({
      width: 400,
      height: 400,
      cropping: true,
      mediaType: 'photo',
      includeBase64: true,
      cropperToolbarTitle: 'Редактирование',
    })
      .then(({ data, path }) => {
        setImage({ path, base64: data });
        onUpdateUserPhoto(data);
      })
      .catch((e) => {
        console.log('Error set avatar', e);
      });
  };

  return (
    <>
      <ProfileView
        userHeight={user.height ?? 0}
        image={image}
        birthday={user.b_day}
        onUploadPhoto={onUploadPhoto}
        currentData={user}
        setActivity={onUpdateUserActivity}
        setGoal={onUpdateUserGoal}
        setName={setName}
        sendBirthday={sendBirthday}
      />
      <LoadingIndicator visible={changeUserData.loading || updateUserPhotoStatus.loading} />
    </>
  );
};
