import { Bottom } from '@screens/recipes/ui/header.style';
import React, { useState } from 'react';
import { KeyboardAvoidingView, Text, TextInput, TouchableOpacity, View } from 'react-native';

import { font20m, font20r } from 'src/commonStyles/fonts';
import Button from 'src/components/Button';
import Icon from 'src/components/Icon';
import { getScaleSize } from 'src/utils/dimensions';

import { Container, styles } from './styles';

interface UserNameScreenProps {
  currentName: string | null;
  setName: (name: string) => void;
  hideScreen: () => void;
}
export const UserNameScreen = ({
  currentName = '',
  setName,
  hideScreen,
}: UserNameScreenProps): React.ReactElement => {
  const [name, setLocalName] = useState(currentName);

  return (
    <KeyboardAvoidingView style={{ flex: 1 }} behavior="padding">
      <Container>
        <View>
          <Bottom mb={getScaleSize(81)} />
          <Text style={styles.textTitle}>Как вас зовут?</Text>
          <Text style={styles.textSubTitle}>Давайте познакомимся! Введите ваше имя ниже</Text>
          <TextInput
            style={[styles.input, name?.length ?? 0 ? font20m : font20r]}
            value={name ?? ''}
            onChangeText={setLocalName}
            textAlign={'center'}
            placeholder={'Введите ваше имя'}
            placeholderTextColor={'#8C8C8C'}
          />
          <View style={styles.containerIcon}>
            <Icon name="fingerUp" />
            <Text style={styles.textUnderIcon}>Нажмите, для ручного ввода имени</Text>
          </View>
        </View>
        <View style={styles.paddingHorizontal}>
          <Button uppercase content="Продолжить" onPress={() => setName(name)} />
          <TouchableOpacity style={styles.buttonBack} onPress={hideScreen}>
            <Icon name="arrowBack" size={12} color="primary" />
            <Text style={styles.textButtonBack}>Назад</Text>
          </TouchableOpacity>
        </View>
      </Container>
    </KeyboardAvoidingView>
  );
};
