import { color } from '@shared/config/color';
import { styled } from '@shared/config/styled';
import { theme } from '@shared/config/theme';
import { StyleSheet, View } from 'react-native';

import { purpleColor } from 'src/commonStyles/colors';
import { font12m, font12r, font14r, font20m, font30m } from 'src/commonStyles/fonts';
import { getScaleSize } from 'src/utils/dimensions';

export const Container = styled(View, {
  paddingTop: getScaleSize(10),
  flexGrow: 1,
  justifyContent: 'space-between',
  paddingBottom: getScaleSize(70),
  backgroundColor: color('white'),
});

export const styles = StyleSheet.create({
  paddingHorizontal: {
    paddingHorizontal: getScaleSize(15),
  },
  containerTabs: {
    marginBottom: getScaleSize(81),
  },
  textTitle: {
    ...font30m,
    lineHeight: getScaleSize(37),
    color: '#262626',
    marginBottom: getScaleSize(12),
    textAlign: 'center',
  },
  textSubTitle: {
    ...font14r,
    lineHeight: getScaleSize(21),
    color: '#8C8C8C',
    marginBottom: getScaleSize(32),
    textAlign: 'center',
  },
  containerIcon: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: getScaleSize(16),
  },
  input: {
    height: getScaleSize(60),
    ...font20m,
    lineHeight: getScaleSize(27),
    color: purpleColor,
    borderWidth: 1,
    borderColor: theme.colors.gray5,
    borderRadius: getScaleSize(8),
    marginHorizontal: getScaleSize(15),
  },
  textUnderIcon: {
    marginTop: getScaleSize(8),
    ...font12r,
    color: '#8C8C8C',
    width: getScaleSize(150),
    textAlign: 'center',
  },
  buttonBack: {
    paddingVertical: getScaleSize(18),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  textButtonBack: {
    ...font12m,
    color: purpleColor,
    marginLeft: getScaleSize(8),
  },
});
