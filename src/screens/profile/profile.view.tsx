import React, { useState } from 'react';
import { Image, Linking, TouchableOpacity, View } from 'react-native';
import PreLoadedImage from 'src/components/PreLoadedImage';
import ScreenLayout from 'src/components/ScreenLayout';

import { styles } from './styles';
import AsyncStorage from '@react-native-async-storage/async-storage';
import store from 'src/store';
import { Bottom } from '@screens/recipes/ui/header.style';
import {
  Container,
  EditSvg,
  HeaderButton,
  HeaderButtonContainer,
  HeaderButtonDescription,
  HeaderContainer,
  ItemsContainer,
  LogOutSvg,
  UserName,
  UserContainer,
  BottomButtonContainer,
  ProfileButtonContainer,
  ButtonTitle,
  ButtonTitleContainer,
} from './profile.style';
import { hitSlopParams } from 'src/utils/hit-slop-params';
import { statusBarHeight } from 'src/utils/dimensions';
import { Portal } from '@shared/ui/portal';
import { ActivityScreen } from './activity.screen';
import { GoalScreen } from './goal.screen';
import { TUserData } from 'src/api/user';
import { UserNameScreen } from './user-name/user-name.screen';
import { GradientLayoutHeaderView } from '@shared/ui/gradient-layout/gradient-layout-header.view';

import avatar from '../../shared/ui/assets/avatar.png';
import { SetBirthdayScreen } from '@screens/set-birthday-screen/set-birthday-screen';
import { InfoCardView } from './info-card/info-card.view';
import { gradientComponentsColors } from '@shared/ui/gradient-layout/constant';
import { format, parse } from 'date-fns';
import { ru } from 'date-fns/locale';
import { logoutEvent } from 'src/firebase/analytics/analytics-auth/logout-event';
import { firebaseSetUser } from 'src/firebase/set-user-firebase';
import { sessionReset } from 'src/store/session/actions.types';
import { StackActions, useNavigation } from '@react-navigation/core';

const wtiteToSupport = 'https://t.me/ppshka_app_bot';

interface ProfileViewProps {
  userHeight: number;
  currentData: TUserData;
  activity?: 0 | 1 | 2;
  goal?: 0 | 1 | 2;
  setActivity: (activity: 0 | 1 | 2) => void;
  setGoal: (activity: 0 | 1 | 2) => void;
  setName: (name: string) => void;
  onUploadPhoto: () => void;
  sendBirthday: (date: string) => void;
  image: {
    path: string;
    base64: string | null | undefined;
  };
  birthday: string;
}

const activityParse = {
  0: 'Низкая',
  1: 'Средняя',
  2: 'Высокая',
};

export const goalParse = {
  0: 'Сбросить вес',
  1: 'Сохранить вес',
  2: 'Набрать вес',
};

export const ProfileView = ({
  userHeight,
  image,
  setActivity,
  setGoal,
  currentData,
  setName,
  onUploadPhoto,
  birthday,
  sendBirthday,
}: ProfileViewProps) => {
  const navigation = useNavigation();
  const [activeScreenVisible, setActiveScreenVisible] = useState<boolean>(false);
  const [goalScreenVisible, setGoalScreenVisible] = useState<boolean>(false);
  const [nameScreenVisible, setNameScreenVisible] = useState<boolean>(false);
  const [birthdayScreenVisible, setBirthdayScreenVisible] = useState<boolean>(false);

  const logOut = async () => {
    await AsyncStorage.removeItem('refreshToken');
    logoutEvent();
    firebaseSetUser(null);
    store.dispatch(sessionReset());
  };

  return (
    <ScreenLayout edges={['left', 'right']}>
      <Container>
        <View style={styles.containerHeader}>
          <GradientLayoutHeaderView>
            <HeaderContainer>
              <HeaderButtonContainer>
                <HeaderButton onPress={logOut} hitSlop={hitSlopParams(12)}>
                  <LogOutSvg />
                </HeaderButton>
                <HeaderButtonDescription>Выйти</HeaderButtonDescription>
              </HeaderButtonContainer>
              <UserContainer>
                <TouchableOpacity onPress={onUploadPhoto}>
                  {image.path ? (
                    <PreLoadedImage
                      source={{ uri: image.path ?? currentData.user_photo }}
                      style={styles.imageAvatar}
                    />
                  ) : currentData.user_photo ? (
                    <PreLoadedImage
                      source={{ uri: currentData.user_photo }}
                      style={styles.imageAvatar}
                    />
                  ) : (
                    <Image style={styles.imageAvatar} source={avatar} />
                  )}
                </TouchableOpacity>
                <UserName>{currentData.name ? currentData.name : 'Пользователь'}</UserName>
              </UserContainer>
              <HeaderButtonContainer>
                <HeaderButton
                  onPress={() => setNameScreenVisible(true)}
                  hitSlop={hitSlopParams(12)}
                >
                  <EditSvg />
                </HeaderButton>
                <HeaderButtonDescription>Изменить</HeaderButtonDescription>
              </HeaderButtonContainer>
            </HeaderContainer>
            <Bottom mb={20} />
          </GradientLayoutHeaderView>
          <ItemsContainer>
            <InfoCardView
              currentValue={goalParse[currentData.goal as 0 | 1 | 2]}
              onEdit={() => setGoalScreenVisible(true)}
              title="Ваша цель"
            />
            <InfoCardView
              currentValue={activityParse[currentData.activity as 0 | 1 | 2]}
              onEdit={() => setActiveScreenVisible(true)}
              title="Ваша активность"
            />
            <InfoCardView
              currentValue={
                birthday
                  ? format(parse(birthday, 'yyyy-MM-dd', new Date()), 'PPP', {
                      locale: ru,
                    })
                  : 'Не указана'
              }
              onEdit={() => setBirthdayScreenVisible(true)}
              title="Дата рождения"
            />
            <InfoCardView
              currentValue={(userHeight ?? 0).toString()}
              onEdit={() => navigation.dispatch(StackActions.push('SetHeight', {}))}
              title="Ваш рост"
            />
            <BottomButtonContainer>
              <ProfileButtonContainer colors={gradientComponentsColors}>
                <ButtonTitleContainer onPress={() => Linking.openURL(wtiteToSupport)}>
                  <ButtonTitle>Написать в техническую поддержку</ButtonTitle>
                </ButtonTitleContainer>
              </ProfileButtonContainer>
            </BottomButtonContainer>
          </ItemsContainer>
        </View>
        <Bottom mb={statusBarHeight + 64} />
      </Container>
      {activeScreenVisible && (
        <Portal>
          <ActivityScreen
            activity={currentData.activity as 0 | 1 | 2}
            setActivity={(x) => {
              setActiveScreenVisible(false);
              setActivity(x);
            }}
            hideScreen={() => setActiveScreenVisible(false)}
          />
        </Portal>
      )}
      {goalScreenVisible && (
        <Portal>
          <GoalScreen
            goal={currentData.goal as 0 | 1 | 2}
            setGoal={(x) => {
              setGoalScreenVisible(false);
              setGoal(x);
            }}
            hideScreen={() => setGoalScreenVisible(false)}
          />
        </Portal>
      )}
      {nameScreenVisible && (
        <Portal>
          <UserNameScreen
            currentName={currentData.name}
            setName={(x) => {
              setNameScreenVisible(false);
              setName(x);
            }}
            hideScreen={() => setNameScreenVisible(false)}
          />
        </Portal>
      )}
      {birthdayScreenVisible && (
        <Portal>
          <SetBirthdayScreen
            currentDate={parse(birthday, 'yyyy-MM-dd', new Date())}
            setBirthday={(x) => {
              setBirthdayScreenVisible(false);
              sendBirthday(x);
            }}
            hideScreen={() => setBirthdayScreenVisible(false)}
          />
        </Portal>
      )}
    </ScreenLayout>
  );
};
