import { color, ColorType } from '@shared/config/color';
import { styled } from '@shared/config/styled';
import { font } from '@shared/config/text';
import { container } from '@shared/config/view';
import { EditIcon14 } from '@shared/ui/icons/edit.icon-14';
import { LogOutIcon14 } from '@shared/ui/icons/logout.icon-14';
import { Text, TouchableOpacity, View } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { getScaleSize } from 'src/utils/dimensions';

export const ButtonContainer = styled(View, {
  flexDirection: 'row',
  ...container(),
});

export const ProfileButton = styled(TouchableOpacity, {
  paddingHorizontal: getScaleSize(40),
  paddingVertical: getScaleSize(20),
  backgroundColor: color('whiteOpacity'),
  borderRadius: getScaleSize(50),
  borderWidth: 1,
  borderColor: color('white'),
});

export const ProfileButtonText = styled(Text, {
  ...font({ type: 'text3', weight: 'normal' }),
  letterSpacing: 2,
  textTransform: 'uppercase',
  color: color('white'),
});

export const UserName = styled(Text, {
  ...font({ type: 'text1', weight: 'normal' }),
  color: color('white'),
  marginBottom: getScaleSize(4),
  textAlign: 'center',
});

export const Container = styled(View, {
  flex: 1,
});

export const ButtonHeader = styled(TouchableOpacity, {
  justifyContent: 'center',
  alignItems: 'center',
  width: getScaleSize(30),
  height: getScaleSize(30),
  zIndex: 1,
  backgroundColor: color('whiteOpacity'),
  borderRadius: getScaleSize(8),
  marginBottom: getScaleSize(8),
});

export const ContainerActions = styled(View, {
  flexDirection: 'row',
  justifyContent: 'space-between',
  ...container('padding'),
  width: '100%',
});

export const HeaderContainer = styled(View, {
  ...container(),
  flexDirection: 'row',
  alignItems: 'flex-start',
  justifyContent: 'space-between',
});

export const HLine = styled(View, {
  width: '100%',
  height: 2,
  backgroundColor: color('white'),
});

export const HeaderButton = styled(TouchableOpacity, {
  width: getScaleSize(30),
  height: getScaleSize(30),
  justifyContent: 'center',
  alignItems: 'center',
  borderRadius: getScaleSize(8),
  borderWidth: 1,
  borderColor: color('white'),
  backgroundColor: color('whiteOpacity80'),
});

export const HeaderButtonDescription = styled(Text, {
  ...font({ type: 'text5' }),
  color: color('whiteOpacity'),
  marginTop: getScaleSize(3),
});

export const EditSvg = styled(EditIcon14, {
  color: color('white'),
});

export const EditSvgGray = styled(EditIcon14, {
  color: color('gray7'),
});

export const LogOutSvg = styled(LogOutIcon14, {
  color: color('white'),
});

export const HeaderButtonContainer = styled(View, {
  alignItems: 'center',
});

export const UserContainer = styled(View, {
  alignItems: 'center',
});

export const ItemsContainer = styled(View, {
  marginTop: -getScaleSize(45),
});

export const Hline = styled(
  View,
  (props: { mt?: number; mb?: number; color?: ColorType }) =>
    ({
      height: 1,
      marginTop: props.mt && getScaleSize(props.mt),
      marginBottom: props.mb && getScaleSize(props.mb),
      backgroundColor: color(props.color ?? 'black'),
    } as const),
);

export const BottomButtonContainer = styled(View, {
  ...container(),
  marginTop: getScaleSize(16),
});

export const ProfileButtonContainer = styled(LinearGradient, {
  ...container(),
  height: getScaleSize(55),
  borderRadius: getScaleSize(30),
  overflow: 'hidden',
});

export const ButtonTitleContainer = styled(TouchableOpacity, {
  flex: 1,
  margin: 1,
  justifyContent: 'center',
  backgroundColor: color('white'),
  borderRadius: getScaleSize(30),
  overflow: 'hidden',
});

export const ButtonTitle = styled(Text, {
  ...font({ type: 'text3', weight: 'bold' }),
  color: color('gray10'),
  textTransform: 'uppercase',
  textAlignVertical: 'center',
  textAlign: 'center',
});
