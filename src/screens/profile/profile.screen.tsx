import React from 'react';
import { Profile } from './profile.container';

export const ProfileScreen = (): React.ReactElement => {
  return <Profile />;
};
