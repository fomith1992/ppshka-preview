import { theme } from '@shared/config/theme';
import { StyleSheet } from 'react-native';

import { font12r, font14r, font16m } from 'src/commonStyles/fonts';
import { bottomTabsHeight, getScaleSize, windowWidth } from 'src/utils/dimensions';

export const styles = StyleSheet.create({
  containerHeader: {
    flex: 1,
  },
  imageAvatar: {
    width: getScaleSize(100),
    height: getScaleSize(100),
    borderRadius: getScaleSize(12),
    marginBottom: getScaleSize(12),
  },
  smallImageAvatar: {
    width: getScaleSize(79),
    height: getScaleSize(79),
    borderRadius: getScaleSize(12),
    marginBottom: getScaleSize(12),
  },
  textDescription: {
    ...font14r,
    lineHeight: getScaleSize(22),
    color: theme.colors.white,
    marginBottom: getScaleSize(20),
  },
  containerMainInfo: {
    width: '100%',
    paddingHorizontal: getScaleSize(21),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: getScaleSize(12),
    marginBottom: getScaleSize(16),
  },
  textMainInfoValue: {
    ...font16m,
    color: theme.colors.white,
    marginBottom: getScaleSize(4),
  },
  textMainInfoName: {
    ...font12r,
    color: theme.colors.white,
  },
  containerMainInfoItem: {
    alignItems: 'center',
  },
  containerScrollView: {
    // flex: 1,
    backgroundColor: '#FAFAFA',
    paddingHorizontal: getScaleSize(15),
    paddingTop: getScaleSize(20),
    paddingBottom: getScaleSize(50) + bottomTabsHeight,
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
  },
  containerPhoto: {
    marginBottom: getScaleSize(12),
  },
  imagePhoto: {
    width: (windowWidth - getScaleSize(54)) / 3,
    aspectRatio: 1,
    borderRadius: getScaleSize(15),
  },
});
