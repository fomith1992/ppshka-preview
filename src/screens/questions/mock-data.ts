export interface QuestionsDataInterface {
  id: number;
  title: string;
  description: string;
  neededAnswers: {
    id: number;
    answer: number;
  }[];
  answers: {
    id: number;
    answer: string;
  }[];
}

export const foodData: QuestionsDataInterface[][] = [
  [
    {
      id: 0,
      title: 'Имеется ли у вас аллергия на продукты (шаг 1)',
      description:
        'Если выбрать "Да, на рыбные продукты", тогда вы получите другой следующий вопрос',
      neededAnswers: [],
      answers: [
        {
          id: 0,
          answer: 'Да, на мясные продукты',
        },
        {
          id: 1,
          answer: 'Да, на рыбные продукты',
        },
        {
          id: 2,
          answer: 'Аллергии на продукты нет',
        },
      ],
    },
  ],
  [
    {
      id: 1,
      title: 'ДРУГОЙ ВОПРОС  (шаг 2)',
      description: '{ id: 0, answer: 1 }',
      neededAnswers: [{ id: 0, answer: 1 }],
      answers: [
        {
          id: 0,
          answer: 'Да, на мясные продукты',
        },
        {
          id: 1,
          answer: 'Да, на рыбные продукты',
        },
        {
          id: 2,
          answer: 'Аллергии на продукты нет',
        },
      ],
    },
    {
      id: 1,
      title: 'Имеется ли у вас аллергия на продукты  (шаг 2)',
      description: 'Пожалуйста, ответьте на данный вопрос. Мы подберём специальные рецепты для Вас',
      neededAnswers: [],
      answers: [
        {
          id: 0,
          answer: 'Да, на мясные продукты',
        },
        {
          id: 1,
          answer: 'Да, на рыбные продукты',
        },
        {
          id: 2,
          answer: 'Аллергии на продукты нет',
        },
      ],
    },
  ],
  [
    {
      id: 2,
      title: 'Имеется ли у вас аллергия на продукты  (шаг 3)',
      description: 'Пожалуйста, ответьте на данный вопрос. Мы подберём специальные рецепты для Вас',
      neededAnswers: [],
      answers: [
        {
          id: 0,
          answer: 'Да, на мясные продукты',
        },
        {
          id: 1,
          answer: 'Да, на рыбные продукты',
        },
        {
          id: 2,
          answer: 'Аллергии на продукты нет',
        },
      ],
    },
  ],
  [
    {
      id: 3,
      title: 'Имеется ли у вас аллергия на продукты  (шаг 4)',
      description: 'Пожалуйста, ответьте на данный вопрос. Мы подберём специальные рецепты для Вас',
      neededAnswers: [],
      answers: [
        {
          id: 0,
          answer: 'Да, на мясные продукты',
        },
        {
          id: 1,
          answer: 'Да, на рыбные продукты',
        },
        {
          id: 2,
          answer: 'Аллергии на продукты нет',
        },
      ],
    },
  ],
  [
    {
      id: 4,
      title: 'Имеется ли у вас аллергия на продукты  (шаг 5)',
      description: 'Пожалуйста, ответьте на данный вопрос. Мы подберём специальные рецепты для Вас',
      neededAnswers: [],
      answers: [
        {
          id: 0,
          answer: 'Да, на мясные продукты',
        },
        {
          id: 1,
          answer: 'Да, на рыбные продукты',
        },
        {
          id: 2,
          answer: 'Аллергии на продукты нет',
        },
      ],
    },
  ],
];

export const triningData: QuestionsDataInterface[][] = [
  [
    {
      id: 0,
      title: 'Занимаетесь ли Вы гиревым спортом? (шаг 1)',
      description:
        'Если выбрать "Да, на рыбные продукты", тогда вы получите другой следующий вопрос',
      neededAnswers: [],
      answers: [
        {
          id: 0,
          answer: 'Да',
        },
        {
          id: 1,
          answer: 'Нет',
        },
        {
          id: 2,
          answer: 'Весом не более 10 кг',
        },
      ],
    },
  ],
  [
    {
      id: 1,
      title: 'ДРУГОЙ ВОПРОС  (шаг 2)',
      description: '{ id: 0, answer: 1 }',
      neededAnswers: [{ id: 0, answer: 1 }],
      answers: [
        {
          id: 0,
          answer: 'Да, на мясные продукты',
        },
        {
          id: 1,
          answer: 'Да, на рыбные продукты',
        },
        {
          id: 2,
          answer: 'Аллергии на продукты нет',
        },
      ],
    },
    {
      id: 1,
      title: 'Занимаетесь ли Вы гиревым спортом?  (шаг 2)',
      description: 'Пожалуйста, ответьте на данный вопрос. Мы подберём специальные рецепты для Вас',
      neededAnswers: [],
      answers: [
        {
          id: 0,
          answer: 'Да, на мясные продукты',
        },
        {
          id: 1,
          answer: 'Да, на рыбные продукты',
        },
        {
          id: 2,
          answer: 'Аллергии на продукты нет',
        },
      ],
    },
  ],
  [
    {
      id: 2,
      title: 'Занимаетесь ли Вы гиревым спортом?  (шаг 3)',
      description: 'Пожалуйста, ответьте на данный вопрос. Мы подберём специальные рецепты для Вас',
      neededAnswers: [],
      answers: [
        {
          id: 0,
          answer: 'Да, на мясные продукты',
        },
        {
          id: 1,
          answer: 'Да, на рыбные продукты',
        },
        {
          id: 2,
          answer: 'Аллергии на продукты нет',
        },
      ],
    },
  ],
  [
    {
      id: 3,
      title: 'Занимаетесь ли Вы гиревым спортом?  (шаг 4)',
      description: 'Пожалуйста, ответьте на данный вопрос. Мы подберём специальные рецепты для Вас',
      neededAnswers: [],
      answers: [
        {
          id: 0,
          answer: 'Да, на мясные продукты',
        },
        {
          id: 1,
          answer: 'Да, на рыбные продукты',
        },
        {
          id: 2,
          answer: 'Аллергии на продукты нет',
        },
      ],
    },
  ],
  [
    {
      id: 4,
      title: 'Занимаетесь ли Вы гиревым спортом?  (шаг 5)',
      description: 'Пожалуйста, ответьте на данный вопрос. Мы подберём специальные рецепты для Вас',
      neededAnswers: [],
      answers: [
        {
          id: 0,
          answer: 'Да, на мясные продукты',
        },
        {
          id: 1,
          answer: 'Да, на рыбные продукты',
        },
        {
          id: 2,
          answer: 'Аллергии на продукты нет',
        },
      ],
    },
  ],
];
