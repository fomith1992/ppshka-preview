import React, { useRef, useState } from 'react';
import {
  Animated,
  ScrollView,
  StatusBar,
  Text,
  TouchableOpacity,
  useWindowDimensions,
  View,
} from 'react-native';

import { purpleColor } from 'src/commonStyles/colors';
import Button from 'src/components/Button';
import Icon from 'src/components/Icon';
import OnboardingTabs from 'src/components/OnboardingTabs';
import ScreenLayout from 'src/components/ScreenLayout';

import { QuestionsDataInterface } from './mock-data';
import { styles } from './styles';

interface FoodQuestionsViewProps {
  data: QuestionsDataInterface[][];
  onSelectAllAnswers: (data: number[]) => void;
}

export const FoodQuestionsView = ({
  data,
  onSelectAllAnswers,
}: FoodQuestionsViewProps): React.ReactElement => {
  const scrollX = useRef<ScrollView>(null);
  const scroll = useRef(new Animated.Value(0)).current;
  const { width: windowWidth } = useWindowDimensions();
  const [page, setPage] = useState(0);
  const [selectedChoice, setSelectedChoice] = useState<string>(JSON.stringify([]));

  const selectedChoiceParse: number[] = JSON.parse(selectedChoice);

  const moveBody = (index: number): void => {
    scrollX.current?.scrollTo({
      x: index * windowWidth,
      animated: true,
    });
    setPage(index);
  };

  return (
    <ScreenLayout>
      <View style={styles.container}>
        <StatusBar animated backgroundColor="#fff" barStyle="dark-content" />
        <OnboardingTabs activeValue={page + 1} totalValues={data.length} />
        <ScrollView
          horizontal
          pagingEnabled
          scrollEnabled={false}
          showsHorizontalScrollIndicator={false}
          ref={scrollX}
          onScroll={Animated.event(
            [
              {
                nativeEvent: {
                  contentOffset: {
                    x: scroll,
                  },
                },
              },
            ],
            { useNativeDriver: false },
          )}
          scrollEventThrottle={1}
        >
          {data.map((item, idx) => {
            let neededItem: QuestionsDataInterface = item[0];
            if (item.length > 1) {
              const filteredItem = item.find((x) =>
                x.neededAnswers.some((y) => selectedChoiceParse[y.id] === y.answer),
              );
              if (filteredItem) {
                neededItem = filteredItem;
              } else {
                neededItem = item.find((x) => x.neededAnswers.length === 0) ?? item[0];
              }
            }

            return (
              <View
                key={neededItem.id}
                style={{ flexGrow: 1, width: windowWidth, justifyContent: 'center' }}
              >
                <Text style={styles.textTitle}>{neededItem.title}</Text>
                <Text style={styles.textSubTitle}>{neededItem.description}</Text>
                {neededItem.answers.map((choice) => (
                  <TouchableOpacity
                    key={`${idx} ${choice.id}`}
                    onPress={() => {
                      const selectedChoicesLocal = selectedChoiceParse;
                      selectedChoiceParse[neededItem.id] = choice.id;
                      setSelectedChoice(JSON.stringify(selectedChoicesLocal));
                    }}
                    style={[
                      styles.containerChoice,
                      selectedChoiceParse[neededItem.id] === choice.id
                        ? styles.selectedContainerChoice
                        : null,
                    ]}
                  >
                    {selectedChoiceParse[neededItem.id] === choice.id ? (
                      <Icon name="tick" style={styles.containerIconTick} color={purpleColor} />
                    ) : null}
                    <Text
                      style={[
                        styles.textChoice,
                        selectedChoiceParse[neededItem.id] === choice.id
                          ? { color: purpleColor }
                          : null,
                      ]}
                    >
                      {choice.answer}
                    </Text>
                  </TouchableOpacity>
                ))}
              </View>
            );
          })}
        </ScrollView>
        <View style={styles.paddingHorizontal}>
          <Button
            uppercase
            disabled={selectedChoiceParse[page] === undefined}
            content="Продолжить"
            onPress={() => {
              if (data.length - 1 === page) {
                onSelectAllAnswers(selectedChoiceParse);
              } else {
                moveBody(page + 1);
              }
            }}
          />
          {page === 0 ? (
            <View style={{ ...styles.buttonBack, opacity: 0 }}>
              <Text style={styles.textButtonBack}>Назад</Text>
            </View>
          ) : (
            <TouchableOpacity style={styles.buttonBack} onPress={() => moveBody(page - 1)}>
              <Icon name="arrowBack" size={12} color={purpleColor} />
              <Text style={styles.textButtonBack}>Назад</Text>
            </TouchableOpacity>
          )}
        </View>
      </View>
    </ScreenLayout>
  );
};
