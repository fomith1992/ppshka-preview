import React from 'react';

import { FoodQuestions } from './food-questions.container';
import { TrainingQuestions } from './training-questions.container';

export const FoodQuestionsScreen = (): React.ReactElement => {
  return <FoodQuestions />;
};

export const TrainingQuestionsScreen = (): React.ReactElement => {
  return <TrainingQuestions />;
};
