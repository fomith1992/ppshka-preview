import { styled } from '@shared/config/styled';
import { container } from '@shared/config/view';
import { View } from 'react-native';

import { getScaleSize, screenWidth, screenHeight } from 'src/utils/dimensions';
import VideoPlayerWC from 'react-native-video-controls/VideoPlayer';

export const Container = styled(View, {
  flex: 1,
  backgroundColor: 'black',
});

export const VideoPlayerView = styled(VideoPlayerWC, {
  position: 'absolute',
  top: 0,
  bottom: 0,
  left: 0,
  right: 0,
  width: screenWidth,
  height: screenHeight,
});

export const BottomContainer = styled(View, {
  ...container('margin'),
  position: 'absolute',
  flexDirection: 'row',
  bottom: getScaleSize(16),
});

export const FinishButtonContainer = styled(View, {
  flex: 1,
  marginRight: getScaleSize(14),
});

export const TogglePlayButtonContainer = View;
