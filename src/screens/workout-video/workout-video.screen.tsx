import React, { useEffect, useState } from 'react';

import { useNavigation } from '@react-navigation/native';

import { StackScreenProps } from '@react-navigation/stack';
import { AuthorizedStackParamList } from 'src/navigation/AuthorizedNavigator/AuthorizedNavigator';
import { useSelector } from 'react-redux';
import { AppState } from 'src/store';
import {
  BottomContainer,
  Container,
  FinishButtonContainer,
  TogglePlayButtonContainer,
  VideoPlayerView,
} from './styles';
import { workoutAPI } from 'src/api';
import { LoadingIndicator } from 'src/components/ui/Loader/loading-indicator';
import useAsync from 'react-use/lib/useAsync';
import { Alert } from 'react-native';
import ScreenLayout from 'src/components/ScreenLayout';
import { theme } from '@shared/config/theme';
import Button from 'src/components/Button';
import useAsyncFn from 'react-use/lib/useAsyncFn';
import { PauseIcon25 } from '@shared/ui/icons/pause.icon-25';
import { TogglePlayVideoButton } from 'src/components/Button/toggle-play-video-btn.view';
import { PlayIcon23 } from '@shared/ui/icons/play.icon-35';
import { startWorkoutEvent } from 'src/firebase/analytics/analytics-workout/start-workout-event';
import { viewDurationWorkoutEvent } from 'src/firebase/analytics/analytics-workout/view-duration-workout-event';
import { finishWorkoutEvent } from 'src/firebase/analytics/analytics-workout/finish-workout-event';
import { firebaseRecordError } from 'src/firebase/crashlytics/record-error-event';
import { firebaseLogEvent } from 'src/firebase/analytics/common/firebase-log-event';

const AWAITING_TIME = 60 * 5; // 3 minutes

const WorkoutVideoScreen = ({
  route,
}: StackScreenProps<AuthorizedStackParamList, 'WorkoutVideoView'>): React.ReactElement => {
  const { id: workoutId, dateStart: workoutDateStart } = route.params;
  const navigation = useNavigation();
  const access_token = useSelector((state: AppState) => state.session.tokens.accessToken);

  const [isPlay, setIsPlay] = useState(true);
  const [timer, setTimer] = useState<number>(0);
  const togglePlay = () => setIsPlay(!isPlay);

  const workoutVideoApi = useAsync(async () => {
    return await workoutAPI
      .getWorkoutById({ id: workoutId, access_token })
      .then(async (data) => {
        return data;
      })
      .catch((e) => {
        console.log(e);
        Alert.alert('Произошла ошибка');
      });
  }, [workoutId]);

  const [finishWorkoutState, finishWorkout] = useAsyncFn(async () => {
    return await workoutAPI
      .finishWorkout({
        access_token,
        workout_id: workoutId,
        date_start: workoutDateStart,
      })
      .then(async (response) => {
        if (response.data.status === 'ok') {
          finishWorkoutEvent(workoutId.toString());
          navigation.navigate('ProgramsList');
        } else {
        }
      })
      .catch((e) => {
        console.log(e);
        Alert.alert('Произошла ошибка');
      });
  }, [workoutId, access_token]);

  useEffect(() => {
    switch (timer) {
      case AWAITING_TIME:
        startWorkoutEvent(workoutId.toString());
        break;
      case 60 * 5:
      case 60 * 10:
      case 60 * 15:
      case 60 * 30:
        viewDurationWorkoutEvent(workoutId.toString(), timer / 60);
        break;
    }
    const interval = setInterval(() => setTimer(timer + 1), 1000);
    return () => clearInterval(interval);
  }, [timer, workoutId]);

  useEffect(() => {
    const backNAvigationListener = navigation.addListener('beforeRemove', (e) => {
      e.preventDefault();
      handlePressFinishWorkoutButton();
    });

    return backNAvigationListener;
  });

  function handlePressFinishWorkoutButton(): void {
    if (timer < AWAITING_TIME) {
      return navigation.navigate('ProgramsList');
    }
    Alert.alert('Предупреждение!', 'Вы уверенны что хотите завершить тренировку?', [
      {
        text: 'Да',
        onPress: () => finishWorkout(),
      },
      {
        text: 'Отмена',
        onPress: () => null,
        style: 'cancel',
      },
    ]);
  }

  if (workoutVideoApi.loading || workoutVideoApi.value == null || finishWorkoutState.loading)
    return <LoadingIndicator visible />;
  const uriVideo = workoutVideoApi.value.data.data.url_video;
  const uriImage = workoutVideoApi.value.data.data.url_image;

  return (
    <ScreenLayout safeArea={false} edges={['top', 'bottom']}>
      <Container>
        <VideoPlayerView
          source={{ uri: uriVideo }}
          fullscreen={false}
          repeat={true}
          paused={!isPlay}
          poster={uriImage}
          disableBack={true}
          disableFullscreen={true}
          disableTimer={true}
          disablePlayPause={true}
          disableVolume={true}
          disableSeekbar={true}
          tapAnywhereToPause={true}
          onEnd={finishWorkout}
          onStart={() => firebaseLogEvent(`Start video by uri: ${uriVideo}`)}
          onError={(err: Error) => {
            firebaseRecordError(err);
          }}
          seekColor={theme.colors.primary}
          /* bufferConfig={{
            minBufferMs: 4000,
            maxBufferMs: 8000,
            bufferForPlaybackMs: 500,
            bufferForPlaybackAfterRebufferMs: 2000,
          }} */
        />
        <BottomContainer>
          <FinishButtonContainer>
            <Button
              uppercase
              onPress={handlePressFinishWorkoutButton}
              content={timer > AWAITING_TIME ? 'Завершить тренировку' : 'Вернуться'}
            />
          </FinishButtonContainer>
          <TogglePlayButtonContainer>
            <TogglePlayVideoButton
              onPress={togglePlay}
              content={isPlay ? <PauseIcon25 /> : <PlayIcon23 />}
            />
          </TogglePlayButtonContainer>
        </BottomContainer>
      </Container>
    </ScreenLayout>
  );
};

export default WorkoutVideoScreen;
