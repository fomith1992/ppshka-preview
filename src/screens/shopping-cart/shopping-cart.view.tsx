import { Bottom } from '@screens/recipes/ui/header.style';
import { GradientLayoutView } from '@shared/ui/gradient-layout/gradient-layout.view';
import { BagIcon18 } from '@shared/ui/icons/bag.icon-18';
import { CheckMarkEmpty16 } from '@shared/ui/icons/empty-check-mark.icon-16';
import { CheckMarkFull16 } from '@shared/ui/icons/full-check-mark.icon-16';
import React from 'react';
import {
  Animated,
  FlatList,
  Image,
  LayoutAnimation,
  Platform,
  UIManager,
  View,
} from 'react-native';
import { TCheckList } from 'src/api/food-basket';
import { Spring } from 'src/components/chapter-description/chapter-description.style';
import ScreenLayout from 'src/components/ScreenLayout';
import { Dot } from 'src/components/slider/styles';
import { LoaderModal } from 'src/components/ui/Loader/loading-indicator.modal';
import emptyFoodBasket from 'src/shared/ui/assets/empty-food-basket.png';
import { hitSlopParams } from 'src/utils/hit-slop-params';
import {
  ClearAllButton,
  ClearAllButtonText,
  Container,
  Description,
  EmptyContainer,
  HeaderTitle,
  IngredientWeight,
  ItemButton,
  styles,
  TitleRow,
} from './shopping-cart.style';

interface ShoppingCartItemProps {
  item: TCheckList[];
  onPress: (itemId: string) => void;
  onClearFoodBasket: () => void;
}

interface ShoppingCartViewProps {
  data: TCheckList[][];
  onCompleteItem: (itemId: string) => void;
  onClearFoodBasket: () => void;
}

export const ShoppingCartItem = ({
  item,
  onPress,
  onClearFoodBasket,
}: ShoppingCartItemProps): React.ReactElement => {
  if (Platform.OS === 'android' && UIManager.setLayoutAnimationEnabledExperimental) {
    UIManager.setLayoutAnimationEnabledExperimental(true);
  }
  LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);

  const newArray = item
    .map((x) => ({ ...x, index: x.index + (x.done ? 900 : 0) }))
    .sort((a, b) => a.index - b.index);

  return (
    <Container>
      <TitleRow alignItems spaceBeetween>
        <View style={{ flex: 1 }}>
          <Animated.FlatList
            data={newArray}
            renderItem={({ item }) => (
              <ItemButton onPress={() => onPress(item.id)}>
                {item.done ? <CheckMarkFull16 /> : <CheckMarkEmpty16 />}
                <Description numberOfLines={2} isComplete={item.done}>
                  {item.name}
                </Description>
                <Spring />
                <Dot isActive />
                <BagIcon18 />
                <IngredientWeight>{item.gram}г</IngredientWeight>
              </ItemButton>
            )}
            ListFooterComponent={() => (
              <ClearAllButton onPress={onClearFoodBasket} hitSlop={hitSlopParams('L')}>
                <ClearAllButtonText>Очистить продуктовую корзину</ClearAllButtonText>
              </ClearAllButton>
            )}
          />
        </View>
      </TitleRow>
    </Container>
  );
};

export const ShoppingCartView = ({
  data,
  onCompleteItem,
  onClearFoodBasket,
}: ShoppingCartViewProps): React.ReactElement => {
  return (
    <ScreenLayout safeArea edges={['left', 'right']}>
      <GradientLayoutView />
      <HeaderTitle>Продуктовая{'\n'}корзина</HeaderTitle>
      <LoaderModal visible={false} />
      <FlatList
        initialNumToRender={2}
        contentContainerStyle={styles.tabsBottomSpace}
        data={data}
        renderItem={({ item }) => (
          <ShoppingCartItem
            onClearFoodBasket={onClearFoodBasket}
            item={item}
            onPress={onCompleteItem}
          />
        )}
        keyExtractor={(_, idx) => idx.toString()}
        showsVerticalScrollIndicator={false}
        ListFooterComponent={() => <Bottom mb={90} />}
        ListEmptyComponent={() => (
          <EmptyContainer>
            <Image
              style={{ width: 200, height: 250, resizeMode: 'contain' }}
              source={emptyFoodBasket}
            />
          </EmptyContainer>
        )}
      />
    </ScreenLayout>
  );
};
