import React from 'react';
import { ShoppingCart } from './shopping-cart.container';

export const ShoppingCartScreen = (): React.ReactElement => {
  return <ShoppingCart />;
};
