import { useNavigation } from '@react-navigation/core';
import React, { useState } from 'react';
import { Alert } from 'react-native';
import { useSelector } from 'react-redux';
import useAsync from 'react-use/lib/useAsync';
import useAsyncFn from 'react-use/lib/useAsyncFn';
import { clearBasketEvent } from 'src/firebase/analytics/analytics-food-basket/clear-food-basket';
import { foodBasketAPI } from 'src/api';
import { TCheckList } from 'src/api/food-basket';
import { AppState } from 'src/store';
import { ShoppingCartView } from './shopping-cart.view';

export const ShoppingCart = (): React.ReactElement => {
  const navigation = useNavigation();
  const [data, setData] = useState<TCheckList[][]>([]);
  const access_token = useSelector((state: AppState) => state.session.tokens.accessToken);

  useAsync(async () => {
    return await foodBasketAPI
      .getCheckList({
        access_token,
      })
      .then(async ({ data }) => {
        setData(data.data.length > 0 ? [data.data] : []);
      })
      .catch((e) => {
        console.log(e);
        Alert.alert('Произошла ошибка');
      });
  }, []);

  const [_, completeItem] = useAsyncFn(
    async (itemId: string) => {
      return await foodBasketAPI
        .applyIngredient({ access_token, ingredient_id: itemId })
        .then(async () => {
          await foodBasketAPI
            .getCheckList({
              access_token,
            })
            .then(async ({ data }) => {
              setData(data.data.length > 0 ? [data.data] : []);
            });
        })
        .catch(() => Alert.alert('Произошла ошибка'));
    },
    [data],
  );

  const [__, onClearFoodBasket] = useAsyncFn(async () => {
    return await foodBasketAPI
      .clearBasket({ access_token })
      .then(async () => {
        clearBasketEvent();
        setData([]);
        navigation.goBack();
      })
      .catch(() => Alert.alert('Произошла ошибка'));
  }, [data]);

  const onCompleteItem = (itemId: string) => {
    const notBuyingIngredientsCount = data[0]?.filter((x) => !x.done).length ?? 0;
    if (notBuyingIngredientsCount === 1 && data[0]?.find((x) => x.id === itemId && !x.done)) {
      Alert.alert('Внимание', 'Очистить продуктовую корзину?', [
        {
          text: 'Не очищать',
          style: 'cancel',
          onPress: () => completeItem(itemId),
        },
        {
          text: 'Очистить',
          onPress: () => onClearFoodBasket(),
        },
      ]);
    } else completeItem(itemId);
  };

  return (
    <ShoppingCartView
      onClearFoodBasket={onClearFoodBasket}
      data={data}
      onCompleteItem={onCompleteItem}
    />
  );
};
