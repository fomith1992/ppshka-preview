import { Row } from '@screens/recipes/ui/header.style';
import { color } from '@shared/config/color';
import { styled } from '@shared/config/styled';
import { font } from '@shared/config/text';
import { container } from '@shared/config/view';
import { blockShadow } from '@shared/ui/block-shadow/block-shadow';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { bottomTabsHeight, getScaleSize, screenHeight } from 'src/utils/dimensions';

export const Container = styled(View, {
  ...container('padding'),
  marginTop: getScaleSize(16),
  marginHorizontal: getScaleSize(15),
  paddingVertical: getScaleSize(16),
  backgroundColor: color('white'),
  borderRadius: getScaleSize(15),
  ...blockShadow(),
});

export const TitleRow = styled(Row, {
  alignItems: 'flex-start',
});

export const ItemButton = styled(TouchableOpacity, {
  flexDirection: 'row',
  alignItems: 'center',
  backgroundColor: color('white'),
  paddingVertical: getScaleSize(13),
  paddingHorizontal: getScaleSize(15),
  borderRadius: getScaleSize(10),
  marginTop: getScaleSize(10),
  ...blockShadow(),
});

export const Description = styled(
  Text,
  (props: { isComplete: boolean }) =>
    ({
      ...font({ type: 'text1' }),
      color: color(props.isComplete ? 'tertiary' : 'gray10'),
      width: '65%',
      lineHeight: getScaleSize(28),
      textDecorationLine: props.isComplete ? 'line-through' : undefined,
      textDecorationStyle: props.isComplete ? 'solid' : undefined,
      marginLeft: getScaleSize(8),
    } as const),
);

export const IngredientWeight = styled(Text, {
  ...font({ type: 'text3' }),
  color: color('gray10'),
  marginLeft: getScaleSize(6),
});

export const HeaderTitle = styled(Text, {
  ...container(),
  ...font({ type: 'h1' }),
  color: color('white'),
  marginTop: getScaleSize(50),
});

export const EmptyContainer = styled(View, {
  ...container(),
  flex: 1,
  height: screenHeight * 0.5,
  backgroundColor: color('white'),
  borderRadius: getScaleSize(16),
  justifyContent: 'center',
  alignItems: 'center',
});

export const styles = StyleSheet.create({
  tabsBottomSpace: {
    flexGrow: 1,
    paddingTop: getScaleSize(25),
    paddingBottom: getScaleSize(60) + bottomTabsHeight,
  },
});

export const ClearAllButton = styled(TouchableOpacity, {
  flexDirection: 'row',
  alignItems: 'center',
  justifyContent: 'center',
  marginTop: getScaleSize(40),
});

export const ClearAllButtonText = styled(Text, {
  ...font({ type: 'text1' }),
  color: color('primary'),
  marginHorizontal: getScaleSize(12),
  marginBottom: getScaleSize(12),
  textAlign: 'center',
});
