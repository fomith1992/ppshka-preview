import React from 'react';
import { FullPost } from './full-post.container';

export const FullPostScreen = (): React.ReactElement => {
  return <FullPost />;
};
