import React from 'react';
import { FullPostView } from './full-post.view';

export const FullPost = (): React.ReactElement => {
  return <FullPostView />;
};
