import React, { useEffect, useState } from 'react';
import { Keyboard, KeyboardAvoidingView } from 'react-native';
import { ScrollView, View } from 'react-native';
import { CommentCreatorView } from 'src/components/comment-creator/comment-creator.view';
import { HeaderLight } from 'src/components/header-light/header-light.view';
import { PostCard } from 'src/components/post-card/post-card.container';
import { bottomTabsHeight } from 'src/utils/dimensions';

const mockPhoto = 'https://www.gastronom.ru/binfiles/images/20170404/b826aef2.jpg';

export const FullPostView = (): React.ReactElement => {
  const [bottomTabsHeightState, setBottomTabsHeight] = useState<number>(bottomTabsHeight);

  useEffect(() => {
    const keyboardDidShowSubscription = Keyboard.addListener('keyboardDidShow', () =>
      setBottomTabsHeight(0),
    );
    const keyboardDidHideSubscription = Keyboard.addListener('keyboardDidHide', () =>
      setBottomTabsHeight(bottomTabsHeight),
    );

    return () => {
      keyboardDidShowSubscription.remove();
      keyboardDidHideSubscription.remove();
    };
  }, []);

  return (
    <KeyboardAvoidingView behavior="padding" style={{ flex: 1 }}>
      <HeaderLight userAvatar={mockPhoto} />
      <View style={{ flexShrink: 1 }}>
        <ScrollView contentContainerStyle={{ paddingTop: 12 }}>
          <PostCard full />
        </ScrollView>
      </View>
      <View style={{ paddingBottom: bottomTabsHeightState + 30, backgroundColor: '#fff' }}>
        <CommentCreatorView onAddComment={() => console.log('hi')} />
      </View>
    </KeyboardAvoidingView>
  );
};
