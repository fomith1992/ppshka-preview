import { styled } from '@shared/config/styled';
import { StyleSheet, View } from 'react-native';
import FastImage from 'react-native-fast-image';
import { screenWidth } from 'src/utils/dimensions';

export const HeaderImage = styled(FastImage, {
  width: screenWidth,
  height: (screenWidth / 4) * 5,
});

export const ScrollContainer = styled(View, {
  ...StyleSheet.absoluteFillObject,
});
