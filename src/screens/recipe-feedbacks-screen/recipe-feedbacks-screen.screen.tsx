import React from 'react';
import ScreenLayout from 'src/components/ScreenLayout';
import { FoodStackParamList } from 'src/navigation/FoodNavigator/FoodNavigator';
import { StackScreenProps } from '@react-navigation/stack';
import { RecipeRatingsList } from 'src/components/ratings-list/recipe-ratings-list.container';
import { HeaderImage, ScrollContainer } from './recipe-feedbacks-screen.style';

export const RecipeFeedbacksScreen = ({
  route: {
    params: { id, image },
  },
}: StackScreenProps<FoodStackParamList, 'RecipeFeedbacks'>): React.ReactElement => {
  return (
    <ScreenLayout safeArea edges={['left', 'right']}>
      <HeaderImage source={{ uri: image }} />
      <ScrollContainer>
        <RecipeRatingsList id={id} />
      </ScrollContainer>
    </ScreenLayout>
  );
};
