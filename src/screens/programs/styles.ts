import { Platform, Text, View } from 'react-native';
import { styled } from '@shared/config/styled';
import { container } from '@shared/config/view';

import { bottomTabsHeight, getScaleSize } from '../../utils/dimensions';
import { font } from '@shared/config/text';
import { color } from '@shared/config/color';
import { ScrollView } from 'react-native-gesture-handler';

export const HeaderContainer = styled(View, {
  marginTop: getScaleSize(50),
  ...Platform.select({
    android: {
      elevation: 1,
    },
    ios: {
      shadowColor: color('black'),
      shadowOffset: {
        width: 0,
        height: 1,
      },
      shadowOpacity: 0.06,
      shadowRadius: 1,
    },
  }),
});

export const HeaderTitle = styled(Text, {
  ...font({ type: 'h1' }),
  color: color('white'),
  marginHorizontal: getScaleSize(15),
  marginBottom: getScaleSize(12),
});

export const SectionTitle = styled(Text, {
  ...font({ type: 'h2' }),
  color: color('white'),
});

export const Container = styled(View, {
  ...container('margin'),
  borderRadius: getScaleSize(15),
  paddingTop: getScaleSize(15),
  marginBottom: getScaleSize(80) + bottomTabsHeight,
});

export const EmptyIconContainer = styled(View, {
  alignSelf: 'center',
});

export const EmptyContainer = styled(View, {
  ...container('margin'),
  flex: 1,
  justifyContent: 'center',
});

export const EmptyTitle = styled(Text, {
  ...font({ type: 'h2' }),
  color: color('gray6'),
  textAlign: 'center',
  paddingTop: getScaleSize(20),
});

export const EmptyDescription = styled(Text, {
  ...font({ type: 'text2', weight: 'light' }),
  textAlign: 'center',
  paddingTop: getScaleSize(12),
});

export const ButtonContainer = styled(View, {
  paddingTop: getScaleSize(20),
});

export const WorkoutsTitle = styled(Text, {
  ...font({ type: 'h2' }),
  paddingBottom: getScaleSize(10),
  color: color('white'),
});

export const SectionWorkoutsContainer = styled(View, {
  marginBottom: getScaleSize(20),
});

export const UpcomingWorkoutsList = ScrollView;

export const ContentContainer = styled(View, {});

export const WorkoutCardContainer = styled(View, {});

export const Spring = styled(View, {
  flex: 1,
  height: getScaleSize(16),
  width: getScaleSize(16),
});

export const ProgramPauseButton = styled(View, {
  marginHorizontal: getScaleSize(38),
  marginTop: getScaleSize(20),
});

export const FooterSpring = styled(View, {
  // flex: 1,
  flexDirection: 'row',
  justifyContent: 'center',
  height: getScaleSize(120),
  width: getScaleSize(16),
});

export const ProgramsTitleContainer = styled(View, {
  marginTop: getScaleSize(20),
});

export const PopupContainer = styled(View, {
  position: 'absolute',
  top: 0,
  bottom: 0,
  left: 0,
  right: 0,
  backgroundColor: color('whiteOpacity50'),
  alignItems: 'center',
  justifyContent: 'center',
});

export const PopupBlock = styled(View, {
  backgroundColor: color('white'),
  width: '90%',
  height: '30%',
  borderRadius: getScaleSize(12),
  paddingTop: getScaleSize(12),
  ...Platform.select({
    android: {
      elevation: 1,
    },
    ios: {
      shadowColor: color('black'),
      shadowOffset: {
        width: 0,
        height: 1,
      },
      shadowOpacity: 0.06,
      shadowRadius: 1,
    },
  }),
});

export const BottomButtons = styled(View, {
  ...container(),
  marginTop: '10%',
});

export const PopUpDescription = styled(Text, {
  ...font({ type: 'text2', weight: 'normal' }),
  color: color('gray10'),
  marginHorizontal: getScaleSize(16),
});

export const PopUpTitle = styled(Text, {
  ...font({ type: 'h2' }),
  color: color('gray10'),
  marginHorizontal: getScaleSize(8),
  textAlign: 'center',
  /* marginBottom: getScaleSize(12), */
});
