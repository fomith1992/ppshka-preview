import React, { useState } from 'react';
import { useFocusEffect } from '@react-navigation/native';

import { format, parse } from 'date-fns';
import { ru } from 'date-fns/locale';

import ScreenLayout from 'src/components/ScreenLayout';
import ProgramCard from 'src/components/program-card';

import {
  Container,
  ContentContainer,
  // ButtonContainer,
  EmptyContainer,
  // EmptyDescription,
  EmptyIconContainer,
  EmptyTitle,
  FooterSpring,
  HeaderContainer,
  HeaderTitle,
  ProgramsTitleContainer,
  Spring,
  SectionWorkoutsContainer,
  WorkoutCardContainer,
  WorkoutsTitle,
  ProgramPauseButton,
  PopupContainer,
  PopupBlock,
  BottomButtons,
  PopUpTitle,
  PopUpDescription,
} from './styles';
// import { Calendar } from 'src/components/calendar/calendar.view';
// import Button from 'src/components/Button';
// import { TrainingIcon30 } from '@shared/ui/icons/training.icon-30';
import WorkoutSmallCard from 'src/components/workout-card-small';
import { useSelector } from 'react-redux';
import { AppState } from 'src/store';
import { programsAPI, workoutAPI } from 'src/api';
import { FlatList } from 'react-native';
import { LoadingIndicator } from 'src/components/ui/Loader/loading-indicator';
import { Program } from 'src/api/programs';
import { WorkoutCalendar } from 'src/api/workouts';
import { TrainingIcon30 } from '@shared/ui/icons/training.icon-30';
import useAsyncFn from 'react-use/lib/useAsyncFn';
import { GradientLayoutView } from '@shared/ui/gradient-layout/gradient-layout.view';
import { ChosenProgramCard } from 'src/components/chosen-program-card/chosen-program-card.container';
import { ListPastWorkouts } from 'src/components/list-workouts-past/list-past-workouts.container';
import { getCacheContent } from 'src/utils/cache-content';
import Button from 'src/components/Button';

function WorkoutEmptyView(/* { onPressChooseWorkoutBtn }: WorkoutEmptyViewProps */): React.ReactElement {
  return (
    <EmptyContainer>
      <EmptyIconContainer>
        <TrainingIcon30 size={55} color="#bfbfbf" />
      </EmptyIconContainer>
      <EmptyTitle>Пока что нет доступных программ</EmptyTitle>
      {/* <EmptyDescription>
        Для отображения календаря выберите хотя бы одну тренировочную программу
      </EmptyDescription>
      <ButtonContainer>
        <Button onPress={onPressChooseWorkoutBtn} title="Выбрать программу" />
      </ButtonContainer> */}
    </EmptyContainer>
  );
}

function UpcomingWorkoutList({
  setPauseProgramsVisible,
}: AvailableWorkoutsListProps): React.ReactElement {
  const access_token = useSelector((state: AppState) => state.session.tokens.accessToken);
  const [workoutList, setWorkoutList] = useState<{
    data: WorkoutCalendar[];
    pause_program: boolean;
  }>({ data: [], pause_program: false });

  const [loadingData, getWorkoutsCalendarList] = useAsyncFn(async () => {
    await workoutAPI
      .getWorkoutsCalendarList({ access_token })
      .then(({ data }) => {
        setWorkoutList(data);
        setPauseProgramsVisible(data.pause_program);
      })
      .catch((e) => {
        console.log('getWorkoutsCalendarList: ', e);
      });
  }, []);

  const [, setPausePrograms] = useAsyncFn(async () => {
    await programsAPI
      .pausePrograms({ access_token })
      .then(getWorkoutsCalendarList)
      .catch(console.warn);
  });

  useFocusEffect(
    React.useCallback(() => {
      getWorkoutsCalendarList();
      return;
    }, []), // eslint-disable-line react-hooks/exhaustive-deps
  );

  if (loadingData.loading) {
    return <LoadingIndicator visible />;
  }

  if (workoutList.data.length === 0) {
    return <></>;
  }

  return (
    <SectionWorkoutsContainer>
      <WorkoutsTitle>Предстоящая тренировка</WorkoutsTitle>
      <FlatList<WorkoutCalendar>
        initialNumToRender={2}
        horizontal={true}
        showsHorizontalScrollIndicator={false}
        data={workoutList.data}
        viewabilityConfig={{ itemVisiblePercentThreshold: 85 }}
        ItemSeparatorComponent={() => <Spring />}
        renderItem={({ item }) => {
          const now = new Date();
          const isToday =
            item.date_start ===
            format(now, 'yyyy-MM-dd', {
              locale: ru,
            });
          if (isToday) {
            getCacheContent({
              url: item.workout.preview_file_path,
              format: 'mp4',
            });
          }
          const availableOnFreeVersion = item.number_of_list < 3;
          return (
            <WorkoutSmallCard
              data={{
                id: item.workout.id,
                calendar_id: item.id,
                // TODO: REMOVE AFTER FIX API
                dateStart: item.date_start,
                name: isToday
                  ? item.workout.name
                  : format(parse(item.date_start, 'yyyy-MM-dd', new Date()), 'd MMMM', {
                      locale: ru,
                    }),
                isToday: isToday,
                isAvailableOnFree: availableOnFreeVersion,
                duration: item.workout.duration,
                photoUri: item.workout.url_image,
              }}
            />
          );
        }}
        keyExtractor={({ id, date_start }) => `${id.toString()}-${date_start}`}
      />
      <ProgramPauseButton>
        <Button
          size="S"
          uppercase
          onPress={setPausePrograms}
          content={'Поставить программу на паузу'}
        />
      </ProgramPauseButton>
    </SectionWorkoutsContainer>
  );
}

interface AvailableWorkoutsListProps {
  setPauseProgramsVisible: (status: boolean) => void;
}

function AvailableWorkoutsList({
  setPauseProgramsVisible,
}: AvailableWorkoutsListProps): React.ReactElement {
  const access_token = useSelector((state: AppState) => state.session.tokens.accessToken);

  const [serverCurrentPage, setServerCurrentPage] = useState(1);
  const [currentPage, setCurrentPage] = useState(1);
  const [pagesCount, setPagesCount] = useState(1);

  const [programsListData, setProgramsListData] = useState<Program[]>([]);

  const [fetchProgramsListDataState, fetchProgramsListData] = useAsyncFn(async () => {
    return await programsAPI
      .getAvailableProgramsList(access_token, currentPage)
      .then(async ({ data: { data } }) => {
        setPagesCount(data.page_cnt);
        if (data.current_page === 1) {
          setProgramsListData(data.data);
        } else if (data.current_page > 1 && data.current_page === serverCurrentPage + 1) {
          setProgramsListData(programsListData.concat(data.data));
          setServerCurrentPage(data.current_page);
          setCurrentPage(data.current_page);
        }
      })
      .catch((e) => {
        console.log('getAvailableProgramsList: ', e);
        /* Alert.alert('Произошла ошибка'); */
      });
  }, [currentPage, serverCurrentPage]);

  useFocusEffect(
    React.useCallback(() => {
      fetchProgramsListData();
      return;
    }, [fetchProgramsListData]),
  );

  if (fetchProgramsListDataState.loading) return <LoadingIndicator visible />;

  return (
    <ContentContainer>
      <FlatList<Program>
        initialNumToRender={2}
        data={programsListData}
        viewabilityConfig={{ itemVisiblePercentThreshold: 85 }}
        ListHeaderComponent={() => (
          <>
            <ChosenProgramCard
              header={<WorkoutsTitle>Ваша тренировочная программа</WorkoutsTitle>}
            />
            <UpcomingWorkoutList setPauseProgramsVisible={setPauseProgramsVisible} />
            <ListPastWorkouts />
            <ProgramsTitleContainer>
              <WorkoutsTitle>Доступные тренировочные программы</WorkoutsTitle>
            </ProgramsTitleContainer>
          </>
        )}
        onEndReached={() => {
          pagesCount > currentPage ? setCurrentPage(serverCurrentPage + 1) : undefined;
        }}
        ItemSeparatorComponent={() => <Spring />}
        ListEmptyComponent={() => <WorkoutEmptyView />}
        renderItem={({ item }) => {
          return (
            <WorkoutCardContainer key={item.id}>
              <ProgramCard
                id={item.id}
                title={item.name}
                description={item.description}
                duration={item.col_days}
                photoUri={item.url_image}
              />
            </WorkoutCardContainer>
          );
        }}
        keyExtractor={({ id }) => id.toString()}
        ListFooterComponent={() => (
          <FooterSpring>
            <LoadingIndicator
              visible={fetchProgramsListDataState.loading && programsListData.length !== 0}
            />
          </FooterSpring>
        )}
        showsVerticalScrollIndicator={false}
      />
    </ContentContainer>
  );
}

const ProgramsScreen = () => {
  const [pauseProgramsVisible, setPauseProgramsVisible] = useState(false);
  const access_token = useSelector((state: AppState) => state.session.tokens.accessToken);

  // const [selectedDate, setSelectedDate] = useState(/* date ??  */ new Date());

  const [, setPlayPrograms] = useAsyncFn(async () => {
    await programsAPI
      .playPrograms({ access_token, date_start: 'today' })
      .then(() => setPauseProgramsVisible(false))
      .catch(console.warn);
  });

  return (
    <ScreenLayout safeArea edges={['left', 'right']}>
      <GradientLayoutView />
      <HeaderContainer>
        <HeaderTitle>Тренировки</HeaderTitle>
        {/* <Calendar selectedDate={selectedDate} setSelectedDate={setSelectedDate} /> */}
      </HeaderContainer>
      <Container>
        <AvailableWorkoutsList setPauseProgramsVisible={setPauseProgramsVisible} />
      </Container>
      {pauseProgramsVisible && (
        <PopupContainer>
          <PopupBlock>
            <PopUpTitle>Тренировочные программы приостановлены</PopUpTitle>
            <PopUpDescription>Восстановить тренировки вы можете в любой момент </PopUpDescription>
            <BottomButtons>
              <Button
                uppercase
                content="Восстановить тренировки"
                onPress={() => setPlayPrograms()}
              />
            </BottomButtons>
          </PopupBlock>
        </PopupContainer>
      )}
    </ScreenLayout>
  );
};

export default ProgramsScreen;
