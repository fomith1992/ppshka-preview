import React, { useRef } from 'react';
import { Image, ScrollView, StatusBar, Text, TouchableOpacity, View } from 'react-native';
import VideoPlayer from 'react-native-video';

import { useNavigation } from '@react-navigation/native';

import Icon from 'src/components/Icon';
import ScreenLayout from 'src/components/ScreenLayout';
import Step from 'src/components/ui/Step';

import { uriMockImage } from './image';
import { styles } from './styles';

const RecipeDetailsScreen = () => {
  const navigation = useNavigation();

  const playerRef = useRef<VideoPlayer>(null);

  return (
    <ScreenLayout safeArea={false}>
      <ScrollView
        bounces={false}
        contentContainerStyle={styles.containerScroll}
        showsVerticalScrollIndicator={false}
      >
        <View>
          <StatusBar barStyle={'light-content'} />
          <VideoPlayer
            source={{
              uri: 'https://drive.google.com/uc?export=download&id=1J3a6NolcZ0bpEL6j_fRTaOKTFT_r-7f5',
            }}
            // videoWidth={300}
            // videoHeight={375}
            // style={{
            //   height: 500
            // }}
            // thumbnail={require('../../../assets/thumbnailVideo1.png')}
            onLoad={(load) => console.log('Load', load)}
            ref={playerRef}
            controls={true}
            // customStyles={{
            //   seekBarProgress: {
            //     backgroundColor: purpleColor,
            //   },
            //   seekBarKnob: {
            //     backgroundColor: purpleColor,
            //   },
            // }}
          />
          {/* <PreLoadedImage
            source={{uri: 'https://i2.wp.com/www.downshiftology.com/wp-content/uploads/2018/12/Shakshuka-19.jpg'}}
            style={styles.image}
          /> */}
          <TouchableOpacity
            style={[styles.buttonHeader, styles.buttonHeaderBack]}
            onPress={() => navigation.goBack()}
          >
            <Icon name="arrowBack" />
          </TouchableOpacity>
          <TouchableOpacity style={[styles.buttonHeader, styles.buttonHeaderFavorites]}>
            <Icon name="favorites" color="#6034A8" size={16} />
          </TouchableOpacity>
        </View>
        <View style={styles.containerContent}>
          <View style={styles.containerBlock}>
            <View style={styles.containerBurnTime}>
              <Icon name="burn" />
              <Text style={styles.textBurn}>225 Ккал</Text>
              <View style={styles.circle} />
              <Icon name="clock" />
              <Text style={styles.textTime}>25 мин</Text>
            </View>
            <Text style={styles.textTitle}>Лобио из красной фасоли</Text>
            <Text style={styles.textSubTitle}>Размер порции: 250г</Text>
            <Text style={styles.textDescription}>
              Лобио из красной фасоли будет отличным гарниром или салатом за постным или праздничным
              ужином. Фасоль получается в меру острая, ароматная благодаря чесноку и специям,
              попробуйте!
            </Text>
            <View style={styles.separator} />
            <View style={styles.containerInfo}>
              <View>
                <Text style={styles.textItemValue}>15 г.</Text>
                <Text style={styles.textItemName}>Белки</Text>
              </View>
              <View>
                <Text style={styles.textItemValue}>45 г.</Text>
                <Text style={styles.textItemName}>Углеводы</Text>
              </View>
              <View>
                <Text style={styles.textItemValue}>22 г.</Text>
                <Text style={styles.textItemName}>Жиры</Text>
              </View>
              <View>
                <View style={styles.containerComplexityIndicators}>
                  <View style={styles.complexityIndicatorActive} />
                  <View style={styles.complexityIndicatorActive} />
                  <View style={styles.complexityIndicatorActive} />
                  <View style={styles.complexityIndicatorActive} />

                  <View style={styles.complexityIndicator} />
                  <View style={styles.complexityIndicator} />
                  <View style={styles.complexityIndicator} />
                </View>
                <Text style={styles.textItemName}>Сложность</Text>
              </View>
            </View>
          </View>
          <View style={styles.containerBlock}>
            <Text style={styles.textSecondTitle}>Ингридиенты</Text>
            <View style={styles.separator} />
            <View style={styles.containerItemIngredients}>
              <Text style={styles.textIngredientsName}>Фасоль красная</Text>
              <Text style={styles.textIngredientsValue}>300 г.</Text>
            </View>
            <View style={styles.containerItemIngredients}>
              <Text style={styles.textIngredientsName}>Хмели-сунели</Text>
              <Text style={styles.textIngredientsValue}>0,5 ч.л.</Text>
            </View>
            <View style={styles.containerItemIngredients}>
              <Text style={styles.textIngredientsName}>Болгарский перец</Text>
              <Text style={styles.textIngredientsValue}>1 шт.</Text>
            </View>
            <View style={styles.containerItemIngredients}>
              <Text style={styles.textIngredientsName}>Перец черный молотый</Text>
              <Text style={styles.textIngredientsValue}>1 щепотка</Text>
            </View>
            <View style={styles.containerItemIngredients}>
              <Text style={styles.textIngredientsName}>Масло подслонечное</Text>
              <Text style={styles.textIngredientsValue}>4 ст.л.</Text>
            </View>
            <View style={styles.containerItemIngredients}>
              <Text style={styles.textIngredientsName}>Соль</Text>
              <Text style={styles.textIngredientsValue}>По вкусу</Text>
            </View>
            <View style={[styles.containerItemIngredients, { marginBottom: 0 }]}>
              <Text style={styles.textIngredientsName}>Кинза</Text>
              <Text style={styles.textIngredientsValue}>1 пучок</Text>
            </View>
          </View>
          <View style={styles.containerBlock}>
            <Text style={styles.textSecondTitle}>Пошаговый рецепт</Text>
            <View style={styles.separator} />
            <Step value={1} style={styles.containerStep} />
            <Text style={styles.textRecipe}>
              Приготовьте продукты. Хочу обратить внимание на то, что болгарский перец не
              обязателен, но на мой взгляд с ним гораздо вкуснее
            </Text>
            <Image style={styles.imageRecipe} source={{ uri: uriMockImage }} />
            <Step value={2} style={styles.containerStep} />
            <Text style={styles.textRecipe}>
              Красную фасоль переберите от порченных зерен. Промойте под проточной водой, замочите
              на несколько часов в холодной воде. После воду слейте, наберите свежей и поставьте
              вариться. Солить фасоль можно в конце варки. Время варки фасоли индивидуально, зависит
              от сорта красной фасоли, пробуйте
            </Text>
            <Image
              style={styles.imageRecipe}
              source={{
                uri: 'https://i2.wp.com/www.downshiftology.com/wp-content/uploads/2018/12/Shakshuka-19.jpg',
              }}
            />
          </View>
        </View>
      </ScrollView>
    </ScreenLayout>
  );
};

export default RecipeDetailsScreen;
