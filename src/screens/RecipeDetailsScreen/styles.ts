import { StyleSheet } from 'react-native';

import { font12m, font12r, font20m, font30m } from 'src/commonStyles/fonts';
import { bottomTabsHeight, getScaleSize } from 'src/utils/dimensions';

import { font14m, font14r, font16m } from '../../commonStyles/fonts';
import { screenWidth } from '../../utils/dimensions';

export const styles = StyleSheet.create({
  containerScroll: {
    backgroundColor: '#FAFAFA',
    flexGrow: 1,
    paddingBottom: getScaleSize(15) + bottomTabsHeight,
  },
  image: {
    height: getScaleSize(222),
  },
  containerContent: {
    // top: -getScaleSize(12),
  },
  buttonHeader: {
    position: 'absolute',
    width: getScaleSize(30),
    height: getScaleSize(30),
    borderRadius: getScaleSize(8),
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonHeaderBack: {
    top: getScaleSize(50),
    left: getScaleSize(15),
  },
  buttonHeaderFavorites: {
    top: getScaleSize(50),
    right: getScaleSize(15),
  },
  containerBlock: {
    borderRadius: getScaleSize(12),
    paddingVertical: getScaleSize(20),
    backgroundColor: 'white',
    shadowColor: '#000',
    shadowOffset: {
      width: 4,
      height: 0,
    },
    shadowOpacity: 0.05,
    shadowRadius: 50,
    elevation: 1,
    marginBottom: getScaleSize(20),
  },
  containerBurnTime: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: getScaleSize(14),
    paddingHorizontal: getScaleSize(35),
  },
  textBurn: {
    ...font12m,
    marginLeft: getScaleSize(5),
    color: '#EF4923',
  },
  circle: {
    width: getScaleSize(4),
    height: getScaleSize(4),
    borderRadius: getScaleSize(4) / 2,
    backgroundColor: '#D9D9D9',
    marginHorizontal: getScaleSize(10),
  },
  textTime: {
    ...font12m,
    marginLeft: getScaleSize(5),
    color: '#6034A8',
  },
  textTitle: {
    ...font30m,
    marginBottom: getScaleSize(5),
    marginHorizontal: getScaleSize(35),
  },
  textSecondTitle: {
    ...font20m,
    color: '#262626',
    marginHorizontal: getScaleSize(35),
  },
  textSubTitle: {
    ...font12r,
    color: '#8C8C8C',
    marginBottom: getScaleSize(20),
    marginHorizontal: getScaleSize(35),
  },
  textDescription: {
    ...font14r,
    lineHeight: getScaleSize(22),
    color: '#8C8C8C',
    marginHorizontal: getScaleSize(35),
  },
  separator: {
    height: 1,
    width: '100%',
    backgroundColor: '#F5F5F5',
    marginVertical: getScaleSize(20),
  },
  containerInfo: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginHorizontal: getScaleSize(35),
  },
  textItemValue: {
    ...font16m,
    color: '#262626',
  },
  textItemName: {
    ...font12r,
    color: '#8C8C8C',
    marginTop: getScaleSize(2),
  },
  containerComplexityIndicators: {
    flexDirection: 'row',
    marginBottom: getScaleSize(3),
  },
  complexityIndicator: {
    width: getScaleSize(3),
    height: getScaleSize(15),
    marginRight: getScaleSize(5),
    backgroundColor: '#F0F0F0',
  },
  complexityIndicatorActive: {
    width: getScaleSize(3),
    height: getScaleSize(15),
    marginRight: getScaleSize(5),
    backgroundColor: '#FF612F',
  },
  containerItemIngredients: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginHorizontal: getScaleSize(35),
    marginBottom: getScaleSize(15),
  },
  textIngredientsName: {
    ...font14r,
    lineHeight: getScaleSize(22),
    color: '#262626',
  },
  textIngredientsValue: {
    ...font14m,
    lineHeight: getScaleSize(22),
    color: '#262626',
  },
  containerStep: {
    marginBottom: getScaleSize(8),
    marginHorizontal: getScaleSize(35),
  },
  textRecipe: {
    ...font14r,
    lineHeight: getScaleSize(21),
    color: '#262626',
    marginBottom: getScaleSize(12),
    marginHorizontal: getScaleSize(35),
  },
  imageRecipe: {
    marginHorizontal: getScaleSize(35),
    height: getScaleSize(140),
    width: screenWidth - getScaleSize(70),
    marginBottom: getScaleSize(20),
    borderRadius: getScaleSize(12),
  },
});
