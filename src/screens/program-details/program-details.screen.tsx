import React from 'react';

import { useNavigation } from '@react-navigation/native';

import Icon from 'src/components/Icon';
import ScreenLayout from 'src/components/ScreenLayout';

import {
  BackButtonContainer,
  BottomSpring,
  BubbleContainer,
  // BubbleInfoRow,
  // BubbleInfoRowTitle,
  // BubbleInfoRowValue,
  // BubbleInfoTitle,
  ButtonContainer,
  ContentContainer,
  Delimeter,
  DescriptionMain,
  // FavoriteButtonContainer,
  ScrollContainer,
  // SubTitleMain,
  TabsInfoContainer,
  TitleMain,
  VideoDemo,
  VideoDemoContainer,
} from './styles';
import Button from 'src/components/Button';
import { StackScreenProps } from '@react-navigation/stack';
import { WorkoutStackParamList } from 'src/navigation/WorkoutNavigator/WorkoutNavigator';
import useAsync from 'react-use/lib/useAsync';
import { programsAPI } from 'src/api';
import { useSelector } from 'react-redux';
import { AppState } from 'src/store';
import { Alert } from 'react-native';
import { LoadingIndicator } from 'src/components/ui/Loader/loading-indicator';
import useAsyncFn from 'react-use/lib/useAsyncFn';
import { TabInfo } from 'src/components/tabs/info-tab.view copy';
import { TabComplexity } from 'src/components/tabs/complexity-tab.view';
import { startProgramEvent } from 'src/firebase/analytics/analytics-program/start-program-event';

// interface BubleInfoProps {
//   title: string;
//   data: Array<{
//     title: string;
//     value: string;
//   }>;
// }

// function BubleInfo({ title, data }: BubleInfoProps) {
//   return (
//     <BubbleContainer>
//       <BubbleInfoTitle>{title}</BubbleInfoTitle>
//       <Delimeter />
//       {data.map((value) => (
//         <BubbleInfoRow key={`key-${title}-${value.value}`}>
//           <BubbleInfoRowTitle>{value.title}</BubbleInfoRowTitle>
//           <BubbleInfoRowValue>{value.value}</BubbleInfoRowValue>
//         </BubbleInfoRow>
//       ))}
//     </BubbleContainer>
//   );
// }

const WorkoutDetailsScreen = ({
  route,
}: StackScreenProps<WorkoutStackParamList, 'ProgramDetails'>) => {
  const programId = route.params.id;
  const navigation = useNavigation();
  const access_token = useSelector((state: AppState) => state.session.tokens.accessToken);

  const programApi = useAsync(async () => {
    return await programsAPI
      .getProgramById({ id: programId, access_token })
      .then(async (data) => {
        return data;
      })
      .catch((e) => {
        console.log(e);
        Alert.alert('Произошла ошибка');
      });
  }, [programId]);
  const [subscribeState, subscribeToProgram] = useAsyncFn(
    async (start_day: 'tomorrow' | 'today') => {
      return await programsAPI
        .subscribeToProgram({
          access_token,
          program_id: programId,
          date_start: start_day,
        })
        .then(async () => {
          startProgramEvent(programId.toString());
          navigation.goBack();
        })
        .catch((e) => {
          console.log(e);
          Alert.alert('Произошла ошибка');
        });
    },
    [programId],
  );
  function handlePressSubscribeProgram(): void {
    Alert.alert('Внимание', 'Когда вы хотите начать тренировку?', [
      {
        text: 'Отмена',
        style: 'cancel',
      },
      {
        text: 'Сегодня',
        onPress: () => subscribeToProgram('today'),
      },
      {
        text: 'Завтра',
        onPress: () => subscribeToProgram('tomorrow'),
      },
    ]);
  }
  if (programApi.loading || programApi.value == null || subscribeState.loading)
    return <LoadingIndicator visible />;
  const videoUri = programApi.value.data.data.url_image;
  const name = programApi.value.data.data.name;
  const description = programApi.value.data.data.description;
  const duration = programApi.value.data.data.col_days;
  const complexity = programApi.value.data.data.program_complexity;
  return (
    <ScreenLayout safeArea={false}>
      <ScrollContainer bounces={false} showsVerticalScrollIndicator={false}>
        <VideoDemoContainer>
          <VideoDemo
            source={{
              uri: videoUri,
            }}
          />
          <BackButtonContainer onPress={() => navigation.goBack()}>
            <Icon name="arrowBack" />
          </BackButtonContainer>
          {/* <FavoriteButtonContainer>
            <Icon name="favorites" color="gray9" size={16} />
          </FavoriteButtonContainer> */}
        </VideoDemoContainer>
        <ContentContainer>
          <BubbleContainer>
            <TitleMain>{name}</TitleMain>
            {/* <SubTitleMain>Упражнение для ног</SubTitleMain> */}
            <DescriptionMain>{description}</DescriptionMain>
            <ButtonContainer>
              <Button uppercase onPress={handlePressSubscribeProgram} content="Выбрать программу" />
            </ButtonContainer>
            <Delimeter />
            <TabsInfoContainer>
              <TabInfo title={`${duration} дней`} description="Время программы" />
              {/* <TabInfo title="5000 р" description="Стоимость порграммы" /> */}
              <TabComplexity current={complexity} total={7} description="Сложность упражнений" />
            </TabsInfoContainer>
          </BubbleContainer>
          {/* <BubleInfo
            title="Общая информация"
            data={[
              {
                title: 'Время одной тренировки',
                value: '15 мин.',
              },
              {
                title: 'Количество потраченных ккал',
                value: '~450 ккал.',
              },
            ]}
          /> */}
        </ContentContainer>
        <BottomSpring />
      </ScrollContainer>
    </ScreenLayout>
  );
};

export default WorkoutDetailsScreen;
