import { Row } from '@screens/recipes/ui/header.style';
import { color } from '@shared/config/color';
import { styled } from '@shared/config/styled';
import { font } from '@shared/config/text';
import { container } from '@shared/config/view';
import { Platform, Text, TextInput, TouchableOpacity, View } from 'react-native';
import PreLoadedImage from 'src/components/PreLoadedImage';
import { getScaleSize, windowWidth } from 'src/utils/dimensions';
import { font14r } from '../../../commonStyles/fonts';

export const Container = styled(View, {
  ...container(),
  marginTop: getScaleSize(16),
  paddingTop: getScaleSize(15),
  backgroundColor: color('white'),
  borderRadius: getScaleSize(15),
  ...Platform.select({
    android: {
      elevation: 1,
    },
    ios: {
      shadowColor: color('black'),
      shadowOffset: {
        width: 0,
        height: 1,
      },
      shadowOpacity: 0.06,
      shadowRadius: 1,
    },
  }),
});

export const TitileContainer = styled(View, {
  flexDirection: 'row',
  alignItems: 'center',
});

export const TitleRow = styled(Row, {
  paddingRight: getScaleSize(20),
  paddingLeft: getScaleSize(17),
  alignItems: 'flex-start',
});

export const TitleContainer = styled(View, {
  ...container('padding'),
  flexDirection: 'row',
  justifyContent: 'space-between',
});

export const Title = styled(Text, {
  ...font({ type: 'h2', weight: 'normal' }),
  marginLeft: getScaleSize(4),
});

export const DeleteText = styled(Text, {
  ...font({ type: 'text3' }),
  color: color('gray6'),
  marginRight: getScaleSize(4),
});

export const LineButton = styled(TouchableOpacity, {
  flexDirection: 'row',
  alignItems: 'center',
  justifyContent: 'center',
});

export const CurrentWeight = styled(Text, {
  ...font({ type: 'text1', weight: 'normal' }),
  color: color('tertiary'),
});

export const CurrentWeightInfo = styled(Text, {
  ...font({ type: 'text1', weight: 'normal' }),
  color: color('gray10'),
});

export const CurrentWeightDescription = styled(Text, {
  ...font({ type: 'text4', weight: 'normal' }),
  color: color('gray7'),
});

export const CurrentWeightTitle = styled(Text, {
  ...font({ type: 'text3', weight: 'normal' }),
  color: color('gray10'),
  marginTop: getScaleSize(12),
});

export const LeftContainer = styled(View, {
  marginTop: getScaleSize(8),
  marginLeft: getScaleSize(15),
});

export const ProgressChartContainer = styled(View, {});

export const InfoContainer = styled(View, {
  position: 'absolute',
  top: 0,
  bottom: 0,
  left: 0,
  right: 0,
  justifyContent: 'center',
  alignItems: 'center',
});

export const ChartItemContainer = styled(View, {});

export const ChartItemView = styled(
  View,
  (props: { height: number; active: boolean }) =>
    ({
      width: windowWidth / 8,
      height: getScaleSize(props.height),
      backgroundColor: color(props.active ? 'primary' : 'white'),
      borderRadius: 8,
      borderWidth: props.active ? undefined : 1,
      borderColor: color(props.active ? 'white' : 'gray4'),
      justifyContent: 'center',
      alignItems: 'center',
    } as const),
);

export const ChartItemDescription = styled(Text, {
  ...font({ type: 'text3', weight: 'normal' }),
  color: color('gray7'),
  marginTop: getScaleSize(12),
  textAlign: 'center',
});

export const ChartTextTitle = styled(
  Text,
  (props: { active: boolean }) =>
    ({
      ...font({ type: 'text4', weight: 'normal' }),
      color: color(props.active ? 'white' : 'gray10'),
    } as const),
);

export const ChartTextDescription = styled(Text, {
  ...font({ type: 'text4', weight: 'normal' }),
  color: color('white'),
});

export const Input = styled(TextInput, {
  ...font14r,
  paddingHorizontal: getScaleSize(8),
});

export const InputContainer = styled(View, {
  flex: 1,
  flexDirection: 'row',
  alignItems: 'center',
  backgroundColor: color('white'),
  margin: 1,
  borderRadius: 5,
  overflow: 'hidden',
});

export const CardContainer = styled(View, {
  height: 37, // don`t give need effect, if use getScaleSize
  alignItems: 'center',
  marginTop: getScaleSize(15),
  borderRadius: 5,
  overflow: 'hidden',
});

export const ChartContainer = styled(View, {
  marginTop: getScaleSize(5),
});

export const ButtonContainer = styled(View, {
  alignItems: 'center',
});

export const ImageContainer = styled(View, {
  width: '100%',
  height: getScaleSize(340),
});

export const EmptyCardButton = styled(TouchableOpacity, {
  flexDirection: 'row',
});

export const Image = styled(PreLoadedImage, {
  flex: 1,
  borderBottomRightRadius: getScaleSize(15),
  borderBottomLeftRadius: getScaleSize(15),
});

export const EmptyCardText = styled(Text, {
  ...font({ type: 'text1', weight: 'normal' }),
  color: color('primary'),
  marginHorizontal: getScaleSize(12),
  marginBottom: getScaleSize(12),
});
