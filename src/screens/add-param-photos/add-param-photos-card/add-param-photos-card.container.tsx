import React from 'react';
import ImageCropPicker from 'react-native-image-crop-picker';
import { AddParamsPhotoCardView } from './add-param-photos-card.view';

interface MeasurementsCardProps {
  cardName: string;
  image: {
    base64: string | null;
    image: string | null;
  } | null;
  setPhoto: (data: { base64: string | null; image: string | null } | null) => void;
}

export const AddParamsPhotoCard = ({
  cardName,
  image,
  setPhoto,
}: MeasurementsCardProps): React.ReactElement => {
  const onUploadPhoto = () => {
    ImageCropPicker.openPicker({
      width: 800,
      height: 1000,
      cropping: true,
      mediaType: 'photo',
      includeBase64: true,
    })
      .then(({ data, path }) => {
        setPhoto({ base64: data ?? null, image: path });
      })
      .catch((e) => {
        console.log('Error set avatar', e);
      });
  };

  return (
    <AddParamsPhotoCardView
      cardName={cardName}
      image={image}
      setPhoto={onUploadPhoto}
      onDelImage={() => setPhoto(null)}
    />
  );
};
