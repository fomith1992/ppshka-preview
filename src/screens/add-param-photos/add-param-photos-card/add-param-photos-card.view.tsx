import { Bottom } from '@screens/recipes/ui/header.style';
import React from 'react';
import Icon from 'src/components/Icon';
import { hitSlopParams } from 'src/utils/hit-slop-params';
import {
  Container,
  Title,
  ChartContainer,
  ButtonContainer,
  EmptyCardButton,
  EmptyCardText,
  ImageContainer,
  Image,
  TitleContainer,
  LineButton,
  DeleteText,
} from './add-param-photos-card.style';

export interface CardItemProps {
  placeholder: string;
  value: string;
  onChange: (value: string) => void;
}

interface AddParamsPhotoCardViewProps {
  cardName: string;
  image: {
    base64: string | null;
    image: string | null;
  } | null;
  setPhoto: () => void;
  onDelImage: () => void;
}

export const AddParamsPhotoCardView = ({
  cardName,
  image,
  setPhoto,
  onDelImage,
}: AddParamsPhotoCardViewProps): React.ReactElement => {
  return (
    <Container>
      <TitleContainer>
        <Title>{cardName}</Title>
        {image?.image && (
          <LineButton onPress={onDelImage} hitSlop={hitSlopParams('L')}>
            <DeleteText>Удалить</DeleteText>
            <Icon color="gray6" name="trash" />
          </LineButton>
        )}
      </TitleContainer>
      <ChartContainer>
        {image?.image == null ? (
          <ButtonContainer>
            <Bottom mb={12} />
            <EmptyCardButton onPress={setPhoto} hitSlop={hitSlopParams('L')}>
              <EmptyCardText>Добавить фото</EmptyCardText>
            </EmptyCardButton>
            <Bottom mb={16} />
          </ButtonContainer>
        ) : (
          <ImageContainer>
            <Bottom mb={12} />
            <Image source={{ uri: image?.image }} />
          </ImageContainer>
        )}
      </ChartContainer>
    </Container>
  );
};
