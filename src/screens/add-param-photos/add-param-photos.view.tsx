import React from 'react';
import {
  ButtonContainer,
  HeaderContainer,
  HeaderDescription,
  HeaderTitle,
  ScrollContainer,
  styles,
} from './add-param-photos.style';
import { AddParamsPhotoCard } from './add-param-photos-card/add-param-photos-card.container';
import { GradientLayoutHeaderView } from '@shared/ui/gradient-layout/gradient-layout-header.view';
import { KeyboardAvoidingView, Platform, View } from 'react-native';
import Button from 'src/components/Button';
import ScreenLayout from 'src/components/ScreenLayout';

interface AddParamPhotosViewProps {
  photoFron: {
    base64: string | null;
    image: string | null;
  } | null;
  photoBack: {
    base64: string | null;
    image: string | null;
  } | null;
  photoSide: {
    base64: string | null;
    image: string | null;
  } | null;
  setPhotoFron: (
    data: {
      base64: string | null;
      image: string | null;
    } | null,
  ) => void;
  setPhotoBack: (
    data: {
      base64: string | null;
      image: string | null;
    } | null,
  ) => void;
  setPhotoSide: (
    data: {
      base64: string | null;
      image: string | null;
    } | null,
  ) => void;
  onSubmit: () => void;
}

export const AddParamPhotosView = ({
  photoFron,
  photoBack,
  photoSide,
  setPhotoFron,
  setPhotoBack,
  setPhotoSide,
  onSubmit,
}: AddParamPhotosViewProps): React.ReactElement => {
  return (
    <ScreenLayout
      safeArea
      style={{
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
      }}
      edges={['bottom']}
    >
      <KeyboardAvoidingView
        style={{ flex: 1 }}
        behavior={Platform.OS === 'ios' ? 'padding' : 'padding'}
      >
        <View style={{ flex: 1 }}>
          <GradientLayoutHeaderView>
            <HeaderContainer>
              <HeaderTitle>Фото</HeaderTitle>
              <HeaderDescription>
                Добавьте пожалуйста фотографии, чтобы вы могли наблюдать результат не только по
                цифрам но и визуально
              </HeaderDescription>
            </HeaderContainer>
          </GradientLayoutHeaderView>
          <ScrollContainer contentContainerStyle={styles.scrollContainer}>
            <AddParamsPhotoCard cardName="Вид спереди" image={photoFron} setPhoto={setPhotoFron} />
            <AddParamsPhotoCard cardName="Вид сзади" image={photoBack} setPhoto={setPhotoBack} />
            <AddParamsPhotoCard cardName="Вид сбоку" image={photoSide} setPhoto={setPhotoSide} />
            <ButtonContainer>
              <Button
                disabled={!photoFron?.base64 && !photoBack?.base64 && !photoSide?.base64}
                uppercase
                onPress={onSubmit}
                content="Прикрепить фото"
              />
            </ButtonContainer>
          </ScrollContainer>
        </View>
      </KeyboardAvoidingView>
    </ScreenLayout>
  );
};
