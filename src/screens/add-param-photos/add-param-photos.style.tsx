import { color } from '@shared/config/color';
import { styled } from '@shared/config/styled';
import { font } from '@shared/config/text';
import { Platform, StyleSheet, Text, View } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { bottomTabsHeight, getScaleSize } from 'src/utils/dimensions';

export const HeaderContainer = styled(View, {
  paddingTop: getScaleSize(6),
  paddingBottom: getScaleSize(24),
  alignItems: 'center',
  ...Platform.select({
    android: {
      elevation: 1,
    },
    ios: {
      shadowColor: color('black'),
      shadowOffset: {
        width: 0,
        height: 1,
      },
      shadowOpacity: 0.06,
      shadowRadius: 1,
    },
  }),
});

export const ScrollViewBlock = styled(ScrollView, {
  paddingTop: getScaleSize(4),
});

export const HeaderTitle = styled(Text, {
  ...font({ type: 'h1' }),
  color: color('white'),
  marginHorizontal: getScaleSize(15),
  marginTop: getScaleSize(16),
});

export const HeaderDescription = styled(Text, {
  ...font({ type: 'text2' }),
  color: color('white'),
  marginHorizontal: getScaleSize(15),
  marginTop: getScaleSize(16),
  marginBottom: getScaleSize(12),
  textAlign: 'center',
});

export const styles = StyleSheet.create({
  scrollContainer: {
    paddingBottom: bottomTabsHeight + getScaleSize(30),
  },
});
export const ScrollContainer = styled(ScrollView, {
  marginTop: -getScaleSize(45),
});

export const ButtonContainer = styled(View, {
  marginHorizontal: getScaleSize(15),
  marginTop: getScaleSize(15),
});
