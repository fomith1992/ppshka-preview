import { useNavigation, CommonActions } from '@react-navigation/core';
import React, { useState } from 'react';
import { Alert } from 'react-native';
import { useSelector } from 'react-redux';
import useAsyncFn from 'react-use/lib/useAsyncFn';
import { measurementsAPI } from 'src/api';
import { LoadingIndicator } from 'src/components/ui/Loader/loading-indicator';
import { AppState } from 'src/store';
import { AddParamPhotosView } from './add-param-photos.view';

export const AddParamPhotos = (): React.ReactElement => {
  const userId = useSelector((state: AppState) => state.session.user.id);
  const navigation = useNavigation();

  const [photoFron, setPhotoFron] = useState<{
    base64: string | null;
    image: string | null;
  } | null>(null);
  const [photoBack, setPhotoBack] = useState<{
    base64: string | null;
    image: string | null;
  } | null>(null);
  const [photoSide, setPhotoSide] = useState<{
    base64: string | null;
    image: string | null;
  } | null>(null);

  const [updateUserPhotoStatus, onUpdateUserPhoto] = useAsyncFn(async () => {
    if (userId) {
      return await measurementsAPI
        .setMeasurementsPhotos({
          id: userId,
          img_behind: photoBack && photoBack.base64,
          img_front: photoFron && photoFron.base64,
          img_sideways: photoSide && photoSide.base64,
        })
        .then(async (data) => {
          if (data.data.status === 'ok') {
            navigation.dispatch(
              CommonActions.reset({
                index: 0,
                routes: [
                  {
                    name: 'TabNavigator',
                    params: {
                      screen: 'Dashboard',
                      initial: false,
                      params: {
                        initial: false,
                        screen: 'Dashboard',
                        params: {},
                      },
                    },
                  },
                ],
              }),
            );
          }
        })
        .catch(() => Alert.alert('Произошла ошибка'));
    }
  }, [photoFron, photoBack, photoSide, userId]);

  return (
    <>
      <AddParamPhotosView
        photoFron={photoFron}
        photoBack={photoBack}
        photoSide={photoSide}
        setPhotoFron={setPhotoFron}
        setPhotoBack={setPhotoBack}
        setPhotoSide={setPhotoSide}
        onSubmit={onUpdateUserPhoto}
      />
      <LoadingIndicator visible={updateUserPhotoStatus.loading} />
    </>
  );
};
