import React from 'react';
import { View } from 'react-native';

import { onLogout } from 'src/api/auth0';
import Button from 'src/components/Button';

import { styles } from './styles';

const FavoritesScreen = () => {
  return (
    <View style={styles.container}>
      <Button uppercase content="Logout" onPress={onLogout} />
    </View>
  );
};

export default FavoritesScreen;
