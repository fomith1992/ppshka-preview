import { StyleSheet } from 'react-native';

import { font12b, font12m, font12r, font35m } from 'src/commonStyles/fonts';

import { getScaleSize, screenHeight, screenWidth } from '../../utils/dimensions';

export const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
  },
  image: {
    position: 'absolute',
    bottom: 0,
    width: screenWidth,
    height: screenHeight,
  },
  containerContent: {
    justifyContent: 'flex-end',
    paddingHorizontal: getScaleSize(15),
    paddingBottom: getScaleSize(38),
    flexGrow: 1,
  },
  textTitle: {
    ...font35m,
    color: 'white',
    marginBottom: getScaleSize(35),
  },
  containerButton: {
    flexDirection: 'row',
    height: getScaleSize(60),
    alignItems: 'center',
    backgroundColor: 'white',
    borderRadius: 5,
    justifyContent: 'center',
    marginBottom: getScaleSize(10),
  },
  textButtonSignUp: {
    ...font12b,
    color: 'white',
    textTransform: 'uppercase',
  },
  textLoginMethod: {
    ...font12r,
    lineHeight: getScaleSize(14.32),
    color: '#0B0B0A',
  },
  containerIcon: {
    position: 'absolute',
    left: getScaleSize(40),
  },
  containerLoginBottom: {
    flexDirection: 'row',
    alignSelf: 'center',
    marginTop: getScaleSize(20),
  },
  textLogin: {
    ...font12r,
    lineHeight: getScaleSize(14.32),
    color: 'white',
  },
  containerTextLoginBold: {
    borderBottomWidth: 1,
    borderColor: 'white',
  },
  textLoginBold: {
    ...font12m,
    lineHeight: getScaleSize(14.32),
    color: 'white',
  },
  containerSeparator: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: getScaleSize(25),
  },
  line: {
    height: 1,
    backgroundColor: 'white',
    flex: 1,
  },
  textSeparator: {
    ...font12r,
    lineHeight: getScaleSize(14.16),
    marginHorizontal: getScaleSize(14),
    color: 'white',
    opacity: 0.7,
  },
});
