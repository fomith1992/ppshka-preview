import React from 'react';
import { Image, Platform, Text, View } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';

import { onLogin } from 'src/api/auth0';
import Icon from 'src/components/Icon';
import { getScaleSize } from 'src/utils/dimensions';

import { styles } from './styles';

const LoginScreen = () => {
  return (
    <View style={styles.container}>
      <Image
        source={require('../../../assets/login/girl-1.png')}
        resizeMode="cover"
        style={styles.image}
      />
      <View style={styles.containerContent}>
        <Text style={styles.textTitle}>ПП-шка — первая социальная сеть, в которой худеют</Text>
        <TouchableOpacity
          style={[
            styles.containerButton,
            { backgroundColor: '#FFA826', marginBottom: getScaleSize(25) },
          ]}
        >
          <Text style={styles.textButtonSignUp}>Зарегистрироваться</Text>
        </TouchableOpacity>
        <View style={styles.containerSeparator}>
          <View style={styles.line} />
          <Text style={styles.textSeparator}>Или</Text>
          <View style={styles.line} />
        </View>
        {Platform.OS === 'ios' && (
          <TouchableOpacity style={[styles.containerButton, { backgroundColor: '#0B0B0A' }]}>
            <View style={styles.containerIcon}>
              <Icon name="apple" />
            </View>
            <Text style={[styles.textLoginMethod, { color: 'white' }]}>
              Войти с помощью Apple ID
            </Text>
          </TouchableOpacity>
        )}
        <TouchableOpacity style={styles.containerButton} onPress={() => onLogin('google')}>
          <View style={styles.containerIcon}>
            <Icon name="google" />
          </View>
          <Text style={styles.textLoginMethod}>Войти с помощью Google</Text>
        </TouchableOpacity>
        <View style={styles.containerLoginBottom}>
          <Text style={styles.textLogin}>{'У меня уже есть аккаунт. '}</Text>
          <TouchableOpacity
            style={styles.containerTextLoginBold}
            onPress={() => console.log('Sign in')}
          >
            <Text style={styles.textLoginBold}>Войти</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

export default LoginScreen;
