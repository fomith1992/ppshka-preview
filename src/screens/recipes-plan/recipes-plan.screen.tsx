import { StackScreenProps } from '@react-navigation/stack';
import React from 'react';
import ScreenLayout from 'src/components/ScreenLayout';
import { MealPlanParamList } from 'src/navigation/meal-plan-navigator/meal-plan-navigator';
import { RecipesPlan } from './recipes-plan.container';

export const RecipesPlanScreen = ({
  route: { params },
}: StackScreenProps<MealPlanParamList, 'MealPlan'>) => {
  return (
    <ScreenLayout edges={['bottom']}>
      <RecipesPlan date={params ? params.date : undefined} />
    </ScreenLayout>
  );
};
