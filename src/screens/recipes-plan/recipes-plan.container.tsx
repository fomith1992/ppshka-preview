import { useFocusEffect, useNavigation } from '@react-navigation/core';
import { add } from 'date-fns';
import { format } from 'date-fns/esm';
import React, { useState } from 'react';
import { Alert } from 'react-native';
import { useSelector } from 'react-redux';
import useAsyncFn from 'react-use/lib/useAsyncFn';
import { addRecipeToBasketEvent } from 'src/firebase/analytics/analytics-food-basket/add-recipe-to-food-basket';
import { delRecipeToPlanEvent } from 'src/firebase/analytics/analytics-recipes-plan/del-recipe-to-plan-event';
import { foodBasketAPI, meelAPI } from 'src/api';
import { TMeetItem } from 'src/api/meal-plan';
import { AvailableInPropopUp } from 'src/components/calendar/calendar.view';
import { AppState } from 'src/store';
import { RecipesPlanView } from './recipes-plan.view';

interface RecipesPlanProps {
  date?: Date;
}

export const RecipesPlan = ({ date }: RecipesPlanProps): React.ReactElement => {
  const isPro = useSelector((state: AppState) => state.session.user.is_pro);
  const categories = useSelector((state: AppState) => state.session.planCategories);
  const [popUpVisible, setPopUpVisible] = useState(false);
  const [selectedDate, setSelectedDate] = useState(date ?? new Date());
  const [mealData, setMealData] = useState<TMeetItem[]>([]);
  const access_token = useSelector((state: AppState) => state.session.tokens.accessToken);
  const navigation = useNavigation();

  const [dontBuyIngredientsLenght, setDontBuyIngredientsLenght] = useState<number>(0);
  const [isBasketClear, setIsBasketClear] = useState<boolean>(true);

  const [, onGetDontBuyIngredients] = useAsyncFn(async () => {
    return await foodBasketAPI
      .getDontBuyIngredients({ access_token })
      .then(async (data) => {
        setDontBuyIngredientsLenght(data.data.data.length);
        await foodBasketAPI
          .getCheckList({
            access_token,
          })
          .then(async ({ data }) => {
            setIsBasketClear(data.data.length === 0);
          })
          .catch((e) => {
            console.log(e);
            Alert.alert('Произошла ошибка');
          });
      })
      .catch(() => Alert.alert('Произошла ошибка'));
  }, [selectedDate, mealData]);

  const [, onGetMeelDataStatus] = useAsyncFn(async () => {
    const parseDate = format(selectedDate, 'y-MM-dd');
    return await meelAPI
      .getMeelData(access_token, parseDate)
      .then(({ data }) => {
        setMealData(data.data);
      })
      .catch((e) => {
        console.log('onGetMeelDataStatus: ', e);
        Alert.alert('Произошла ошибка');
      });
  }, [selectedDate]);

  useFocusEffect(
    React.useCallback(() => {
      onGetDontBuyIngredients();
      onGetMeelDataStatus();
      return;
    }, [JSON.stringify(mealData), selectedDate]), // eslint-disable-line react-hooks/exhaustive-deps
  );

  const [, onDeleteMeel] = useAsyncFn(
    async (id: number) => {
      return await meelAPI
        .delMeelData(access_token, id)
        .then(async () => {
          delRecipeToPlanEvent(id.toString() ?? null);
          const parseDate = format(selectedDate, 'y-MM-dd');
          await meelAPI
            .getMeelData(access_token, parseDate)
            .then(({ data }) => {
              setMealData(data.data);
            })
            .catch((e) => {
              console.log('onDeleteMeel: ', e);
              Alert.alert('Произошла ошибка');
            });
        })
        .catch(() => Alert.alert('Произошла ошибка'));
    },
    [selectedDate],
  );

  const [, onAddToFoodBasket] = useAsyncFn(
    async (date: Date) => {
      if (mealData.length > 0) {
        const parseDate = format(date, 'y-MM-dd');
        return await foodBasketAPI
          .addToFoodBasket({ access_token, date: parseDate })
          .then(async () => {
            onGetMeelDataStatus();
          })
          .catch(() => Alert.alert('Произошла ошибка'));
      } else {
        Alert.alert(
          'Внимание!',
          'Для того, чтобы добавить план питания в продуктовую корзину, должны быть выбраны все приемы пищи',
        );
      }
    },
    [selectedDate, mealData],
  );

  const [, onDeleteToFoodBasket] = useAsyncFn(
    async (date: Date) => {
      const parseDate = format(date, 'y-MM-dd');
      return await foodBasketAPI
        .deleteFoodBasket({ access_token, date: parseDate })
        .then(async () => {
          onGetMeelDataStatus();
        })
        .catch(() => Alert.alert('Произошла ошибка'));
    },
    [selectedDate],
  );

  const [, onAddRecipeToBasket] = useAsyncFn(
    async (data: { recipe_id: number; col: number; meal_plan_id: number }) => {
      return await foodBasketAPI
        .addRecipeToBasket({
          access_token,
          recipe_id: data.recipe_id,
          col: data.col,
          meal_plan_id: data.meal_plan_id,
        })
        .then(async () => {
          addRecipeToBasketEvent(data.recipe_id.toString());
          onGetMeelDataStatus();
        })
        .catch((e) => {
          console.log('onAddRecipeToBasket', e);
          Alert.alert('Произошла ошибка');
        });
    },
    [selectedDate],
  );

  const [, onDelRecipeToBasket] = useAsyncFn(
    async (recipe_id: number) => {
      return await foodBasketAPI
        .delRecipeToBasket({ access_token, recipe_id })
        .then(async () => {
          onGetMeelDataStatus();
        })
        .catch((e) => {
          console.log('onDelRecipeToBasket', e);
          Alert.alert('Произошла ошибка');
        });
    },
    [selectedDate],
  );

  const [, onChangeColServing] = useAsyncFn(
    async (data: { col: number; meal_id: number }) => {
      return await meelAPI
        .changeColServing({ access_token, col: data.col, meal_id: data.meal_id })
        .then(async () => {
          onGetMeelDataStatus();
        })
        .catch((e) => {
          console.log('onChangeColServing', e);
          Alert.alert('Произошла ошибка');
        });
    },
    [selectedDate],
  );

  const data = categories.map((x) => ({
    title: x.name,
    meal_type_id: x.id,
    data: mealData.find((y) => y.time_category === x.system_key),
  }));

  const isInFoodBasket = mealData.some((item) => item.food_basket);

  return (
    <>
      <RecipesPlanView
        data={data}
        isBasketClear={isBasketClear}
        setCount={onChangeColServing}
        onAddRecipeToBasket={onAddRecipeToBasket}
        onDelRecipeToBasket={onDelRecipeToBasket}
        dontBuyIngredientsLenght={dontBuyIngredientsLenght}
        isInFoodBasket={isInFoodBasket}
        onAddToFoodBasket={isInFoodBasket ? onDeleteToFoodBasket : onAddToFoodBasket}
        selectedDate={selectedDate}
        setSelectedDate={(date) =>
          !isPro && date > add(Date.now(), { days: 1 })
            ? setPopUpVisible(true)
            : setSelectedDate(date)
        }
        onDeleteMeel={onDeleteMeel}
        onGoSelectMeel={(type) =>
          navigation.navigate('SelectMeel', { type, date: selectedDate, returnScreen: 'MealPlan' })
        }
        onGoToFoodBasket={() =>
          isPro ? navigation.navigate('ShoppingCart', {}) : setPopUpVisible(true)
        }
      />
      <AvailableInPropopUp popUpVisible={popUpVisible} setPopUpVisible={setPopUpVisible} />
    </>
  );
};
