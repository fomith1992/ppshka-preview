import { Row } from '@screens/recipes/ui/header.style';
import { color } from '@shared/config/color';
import { styled } from '@shared/config/styled';
import { font } from '@shared/config/text';
import { container } from '@shared/config/view';
import { blockShadow } from '@shared/ui/block-shadow/block-shadow';
import { Image, Platform, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { font12m, font14m, font14r } from 'src/commonStyles/fonts';
import PreLoadedImage from 'src/components/PreLoadedImage';
import { bottomTabsHeight, getScaleSize } from 'src/utils/dimensions';
import { ShoppingCartIcon24 } from '@shared/ui/icons/shopping-cart.icon-16';

export const HeaderContainer = styled(View, {
  paddingTop: getScaleSize(6),
  paddingBottom: getScaleSize(20),
});

export const TitleContainer = styled(View, {
  alignItems: 'center',
});

export const ContentContainer = styled(View, {
  backgroundColor: color('white'),
  marginHorizontal: getScaleSize(15),
  paddingBottom: getScaleSize(15),
  borderRadius: getScaleSize(8),
  ...blockShadow(),
});

export const ScrollContainer = styled(ScrollView, {
  marginTop: -getScaleSize(45),
  paddingTop: getScaleSize(15),
});

export const HeaderTitle = styled(Text, {
  ...font({ type: 'h1' }),
  color: color('white'),
});

export const DateText = styled(Text, {
  ...font14m,
  color: color('white'),
  marginHorizontal: getScaleSize(15),
});

export const FoodCardText = styled(Text, {
  ...font({ type: 'text4', weight: 'normal' }),
  fontSize: getScaleSize(13),
});

export const DateTextColored = styled(Text, {
  ...font14m,
  marginHorizontal: getScaleSize(15),
  color: color('white'),
});

export const LineButtonText = styled(Text, {
  ...font({ type: 'text1', weight: 'normal' }),
  color: color('gray6'),
  marginHorizontal: getScaleSize(8),
});

export const LineButton = styled(TouchableOpacity, {
  flexDirection: 'row',
  alignItems: 'center',
  justifyContent: 'center',
});

export const Title = styled(Text, {
  ...font({ type: 'h2', weight: 'normal' }),
});

export const TitleRow = styled(Row, {
  marginTop: getScaleSize(8),
  ...container(),
});

export const Avatar = styled(PreLoadedImage, {
  width: '30%',
  height: getScaleSize(90),
  borderTopLeftRadius: getScaleSize(8),
  borderBottomLeftRadius: getScaleSize(8),
  marginRight: getScaleSize(8),
});

export const FoodCardContainer = styled(TouchableOpacity, {
  paddingRight: getScaleSize(16),
  backgroundColor: color('white'),
  borderRadius: getScaleSize(15),
  marginTop: getScaleSize(12),
  zIndex: 991,
  ...blockShadow(),
});

export const DescriptionContainer = styled(View, {
  flex: 1,
  justifyContent: 'flex-end',
  marginVertical: getScaleSize(8),
});

export const ItemContainer = styled(View, {
  ...container(),
  marginTop: getScaleSize(15),
  backgroundColor: color('gray3'),
  borderRadius: getScaleSize(15),
  ...blockShadow(),
});

export const BurnText = styled(Text, {
  ...font12m,
  marginLeft: getScaleSize(5),
  color: '#EF4923',
});

export const TimeText = styled(Text, {
  ...font12m,
  marginLeft: getScaleSize(5),
  color: '#6034A8',
});

export const SizeText = styled(Text, {
  ...font12m,
  color: '#BD2B98',
});

export const Circle = styled(View, {
  width: getScaleSize(4),
  height: getScaleSize(4),
  borderRadius: getScaleSize(4) / 2,
  backgroundColor: '#D9D9D9',
  marginHorizontal: getScaleSize(6),
});

export const EmptyCard = styled(View, {
  justifyContent: 'center',
  alignItems: 'center',
});

export const FoodCardRow = styled(View, {
  flexDirection: 'row',
  alignItems: 'center',
  justifyContent: 'space-between',
});

export const EmptyCardDescription = styled(Text, {
  ...font14r,
  lineHeight: getScaleSize(21),
  color: color('gray7'),
});

export const ShoppingCartButton = styled(TouchableOpacity, {
  flexDirection: 'row',
  alignItems: 'center',
  borderWidth: 2,
  borderColor: color('white'),
  padding: getScaleSize(4),
  borderRadius: getScaleSize(8),
});

export const EmptyCardButton = styled(TouchableOpacity, {
  flexDirection: 'row',
  alignItems: 'center',
  justifyContent: 'center',
});

export const EmptyCardText = styled(Text, {
  ...font({ type: 'text1', weight: 'normal' }),
  color: color('primary'),
  marginHorizontal: getScaleSize(12),
  marginBottom: getScaleSize(12),
});

export const FoodCardButtonText = styled(Text, {
  ...font({ type: 'text4' }),
  lineHeight: getScaleSize(12),
  color: color('gray6'),
  margin: getScaleSize(4),
  textAlign: 'right',
});

export const ButtonBasket = styled(Image, {
  width: getScaleSize(20),
  height: getScaleSize(20),
});

export const ShoppingCartButtonDescription = styled(Text, {
  ...font({ type: 'text4' }),
  lineHeight: getScaleSize(12),
  color: color('whiteOpacity50'),
  marginHorizontal: getScaleSize(4),
  textAlign: 'right',
});

export const BottomButtonDescription = styled(Text, {
  ...font({ type: 'text3' }),
  color: color('gray7'),
  textAlign: 'center',
  marginHorizontal: getScaleSize(4),
  marginBottom: getScaleSize(8),
});

export const Badge = styled(Text, {
  ...font({ type: 'text4' }),
  color: color('white'),
  textAlign: 'center',
});

export const BadgeContainer = styled(View, {
  height: getScaleSize(20),
  width: getScaleSize(20),
  borderRadius: getScaleSize(8),
  overflow: 'hidden',
  position: 'absolute',
  alignItems: 'center',
  justifyContent: 'center',
  bottom: -getScaleSize(10),
  right: -getScaleSize(10),
});

export const ShoppingCartSvg = styled(ShoppingCartIcon24, {
  width: getScaleSize(30),
  height: getScaleSize(27),
});

export const styles = StyleSheet.create({
  scrollContainer: {
    ...Platform.select({
      android: {
        paddingBottom: getScaleSize(75) + bottomTabsHeight,
      },
      ios: {
        paddingBottom: getScaleSize(55) + bottomTabsHeight,
      },
    }),
  },
});

export const ButtonContainer = styled(View, {
  ...container(),
  marginTop: getScaleSize(16),
  justifyContent: 'center',
});

export const CounterBlock = styled(View, {
  alignItems: 'center',
});

export const Spring = styled(View, {
  flex: 1,
});
