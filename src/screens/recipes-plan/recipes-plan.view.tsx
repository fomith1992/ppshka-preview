import { useNavigation } from '@react-navigation/native';
import { Bottom, Row } from '@screens/recipes/ui/header.style';
import { GradientLayoutHeaderView } from '@shared/ui/gradient-layout/gradient-layout-header.view';
import { add, format } from 'date-fns';
import { ru } from 'date-fns/locale';
import React from 'react';
import { Image, View } from 'react-native';
import { TMeetItem } from 'src/api/meal-plan';
import { HLine } from 'src/components/add-post/add-post.style';
import Icon from 'src/components/Icon';
import { hitSlopParams } from 'src/utils/hit-slop-params';
import { isEqualDay } from 'src/utils/date';
import { Calendar } from '../../components/calendar/calendar.view';
import {
  HeaderContainer,
  HeaderTitle,
  LineButton,
  DateText,
  LineButtonText,
  DateTextColored,
  TitleRow,
  Title,
  FoodCardContainer,
  Avatar,
  DescriptionContainer,
  BurnText,
  TimeText,
  Circle,
  FoodCardText,
  EmptyCard,
  EmptyCardDescription,
  EmptyCardButton,
  EmptyCardText,
  TitleContainer,
  ContentContainer,
  ScrollContainer,
  styles,
  ShoppingCartButton,
  ButtonContainer,
  ItemContainer,
  SizeText,
  ShoppingCartButtonDescription,
  ShoppingCartSvg,
  FoodCardButtonText,
  FoodCardRow,
  Badge,
  ButtonBasket,
  BottomButtonDescription,
  BadgeContainer,
  Spring,
} from './recipes-plan.style';

import questionMarkIcon from '../../shared/ui/assets/question-mark.png';
import { ChapterDescription } from 'src/components/chapter-description/chapter-description.view';
import ScreenLayout from 'src/components/ScreenLayout';
import { screenHeight } from 'src/utils/dimensions';

import addToBasket from '../../shared/ui/assets/basket_icon-add.png';
import delInBasket from '../../shared/ui/assets/basket_icon-del.png';
import { GradientLayoutView } from '@shared/ui/gradient-layout/gradient-layout.view';
import { RecipeCounter } from 'src/components/recipes-counter/recipes-counter.view';
import { expectDefined } from 'src/utils/expect-defined';
import { useSelector } from 'react-redux';
import { AppState } from 'src/store';
import useAsync from 'react-use/lib/useAsync';
import { getCacheContent } from 'src/utils/cache-content';

interface RecipesPlanViewProps {
  data: {
    title: string;
    meal_type_id: number;
    data?: TMeetItem;
  }[];
  breackFast?: TMeetItem;
  lunch?: TMeetItem;
  diner?: TMeetItem;
  snack?: TMeetItem;
  selectedDate: Date;
  isInFoodBasket: boolean;
  isBasketClear: boolean;
  dontBuyIngredientsLenght: number;
  setCount: (data: { col: number; meal_id: number }) => void;
  onAddToFoodBasket: (date: Date) => void;
  setSelectedDate: (item: Date) => void;
  onDeleteMeel: (item: number) => void;
  onAddRecipeToBasket: (data: { recipe_id: number; col: number; meal_plan_id: number }) => void;
  onDelRecipeToBasket: (recipe_id: number) => void;
  onGoSelectMeel: (item: number) => void;
  onGoToFoodBasket: () => void;
}

interface FoodCardProps {
  data: TMeetItem;
  small?: boolean;
  setCount: (data: { col: number; meal_id: number }) => void;
  onDeleteMeel: (item: number) => void;
}

export const FoodCard = ({ data, setCount, onDeleteMeel }: FoodCardProps): React.ReactElement => {
  const photo = useAsync(async () => {
    return await getCacheContent({ url: data.url_image, format: 'png' });
  }, [data]);

  return (
    <Row alignItems>
      <Avatar source={{ uri: photo.value }} />
      <DescriptionContainer>
        <Row alignItems>
          <Icon name="burn" />
          <BurnText>{data.ccal} Ккал</BurnText>
          <Circle />
          <Icon name="clock" />
          <TimeText>{data.serving_time} мин</TimeText>
          <Circle />
          <Icon name="portionSize" />
          <SizeText>{data.cooking_size} г</SizeText>
        </Row>
        <Spring />
        <FoodCardText numberOfLines={1}>{data.name}</FoodCardText>
        <Spring />
        <FoodCardRow>
          <RecipeCounter
            count={data.col_serving}
            setCount={(count) => setCount({ col: count, meal_id: data.meal_id })}
          />
          <LineButton onPress={() => onDeleteMeel(data.meal_id)} hitSlop={hitSlopParams('L')}>
            <Icon color="gray6" name="trash" />
          </LineButton>
        </FoodCardRow>
      </DescriptionContainer>
    </Row>
  );
};

interface CardItemProps {
  data: {
    title: string;
    meal_type_id: number;
    data?: TMeetItem;
  };
  setCount: (data: { col: number; meal_id: number }) => void;
  onAddRecipeToBasket: (data: { recipe_id: number; col: number; meal_plan_id: number }) => void;
  onDelRecipeToBasket: (recipe_id: number) => void;
  onDeleteMeel: (item: number) => void;
  onGoSelectMeel: (item: number) => void;
}

const CardItem = ({
  data,
  setCount,
  onDeleteMeel,
  onGoSelectMeel,
  onAddRecipeToBasket,
  onDelRecipeToBasket,
}: CardItemProps): React.ReactElement => {
  const navigation = useNavigation();
  const categories = useSelector((state: AppState) => state.session.categories);

  return (
    <ItemContainer>
      <TitleRow alignItems spaceBeetween>
        <Title>{data.title}</Title>
        {data.data && (
          <EmptyCardButton
            onPress={() => {
              data.data
                ? data.data.food_basket
                  ? onDelRecipeToBasket(data.data.recipe_id)
                  : onAddRecipeToBasket({
                      col: data.data.col_serving,
                      meal_plan_id: data.data.meal_id,
                      recipe_id: data.data.recipe_id,
                    })
                : null;
            }}
            hitSlop={hitSlopParams('L')}
          >
            <FoodCardButtonText>
              {data.data.food_basket ? `Удалить из${'\n'}корзины` : `Добавить в${'\n'}корзину`}
            </FoodCardButtonText>
            <ButtonBasket source={data.data.food_basket ? delInBasket : addToBasket} />
          </EmptyCardButton>
        )}
      </TitleRow>
      {data.data ? (
        <FoodCardContainer
          onPress={() =>
            navigation.navigate('RecipeFull', {
              id: data.data?.recipe_id,
              category: expectDefined(
                categories.find((x) => x.system_key === data.data?.time_category),
              ),
            })
          }
        >
          <FoodCard setCount={setCount} onDeleteMeel={onDeleteMeel} data={data.data} small />
        </FoodCardContainer>
      ) : (
        <EmptyCard>
          <Bottom mb={-10} />
          <Image style={{ width: 38, height: 45 }} source={questionMarkIcon} />
          <FoodCardText>Нет добавленных рецептов</FoodCardText>
          <EmptyCardDescription>
            Добавьте один из рецептов, чтобы {'\n'} придерживаться плана питания
          </EmptyCardDescription>
          <Bottom mb={12} />
          <EmptyCardButton
            onPress={() => onGoSelectMeel(data.meal_type_id)}
            hitSlop={hitSlopParams('L')}
          >
            <EmptyCardText>Добавить {data.title.toLowerCase()}</EmptyCardText>
          </EmptyCardButton>
        </EmptyCard>
      )}
    </ItemContainer>
  );
};

export const RecipesPlanView = ({
  data,
  selectedDate,
  isInFoodBasket,
  dontBuyIngredientsLenght,
  isBasketClear,
  setCount,
  onAddRecipeToBasket,
  onDelRecipeToBasket,
  onAddToFoodBasket,
  onDeleteMeel,
  onGoSelectMeel,
  setSelectedDate,
  onGoToFoodBasket,
}: RecipesPlanViewProps): React.ReactElement => {
  const parseDate = format(selectedDate, 'd MMMM', { locale: ru });
  return (
    <ScreenLayout safeArea edges={['left', 'right']}>
      <View style={{ position: 'absolute', height: screenHeight }}>
        <GradientLayoutHeaderView>
          <TitleContainer>
            <HeaderContainer>
              <Row ph={15} alignItems spaceBeetween>
                <HeaderTitle>План питания</HeaderTitle>
                <ShoppingCartButton hitSlop={hitSlopParams('XL')} onPress={onGoToFoodBasket}>
                  <ShoppingCartButtonDescription>
                    Продуктовая{'\n'}корзина
                  </ShoppingCartButtonDescription>
                  <ShoppingCartSvg />
                  {!isBasketClear && (
                    <BadgeContainer>
                      <GradientLayoutView reverseColor />
                      <Badge>{dontBuyIngredientsLenght}</Badge>
                    </BadgeContainer>
                  )}
                </ShoppingCartButton>
              </Row>
              <Bottom mb={12} />
              <Calendar selectedDate={selectedDate} setSelectedDate={setSelectedDate} />
              <Bottom mb={20} />
              <HLine />
              <Bottom mb={20} />
              <Row spaceBeetween ph={15} alignItems>
                <LineButton
                  disabled={isEqualDay(selectedDate, new Date())}
                  onPress={() => setSelectedDate(add(selectedDate, { days: -1 }))}
                  hitSlop={hitSlopParams('L')}
                >
                  <Icon color="gray6" name="arrowBack" />
                  <LineButtonText>Вчера</LineButtonText>
                </LineButton>
                <DateText>
                  {isEqualDay(selectedDate, new Date()) && (
                    <DateTextColored>Сегодня, </DateTextColored>
                  )}
                  {format(selectedDate, 'd MMM y', { locale: ru })}
                </DateText>
                <LineButton
                  disabled={format(selectedDate, 'd') === format(add(Date.now(), { days: 9 }), 'd')}
                  onPress={() => setSelectedDate(add(selectedDate, { days: 1 }))}
                  hitSlop={hitSlopParams('L')}
                >
                  <LineButtonText>Завтра</LineButtonText>
                  <Icon
                    color="gray6"
                    style={{ transform: [{ rotate: '180deg' }] }}
                    name="arrowBack"
                  />
                </LineButton>
              </Row>
            </HeaderContainer>
          </TitleContainer>
        </GradientLayoutHeaderView>
        <ScrollContainer
          contentContainerStyle={styles.scrollContainer}
          showsVerticalScrollIndicator={false}
        >
          <ContentContainer>
            {data.map((item, idx) => (
              <CardItem
                data={item}
                onAddRecipeToBasket={onAddRecipeToBasket}
                onDelRecipeToBasket={onDelRecipeToBasket}
                onDeleteMeel={onDeleteMeel}
                onGoSelectMeel={onGoSelectMeel}
                setCount={setCount}
                key={idx}
              />
            ))}
          </ContentContainer>
          {data.some((item) => item.data) && (
            <ButtonContainer>
              <BottomButtonDescription>
                Вы можете добавить все рецепты из плана питания на {parseDate} в продуктовую корзину
              </BottomButtonDescription>
              <EmptyCardButton
                onPress={() => onAddToFoodBasket(selectedDate)}
                hitSlop={hitSlopParams('L')}
              >
                <EmptyCardText>
                  {isInFoodBasket
                    ? 'Удалить из продуктовой корзины'
                    : 'Добавить в продуктовую корзину'}
                </EmptyCardText>
              </EmptyCardButton>
            </ButtonContainer>
          )}
        </ScrollContainer>
      </View>
      <ChapterDescription chapter="plain" />
    </ScreenLayout>
  );
};
