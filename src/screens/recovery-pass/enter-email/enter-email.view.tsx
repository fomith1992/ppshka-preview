import React from 'react';
import { Text } from 'react-native';
import Button from 'src/components/Button';
import Input from 'src/components/Input';
import ScreenLayout from 'src/components/ScreenLayout';

import { styles } from './styles';

interface EnterEmailViewProps {
  email: string;
  error: string | null;
  setEmail: (email: string) => void;
  onSubmitData: () => void;
}

export const EnterEmailView = ({
  email,
  error,
  setEmail,
  onSubmitData,
}: EnterEmailViewProps): React.ReactElement => {
  /* const [agreement, setAgreement] = useState(false); */

  return (
    <ScreenLayout style={styles.container} edges={['left', 'right', 'bottom']}>
      <Text style={styles.title}>Введите ваш email</Text>
      <Text style={styles.subTitle}>
        Добро пожаловать! Пожалуйста,{'\n'}введите свой email, чтобы продолжить
      </Text>
      <Input
        error={error}
        value={email}
        style={styles.marginBottom24}
        onChange={setEmail}
        placeholder={'Введите ваш email'}
      />
      <Button
        uppercase
        /* disabled={!agreement} */ content="Сбросить пароль"
        onPress={() => onSubmitData()}
      />
      {/* <Bottom mb={12} />
      <Checkbox value={agreement} onChangeValue={setAgreement} style={styles.containerCheckbox}>
        <Text style={styles.textAgreement}>
          Нажимая кнопку «Зарегистрироваться» вы соглашаетесь
          <Text style={styles.textColorAgreement}>{' с правилами обработки '}</Text>
          персональных данных
        </Text>
      </Checkbox> */}
    </ScreenLayout>
  );
};
