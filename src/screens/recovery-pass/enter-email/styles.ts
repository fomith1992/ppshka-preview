import { StyleSheet } from 'react-native';

import { purpleColor } from 'src/commonStyles/colors';
import { font10r, font12m, font12r, font14r, font30m } from 'src/commonStyles/fonts';
import { getScaleSize } from 'src/utils/dimensions';

export const styles = StyleSheet.create({
  container: {
    paddingHorizontal: getScaleSize(15),
    paddingBottom: getScaleSize(20),
    justifyContent: 'center',
    flexGrow: 1,
  },
  title: {
    ...font30m,
    lineHeight: getScaleSize(37),
    color: '#262626',
    // marginTop: getScaleSize(80),
    marginBottom: getScaleSize(12),
    textAlign: 'center',
  },
  subTitle: {
    ...font14r,
    lineHeight: getScaleSize(17),
    color: '#262626',
    marginBottom: getScaleSize(16),
    textAlign: 'center',
  },
  marginBottom24: {
    marginBottom: getScaleSize(24),
  },
  button: {
    borderRadius: 8,
    marginBottom: getScaleSize(12),
  },
  textAgreement: {
    ...font10r,
    color: '#8C8C8C',
  },
  textColorAgreement: {
    color: purpleColor,
  },
  containerCheckbox: {
    marginBottom: getScaleSize(32),
  },
  containerSeparator: {
    marginBottom: getScaleSize(32),
    flexDirection: 'row',
    alignItems: 'center',
  },
  partSeparator: {
    height: getScaleSize(1),
    backgroundColor: '#F0F0F0',
    flex: 1,
  },
  textSeparator: {
    ...font12r,
    marginHorizontal: getScaleSize(10),
    color: '#8C8C8C',
  },
  textLoginMethod: {
    ...font12r,
    lineHeight: getScaleSize(14.32),
    color: '#262626',
  },
  containerIcon: {
    position: 'absolute',
    left: getScaleSize(40),
  },
  containerButton: {
    flexDirection: 'row',
    height: getScaleSize(50),
    alignItems: 'center',
    backgroundColor: 'white',
    borderRadius: getScaleSize(8),
    justifyContent: 'center',
    marginBottom: getScaleSize(16),
  },

  containerLoginBottom: {
    flexDirection: 'row',
    alignSelf: 'center',
    marginTop: getScaleSize(20),
  },
  textLogin: {
    ...font12r,
    lineHeight: getScaleSize(14.32),
    color: '#8C8C8C',
  },
  containerTextLoginBold: {
    borderBottomWidth: 1,
    borderColor: 'white',
  },
  textLoginBold: {
    ...font12m,
    lineHeight: getScaleSize(14.32),
    color: purpleColor,
  },
});
