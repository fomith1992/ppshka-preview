import { useNavigation } from '@react-navigation/native';
import React, { useState } from 'react';
import { EnterEmailView } from './enter-email.view';

import * as yup from 'yup';
import { Alert } from 'react-native';
import useAsyncFn from 'react-use/lib/useAsyncFn';
import { auth } from 'src/api';
import { LoaderModal } from 'src/components/ui/Loader/loading-indicator.modal';

const shema = yup
  .object({
    email: yup
      .string()
      .required('email является обязательным параметром')
      .matches(/.+@.+\..+/i, 'Проверьте правильность введенного email'),
  })
  .required();

export const EnterEmail = (): React.ReactElement => {
  const navigation = useNavigation();

  const [email, setEmail] = useState('');
  const [error, setError] = useState<string | null>(null);

  const [signInState, onSignIn] = useAsyncFn(async () => {
    return await auth
      .resetPassSendCode({ email })
      .then(async ({ data }) => {
        navigation.navigate('ConfirmEmail', { access_token: data.data.access_token, email });
      })
      .catch((e) => {
        console.log(e);
        Alert.alert('Произошла ошибка');
      });
  }, [email]);

  const onSubmit = () => {
    setError(null);
    shema
      .validate({ email })
      .then(() => {
        onSignIn();
      })
      .catch((e: yup.ValidationError) => setError(e.message));
  };

  return (
    <>
      <EnterEmailView email={email} setEmail={setEmail} error={error} onSubmitData={onSubmit} />
      <LoaderModal visible={signInState.loading} />
    </>
  );
};
