import React from 'react';
import { Alert } from 'react-native';
import useAsyncFn from 'react-use/lib/useAsyncFn';
import { auth } from 'src/api';
import { LoaderModal } from 'src/components/ui/Loader/loading-indicator.modal';
import { NewPasswordView } from './new-password.view';

import * as storage from 'src/utils/storage/storage';
import { useDispatch } from 'react-redux';
import { sessionSetTokens } from 'src/store/session/actions.types';

export const NewPassword = (props: { access_token: string }): React.ReactElement => {
  const dispatch = useDispatch();

  const [sendPass, onSendPass] = useAsyncFn(
    async (new_password: string, confirmation_password: string) => {
      return await auth
        .updatePass({ access_token: props.access_token, confirmation_password, new_password })
        .then(async ({ data }) => {
          await storage.setRefreshToken({
            refresh_token: data.data.refresh_token,
            type: 'email',
          });
          dispatch(
            sessionSetTokens({
              accessToken: data.data.access_token,
              refreshToken: data.data.refresh_token ?? null,
            }),
          );
        })
        .catch(() => Alert.alert('Произошла ошибка'));
    },
    [props.access_token],
  );

  return (
    <>
      <NewPasswordView onSubmitData={onSendPass} />
      <LoaderModal visible={sendPass.loading} />
    </>
  );
};
