import React, { useState } from 'react';
import { Text } from 'react-native';
import Button from 'src/components/Button';
import Input from 'src/components/Input';
import ScreenLayout from 'src/components/ScreenLayout';
import * as yup from 'yup';

import { styles } from './styles';

interface NewPasswordViewProps {
  onSubmitData: (password: string, repeatPassword: string) => void;
}

const MIN_LENGTH = 8;
const MAX_LENGTH = 20;
const ERROR_MIN_MESSAGE = 'Пожалуйста удлините пароль';
const ERROR_MAX_MESSAGE =
  'Максимальная длина пароля - 20 символов. Пожалуйста, сократите ваш пароль. ';
const ERROR_SAME_PASSWORDS =
  'В обоих строках должны быть одинаковые пароли. Пожалуйста, проверьте введенные данные.';

const passwordSchema = yup
  .object({
    password: yup
      .string()
      .required(ERROR_MIN_MESSAGE)
      .matches(
        /^[a-zA-Z0-9!@#$%^&-]+$/,
        'Пароль может содержать только латинские буквы, цифры и символы !@#$%^&-',
      )
      .min(MIN_LENGTH, ERROR_MIN_MESSAGE)
      .max(MAX_LENGTH, ERROR_MAX_MESSAGE),
    repeatPassword: yup.string().oneOf([yup.ref('password')], ERROR_SAME_PASSWORDS),
  })
  .required()
  .test({
    name: 'passwords match',
    message: ERROR_SAME_PASSWORDS,
    test: ({ password, repeatPassword }) => password === repeatPassword,
  });

export const NewPasswordView = ({ onSubmitData }: NewPasswordViewProps): React.ReactElement => {
  const [password, setPassword] = useState('');
  const [repeatPassword, setRepeatPassword] = useState('');

  const [error, setError] = useState<{ path?: string; msg: string } | null>(null);

  const onChangeTextPassword = (text: string) => {
    setPassword(text);
    setError(null);
  };

  const onChangeTextRepeatPassword = (text: string) => {
    setRepeatPassword(text);
    setError(null);
  };

  const onSubmit = () => {
    setError(null);
    passwordSchema
      .validate({ password, repeatPassword })
      .then(() => {
        onSubmitData(password, repeatPassword);
      })
      .catch((e: yup.ValidationError) => setError({ path: e.path, msg: e.message }));
  };

  return (
    <ScreenLayout style={styles.container} edges={['left', 'right', 'bottom']}>
      <Text style={styles.title}>Введите новый пароль</Text>
      <Text style={styles.subTitle}>
        В дальнейшем этот пароль{'\n'}будет использоваться для входа
      </Text>
      <Input
        value={password}
        onChange={(text) => onChangeTextPassword(text)}
        style={styles.marginBottom24}
        isPassword={true}
        placeholder={'Введите пароль'}
        error={error?.path === 'password' ? error?.msg : undefined}
      />
      <Input
        value={repeatPassword}
        onChange={(text) => onChangeTextRepeatPassword(text)}
        style={styles.marginBottom24}
        isPassword={true}
        placeholder={'Повторите пароль'}
        error={error?.path === '' ? error.msg : undefined}
      />
      <Button uppercase content="Сбросить пароль" onPress={() => onSubmit()} />
    </ScreenLayout>
  );
};
