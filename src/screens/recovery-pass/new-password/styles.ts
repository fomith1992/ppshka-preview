import { StyleSheet } from 'react-native';

import { font14r, font30m } from 'src/commonStyles/fonts';
import { getScaleSize } from 'src/utils/dimensions';

export const styles = StyleSheet.create({
  container: {
    paddingHorizontal: getScaleSize(15),
    paddingBottom: getScaleSize(20),
    justifyContent: 'center',
    flexGrow: 1,
  },
  title: {
    ...font30m,
    lineHeight: getScaleSize(37),
    color: '#262626',
    // marginTop: getScaleSize(80),
    marginBottom: getScaleSize(12),
    textAlign: 'center',
  },
  subTitle: {
    ...font14r,
    lineHeight: getScaleSize(17),
    color: '#262626',
    marginBottom: getScaleSize(16),
    textAlign: 'center',
  },
  marginBottom24: {
    marginBottom: getScaleSize(24),
  },
  button: {
    borderRadius: 8,
    marginBottom: getScaleSize(12),
  },
});
