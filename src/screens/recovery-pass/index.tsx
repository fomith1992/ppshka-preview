import { StackScreenProps } from '@react-navigation/stack';
import React from 'react';
import { RootStackParamList } from 'src/navigation/RootNavigator';
import { ConfirmEmail } from './confirm-email/confirm-email.container';
import { EnterEmail } from './enter-email/enter-email.container';
import { NewPassword } from './new-password/new-password.container';

export const ConfirmEmailScreen = ({
  route,
}: StackScreenProps<RootStackParamList, 'ConfirmEmail'>) => {
  return <ConfirmEmail email={route.params.email} access_token={route.params.access_token} />;
};

export const EnterEmailScreen = (): React.ReactElement => {
  return <EnterEmail />;
};

export const NewPasswordScreen = ({
  route,
}: StackScreenProps<RootStackParamList, 'NewPassword'>) => {
  return <NewPassword access_token={route.params.access_token} />;
};
