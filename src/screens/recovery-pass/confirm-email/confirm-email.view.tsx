import React, { useEffect, useState } from 'react';
import { Text, TouchableOpacity, View } from 'react-native';

import AsyncStorage from '@react-native-async-storage/async-storage';

import { theme } from '@shared/config/theme';

import Button from 'src/components/Button';
import { PinCodeInput } from 'src/components/pin-code-input/pin-code-input.view';
import ScreenLayout from 'src/components/ScreenLayout';
import { screenWidth } from 'src/utils/dimensions';
import { hitSlopParams } from 'src/utils/hit-slop-params';

import { styles } from './styles';

interface ConfirmEmailViewProps {
  code: string;
  setCode: (code: string) => void;
  onSubmitData: () => void;
  onChangeEmail: () => void;
  onResendCode: () => void;
}

const codeLenght = 4;
const containerWidht = screenWidth - 2 * 15 - 9 * (codeLenght - 1);

export const ConfirmEmailView = ({
  code,
  setCode,
  onSubmitData,
  onChangeEmail,
  onResendCode,
}: ConfirmEmailViewProps): React.ReactElement => {
  const [timerTime, setTimerTime] = useState(0);

  const storageRequest = () => {
    AsyncStorage.getItem('TIMERTIME', (err, result) => {
      if (err != null) {
        console.log('AsyncStorage err', err);
      }
      if (result !== undefined) {
        setTimerTime(Math.round((JSON.parse(result) - Date.now()) / 1000));
      }
    });
  };

  useEffect(() => {
    storageRequest();
  }, []);

  useEffect(() => {
    if (timerTime > 0) {
      setTimeout(() => setTimerTime(timerTime - 1), 1000);
    }
  }, [timerTime]);

  return (
    <ScreenLayout style={styles.container} edges={['left', 'right', 'bottom']}>
      <Text style={styles.title}>Подтвердите{'\n'}ваш email</Text>
      <Text style={styles.subTitle}>
        На ваш email был отправлен проверочный код,{'\n'}пожалуйста, введите его ниже
      </Text>
      <View style={{ marginBottom: 32, alignItems: 'center' }}>
        <PinCodeInput
          keyboardType={'numeric'}
          codeLength={codeLenght}
          cellSize={{ width: containerWidht / codeLenght, height: containerWidht / codeLenght }}
          cellStyle={{
            width: containerWidht / codeLenght,
            height: containerWidht / codeLenght,
            borderWidth: 1,
            borderRadius: 12,
            borderColor: theme.colors.gray4,
          }}
          cellStyleFocused={{
            width: containerWidht / codeLenght,
            height: containerWidht / codeLenght,
            borderWidth: 1.3,
            borderRadius: 12,
            borderColor: theme.colors.gray6,
          }}
          textStyle={styles.pinCodeText}
          value={code}
          onTextChange={setCode}
        />
      </View>
      <View style={{ alignItems: 'center', marginBottom: 12 }}>
        {timerTime > 0 ? (
          <Text style={styles.textLoginBold}>
            Подождите 00:{timerTime} <Text style={styles.textFont12m}>Секунд</Text>
          </Text>
        ) : (
          <TouchableOpacity onPress={onResendCode} hitSlop={hitSlopParams('XL')}>
            <Text style={styles.textFont12m}>
              Не пришел код? <Text style={styles.textLoginBold}>Отправить повторно</Text>
            </Text>
          </TouchableOpacity>
        )}
      </View>
      <Button
        uppercase
        disabled={code.length !== codeLenght}
        content="продолжить"
        onPress={onSubmitData}
      />
      <View style={{ alignItems: 'center', marginTop: 16 }}>
        <TouchableOpacity onPress={onChangeEmail} hitSlop={hitSlopParams('XL')}>
          <Text style={styles.textLogin}>Ошиблись номером?</Text>
        </TouchableOpacity>
      </View>
    </ScreenLayout>
  );
};
