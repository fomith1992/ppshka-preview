import { useNavigation } from '@react-navigation/native';
import React, { useState } from 'react';
import { Alert } from 'react-native';
import useAsyncFn from 'react-use/lib/useAsyncFn';
import { auth } from 'src/api';
import { LoaderModal } from 'src/components/ui/Loader/loading-indicator.modal';
import { ConfirmEmailView } from './confirm-email.view';
import AsyncStorage from '@react-native-async-storage/async-storage';

export const ConfirmEmail = (props: {
  access_token: string;
  email: string;
}): React.ReactElement => {
  const navigation = useNavigation();
  const [code, setCode] = useState('');

  const [signInState, onSignIn] = useAsyncFn(async () => {
    return await auth
      .resetPassConfirmCode({ access_token: props.access_token, confirmation_code: code })
      .then(async ({ data }) => {
        navigation.navigate('NewPassword', { access_token: data.data.access_token });
      })
      .catch(() => Alert.alert('Произошла ошибка'));
  }, [code]);

  const sendTimerToStore = () => {
    AsyncStorage.setItem('TIMERTIME', JSON.stringify(Date.now() + 60 * 1000));
  };

  const [resendCode, onResendCode] = useAsyncFn(async () => {
    return await auth
      .resetPassSendCode({ email: props.email })
      .then(() => {
        sendTimerToStore();
      })
      .catch(() => Alert.alert('Произошла ошибка'));
  }, [props.email]);

  return (
    <>
      <ConfirmEmailView
        code={code}
        setCode={setCode}
        onSubmitData={onSignIn}
        onChangeEmail={() => navigation.goBack()}
        onResendCode={onResendCode}
      />
      <LoaderModal visible={signInState.loading || resendCode.loading} />
    </>
  );
};
