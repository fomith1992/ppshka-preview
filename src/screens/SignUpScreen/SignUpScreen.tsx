import React, { useState } from 'react';
import { Alert, Text, TouchableOpacity, View } from 'react-native';

import { useNavigation } from '@react-navigation/native';

/* import Icon from 'src/components/Icon';
import { onLogin } from 'src/api/auth0'; */
import Button from 'src/components/Button';
/* import Checkbox from 'src/components/Checkbox'; */
import Input from 'src/components/Input';
import ScreenLayout from 'src/components/ScreenLayout';

import { ContentContainer, Description, Logo, styles, Title, TitleContainer } from './styles';
import useAsyncFn from 'react-use/lib/useAsyncFn';
import { auth } from 'src/api';
import { LoaderModal } from 'src/components/ui/Loader/loading-indicator.modal';

import * as yup from 'yup';
import { GradientLayoutHeaderView } from '@shared/ui/gradient-layout/gradient-layout-header.view';
import { signUpEvent } from 'src/firebase/analytics/analytics-auth/signup-event';

const MIN_LENGTH = 6;
const MAX_LENGTH = 20;
const ERROR_MIN_MESSAGE = 'Слишком короткий пароль';
const ERROR_MAX_MESSAGE =
  'Максимальная длина пароля - 20 символов. Пожалуйста, сократите ваш пароль. ';

const emailShema = yup
  .object({
    email: yup
      .string()
      .required()
      .matches(/.+@.+\..+/i, 'Проверьте правильность введенного email'),
    password: yup
      .string()
      .required(ERROR_MIN_MESSAGE)
      .matches(
        /^[a-zA-Z0-9!@#$%^&-]+$/,
        'Пароль может содержать только латинские буквы, цифры и символы !@#$%^&-',
      )
      .min(MIN_LENGTH, ERROR_MIN_MESSAGE)
      .max(MAX_LENGTH, ERROR_MAX_MESSAGE),
  })
  .required();

const SignUpScreen = () => {
  const navigation = useNavigation();
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  /*   const [agreement, setAgreement] = useState(false); */

  const [error, setError] = useState<{ path?: string; msg: string } | null>(null);

  const onGoSignIn = () => {
    navigation.navigate('SignIn');
  };

  const [registerState, onSignUp] = useAsyncFn(async () => {
    return await auth.registerByEmailPass({ email, password }).then(async ({ data }) => {
      if (data.status === 'ok') {
        signUpEvent('email');
        navigation.navigate('OnboardingName', { ...data.data });
      } else {
        if (data.error_code === 0) Alert.alert('Ошибка', 'password и email не введены');
        if (data.error_code === 1) Alert.alert('Ошибка', 'email не введен');
        if (data.error_code === 2) Alert.alert('Ошибка', 'password не введен');
        if (data.error_code === 3)
          Alert.alert('Ошибка', 'Вы ввели некорректный email, попробуйте еще раз');
        if (data.error_code === 4)
          Alert.alert('Ошибка', 'Пароль не прошел валидацию(не должен быть меньше 6 символов)');
        if (data.error_code === 5)
          Alert.alert('Ошибка', 'Пользователь с данной почтой уже существует');
      }
    });
  }, [email, password]);

  const onSubmit = () => {
    setError(null);
    emailShema
      .validate({ email, password })
      .then(() => {
        onSignUp();
      })
      .catch((e: yup.ValidationError) => setError({ path: e.path, msg: e.message }));
  };

  return (
    <ScreenLayout edges={['left', 'right', 'bottom']}>
      <GradientLayoutHeaderView>
        <TitleContainer>
          <Logo source={require('../../../assets/logo.png')} />
          <Title>Зарегистрируйся</Title>
          <Description>
            Добро пожаловать! Пожалуйста,{'\n'}зарегистрируйтесь в приложении, чтобы{'\n'}продолжить
          </Description>
        </TitleContainer>
      </GradientLayoutHeaderView>
      <ContentContainer>
        <Input
          error={error?.path === 'email' ? error?.msg : undefined}
          value={email}
          onChange={(text) => setEmail(text)}
          placeholder={'Введите ваш e-mail'}
        />
        <Input
          value={password}
          onChange={(text) => setPassword(text)}
          style={styles.marginBottom24}
          isPassword={true}
          placeholder={'Введите пароль'}
          error={error?.path === 'password' ? error?.msg : undefined}
        />
        <Button
          uppercase
          content="Зарегистрироваться"
          /* disabled={!agreement} */ onPress={onSubmit}
        />
        {/* <Bottom mb={24} />
        <Checkbox value={agreement} onChangeValue={setAgreement} style={styles.containerCheckbox}>
          <Text style={styles.textAgreement}>
            Нажимая кнопку «Зарегистрироваться»{'\n'}вы соглашаетесь
            <Text style={styles.textColorAgreement}>{' с правилами обработки '}</Text>
            персональных данных
          </Text>
        </Checkbox> */}
        {/* <View style={styles.containerSeparator}>
          <View style={styles.partSeparator} />
          <Text style={styles.textSeparator}>Или</Text>
          <View style={styles.partSeparator} />
        </View>

        <TouchableOpacity
          style={[
            styles.containerButton,
            {
              borderWidth: 1,
              borderColor: '#F0F0F0',
            },
          ]}
          onPress={() => onLogin('google')}
        >
          <View style={styles.containerIcon}>
            <Icon name="google" />
          </View>
          <Text style={styles.textLoginMethod}>Войти с помощью Google</Text>
        </TouchableOpacity>
        {Platform.OS === 'ios' && (
          <TouchableOpacity style={[styles.containerButton, { backgroundColor: '#0B0B0A' }]}>
            <View style={styles.containerIcon}>
              <Icon name="apple" />
            </View>
            <Text style={[styles.textLoginMethod, { color: 'white' }]}>
              Войти с помощью Apple ID
            </Text>
          </TouchableOpacity>
        )} */}
        <View style={styles.containerLoginBottom}>
          <Text style={styles.textLogin}>{'У меня уже есть аккаунт. '}</Text>
          <TouchableOpacity style={styles.containerTextLoginBold} onPress={onGoSignIn}>
            <Text style={styles.textLoginBold}>Войти</Text>
          </TouchableOpacity>
        </View>
      </ContentContainer>
      <LoaderModal visible={registerState.loading} />
    </ScreenLayout>
  );
};

export default SignUpScreen;
