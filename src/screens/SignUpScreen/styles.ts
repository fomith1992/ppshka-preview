import { color } from '@shared/config/color';
import { styled } from '@shared/config/styled';
import { font } from '@shared/config/text';
import { Image, Platform, StyleSheet, Text, View } from 'react-native';

import { purpleColor } from 'src/commonStyles/colors';
import { font10r, font12m, font12r } from 'src/commonStyles/fonts';
import { getScaleSize } from 'src/utils/dimensions';

export const TitleContainer = styled(View, {
  alignItems: 'center',
});

export const Logo = styled(Image, {
  backgroundColor: color('white'),
  borderRadius: getScaleSize(12),
  padding: getScaleSize(4),
});

export const Title = styled(Text, {
  marginTop: getScaleSize(15),
  ...font({ type: 'h1' }),
  color: color('white'),
  textAlign: 'center',
});

export const Description = styled(Text, {
  ...font({ type: 'text2' }),
  marginTop: getScaleSize(15),
  color: color('white'),
  textAlign: 'center',
});

export const ContentContainer = styled(View, {
  backgroundColor: color('white'),
  marginHorizontal: getScaleSize(26),
  paddingHorizontal: getScaleSize(26),
  paddingVertical: getScaleSize(40),
  borderRadius: getScaleSize(8),
  marginTop: -getScaleSize(29),
  ...Platform.select({
    android: {
      elevation: 4,
    },
    ios: {
      shadowColor: color('black'),
      shadowOffset: {
        width: 0,
        height: 1,
      },
      shadowOpacity: 0.06,
      shadowRadius: 1,
    },
  }),
});

export const styles = StyleSheet.create({
  marginBottom24: {
    marginBottom: getScaleSize(24),
  },

  textAgreement: {
    ...font10r,
    color: '#8C8C8C',
  },
  textColorAgreement: {
    color: purpleColor,
  },
  containerCheckbox: {
    marginBottom: getScaleSize(32),
  },
  containerSeparator: {
    marginBottom: getScaleSize(32),
    flexDirection: 'row',
    alignItems: 'center',
  },
  partSeparator: {
    height: getScaleSize(1),
    backgroundColor: '#F0F0F0',
    flex: 1,
  },
  textSeparator: {
    ...font12r,
    marginHorizontal: getScaleSize(10),
    color: '#8C8C8C',
  },
  textLoginMethod: {
    ...font12r,
    lineHeight: getScaleSize(14.32),
    color: '#262626',
  },
  containerIcon: {
    position: 'absolute',
    left: getScaleSize(40),
  },
  containerButton: {
    flexDirection: 'row',
    height: getScaleSize(50),
    alignItems: 'center',
    backgroundColor: 'white',
    borderRadius: getScaleSize(8),
    justifyContent: 'center',
    marginBottom: getScaleSize(16),
  },

  containerLoginBottom: {
    flexDirection: 'row',
    alignSelf: 'center',
    marginTop: getScaleSize(20),
  },
  textLogin: {
    ...font12r,
    lineHeight: getScaleSize(14.32),
    color: '#8C8C8C',
  },
  containerTextLoginBold: {
    borderBottomWidth: 1,
    borderColor: 'white',
  },
  textLoginBold: {
    ...font12m,
    lineHeight: getScaleSize(14.32),
    color: purpleColor,
  },
});
