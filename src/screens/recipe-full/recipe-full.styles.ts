import { color } from '@shared/config/color';
import { styled } from '@shared/config/styled';
import { font } from '@shared/config/text';
import { container } from '@shared/config/view';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';

import { font12m, font12r, font20m } from 'src/commonStyles/fonts';
import { bottomTabsHeight, getScaleSize } from 'src/utils/dimensions';

import { font14m, font14r, font16m } from '../../commonStyles/fonts';
import { screenWidth } from '../../utils/dimensions';

export const HeaderButton = styled(TouchableOpacity, {
  position: 'absolute',
  width: getScaleSize(30),
  height: getScaleSize(30),
  justifyContent: 'center',
  alignItems: 'center',
  borderRadius: getScaleSize(8),
  borderWidth: 1,
  borderColor: color('white'),
  backgroundColor: color('whiteOpacity'),
});

export const CounterTitle = styled(Text, {
  ...font({ type: 'text2' }),
  lineHeight: getScaleSize(21),
  color: color('gray6'),
  marginBottom: getScaleSize(4),
});

export const FullRecipeNameText = styled(Text, {
  ...font({ type: 'h2', weight: 'normal' }),
  color: color('gray10'),
  ...container(),
  marginBottom: getScaleSize(20),
});

export const IngridientsDescription = styled(Text, {
  ...container(),
  ...font({ type: 'text3' }),
  textAlign: 'center',
  color: color('tertiary'),
});

export const IngridientsDescriptionBold = styled(Text, {
  ...container(),
  ...font({ type: 'text3', weight: 'bold' }),
  textAlign: 'center',
  color: color('tertiary'),
});

export const CounterBlock = styled(View, {
  alignItems: 'center',
});

export const TitleContainer = styled(View, {
  flexDirection: 'row',
  justifyContent: 'space-between',
  alignItems: 'center',
  marginRight: getScaleSize(16),
});

export const styles = StyleSheet.create({
  containerScroll: {
    backgroundColor: '#FAFAFA',
    flexGrow: 1,
    paddingBottom: getScaleSize(15) + bottomTabsHeight,
  },
  image: {
    height: getScaleSize(222),
  },
  containerContent: {
    top: -getScaleSize(12),
  },
  video: {
    width: screenWidth,
    height: (screenWidth / 4) * 5,
  },
  buttonHeader: {
    position: 'absolute',
    width: getScaleSize(30),
    height: getScaleSize(30),
    borderRadius: getScaleSize(8),
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonHeaderBack: {
    bottom: getScaleSize(26),
    left: getScaleSize(15),
  },
  buttonHeaderFavorites: {
    bottom: getScaleSize(26),
    right: getScaleSize(15),
  },
  container: {
    marginTop: getScaleSize(16),
    marginHorizontal: getScaleSize(45),
  },
  containerBlock: {
    borderRadius: getScaleSize(12),
    paddingTop: getScaleSize(12),
    paddingBottom: getScaleSize(22),
    backgroundColor: 'white',
    shadowColor: '#000',
    shadowOffset: {
      width: 4,
      height: 0,
    },
    shadowOpacity: 0.05,
    shadowRadius: 50,
    elevation: 1,
    marginBottom: getScaleSize(20),
  },
  containerBurnTime: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: getScaleSize(25),
    paddingHorizontal: getScaleSize(15),
  },
  textBurn: {
    ...font12m,
    marginLeft: getScaleSize(5),
    color: '#EF4923',
  },
  circle: {
    width: getScaleSize(4),
    height: getScaleSize(4),
    borderRadius: getScaleSize(4) / 2,
    backgroundColor: '#D9D9D9',
    marginHorizontal: getScaleSize(10),
  },
  textTime: {
    ...font12m,
    marginLeft: getScaleSize(5),
    color: '#6034A8',
  },
  textSecondTitle: {
    ...font20m,
    color: '#262626',
    marginHorizontal: getScaleSize(15),
  },
  textSubTitle: {
    ...font12r,
    color: '#8C8C8C',
    marginBottom: getScaleSize(20),
    marginHorizontal: getScaleSize(15),
  },
  textDescription: {
    ...font14r,
    lineHeight: getScaleSize(22),
    color: '#8C8C8C',
    marginHorizontal: getScaleSize(15),
  },
  separator: {
    height: 1,
    width: '100%',
    backgroundColor: '#F5F5F5',
    marginVertical: getScaleSize(20),
  },
  containerInfo: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginHorizontal: getScaleSize(15),
  },
  textItemValue: {
    ...font16m,
    color: '#262626',
  },
  textItemName: {
    ...font12r,
    color: '#8C8C8C',
    marginTop: getScaleSize(2),
  },
  containerComplexityIndicators: {
    flexDirection: 'row',
    marginBottom: getScaleSize(3),
  },
  complexityIndicator: {
    width: getScaleSize(3),
    height: getScaleSize(15),
    marginRight: getScaleSize(5),
    backgroundColor: '#F0F0F0',
  },
  complexityIndicatorActive: {
    width: getScaleSize(3),
    height: getScaleSize(15),
    marginRight: getScaleSize(5),
    backgroundColor: '#FF612F',
  },
  containerItemIngredients: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginHorizontal: getScaleSize(15),
    marginBottom: getScaleSize(15),
  },
  textIngredientsName: {
    ...font14r,
    lineHeight: getScaleSize(22),
    color: '#262626',
  },
  textIngredientsValue: {
    ...font14m,
    lineHeight: getScaleSize(22),
    color: '#262626',
  },
  containerStep: {
    marginBottom: getScaleSize(8),
    marginHorizontal: getScaleSize(15),
  },
  textRecipe: {
    ...font14r,
    lineHeight: getScaleSize(21),
    color: '#262626',
    marginBottom: getScaleSize(12),
    marginHorizontal: getScaleSize(15),
  },
  imageRecipe: {
    marginHorizontal: getScaleSize(15),
    height: getScaleSize(140),
    width: screenWidth - getScaleSize(30),
    marginBottom: getScaleSize(20),
    borderRadius: getScaleSize(12),
  },
});
