import React, { useState } from 'react';
import { Alert, ScrollView, StatusBar, Text, View } from 'react-native';

import { CommonActions, useNavigation } from '@react-navigation/native';

import Icon from 'src/components/Icon';
import ScreenLayout from 'src/components/ScreenLayout';
import Step from 'src/components/ui/Step';
import { VideoPlayer } from 'src/components/video-player';

import { theme } from '../../shared/config/theme';
import {
  CounterBlock,
  CounterTitle,
  FullRecipeNameText,
  HeaderButton,
  IngridientsDescription,
  IngridientsDescriptionBold,
  styles,
  TitleContainer,
} from './recipe-full.styles';
import useAsync from 'react-use/lib/useAsync';
import { meelAPI, recipesAPI } from 'src/api';
import { StackScreenProps } from '@react-navigation/stack';
import { FoodStackParamList } from 'src/navigation/FoodNavigator/FoodNavigator';
import { AppState } from 'src/store';
import { useSelector } from 'react-redux';
import { LoadingIndicator } from 'src/components/ui/Loader/loading-indicator';
import useAsyncFn from 'react-use/lib/useAsyncFn';
import { favoritesAPI } from 'src/utils/storage';
import Button from 'src/components/Button';
import { format } from 'date-fns';
import { BottomSheetGorhom } from 'src/components/bottom-sheet/bottom-sheet';
import BottomSheet from '@gorhom/bottom-sheet';
import { useRef } from 'react';
import { RecipesBottomSheetView } from './bottom-sheet/recipes-bottom-sheet.view';
import { PopUpWindow } from 'src/components/pop-up-window/pop-up-window.view';
import { Bottom } from '@screens/recipes/ui/header.style';
import { hitSlopParams } from 'src/utils/hit-slop-params';
import { choices } from '@screens/OnboardingQuestionScreen/OnboardingQuestion';
import { addRecipeToPlanEvent } from 'src/firebase/analytics/analytics-recipes-plan/add-recipe-to-plan-event';
import { addRecipeToFavorites } from 'src/firebase/analytics/analytics-recipe/add-recipe-to-favorites';
import { delRecipeToFavorites } from 'src/firebase/analytics/analytics-recipe/del-recipe-to-favorites';
import { openFullRecipeEvent } from 'src/firebase/analytics/analytics-recipe/open-full-recipe';
import { SizeText } from '@screens/recipes-plan/recipes-plan.style';
import { CircleButton } from 'src/components/Button/circle-button';
import { RecipeCounter } from 'src/components/recipes-counter/recipes-counter.view';
import { RecipeRatingCard } from 'src/components/rating-card/recipe-rating-card.container';

export const RecipeFull = ({ route }: StackScreenProps<FoodStackParamList, 'RecipeFull'>) => {
  const access_token = useSelector((state: AppState) => state.session.tokens.accessToken);
  const categories = useSelector((state: AppState) => state.session.categories);
  const planCategories = useSelector((state: AppState) => state.session.planCategories);

  const userGoal = useSelector((state: AppState) => state.session.user.goal);
  const [favorite, setFavorite] = useState<boolean>(false);
  const [modalVisible, setModalVisible] = useState<boolean>(false);
  const navigation = useNavigation();
  const sheetRef = useRef<BottomSheet>(null);

  const [counter, setCount] = useState<number>(1);

  const [_, setToFavoriteStatus] = useAsyncFn(async () => {
    return await recipesAPI
      .addToFavorite({ access_token, recipe_id: route.params.id })
      .then(async () => {
        const isFavorite = await favoritesAPI.checkIsFaveReceipe(route.params.id);
        if (!isFavorite) {
          addRecipeToFavorites(route.params.id.toString());
          await favoritesAPI.setFavoriteReceipe(route.params.id);
        } else {
          delRecipeToFavorites(route.params.id.toString());
          await favoritesAPI.delFavoriteReceipe(route.params.id);
        }
        setFavorite(await favoritesAPI.checkIsFaveReceipe(route.params.id));
      })
      .catch(() => Alert.alert('Произошла ошибка'));
  }, [favorite]);

  const recipeApi = useAsync(async () => {
    return await recipesAPI
      .getRecipe({ id: route.params.id, access_token })
      .then(async (data) => {
        openFullRecipeEvent(route.params.id.toString());
        setFavorite(data.data.data.data.fave !== 0);
        return data.data.data;
      })
      .catch((e) => {
        console.log(e);
        Alert.alert('Произошла ошибка');
      });
  }, []);

  const [_x, addToRecipesPlan] = useAsyncFn(
    async (date?: Date) => {
      if (route.params.date != null || date != null) {
        if (recipeApi.value) {
          return await meelAPI
            .addToMealPlan({
              access_token,
              recipe_id: route.params.id,
              date: format(route.params.date ?? date ?? new Date(), 'y-MM-dd'),
              category_id: route.params.category.id,
            })
            .then(async () => {
              addRecipeToPlanEvent(recipeApi.value?.data.id.toString() ?? null);
              if (route.params.date) {
                navigation.dispatch(
                  CommonActions.reset({
                    index: 0,
                    routes: [
                      {
                        name: 'TabNavigator',
                        params: {
                          screen: route.params.returnScreen,
                          initial: false,
                          params: {
                            initial: false,
                            screen: route.params.returnScreen,
                            params: { date: route.params.date },
                          },
                        },
                      },
                    ],
                  }),
                );
              } else setModalVisible(true);
            })
            .catch((e) => {
              console.log(e);
              Alert.alert('Произошла ошибка');
            });
        }
      } else {
        sheetRef.current?.snapTo(1);
      }
    },
    [route.params, recipeApi],
  );

  const [_y, checkMealPlan] = useAsyncFn(
    async (date: Date) => {
      if (recipeApi.value) {
        return await meelAPI
          .checkMealData({
            access_token,
            date: format(date, 'y-MM-dd'),
            category_id: route.params.category.id,
          })
          .then(async (response) => {
            if (response.data.data.exist) {
              Alert.alert(
                'Предупреждение!',
                'Вы уверены, что хотите заменить ранее выбранный рецепт?',
                [
                  {
                    text: 'Отмена',
                    onPress: () => console.log('close window'),
                  },
                  {
                    text: 'Да',
                    onPress: () => {
                      addToRecipesPlan(date);
                    },
                  },
                ],
                { cancelable: false },
              );
            } else {
              addToRecipesPlan(date);
            }
          })
          .catch(() => Alert.alert('Произошла ошибка'));
      }
    },
    [route.params, recipeApi],
  );

  if (recipeApi.loading || recipeApi.value == null) {
    return <LoadingIndicator visible />;
  }

  const post = recipeApi.value.data;
  const ingredients = recipeApi.value.ingredients ?? [];
  const steps = recipeApi.value.data.steps ?? [];

  return (
    <ScreenLayout safeArea={false}>
      <ScrollView
        bounces={false}
        contentContainerStyle={styles.containerScroll}
        showsVerticalScrollIndicator={false}
      >
        <View>
          <StatusBar barStyle={'light-content'} />
          <VideoPlayer
            source={{ uri: recipeApi.value.data.url_video }}
            fullscreen={true}
            posterResizeMode="cover"
            resizeMode="cover"
            /* repeat={true} */
            paused={false}
            poster={recipeApi.value.data.url_image}
            disableBack={true}
            disableFullscreen={true}
            disableTimer={true}
            disablePlayPause={true}
            disableVolume={true}
            disableSeekbar={true}
            tapAnywhereToPause={true}
            seekColor={theme.colors.primary}
            style={styles.video}
          />
          <HeaderButton
            style={styles.buttonHeaderBack}
            onPress={() => navigation.goBack()}
            hitSlop={hitSlopParams(24)}
          >
            <Icon name="arrowBack" color="white" />
          </HeaderButton>
          <HeaderButton
            hitSlop={hitSlopParams(24)}
            onPress={setToFavoriteStatus}
            style={styles.buttonHeaderFavorites}
          >
            <Icon name={favorite ? 'isFavorites' : 'favorites'} color="white" size={16} />
          </HeaderButton>
        </View>
        <View style={styles.containerContent}>
          <View style={styles.containerBlock}>
            <View style={styles.containerBurnTime}>
              <Icon name="burn" />
              <Text style={styles.textBurn}>{post.ccal} Ккал</Text>
              <View style={styles.circle} />
              <Icon name="clock" />
              <Text style={styles.textTime}>{post.serving_time} мин</Text>
              <View style={styles.circle} />
              <Icon color="tertiary" name="food" size={18} />
              <Text style={{ ...styles.textTime, color: theme.colors.tertiary }}>
                {categories.find((x) => x.id === route.params.id)?.name}
              </Text>
              <View style={styles.circle} />
              <Icon name="portionSize" />
              <SizeText>{post.cooking_size} г</SizeText>
            </View>
            <FullRecipeNameText>{post.name}</FullRecipeNameText>
            <Text style={styles.textDescription}>{post.short_description}</Text>
            {planCategories.some((item) => item.id === route.params.category.id) && (
              <View style={styles.container}>
                <CircleButton
                  uppercase
                  onPress={() => addToRecipesPlan()}
                  content={`добавить в ${
                    route.params.date
                      ? categories.find((x) => x.id === route.params.category.id)?.name
                      : 'план питания'
                  } `}
                />
              </View>
            )}
            <View style={styles.separator} />
            <View style={styles.containerInfo}>
              <View>
                <Text style={styles.textItemValue}>{post.proteins} г.</Text>
                <Text style={styles.textItemName}>Белки</Text>
              </View>
              <View>
                <Text style={styles.textItemValue}>{post.carbonohydrates} г.</Text>
                <Text style={styles.textItemName}>Углеводы</Text>
              </View>
              <View>
                <Text style={styles.textItemValue}>{post.fats} г.</Text>
                <Text style={styles.textItemName}>Жиры</Text>
              </View>
              <View>
                <View style={styles.containerComplexityIndicators}>
                  <View
                    style={
                      post.cooking_complexity >= 1
                        ? styles.complexityIndicatorActive
                        : styles.complexityIndicator
                    }
                  />
                  <View
                    style={
                      post.cooking_complexity >= 2
                        ? styles.complexityIndicatorActive
                        : styles.complexityIndicator
                    }
                  />
                  <View
                    style={
                      post.cooking_complexity >= 3
                        ? styles.complexityIndicatorActive
                        : styles.complexityIndicator
                    }
                  />
                  <View
                    style={
                      post.cooking_complexity >= 4
                        ? styles.complexityIndicatorActive
                        : styles.complexityIndicator
                    }
                  />
                  <View
                    style={
                      post.cooking_complexity >= 5
                        ? styles.complexityIndicatorActive
                        : styles.complexityIndicator
                    }
                  />
                  <View
                    style={
                      post.cooking_complexity >= 6
                        ? styles.complexityIndicatorActive
                        : styles.complexityIndicator
                    }
                  />
                  <View
                    style={
                      post.cooking_complexity >= 7
                        ? styles.complexityIndicatorActive
                        : styles.complexityIndicator
                    }
                  />
                </View>
                <Text style={styles.textItemName}>Сложность</Text>
              </View>
            </View>
          </View>
          {/* //! rating */}
          <View style={styles.containerBlock}>
            <RecipeRatingCard id={route.params.id} image={recipeApi.value.data.url_image} />
          </View>
          {post.show_ingredients && (
            <View style={styles.containerBlock}>
              <TitleContainer>
                <Text style={styles.textSecondTitle}>Ингредиенты</Text>
                <CounterBlock>
                  <CounterTitle>Кол-во порций</CounterTitle>
                  <RecipeCounter count={counter} setCount={setCount} />
                </CounterBlock>
              </TitleContainer>
              <View style={styles.separator} />
              {ingredients.map((ingredient, idx) => (
                <View key={idx} style={styles.containerItemIngredients}>
                  <Text style={styles.textIngredientsName}>{ingredient.name}</Text>
                  <Text style={styles.textIngredientsValue}>{ingredient.weight * counter} г.</Text>
                </View>
              ))}
              <IngridientsDescription>
                Вес каждого продукта рассчитан с точностью до грамма для Вас с целью{' '}
                <IngridientsDescriptionBold>
                  {choices[userGoal as 0 | 1 | 2].toLowerCase()}.
                </IngridientsDescriptionBold>
              </IngridientsDescription>
            </View>
          )}
          {post.show_steps && steps.length > 0 && (
            <View style={styles.containerBlock}>
              <Text style={styles.textSecondTitle}>Пошаговый рецепт</Text>
              <View style={styles.separator} />
              {steps.map((x, idx) => (
                <View key={idx}>
                  <Step value={idx + 1} style={styles.containerStep} />
                  <Text style={styles.textRecipe}>{x}</Text>
                  {/* <Image
                  style={styles.imageRecipe}
                  source={{
                    uri: x.image,
                  }}
                /> */}
                </View>
              ))}
            </View>
          )}
        </View>
      </ScrollView>
      <BottomSheetGorhom sheetRef={sheetRef}>
        <RecipesBottomSheetView
          onSubmit={(date) => {
            checkMealPlan(date);
            sheetRef.current?.close();
          }}
        />
      </BottomSheetGorhom>
      <PopUpWindow modalVisible={modalVisible}>
        <View>
          <Text style={{ ...styles.textSecondTitle, textAlign: 'center' }}>
            Рецепт успешно добавлен
          </Text>
          <Text style={{ ...styles.textDescription, textAlign: 'center' }}>
            Для продолжения нажмите кнопку "Продолжить"
          </Text>
          <Bottom mb={24} />
          <Button uppercase onPress={() => setModalVisible(false)} content="Продолжить" />
        </View>
      </PopUpWindow>
    </ScreenLayout>
  );
};
