import React, { useState } from 'react';
import { TouchableWithoutFeedback } from '@gorhom/bottom-sheet';
import { Platform } from 'react-native';
import Button from 'src/components/Button';
import { Calendar } from 'src/components/calendar/calendar.view';
import { Container, Title, Description } from './recipes-bottom-sheet.style';
import { Bottom } from '@screens/recipes/ui/header.style';
import { BottomButtonContainer, HLine } from 'src/components/add-post/add-post.style';

interface RecipesBottomSheetViewProps {
  onSubmit: (selectedDate: Date) => void;
}

export const RecipesBottomSheetView = ({
  onSubmit,
}: RecipesBottomSheetViewProps): React.ReactElement => {
  const [selectedDate, setSelectedDate] = useState(new Date());
  const isIos = Platform.OS === 'ios';
  return (
    <Container>
      <Title>Выберите день</Title>
      <Description>
        Для того, чтобы добавить рецепт, укажите {'\n'} день, в который вы хотите его добавить
      </Description>
      <Bottom mb={16} />
      <HLine />
      <Bottom mb={16} />
      <Calendar gradient selectedDate={selectedDate} setSelectedDate={setSelectedDate} />
      <Bottom mb={34} />
      <BottomButtonContainer>
        <TouchableWithoutFeedback onPress={() => (!isIos ? onSubmit(selectedDate) : undefined)}>
          <Button
            uppercase
            content="Добавить рецепт"
            onPress={() => (isIos ? onSubmit(selectedDate) : undefined)}
          />
        </TouchableWithoutFeedback>
      </BottomButtonContainer>
      <Bottom mb={96} />
    </Container>
  );
};
