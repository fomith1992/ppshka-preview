import { color } from '@shared/config/color';
import { styled } from '@shared/config/styled';
import { Text } from 'react-native';
import { View } from 'react-native';
import { font14r, font16m } from 'src/commonStyles/fonts';
import { getScaleSize } from 'src/utils/dimensions';

export const Container = styled(View, {
  backgroundColor: color('white'),
});

export const Title = styled(Text, {
  ...font16m,
  lineHeight: getScaleSize(24),
  textAlign: 'center',
});

export const Description = styled(Text, {
  ...font14r,
  marginTop: getScaleSize(4),
  lineHeight: getScaleSize(21),
  color: color('gray7'),
  textAlign: 'center',
});
