import React from 'react';
import { Promo } from './promo.container';

export const PromoScreen = (): React.ReactElement => {
  return <Promo />;
};
