import { color } from '@shared/config/color';
import { styled } from '@shared/config/styled';
import { font } from '@shared/config/text';
import { ScrollView, Text, TouchableOpacity, View } from 'react-native';
import { getScaleSize } from 'src/utils/dimensions';

export const Container = styled(ScrollView, {
  flex: 1,
  backgroundColor: color('grayBackground'),
});

export const ButtonContainer = styled(TouchableOpacity, {
  height: 85,
  marginTop: getScaleSize(12),
  marginHorizontal: getScaleSize(30),
  borderRadius: 20,
  overflow: 'hidden',
});

export const UpdatePlanButton = styled(TouchableOpacity, {
  height: getScaleSize(50),
  marginTop: getScaleSize(25),
  marginHorizontal: getScaleSize(50),
  borderRadius: getScaleSize(30),
  overflow: 'hidden',
});

export const UpdatePlanButtonInfo = styled(View, {
  flex: 1,
  flexDirection: 'row',
  alignItems: 'center',
  justifyContent: 'center',
  paddingHorizontal: getScaleSize(48),
});

export const ButtonContainerBackground = styled(
  View,
  (props: { active: boolean }) =>
    ({
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      borderRadius: 20,
      paddingVertical: getScaleSize(10),
      paddingHorizontal: getScaleSize(20),
      backgroundColor: color('white'),
      borderWidth: 1,
      borderColor: props.active ? 'transparent' : color('gray6'),
    } as const),
);

export const Title = styled(Text, {
  ...font({ type: 'text1', weight: 'normal' }),
});

export const Description = styled(Text, {
  ...font({ type: 'text2' }),
});

export const InfoDescription = styled(Text, {
  ...font({ type: 'text3' }),
});

export const BottomDescription = styled(Text, {
  ...font({ type: 'text4', weight: 'normal' }),
  lineHeight: getScaleSize(13),
  textAlign: 'center',
  marginTop: getScaleSize(22),
});

export const RestorePurchasesText = styled(Text, {
  ...font({ type: 'text4', weight: 'normal' }),
  lineHeight: getScaleSize(13),
  textAlign: 'center',
  marginTop: getScaleSize(12),
  color: color('primary'),
});

export const BottomSubDescription = styled(Text, {
  ...font({ type: 'text5' }),
  color: color('gray6'),
  textAlign: 'center',
  marginTop: getScaleSize(10),
  marginHorizontal: getScaleSize(30),
});

export const UpdatePlanButtonText = styled(Text, {
  ...font({ type: 'text2', weight: 'normal' }),
  textTransform: 'uppercase',
  color: color('white'),
  marginRight: getScaleSize(6),
});
