import React from 'react';
import { PromoView } from './promo.view';

export const Promo = (): React.ReactElement => {
  return <PromoView />;
};
