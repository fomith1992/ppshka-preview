import { Bottom } from '@screens/recipes/ui/header.style';
import { GradientLayoutView } from '@shared/ui/gradient-layout/gradient-layout.view';
import React, { useEffect, useState } from 'react';
import { Alert, Image, Linking, Platform, TouchableOpacity, View } from 'react-native';

import ScreenLayout from 'src/components/ScreenLayout';
import { getScaleSize, windowWidth } from 'src/utils/dimensions';
import { getUserData } from 'src/store/session/actions';

import promo from '../../shared/ui/assets/PRO.png';
import checkBoxInactive from '../../shared/ui/assets/checkBoxInactive.png';
import checkBoxActive from '../../shared/ui/assets/checkBoxActive.png';
import kettlebell from '../../shared/ui/assets/kettlebell.png';
import {
  ButtonContainer,
  ButtonContainerBackground,
  Container,
  Title,
  Description,
  InfoDescription,
  UpdatePlanButton,
  UpdatePlanButtonText,
  UpdatePlanButtonInfo,
  BottomDescription,
  BottomSubDescription,
  RestorePurchasesText,
} from './promo.style';
import { useIAP, Purchase, requestSubscription, getAvailablePurchases } from 'react-native-iap';
import { Subscription } from 'react-native-iap';
import useAsync from 'react-use/lib/useAsync';
import { AppState } from 'src/store';
import { useDispatch, useSelector } from 'react-redux';
import { billingAPI } from 'src/api';
import { CommonActions, useNavigation } from '@react-navigation/native';
import { updateAccountType } from 'src/firebase/analytics/analytics-user/update-account-type';
import useAsyncFn from 'react-use/lib/useAsyncFn';

interface PromoButtonProps {
  item: Subscription;
  active: boolean;
  onPress: () => void;
}

const billingItemsParse = {
  pro1month: {
    title: '3 дня бесплатно,',
    description: 'затем 499 руб/мес',
    subDescription: '1 месяц - 499 руб/ мес',
  },
  pro6month: {
    title: '7 дней бесплатно, ',
    description: 'затем 2350 руб за 6 мес',
    subDescription: '6 месяцев - 2350 руб (390 руб/мес) - 20%',
  },
  pro6: {
    title: '7 дней бесплатно, ',
    description: 'затем 2350 руб за 6 мес',
    subDescription: '6 месяцев - 2350 руб (390 руб/мес) - 20%',
  },
  pro1year: {
    title: '7 дней бесплатно, ',
    description: 'затем 3490 руб за 12 мес',
    subDescription: '12 месяцев - 3490 руб (290 руб/мес) - 40%',
  },
};

const privacyPolicy = 'https://ppshka-app.com/oferta';

const PromoButton = ({ item, active, onPress }: PromoButtonProps): React.ReactElement => {
  return (
    <ButtonContainer onPress={onPress}>
      <GradientLayoutView reverseColor>
        <ButtonContainerBackground active={active}>
          <View>
            <Title>
              {
                billingItemsParse[item.productId as 'pro6month' | 'pro1year' | 'pro1month' | 'pro6']
                  .title
              }
            </Title>
            <Description>
              {
                billingItemsParse[item.productId as 'pro6month' | 'pro1year' | 'pro1month' | 'pro6']
                  .description
              }
            </Description>
            <InfoDescription>
              {
                billingItemsParse[item.productId as 'pro6month' | 'pro1year' | 'pro1month' | 'pro6']
                  .subDescription
              }
            </InfoDescription>
          </View>
          <Image
            style={{ width: 20, height: 18 }}
            resizeMode={'stretch'}
            source={active ? checkBoxActive : checkBoxInactive}
          />
        </ButtonContainerBackground>
      </GradientLayoutView>
    </ButtonContainer>
  );
};

const itemSubs = Platform.select({
  ios: ['pro6', 'pro1year', 'pro1month'],
  android: ['android.pro1month', 'pro6month', 'pro1year', 'pro1month'],
});

export const PromoView = (): React.ReactElement => {
  const [selectedItem, setSelectedItem] = useState<string>('0');
  const access_token = useSelector((state: AppState) => state.session.tokens.accessToken);
  const userId = useSelector((state: AppState) => state.session.user.id);
  const navigation = useNavigation();
  const dispatch = useDispatch();

  const { subscriptions, getSubscriptions, finishTransaction, currentPurchase } = useIAP();

  useEffect(() => {
    const checkCurrentPurchase = async (purchase?: Purchase): Promise<void> => {
      if (purchase) {
        const receipt = purchase.transactionReceipt;
        if (receipt)
          try {
            await finishTransaction(purchase);
          } catch (ackErr) {
            console.warn('ackErr', ackErr);
          }
      }
    };
    checkCurrentPurchase(currentPurchase);
  }, [currentPurchase, finishTransaction]);

  useAsync(async () => {
    await getSubscriptions(itemSubs ?? []);
  }, []);

  const buySubscription = (): void => {
    requestSubscription(selectedItem)
      .then((x) => {
        billingAPI
          .buySubscription({
            access_token,
            productId: x?.productId ?? '',
            platform: Platform.OS,
            response_json: JSON.stringify(x),
            ...Platform.select({
              android: {
                purchaseToken: x?.purchaseToken ?? '--',
              },
              ios: {
                originalTransactionId: x?.originalTransactionIdentifierIOS ?? '--',
              },
            }),
          })
          .then(() => {
            updateAccountType(userId, x?.productId ?? '--');
            dispatch(getUserData(access_token));
            navigation.dispatch(
              CommonActions.reset({
                index: 0,
                routes: [
                  {
                    name: 'TabNavigator',
                    params: {
                      screen: 'Dashboard',
                      initial: false,
                      params: {
                        initial: false,
                        screen: 'Dashboard',
                        params: {},
                      },
                    },
                  },
                ],
              }),
            );
          });
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const [, onRestorePurchases] = useAsyncFn(async () => {
    const availablePurchases = await getAvailablePurchases();
    if (availablePurchases.length > 0) {
      billingAPI
        .buySubscription({
          access_token,
          productId: availablePurchases[0]?.productId ?? '',
          platform: Platform.OS,
          response_json: JSON.stringify(availablePurchases),
          ...Platform.select({
            android: {
              purchaseToken: availablePurchases[0]?.purchaseToken ?? '--',
            },
            ios: {
              originalTransactionId:
                availablePurchases[0]?.originalTransactionIdentifierIOS ?? '--',
            },
          }),
        })
        .then(() => {
          dispatch(getUserData(access_token));
          Alert.alert(
            'Покупки восстановлены',
            'Покупки, которые вы приобретали ранее были восстановлены на текущем аккаунте',
          );
          navigation.dispatch(
            CommonActions.reset({
              index: 0,
              routes: [
                {
                  name: 'TabNavigator',
                  params: {
                    screen: 'Dashboard',
                    initial: false,
                    params: {
                      initial: false,
                      screen: 'Dashboard',
                      params: {},
                    },
                  },
                },
              ],
            }),
          );
        })
        .catch((e) => {
          console.log(e);
          Alert.alert('Ошибка', 'При попытке восстановить данные произошла ошибка');
        });
    } else {
      Alert.alert('Ошибка', 'Нет покупок, доступных для восстановления');
    }
  }, []);

  return (
    <ScreenLayout>
      <View style={{ flex: 1 }}>
        <Container>
          <View>
            <Image
              style={{ width: windowWidth, height: getScaleSize(585) }}
              resizeMode={'stretch'}
              source={promo}
            />
          </View>
          {subscriptions
            .sort((a, b) => +a.price - +b.price)
            .map((item) => (
              <PromoButton
                key={item.productId}
                item={item}
                active={selectedItem === item.productId}
                onPress={() => setSelectedItem(item.productId)}
              />
            ))}
          <UpdatePlanButton onPress={buySubscription}>
            <GradientLayoutView reverseColor>
              <UpdatePlanButtonInfo>
                <UpdatePlanButtonText>Расширить</UpdatePlanButtonText>
                <Image
                  style={{ width: getScaleSize(32), height: getScaleSize(32) }}
                  resizeMode={'stretch'}
                  source={kettlebell}
                />
              </UpdatePlanButtonInfo>
            </GradientLayoutView>
          </UpdatePlanButton>
          <TouchableOpacity onPress={onRestorePurchases}>
            <RestorePurchasesText>Восстановить покупки</RestorePurchasesText>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => Linking.openURL(privacyPolicy)}>
            <BottomDescription>
              Условия использования и политика{'\n'}конфиденциальности
            </BottomDescription>
          </TouchableOpacity>
          <BottomSubDescription>
            *Стоимость будет взиматься с привязанной карты автоматически. Отказаться от подписки
            можно не позднее, чем за 24 часа до окончания периода подписки, в противном случае
            подписка будет продлена автоматически в соотвествии с типом и смоимомть оформленной
            подписки.
          </BottomSubDescription>
          <Bottom mb={80} />
        </Container>
      </View>
    </ScreenLayout>
  );
};
