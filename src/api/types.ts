export interface TResponse<T = unknown> {
  success: boolean;
  data: T;
  meta: {
    [key: string]: unknown;
    message?: string;
  };
}
export interface TResponseReg<T = unknown> {
  status: 'ok' | 'error';
  data: T;
  error_code: number;
  message: string;
}
