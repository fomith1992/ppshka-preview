import AsyncStorage from '@react-native-async-storage/async-storage';
import axios, { AxiosInstance } from 'axios';
import Config from 'react-native-config';

export const instance = async (): Promise<AxiosInstance> => {
  const instanceUrl = (await AsyncStorage.getItem('APIURL')) ?? Config.API_BACKEND;
  return axios.create({
    baseURL: JSON.parse(instanceUrl),
  });
};
