import { instance } from './base.storage';
import { TResponse, TResponseReg } from './types';

export interface TSteps {
  description: string;
  image: string;
}
export interface TRecipe {
  id: number;
  name: string;
  short_description: string;
  url_image: string;
  url_gif: string;
  url_video: string;
  description: string;
  cooking_complexity: number;
  serving_time: number;
  deleted: 0 | 1;
  fave: 0 | 1;
  ccal: number;
  proteins: number;
  fats: number;
  cooking_size: number;
  carbonohydrates: number;
  steps: string[];
  has_access: boolean;
  publish_date: string;
  show_video: boolean;
  show_ingredients: boolean;
  show_steps: boolean;
  category_array: {
    default: boolean;
    id: number;
  }[];
  rate: number;
  rate_result: {
    [key: number]: number;
  };
}

export interface TIngredient {
  carbohydrates: number;
  ccal: number;
  deleted: 0 | 1;
  fats: number;
  gram_per_hundred: number;
  id: number;
  name: string;
  proteins: number;
  weight: number;
}

export interface RecipesInstance {
  page_cnt: number;
  current_page: number;
  data: TRecipe[];
  fave_list: number[];
}

const decodeFilter = {
  0: 'low',
  1: 'average',
  2: 'big',
};

export const getRecipes = async (data: {
  access_token: string;
  page: number;
  filter: { time: number | null; complexity: number | null };
  menuSelectedValue?: number;
  search: string;
}) => {
  return await (
    await instance()
  ).post<TResponse<RecipesInstance>>(`get_recipes`, {
    access_token: data.access_token,
    page: data.page,
    cooking_complexity: decodeFilter[data.filter.complexity as 0 | 1 | 2],
    serving_time: decodeFilter[data.filter.time as 0 | 1 | 2],
    category_id: data.menuSelectedValue,
    search: data.search.length > 0 ? data.search : undefined,
    pagination_limit: 20,
  });
};

export interface TCategories {
  id?: number;
  name: string;
  system_key: string;
}

export const getCategories = async () => {
  return await (await instance()).get<TResponse<{ data: TCategories[] }>>(`categories`);
};

export const getRecipe = async (data: { access_token: string; id: number }) => {
  return await (
    await instance()
  ).post<TResponse<{ data: TRecipe; ingredients: TIngredient[] }>>(`recipes/${data.id}`, {
    ...data,
  });
};

export const addToFavorite = async (data: { access_token: string; recipe_id: number }) => {
  return await (
    await instance()
  ).post<TResponse>(`recipe_to_favorites`, {
    ...data,
  });
};

export const getPopularPecipes = async (data: { access_token: string }) => {
  return await (
    await instance()
  ).post<TResponse<{ data: TRecipe[] }>>(`/recipes_popular`, {
    ...data,
  });
};

export const getNewRecipes = async (data: { access_token: string }) => {
  return await (
    await instance()
  ).post<TResponse<{ data: TRecipe[] }>>(`/get_new_recipes`, {
    ...data,
  });
};

export const addFeedbackToRecipe = async (data: {
  access_token: string;
  recipe_id: number;
  rate: number;
}) => {
  return await (
    await instance()
  )
    // data возвращает id нового отзыва
    .post<TResponseReg<number>>(`/set_recipe_rate_first`, {
      ...data,
    });
};
