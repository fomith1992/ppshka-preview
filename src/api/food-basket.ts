import { instance } from './base.storage';
import { TResponse } from './types';

/* {"category": null, "": 3, "done": false, "id": 1, "name": "Яблоко"} */

export interface TCheckList {
  category: null;
  col: number;
  index: number;
  name: string;
  id: string;
  done: boolean;
  gram: number;
}

export const getCheckList = async (data: { access_token: string }) => {
  return await (
    await instance()
  ).post<TResponse<TCheckList[]>>(`food_basket_show_checklist`, {
    ...data,
  });
};

export const addToFoodBasket = async (data: { access_token: string; date: string }) => {
  return await (
    await instance()
  ).post<TResponse>(`food_basket_add_by_date`, {
    ...data,
  });
};

export const deleteFoodBasket = async (data: { access_token: string; date: string }) => {
  return await (
    await instance()
  ).post<TResponse>(`food_basket_delete_by_date`, {
    ...data,
  });
};

export const applyIngredient = async (data: { access_token: string; ingredient_id: string }) => {
  return await (
    await instance()
  ).post<TResponse>(`food_basket_ingredient_check`, {
    ...data,
  });
};

export const clearBasket = async (data: { access_token: string }) => {
  return await (
    await instance()
  ).post<TResponse>(`clear_all_food_basket`, {
    ...data,
  });
};

export const getDontBuyIngredients = async (data: { access_token: string }) => {
  return await (
    await instance()
  ).post<TResponse<TCheckList[]>>(`get_dont_buy_food_basket`, {
    ...data,
  });
};

export const addRecipeToBasket = async (data: {
  access_token: string;
  recipe_id: number;
  col: number;
  meal_plan_id: number;
}) => {
  return await (
    await instance()
  ).post<TResponse>(`food_basket_add`, {
    ...data,
  });
};

export const delRecipeToBasket = async (data: { access_token: string; recipe_id: number }) => {
  return await (
    await instance()
  ).post<TResponse>(`food_basket_delete`, {
    ...data,
  });
};
