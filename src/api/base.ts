import axios from 'axios';
import Config from 'react-native-config';

export const instance = axios.create({
  baseURL: Config.API_BACKEND,
});

export const instance_spare = axios.create({
  baseURL: Config.API_BACKEND_SPARE,
});

export const getSpareApiUrl = async () => {
  return await instance_spare.get<{ spare_url: string }>(`check_actual_version`);
};
