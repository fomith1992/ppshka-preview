import * as auth from './auth';
import * as location from './location';
import * as recipesAPI from './recipes';
import * as userAPI from './user';
import * as meelAPI from './meal-plan';
import * as versionAPI from './version';
import * as workoutAPI from './workouts';
import * as programsAPI from './programs';
import * as measurementsAPI from './measurements';
import * as billingAPI from './billing';
import * as foodBasketAPI from './food-basket';
import * as feedbackAPI from './feedback';

export {
  auth,
  location,
  recipesAPI,
  userAPI,
  meelAPI,
  versionAPI,
  measurementsAPI,
  workoutAPI,
  programsAPI,
  billingAPI,
  foodBasketAPI,
  feedbackAPI,
};
