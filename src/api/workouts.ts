import { instance } from './base.storage';
import { TResponse, TResponseReg } from './types';

interface WorkoutInventory {
  name: string;
  cnt: string;
}

export interface Workout {
  id: number;
  name: string;
  inventory: WorkoutInventory[];
  description: string;
  url_image: string;
  url_video: string;
  duration: number;
  complexity: number;
  url_video_rezerv: string;
  deleted: number;
  preview_file_path?: string;
}

export interface PastWorkout {
  calendar_id: number;
  workout_id: number;
  program_id: number;
  date_start: string;
  done: boolean;
  number_of_list: number;
  mark: number;
  postpone_workout: boolean;
}

export type WorkoutCalendar = {
  id: number;
  date_start: string;
  number_of_list: number;
  workout: Workout;
};

export interface WorkoutsInstance {
  page_cnt: number;
  data: Workout[];
  fave_list: number[];
}

export const getWorkoutsList = async (data: { access_token: string; page: number }) => {
  return await (
    await instance()
  ).post<TResponse<WorkoutsInstance>>(`workouts`, {
    access_token: data.access_token,
    page: data.page,
  });
};

export const getWorkoutsCalendarList = async (data: { access_token: string }) => {
  return await (
    await instance()
  ).post<{ data: WorkoutCalendar[]; pause_program: boolean }>(
    `get_user_workouts_calendar_v2`,
    data,
  );
};

export const getWorkoutById = async (data: { access_token: string; id: number }) => {
  return await (
    await instance()
  ).post<TResponse<Workout>>(`workouts/${data.id}`, {
    ...data,
  });
};

export const finishWorkout = async (data: {
  access_token: string;
  workout_id: number;
  date_start: string; // format: yyyy-mm-dd
}) => {
  return await (
    await instance()
  ).post<TResponseReg>(`workout_done`, {
    ...data,
  });
};
