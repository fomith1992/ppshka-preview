import { MeasurementsParams } from '@screens/measurements/mock-data/mock-data';
import { instance } from './base.storage';
import { TResponse } from './types';

interface TMeasurements extends MeasurementsParams {
  access_token: string;
}

export const setMeasurements = async (props: TMeasurements) => {
  return await (await instance()).post<TResponse>(`set_user_measurements`, props);
};

export interface TUserMeasurementsHistory {
  data: string;
  weight: number;
}

export const getMeasurements = async (props: { access_token: string }) => {
  return await (
    await instance()
  ).post<TResponse<TUserMeasurementsHistory[]>>(`get_user_measurements_history`, {
    access_token: props.access_token,
  });
};

export const setMeasurementsPhotos = async (props: {
  id: string;
  img_front: string | null;
  img_sideways: string | null;
  img_behind: string | null;
}) => {
  return await (
    await instance()
  ).post<{ data: { id: number }; status: string }>(`set_user_measurements_photo`, props);
};
