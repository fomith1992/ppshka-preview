import { instance } from './base.storage';
import { TResponse, TResponseReg } from './types';

export interface Program {
  id: number;
  name: string;
  url_image: string;
  description: string;
  // TODO
  // compatible_with
  date_open: string;
  block: number;
  deleted: number;
  col_days: number;
  program_complexity: number;
}

export interface ProgramsInstance {
  page_cnt: number;
  current_page: number;
  data: Program[];
  fave_list: number[];
}

export const getProgramsList = async (data: { access_token: string; page: number }) => {
  return await (
    await instance()
  ).post<TResponse<ProgramsInstance>>(`programs`, {
    access_token: data.access_token,
    page: data.page,
  });
};

export const getAvailableProgramsList = async (access_token: string, page: number) => {
  return await (
    await instance()
  ).post<TResponse<ProgramsInstance>>(`get_user_available_programs`, {
    access_token,
    page,
  });
};

export const getProgramById = async (data: { access_token: string; id: number }) => {
  return await (
    await instance()
  ).post<TResponse<Program>>(`programs/${data.id}`, {
    ...data,
  });
};

export const subscribeToProgram = async (data: {
  access_token: string;
  program_id: number;
  date_start: 'today' | 'tomorrow';
}) => {
  return await (
    await instance()
  ).post<TResponseReg>(`subscribe_to_program`, {
    ...data,
  });
};

export const pausePrograms = async (data: { access_token: string }) => {
  return await (
    await instance()
  ).post<TResponseReg>(`pause-program`, {
    ...data,
  });
};

export const playPrograms = async (data: {
  access_token: string;
  date_start: 'today' | 'tomorrow';
}) => {
  return await (
    await instance()
  ).post<TResponseReg>(`renew-program`, {
    ...data,
  });
};
