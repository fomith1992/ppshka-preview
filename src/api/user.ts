import { instance } from './base.storage';
import { TChapter } from 'src/components/chapter-description/chapter-description.view';
import { ProgramsInstance } from './programs';
import { TResponse, TResponseReg } from './types';
import { PastWorkout } from './workouts';
import { format } from 'date-fns';

export interface TUserData {
  id: string | null;
  name: string | null;
  user_photo: string | null;
  activity: number | null;
  goal: number | null;
  is_pro: boolean;
  b_day: string;
  height: number | null;
}

export const userData = async (access_token: string) => {
  return await (await instance()).post<TResponse<TUserData>>(`get_user_data`, { access_token });
};

export interface TUserWeightHistory {
  data: string;
  weight: number;
  full_date: string;
}

export const getUserWeightHistory = async (access_token: string) => {
  return await (
    await instance()
  ).post<TResponse<TUserWeightHistory[]>>(`get_user_weight_history`, {
    access_token,
  });
};

export const getChapterDescription = async (props: { access_token: string; chapter: TChapter }) => {
  return await (await instance()).post<TResponse<TUserWeightHistory[]>>(`get_chapter_text`, props);
};

export const getUserPrograms = async (access_token: string, page: number) => {
  return await (
    await instance()
  ).post<TResponse<ProgramsInstance>>(`get_user_programs`, {
    access_token,
    page,
  });
};

export const setUserBDay = async (props: { access_token: string; b_day: string }) => {
  return await (await instance()).post<TResponse<TUserData>>(`set_user_b_day`, props);
};

export const getCaloricContent = async (props: { access_token: string }) => {
  return await (await instance()).post<TResponse<number>>(`get_user_calories`, props);
};

interface WaterContent {
  water: number;
  data: [
    {
      id: number;
      user_id: number;
      date: string;
      col_water: number;
    },
  ];
}

export const getWaterContent = async (props: { access_token: string }) => {
  return await (
    await instance()
  ).post<TResponse<WaterContent>>(`get_user_water`, {
    ...props,
    date: format(new Date(), 'yyyy-MM-dd'),
  });
};

export const addWaterContent = async (props: { access_token: string; col_water: number }) => {
  return await (
    await instance()
  ).post<TResponse<WaterContent>>(`add_user_water`, {
    ...props,
    date: format(new Date(), 'yyyy-MM-dd'),
  });
};

export const delWaterContent = async (props: { access_token: string; water_id: number }) => {
  return await (
    await instance()
  ).post<TResponse<WaterContent>>(`delete_user_water`, {
    ...props,
  });
};

export const setUserActivity = async (props: { access_token: string; activity: number }) => {
  return await (await instance()).post<TResponse<TUserData>>(`set_user_activity`, props);
};

export const setUserGoal = async (props: { access_token: string; goal: number }) => {
  return await (await instance()).post<TResponse<TUserData>>(`set_user_goal`, props);
};

export const getUserPastWorkouts = async (access_token: string) => {
  return await (
    await instance()
  ).post<TResponseReg<PastWorkout[]>>(`get_user_past_workouts`, {
    access_token,
  });
};

export const userPostponementWorkout = async (
  access_token: string,
  calendar_id: number,
  rebuild: boolean,
) => {
  return await (
    await instance()
  ).post<TResponseReg>(`user_postponement_workout`, {
    access_token,
    calendar_id,
    rebuild,
  });
};

export const setUserSteps = async (
  access_token: string,
  data: { date: string; value: number }[],
) => {
  return await (
    await instance()
  ).post<TResponseReg<PastWorkout[]>>(`add_user_steps`, {
    access_token,
    data: JSON.stringify(data),
  });
};

export const getUserSteps = async (access_token: string) => {
  return await (
    await instance()
  ).post<TResponseReg<[]>>(`get_user_steps`, {
    access_token,
  });
};

export const getUserWeightTarget = async (access_token: string) => {
  return await (
    await instance()
  ).post<TResponseReg<string>>(`get_user_weight_target`, {
    access_token,
  });
};

export const setUserWeightTarget = async (access_token: string, weigth: string) => {
  return await (
    await instance()
  ).post<TResponseReg<number>>(`add_user_weight_target`, {
    access_token,
    weight_target: weigth,
  });
};
