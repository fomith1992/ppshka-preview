import { instance } from './base.storage';
import { TResponseReg } from './types';

export const registerNotificationTarget = async (props: {
  access_token: string;
  deviceId: string;
  os: string;
  token: string;
}) => {
  return await (await instance()).post<TResponseReg>(`register_notification_target`, props);
};
