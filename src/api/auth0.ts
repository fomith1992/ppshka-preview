import Auth0 from 'react-native-auth0';
import { Config } from 'react-native-config';

import store from 'src/store';
import { auth } from 'src/api';

import { TLoginProvider } from '../models';
import { uiSetLoader } from '../store/ui/actions';
import { storage } from 'src/utils/storage';
import { sessionReset, sessionSetTokens } from 'src/store/session/actions.types';

export const auth0 = new Auth0({
  domain: Config.AUTH0_DOMAIN,
  clientId: Config.AUTH0_MOBILE_CLIENT_ID,
});

export const onLogin = (provider: TLoginProvider) => {
  store.dispatch(uiSetLoader(true));
  const getProvider = () => {
    switch (provider) {
      case 'google':
        return 'google-oauth2';

      case 'apple':
        return 'apple';
    }
  };
  // TODO: resolving of type issue. Creating store function   createStore:StoreCreator   returns a store of type Store that has a regular dispatch method. As we have Redux Thunk as a middleware and getMyProfile returns a function but not an action object, we have to redefine despatch's type with ThunkDispatch to calm down TypeScript. And it has no impact on runtime
  // const dispatch = store.dispatch as ThunkDispatch<AppState, null, TSessionActions>;
  // dispatch(sessionLoadingStart());

  auth0.webAuth
    .authorize({
      scope: 'openid profile email offline_access',
      connection: getProvider(),
      prompt: 'login',
    })
    .then(async (credentials) => {
      console.log('Credentials', credentials);
      if (credentials.refreshToken) {
        storage.setRefreshToken({ refresh_token: credentials.refreshToken, type: 'auth0' });
        await auth.authByAuth0(credentials.accessToken).then(({ data }) => {
          if (credentials.refreshToken) {
            store.dispatch(
              sessionSetTokens({
                accessToken: data.data.access_token,
                refreshToken: credentials.refreshToken,
              }),
            );
          }
        });
      } else {
        throw Error('Error get access/refresh tokens');
      }
    })
    .catch((e) => {
      console.log('Error onLogin', e);
    })
    .finally(() => {
      store.dispatch(uiSetLoader(false));
    });
};

export const onLogout = async () => {
  store.dispatch(sessionReset());
  await storage.removeRefreshToken();

  // TODO: cleanup if no troubles with login
  // auth0.webAuth
  //   .clearSession()
  //   .then(() => {
  //   })
  //   .catch((e) => {
  //     console.log('Error logout', e);
  //   });
};
