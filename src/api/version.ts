import { instance } from './base';

interface TCurrentVersoin {
  need_update: boolean;
}

export const getCurrentVersoin = async (data: { version: string }) => {
  return await instance.post<TCurrentVersoin>(`check_actual_version_v2`, { data });
};
