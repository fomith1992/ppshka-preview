import { instance } from './base.storage';
import { TResponse } from './types';

export interface TMeetItem {
  recipe_id: number;
  name: string;
  short_description: string;
  url_image: string;
  cooking_complexity: number;
  serving_time: number;
  col_serving: number;
  category_id: number;
  time_category: string;
  date: string;
  meal_id: number;
  ccal: number;
  cooking_size: number;
  food_basket: boolean;
}
export interface TMeetData {
  data: TMeetItem[];
  food_basket: boolean;
}

export interface TPlanCategories {
  id: number;
  name: string;
  system_key: string;
  sort: number;
  show_in_plan: boolean;
}

export const getMeelData = async (access_token: string, date: string) => {
  return await (await instance()).post<TMeetData>(`meal_plan`, { access_token, date });
};

interface AddToMealPlanProps {
  access_token: string;
  date: string;
  recipe_id: number;
  category_id?: number;
}

export const addToMealPlan = async ({
  access_token,
  category_id,
  date,
  recipe_id,
}: AddToMealPlanProps) => {
  return await (
    await instance()
  ).post<TResponse<TMeetItem[]>>(`/meal_plan_add`, {
    access_token,
    date,
    recipe_id,
    category_id,
  });
};

export const delMeelData = async (access_token: string, id: number) => {
  return await (await instance()).post<TResponse<TMeetItem[]>>(`meal_plan/${id}`, { access_token });
};

export const changeColServing = async (data: {
  access_token: string;
  col: number;
  meal_id: number;
}) => {
  return await (
    await instance()
  ).post<TResponse>(`meal_plan_change_col/${data.meal_id}`, {
    access_token: data.access_token,
    col: data.col,
  });
};

interface CheckMealDataProps {
  access_token: string;
  date: string;
  category_id?: number;
}
export const checkMealData = async ({ access_token, date, category_id }: CheckMealDataProps) => {
  return await (
    await instance()
  ).post<TResponse<{ exist: boolean }>>(`/meal_plan_check`, {
    access_token,
    date,
    category_id,
  });
};

export const getCategoriesPlan = async () => {
  return await (
    await instance()
  ).get<TResponse<{ data: TPlanCategories[] }>>(`categories_for_plan`);
};
