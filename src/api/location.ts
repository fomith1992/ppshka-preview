import { instance } from './base.storage';
import { TResponse } from './types';

export const setTimeZone = async (data: { access_token: string; timezone: string }) => {
  return await (await instance()).post<TResponse>(`set_time_zone`, { ...data });
};
