import { instance } from './base.storage';
import { TResponse, TResponseReg } from './types';

export type Feedback<T> = {
  id: number;
  user_id: number;
  rate: number;
  add_date: string;
  comment: string;
  photo_file_path: string;
  moderation: boolean;
  normal_add_date: string;
  user_name: string;
  user_photo: string;
} & T;

interface FeedbacksInstance<T> {
  page_cnt: number;
  current_page: number;
  data: Feedback<T>[];
}

export type RecipeFeedbacksIstance = FeedbacksInstance<{ recipe_id: number }>;

export const addAttachmentsToFeedback = async (data: {
  feedbackId: number;
  text: string;
  photoPath: string;
}) => {
  return await (
    await instance()
  ).post<TResponseReg>(`/set_recipe_rate_second`, {
    recipe_rate_id: data.feedbackId,
    comment: data.text,
    photo: data.photoPath,
  });
};

export const recipeFeedbacksById = async (data: {
  recipe_id: number;
  page: number;
  access_token: string;
  pagination_limit?: number;
}) => {
  const pagination_limit = data.pagination_limit ?? 5;
  return await (
    await instance()
  ).post<TResponse<RecipeFeedbacksIstance>>(`/get_recipe_rates`, {
    ...data,
    pagination_limit,
  });
};
