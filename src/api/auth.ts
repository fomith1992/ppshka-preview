import { instance } from './base.storage';
import { TResponse, TResponseReg } from './types';

type TVerifySMSCode = {
  email: string;
  password: string;
};

type RefreshAccessToken = {
  access_token: string;
  refresh_token: string;
};

interface RegisterByEmailPass extends TVerifySMSCode {
  name?: string;
}

export const registerByEmailPass = async (body: RegisterByEmailPass) => {
  return await (
    await instance()
  ).post<TResponseReg<RefreshAccessToken>>(`registration`, { ...body });
};

interface RegisterSetUserParams {
  access_token: string;
  name?: string;
  b_day?: string;
  weight?: string;
  height?: string;
  activity?: number;
  goal?: number;
  sex?: 'male' | 'female';
}

export const registerSetUserParams = async (body: RegisterSetUserParams) => {
  return await (
    await instance()
  ).post<TResponseReg<RefreshAccessToken>>(`set_user_samples`, { ...body });
};

export const updateUserPhoto = async (body: {
  file_path: string | null | undefined;
  access_token: string;
}) => {
  return await (await instance()).post<TResponseReg<RefreshAccessToken>>(`load_photo`, { ...body });
};

export const authByEmailPass = async (body: TVerifySMSCode) => {
  return await (
    await instance()
  ).post<TResponseReg<RefreshAccessToken>>(`auth_standard`, { ...body });
};

type TAuthByAuth0 = {
  access_token: string;
};

export const authByAuth0 = async (token: string) => {
  return await (await instance()).post<TResponse<TAuthByAuth0>>(`auth_0`, { token });
};

export const refreshAccessToken = async (props: { refresh_token: string }) => {
  return await (await instance()).post<TResponse<RefreshAccessToken>>(`update_token`, { ...props });
};

export const refreshAccessTokenAuth0 = async (token: string) => {
  return await (await instance()).post<TResponse<TAuthByAuth0>>(`auth_0`, { token });
};

//! reset password
export const resetPassSendCode = async (data: { email: string }) => {
  return await (await instance()).post<TResponseReg<RefreshAccessToken>>(`reset_password`, data);
};

export const resetPassConfirmCode = async (body: {
  access_token: string;
  confirmation_code: string;
}) => {
  return await (
    await instance()
  ).post<TResponseReg<RefreshAccessToken>>(`reset_password_confirm_code`, {
    ...body,
  });
};

export const updatePass = async (body: {
  access_token: string;
  new_password: string;
  confirmation_password: string;
}) => {
  return await (
    await instance()
  ).post<TResponseReg<RefreshAccessToken>>(`reset_password_confirm_password`, {
    ...body,
  });
};
