import { instance } from './base.storage';
import { TResponse } from './types';

export const buySubscription = async (data: {
  access_token: string;
  purchaseToken?: string;
  productId: string;
  originalTransactionId?: string;
  platform: string;
  response_json: string;
}) => {
  return await (
    await instance()
  ).post<TResponse>(`pay_notify_app`, {
    ...data,
  });
};
