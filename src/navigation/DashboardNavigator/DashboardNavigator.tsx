import React from 'react';

import { createStackNavigator } from '@react-navigation/stack';
import { DashboardScreen } from '@screens/dashboard/dashboard.screen';
import { RecipesScreen, ReturnScreenRecipes } from '@screens/recipes/recipes.screen';
import { RecipeFull } from '@screens/recipe-full/recipe-full.screen';
import { MeasurementsScreen } from '@screens/measurements/measurements.screen';
import { PromoScreen } from '@screens/promo/promo.screen';
import { AddParamPhotosScreen } from '@screens/add-param-photos/add-param-photos.screen';
import { SetWeightScreen } from '@screens/dashboard/set-weight/set-weight.container';
import { SetWeightTargetScreen } from '@screens/dashboard/set-weight/set-weight-target.container';

export type DashboardStackParamList = {
  Dashboard: { date?: Date };
  RecipeFull: {
    id: number;
    date?: Date;
    returnScreen?: ReturnScreenRecipes;
    category: { default: boolean; id: number };
  };
  SelectMeel: { type?: number; date?: Date; returnScreen?: ReturnScreenRecipes };
  SetWeight: Record<string, never>;
  SetWeightTarget: Record<string, never>;
  Measurements: Record<string, never>;
  Promo: Record<string, never>;
  AddParamPhotos: Record<string, never>;
};

const Stack = createStackNavigator<DashboardStackParamList>();

const DashboardNavigator = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}
    >
      <Stack.Screen name="Dashboard" component={DashboardScreen} />
      <Stack.Screen name="SelectMeel" component={RecipesScreen} />
      <Stack.Screen name="RecipeFull" component={RecipeFull} />
      <Stack.Screen name="SetWeight" component={SetWeightScreen} />
      <Stack.Screen name="SetWeightTarget" component={SetWeightTargetScreen} />
      <Stack.Screen name="Measurements" component={MeasurementsScreen} />
      <Stack.Screen name="Promo" component={PromoScreen} />
      <Stack.Screen name="AddParamPhotos" component={AddParamPhotosScreen} />
    </Stack.Navigator>
  );
};

export default DashboardNavigator;
