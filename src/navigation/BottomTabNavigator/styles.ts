import { StyleSheet } from 'react-native';

import { bottomTabsHeight, getScaleSize } from 'src/utils/dimensions';

import { deviceBottomPadding } from '../../utils/dimensions';

export const styles = StyleSheet.create({
  tabBarOptionsStyle: {
    height: bottomTabsHeight,
    position: 'absolute',
    marginBottom: getScaleSize(15) + deviceBottomPadding,
    /* bottom: getScaleSize(15) + deviceBottomPadding, */
    left: getScaleSize(15),
    right: getScaleSize(15),
    borderRadius: getScaleSize(12),
    backgroundColor: '#fff',

    // marginBottom: getScaleSize(15),
    // marginHorizontal: getScaleSize(15),

    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.1,
    shadowRadius: 25,
    elevation: 2,
  },
});
