import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { purpleColor } from 'src/commonStyles/colors';
import FoodNavigator from 'src/navigation/FoodNavigator';
import MealPlanNavigator from 'src/navigation/meal-plan-navigator';
import { styles } from './styles';
import DashboardNavigator from '../DashboardNavigator';
import { Image } from 'react-native';

import dashboard from '../../shared/ui/assets/dashboard.png';
import dashboardInactive from '../../shared/ui/assets/dashboard-inactive.png';

import food from '../../shared/ui/assets/food.png';
import foodInactive from '../../shared/ui/assets/food-inactive.png';

import recipes from '../../shared/ui/assets/recipes-plan.png';
import recipesInactive from '../../shared/ui/assets/recipes-plan-inactive.png';

import profile from '../../shared/ui/assets/profile.png';
import profileInactive from '../../shared/ui/assets/profile-inactive.png';

import trainings from '../../shared/ui/assets/trainings.png';
import trainingsInactive from '../../shared/ui/assets/trainings-inactive.png';

import WorkoutNavigator from '../WorkoutNavigator';
import ProfileNavigator from '../profile-navigator';

const Tab = createBottomTabNavigator();

const tabNavigatorOptions = {
  safeAreaInsets: {
    bottom: 0,
    top: 0,
  },
  activeTintColor: purpleColor,
  showLabel: false,
  style: styles.tabBarOptionsStyle,
  keyboardHidesTabBar: true,
};

export const BottomTabNavigator = () => {
  return (
    <Tab.Navigator tabBarOptions={tabNavigatorOptions} initialRouteName="Dashboard">
      <Tab.Screen
        name="MealPlan"
        component={MealPlanNavigator}
        options={{
          tabBarIcon: ({ focused }) => (
            <Image style={{ width: 40, height: 40 }} source={focused ? recipes : recipesInactive} />
          ),
        }}
      />
      <Tab.Screen
        name="Food"
        component={FoodNavigator}
        options={{
          tabBarIcon: ({ focused }) => (
            <Image style={{ width: 40, height: 40 }} source={focused ? food : foodInactive} />
          ),
        }}
      />
      <Tab.Screen
        name="Dashboard"
        component={DashboardNavigator}
        options={{
          tabBarIcon: ({ focused }) => (
            <Image
              style={{ width: 40, height: 40 }}
              source={focused ? dashboard : dashboardInactive}
            />
          ),
        }}
      />
      <Tab.Screen
        name="Workout"
        component={WorkoutNavigator}
        options={{
          tabBarIcon: ({ focused }) => (
            <Image
              style={{ width: 40, height: 40 }}
              source={focused ? trainings : trainingsInactive}
            />
          ),
        }}
      />
      <Tab.Screen
        name="Profile"
        component={ProfileNavigator}
        options={{
          tabBarIcon: ({ focused }) => (
            <Image style={{ width: 40, height: 40 }} source={focused ? profile : profileInactive} />
          ),
        }}
      />
    </Tab.Navigator>
  );
};
