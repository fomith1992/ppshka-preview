import React from 'react';

import { createStackNavigator } from '@react-navigation/stack';

import { BottomTabNavigator } from '../BottomTabNavigator/bottom-tab.navigator';
import WorkoutVideoScreen from '@screens/workout-video';

export type AuthorizedStackParamList = {
  TabNavigator: Record<string, never>;
  WorkoutVideoView: { id: number; dateStart: string };
};

const Stack = createStackNavigator();

const AuthorizedNavigator = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}
    >
      <Stack.Screen name="TabNavigator" component={BottomTabNavigator} />
      <Stack.Screen name="WorkoutVideoView" component={WorkoutVideoScreen} />
    </Stack.Navigator>
  );
};

export default AuthorizedNavigator;
