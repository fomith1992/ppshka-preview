import React from 'react';

import { createStackNavigator } from '@react-navigation/stack';

// import { TrainingQuestionsScreen } from '@screens/questions';

import ProgramDetailsScreen from '@screens/program-details/program-details.screen';
import ProgramsScreen from '@screens/programs';
import WorkoutDetailsScreen from '@screens/workout-details';
import { PromoScreen } from '@screens/promo/promo.screen';

export type WorkoutStackParamList = {
  ProgramsList: Record<string, never>;
  ProgramDetails: { id: number };
  WorkoutDetails: { id: number; dateStart: string; calendar_id: number };
  Promo: Record<string, never>;
};

const Stack = createStackNavigator<WorkoutStackParamList>();

const WorkoutNavigator = () => {
  return (
    <Stack.Navigator screenOptions={{ headerShown: false }}>
      <Stack.Screen name="ProgramsList" component={ProgramsScreen} />
      <Stack.Screen name="ProgramDetails" component={ProgramDetailsScreen} />
      <Stack.Screen name="WorkoutDetails" component={WorkoutDetailsScreen} />
      <Stack.Screen name="Promo" component={PromoScreen} />
    </Stack.Navigator>
  );
};

export default WorkoutNavigator;
