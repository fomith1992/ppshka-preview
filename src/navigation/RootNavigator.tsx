import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { createStackNavigator } from '@react-navigation/stack';

import { ConfirmEmailScreen, EnterEmailScreen, NewPasswordScreen } from '@screens/recovery-pass';

import SignInScreen from 'src/screens/SignInScreen';
import SignUpScreen from 'src/screens/SignUpScreen';
import { AppState } from 'src/store';
import { getCategoriesData, getUserData, sessionBootstrap } from 'src/store/session/actions';

import AuthorizedNavigator from './AuthorizedNavigator/AuthorizedNavigator';
import OnboardingNameScreen from '@screens/OnboardingNameScreen';
import OnboardingAvatarScreen from '@screens/OnboardingAvatarScreen';
import OnboardingWeightScreen from '@screens/OnboardingWeightScreen';
import OnboardingQuestionGoalScreen from '@screens/OnboardingQuestionScreen/OnboardingQuestionGoal';
import { OnboardingUserSexScreen } from '@screens/OnboardingQuestionScreen/OnboardingUserSex';
import { OnboardingUserBirthdayScreen } from '@screens/OnboardingQuestionScreen/OnboardingUserBirthday';
import OnboardingQuestionScreen from '@screens/OnboardingQuestionScreen';
import SplashScreen from 'react-native-splash-screen';
import OnboardingHeightScreen from '@screens/OnboardingWeightScreen/OnboardingHeightScreen';
/* import { getStepsFromDaysAgoCount } from 'src/app/providers/apple-health-kit.provider'; */

export type RootStackParamList = {
  Authorized: Record<string, never>;
  SignIn: Record<string, never>;
  SignUp: Record<string, never>;
  EnterEmail: Record<string, never>;
  ConfirmEmail: { access_token: string; email: string };
  NewPassword: { access_token: string };
  OnboardingName: { access_token: string; refresh_token: string };
  OnboardingAvatar: { access_token: string; refresh_token: string; name: string };
  OnboardingUserSex: { access_token: string; refresh_token: string; name: string };
  OnboardingUserBirthday: {
    access_token: string;
    refresh_token: string;
    name: string;
    sex: 'male' | 'female';
  };
  OnboardingWeight: { access_token: string; refresh_token: string; name: string; birthday: Date };
  OnboardingHeight: {
    access_token: string;
    refresh_token: string;
    name: string;
    weight: string;
    birthday: Date;
  };

  OnboardingQuestionActivity: {
    access_token: string;
    refresh_token: string;
    name: string;
    weight: string;
    height: string;
    sex: 'male' | 'female';
    birthday: Date;
  };
  OnboardingQuestionGoal: {
    access_token: string;
    refresh_token: string;
    name: string;
    weight: string;
    height: string;
    activity: number;
    sex: 'male' | 'female';
    birthday: Date;
  };
};

const Stack = createStackNavigator<RootStackParamList>();

export const RootNavigator = () => {
  const dispatch = useDispatch();
  const accessToken = useSelector((state: AppState) => state.session.tokens.accessToken);

  useEffect(() => {
    if (!accessToken) {
      dispatch(sessionBootstrap());
    } else {
      dispatch(getCategoriesData());
      dispatch(getUserData(accessToken));
      /* getStepsFromDaysAgoCount({ access_token: accessToken, daysCount: 10 }); */
    }
    SplashScreen.hide();
  }, [accessToken]); // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}
      mode="modal"
    >
      {accessToken ? (
        <Stack.Screen name="Authorized" component={AuthorizedNavigator} />
      ) : (
        <>
          <Stack.Screen name="SignUp" component={SignUpScreen} />
          <Stack.Screen name="SignIn" component={SignInScreen} />
          <Stack.Screen name="EnterEmail" component={EnterEmailScreen} />
          <Stack.Screen name="ConfirmEmail" component={ConfirmEmailScreen} />
          <Stack.Screen name="NewPassword" component={NewPasswordScreen} />
          <Stack.Screen name="OnboardingName" component={OnboardingNameScreen} />
          <Stack.Screen name="OnboardingAvatar" component={OnboardingAvatarScreen} />
          <Stack.Screen name="OnboardingUserSex" component={OnboardingUserSexScreen} />
          <Stack.Screen name="OnboardingUserBirthday" component={OnboardingUserBirthdayScreen} />
          <Stack.Screen name="OnboardingWeight" component={OnboardingWeightScreen} />
          <Stack.Screen name="OnboardingHeight" component={OnboardingHeightScreen} />
          <Stack.Screen name="OnboardingQuestionActivity" component={OnboardingQuestionScreen} />
          <Stack.Screen name="OnboardingQuestionGoal" component={OnboardingQuestionGoalScreen} />
          {/* <Stack.Screen name="OnboardingQuestionGoal" component={OnboardingQuestionGoalScreen} /> */}
        </>
      )}
    </Stack.Navigator>
  );
};
