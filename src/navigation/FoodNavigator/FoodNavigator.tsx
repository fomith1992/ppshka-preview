import React from 'react';

import { createStackNavigator } from '@react-navigation/stack';

import { FoodQuestionsScreen } from '@screens/questions';

import RecipeDetailsScreen from 'src/screens/RecipeDetailsScreen';
import { RecipesScreen, ReturnScreenRecipes } from '@screens/recipes/recipes.screen';
import { RecipeFull } from '@screens/recipe-full/recipe-full.screen';
import { PromoScreen } from '@screens/promo/promo.screen';
import { RecipeFeedbacksScreen } from '@screens/recipe-feedbacks-screen/recipe-feedbacks-screen.screen';

export type FoodStackParamList = {
  FoodQuestionsScreen: Record<string, never>;
  Promo: Record<string, never>;
  Food: { type?: number; date?: Date; returnScreen?: ReturnScreenRecipes };
  RecipeDetails: Record<string, never>;
  RecipeFull: {
    id: number;
    date?: Date;
    returnScreen?: ReturnScreenRecipes;
    category: { default: boolean; id: number };
  };
  RecipeFeedbacks: {
    id: number;
    image?: string;
  };
};

const Stack = createStackNavigator<FoodStackParamList>();

const FoodNavigator = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}
    >
      <Stack.Screen name="FoodQuestionsScreen" component={FoodQuestionsScreen} />
      <Stack.Screen name="Food" component={RecipesScreen} />
      <Stack.Screen name="RecipeDetails" component={RecipeDetailsScreen} />
      <Stack.Screen name="RecipeFull" component={RecipeFull} />
      <Stack.Screen name="Promo" component={PromoScreen} />
      <Stack.Screen name="RecipeFeedbacks" component={RecipeFeedbacksScreen} />
    </Stack.Navigator>
  );
};

export default FoodNavigator;
