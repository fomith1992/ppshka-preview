import React from 'react';

import { createStackNavigator } from '@react-navigation/stack';

import FavoritesScreen from 'src/screens/FavoritesScreen';

const Stack = createStackNavigator();

const FavoritesNavigator = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}
    >
      <Stack.Screen name="Favorites" component={FavoritesScreen} />
    </Stack.Navigator>
  );
};

export default FavoritesNavigator;
