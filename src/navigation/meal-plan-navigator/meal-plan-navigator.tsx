import React from 'react';

import { createStackNavigator } from '@react-navigation/stack';

import { FullPostScreen } from '@screens/full-post/full-post.screen';
import { RecipesPlanScreen } from '@screens/recipes-plan/recipes-plan.screen';
import { RecipesScreen, ReturnScreenRecipes } from '@screens/recipes/recipes.screen';
import { RecipeFull } from '@screens/recipe-full/recipe-full.screen';
import { ShoppingCartScreen } from '@screens/shopping-cart/shopping-cart.screen';

export type MealPlanParamList = {
  MealPlan: { date?: Date };
  Food: Record<string, never>;
  ShoppingCart: Record<string, never>;
  FullPost: Record<string, never>;
  SelectMeel: { type?: number; date?: Date; returnScreen?: ReturnScreenRecipes };
  RecipeDetails2: { id: number; date?: Date; returnScreen?: ReturnScreenRecipes };
  RecipeFull: { id: number; date?: Date };
};

const Stack = createStackNavigator<MealPlanParamList>();

const MealPlanNavigator = () => {
  return (
    <Stack.Navigator screenOptions={{ headerShown: false }}>
      <Stack.Screen name="MealPlan" component={RecipesPlanScreen} />
      <Stack.Screen name="FullPost" component={FullPostScreen} />
      <Stack.Screen name="SelectMeel" component={RecipesScreen} />
      <Stack.Screen name="RecipeFull" component={RecipeFull} />
      <Stack.Screen name="ShoppingCart" component={ShoppingCartScreen} />
    </Stack.Navigator>
  );
};

export default MealPlanNavigator;
