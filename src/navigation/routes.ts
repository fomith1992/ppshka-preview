export enum Routes {
  // TODO replace all string values on enums values
  RecipesScreen = 'RecipesScreen',
  WorkoutScreen = 'WorkoutScreen',
  FavoritesScreen = 'FavoritesScreen',
  ProfileScreen = 'ProfileScreen',
  RecipeFull = 'RecipeFull',
}
