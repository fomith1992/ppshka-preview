import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import { ProfileScreen } from '@screens/profile/profile.screen';
import { SetHeightScreen } from '@screens/set-height/set-height.container';

export type FoodStackParamList = {
  Profile: Record<string, never>;
  SetHeight: Record<string, never>;
};

const Stack = createStackNavigator<FoodStackParamList>();

const ProfileNavigator = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}
    >
      <Stack.Screen name="Profile" component={ProfileScreen} />
      <Stack.Screen name="SetHeight" component={SetHeightScreen} />
    </Stack.Navigator>
  );
};

export default ProfileNavigator;
