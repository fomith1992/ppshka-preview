import { format } from 'date-fns';

export const isEqualDay = (day1: Date, day2: Date): boolean => {
  return format(day1, 'd') === format(day2, 'd');
};

export function daysInMonth(month: number, year: number) {
  return new Date(year, month, 0).getDate();
}
