import { Dimensions } from 'react-native';
import { getStatusBarHeight, isIPhoneWithMonobrow } from 'react-native-status-bar-height';

export const windowWidth = Dimensions.get('window').width;
export const windowHeight = Dimensions.get('window').height;

export const screenWidth = Dimensions.get('screen').width;
export const screenHeight = Dimensions.get('screen').height;

export const getScaleSize = (size: number) => {
  const coef = windowWidth / 375 < windowHeight / 812 ? windowWidth / 375 : windowHeight / 812;
  return Math.floor(size * coef);
};

export const statusBarHeight = getStatusBarHeight();

export const deviceBottomPadding = isIPhoneWithMonobrow() ? 12 : 0;

export const bottomTabsHeight = getScaleSize(60);
