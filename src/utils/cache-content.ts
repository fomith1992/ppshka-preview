import { Platform } from 'react-native';
import RNFS from 'react-native-fs';
import md5 from 'md5';

const localPath = (url: string) => {
  return Platform.OS === 'ios'
    ? `${RNFS.DocumentDirectoryPath}/${url}`
    : `${RNFS.ExternalDirectoryPath}/${url}`;
};

const resultLocationParse = (url: string, format: string) => {
  switch (format) {
    case 'png':
      return `file://${url}`;
    default:
      return url;
  }
};

const getLocalFile = async (data: { url: string; format: string }) => {
  const fileName = `${md5(data.url)}.${data.format}`;
  const isExistFile = await RNFS.exists(localPath(fileName));
  if (isExistFile) return resultLocationParse(localPath(fileName), data.format);
  return undefined;
};

export const getCacheContent = async (data: {
  url?: string;
  format: string;
}): Promise<string | undefined> => {
  const { format, url } = data;
  if (url != null) {
    const isExistFile = await RNFS.exists(localPath(`${md5(url)}.${format}`));
    if (isExistFile) return await getLocalFile({ format, url });
    await RNFS.downloadFile({
      fromUrl: url,
      toFile: localPath(`${md5(url)}.${data.format}`),
    })
      .promise.then(async () => {
        console.log(`successful ${data.format} download!`);
      })
      .catch((x) => console.log(x));
    return await getLocalFile({ format, url });
  }
  return undefined;
};
