import AsyncStorage from '@react-native-async-storage/async-storage';
import store from 'src/store';
import { uiSetFavorites } from 'src/store/ui/actions';

export const checkIsFaveReceipe = async (id: number): Promise<boolean> => {
  let isFave = false;
  try {
    await AsyncStorage.getItem('FavoriteReceipes', (_, result) => {
      if (result !== undefined) {
        const data = JSON.parse(result) as number[];
        if (data != null) {
          isFave = data.includes(id);
        } else return;
      } else return;
    });
  } catch (err) {
    console.log(err);
  }
  return isFave;
};

export const setFavoriteReceipe = async (id: number): Promise<void> => {
  const data: number[] = [];
  await AsyncStorage.getItem('FavoriteReceipes', async (_, result) => {
    if (result !== undefined) {
      data.push(...(JSON.parse(result) as number[]), id);
      store.dispatch(uiSetFavorites(data));
      await AsyncStorage.setItem('FavoriteReceipes', JSON.stringify(data));
    }
  });
};

export const delFavoriteReceipe = async (id: number): Promise<void> => {
  await AsyncStorage.getItem('FavoriteReceipes', async (_, result) => {
    if (result !== undefined) {
      const data: number[] = (JSON.parse(result) as number[]).filter((x) => x !== id);
      store.dispatch(uiSetFavorites((JSON.parse(result) as number[]).filter((x) => x !== id)));
      await AsyncStorage.setItem('FavoriteReceipes', JSON.stringify(data));
    }
  });
};

export const setPopularReceipes = async (ids: number[]): Promise<void> => {
  store.dispatch(uiSetFavorites(ids));
  await AsyncStorage.setItem('FavoriteReceipes', JSON.stringify(ids));
};
