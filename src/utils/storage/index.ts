import * as storage from './storage';
import * as receipes from './receipes.storage';
import * as favoritesAPI from './favorites.storage';

export { storage, receipes, favoritesAPI };
