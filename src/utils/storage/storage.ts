import AsyncStorage from '@react-native-async-storage/async-storage';
import { format } from 'date-fns';

interface RefreshTokenEmail {
  refresh_token: string;
  type: 'email';
}

export interface RefreshTokenAuth0 {
  refresh_token: string;
  type: 'auth0';
}
export interface RefreshTokenNull {
  refresh_token: null;
  type: null;
}
export type RefreshToken = RefreshTokenEmail | RefreshTokenAuth0 | RefreshTokenNull;

/**
 * Set refresh tokens
 * @param refreshToken refresh token
 *
 * @returns {Promise<void>}
 */
export const setRefreshToken = async (props: RefreshToken): Promise<void> => {
  try {
    if (props.refresh_token) {
      await AsyncStorage.setItem('refreshToken', JSON.stringify(props));
    }
  } catch (err) {
    console.log(err);
  }
};

/**
 * Get refresh token
 * @returns {?Promise<RefreshTokenEmail | RefreshTokenAuth0 | RefreshTokenNull>}
 *
 */
export const getRefreshToken = async (): Promise<RefreshToken> => {
  try {
    const asyncStorageData = await AsyncStorage.getItem('refreshToken');
    if (asyncStorageData) {
      const refreshToken: RefreshTokenEmail | RefreshTokenAuth0 = JSON.parse(asyncStorageData);
      return refreshToken;
    }
    throw new Error('There is no stored refresh token');
  } catch (err) {
    console.log(err);
    return { refresh_token: null, type: null };
  }
};

/**
 * Set null to refresh token
 * @returns {Promise<void>}
 */
export const removeRefreshToken = async (): Promise<void> => {
  try {
    await AsyncStorage.setItem('refreshToken', '');
  } catch (err) {
    console.log(err);
  }
};

export const setUpdateLocationDate = async (): Promise<void> => {
  const timezone = format(new Date(), 'xx');
  try {
    await AsyncStorage.setItem('UpdateLocationDate', timezone);
  } catch (err) {
    console.log(err);
  }
};

export const isUpdateLocationDate = async () => {
  const timezone = format(new Date(), 'xx');
  const storageData = await AsyncStorage.getItem('UpdateLocationDate');
  return timezone === storageData;
};
