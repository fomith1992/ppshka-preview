import { format } from 'date-fns';
import { TRecipe } from 'src/api/recipes';
import AsyncStorage from '@react-native-async-storage/async-storage';

//! We need get popular receipes once of day
export const isDateChange = async () => {
  const date = format(new Date(), 'DDD');
  const storageDate = await AsyncStorage.getItem('UpdateDate');
  return date !== storageDate;
};

export const setDateChange = async (): Promise<void> => {
  const timezone = format(new Date(), 'DDD');
  try {
    await AsyncStorage.setItem('UpdateDate', timezone);
  } catch (err) {
    console.log(err);
  }
};

export const setPopularReceipes = async (data: TRecipe[]): Promise<void> => {
  try {
    await AsyncStorage.setItem('PopularReceipes', JSON.stringify(data));
  } catch (err) {
    console.log(err);
  }
};

export const getPopularReceipes = async (): Promise<TRecipe[]> => {
  const data: TRecipe[] = [];
  await AsyncStorage.getItem('PopularReceipes', (_, result) => {
    if (result !== undefined) {
      data.push(...(JSON.parse(result) as TRecipe[]));
    }
  });
  return data;
};
