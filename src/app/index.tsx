import { InvalidVersionScreen } from '@screens/invalid-version/invalid-version.view';
import { NavigationContainer, NavigationContainerRef } from '@react-navigation/native';
import { theme, ThemeProvider } from '@shared/config/theme';
import { PortalHost } from '@shared/ui/portal';
import React from 'react';
import ErrorBoundary from 'react-native-error-boundary';
import { withIAPContext } from 'react-native-iap';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { Provider } from 'react-redux';
import { LoadingIndicator } from 'src/components/ui/Loader/loading-indicator';

import { RootNavigator } from 'src/navigation/RootNavigator';
import store from 'src/store';
import { ApiProvider } from './providers/api.provider';
import { FirebaseAnalyticsProvider } from './providers/firebase-analytics.provider';
import { VersionGate } from './providers/version-gate';
import { firebaseScreenViewEvent } from 'src/firebase/analytics/common/firebase-screen-view-event';
import { FirebaseNotificationsProvider } from './providers/firebase-notifications.provider';
import { PushNotificationTargetRegistrator } from 'src/firebase/regisrator/push-notification-target-registrator';
import { KeyboardProvider } from './providers/keyboard-provider';
/* import { AppleHealthKitProvider } from './providers/apple-health-kit.provider'; */

const App = () => {
  const navigationRef = React.createRef<NavigationContainerRef>();
  const routeNameRef = React.useRef<string>();

  const navigationStateChange = () => {
    const previousRouteName = routeNameRef.current;
    const currentRouteName = navigationRef.current?.getCurrentRoute()?.name;
    firebaseScreenViewEvent(previousRouteName, currentRouteName);
    routeNameRef.current = currentRouteName;
  };

  return (
    <ErrorBoundary>
      <ThemeProvider value={theme}>
        <KeyboardProvider>
          <NavigationContainer ref={navigationRef} onStateChange={navigationStateChange}>
            <SafeAreaProvider>
              <Provider store={store}>
                <ApiProvider>
                  <VersionGate
                    errorConnectServerScreen={<LoadingIndicator visible />}
                    invalidScreen={<InvalidVersionScreen />}
                  >
                    <FirebaseAnalyticsProvider>
                      <FirebaseNotificationsProvider navigationRef={navigationRef}>
                        {/* <AppleHealthKitProvider> */}
                        <PortalHost>
                          <PushNotificationTargetRegistrator />
                          <RootNavigator />
                        </PortalHost>
                        {/* </AppleHealthKitProvider> */}
                      </FirebaseNotificationsProvider>
                    </FirebaseAnalyticsProvider>
                  </VersionGate>
                </ApiProvider>
              </Provider>
            </SafeAreaProvider>
          </NavigationContainer>
        </KeyboardProvider>
      </ThemeProvider>
    </ErrorBoundary>
  );
};

export default withIAPContext(App);
