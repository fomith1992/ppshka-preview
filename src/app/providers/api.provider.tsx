import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { setBaseAPI } from 'src/store/session/actions';

interface ApiProviderProps {
  children: React.ReactElement;
}

export function ApiProvider({ children }: ApiProviderProps): React.ReactElement {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(setBaseAPI());
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  return children;
}
