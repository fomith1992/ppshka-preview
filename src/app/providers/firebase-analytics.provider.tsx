import React from 'react';
import analytics from '@react-native-firebase/analytics';
import useAsync from 'react-use/lib/useAsync';

interface FirebaseAnalyticsProviderProps {
  children: React.ReactElement;
}

export function FirebaseAnalyticsProvider({
  children,
}: FirebaseAnalyticsProviderProps): React.ReactElement {
  useAsync(async () => {
    await analytics().logAppOpen();
  }, []);
  useAsync(async () => {
    await analytics().getAppInstanceId();
  }, []);
  return children;
}
