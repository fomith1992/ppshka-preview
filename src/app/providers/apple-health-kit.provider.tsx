/* import { add, format } from 'date-fns';
import React, { useEffect } from 'react';
import { Platform } from 'react-native';
import AppleHealthKit, { HealthKitPermissions, HealthValue } from 'react-native-health';
import { userAPI } from 'src/api';

interface AppleHealthKitProviderProps {
  children: React.ReactElement;
}
export const permissionsHealthKit = {
  permissions: {
    read: [AppleHealthKit.Constants.Permissions.StepCount],
    write: [],
  },
} as HealthKitPermissions;

export const AppleHealthKitProvider = ({
  children,
}: AppleHealthKitProviderProps): React.ReactElement => {
  useEffect(() => {
    Platform.OS === 'ios'
      ? AppleHealthKit.initHealthKit(permissionsHealthKit, (error) => {
          if (error) {
            console.log('[ERROR] Cannot grant permissions!');
            return;
          }
        })
      : console.log('googleFit1');
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  return children;
};

export const getStepsFromDaysAgoCount = async (data: {
  access_token: string;
  daysCount: number;
}) => {
  const array = Array.apply(0, Array(data.daysCount)).map((_, idx) => idx);

  const sendData = async (results: HealthValue) => {
    await userAPI.setUserSteps(data.access_token, [
      { date: format(new Date(results.startDate), 'y-MM-dd'), value: results.value },
    ]);
  };

  array.map((item) => {
    AppleHealthKit.getStepCount(
      {
        date: add(new Date(), { days: -item }).toISOString(),
      },
      (error, results) => {
        if (error) {
          return;
        }
        sendData(results);
      },
    );
  });
};
 */
