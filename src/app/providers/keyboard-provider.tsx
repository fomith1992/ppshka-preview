import React, { useEffect } from 'react';
import { Keyboard, Platform, UIManager } from 'react-native';
import { Immersive } from 'react-native-immersive';

if (Platform.OS === 'android') {
  if (UIManager.setLayoutAnimationEnabledExperimental) {
    UIManager.setLayoutAnimationEnabledExperimental(true);
  }
}

const restoreImmersive = () => {
  Immersive.on();
};

const onKeyboardShow = () => {
  console.log(`%c onKeyboardShow->=>`, 'color: lime');
  /* Platform.OS === 'android' && Immersive.off(); */
};

const onKeyboardHide = () => {
  console.log(`%c onKeyboardHide->=>`, 'color: lime');
  Platform.OS === 'android' && Immersive.on();
};

export const KeyboardProvider = (props: { children: React.ReactElement }) => {
  useEffect(() => {
    const keyboardDidShowSubscription = Keyboard.addListener('keyboardDidShow', onKeyboardShow);
    const keyboardDidHideSubscription = Keyboard.addListener('keyboardDidHide', onKeyboardHide);

    if (Platform.OS === 'android') {
      Immersive.addImmersiveListener(restoreImmersive);
      Immersive.on();
    }

    return () => {
      keyboardDidShowSubscription.remove();
      keyboardDidHideSubscription.remove();
      if (Platform.OS === 'android') {
        Immersive.removeImmersiveListener(restoreImmersive);
      }
    };
  }, []);

  return props.children;
};
