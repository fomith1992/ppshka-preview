import { NavigationContainerRef } from '@react-navigation/native';
import React, { RefObject, useEffect } from 'react';
import messaging from '@react-native-firebase/messaging';
import useAsync from 'react-use/lib/useAsync';

interface FirebaseNotificationsContainerProps {
  children: React.ReactElement;
  // TODO: remove ?
  navigationRef?: RefObject<NavigationContainerRef>;
}

export function FirebaseNotificationsProvider({
  children,
}: FirebaseNotificationsContainerProps): React.ReactElement {
  // useOnNotificationOpenedApp(navigationRef);
  // useOnNotificationShowInAppMessage(navigationRef);

  useEffect(
    () =>
      messaging().onMessage((message) => {
        console.log('Message', message);
      }),
    [],
  );
  useAsync(async () => {
    const authStatus = await messaging().requestPermission();
    const enabled =
      authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
      authStatus === messaging.AuthorizationStatus.PROVISIONAL;

    if (enabled) {
      console.log('Authorization status:', authStatus);
    }
  }, []);
  useAsync(async () => {
    console.log('Token', await messaging().getToken());
  }, []);
  useEffect(
    () =>
      messaging().onTokenRefresh((token) => {
        console.log('Token refreshed', token);
      }),
    [],
  );
  return children;
}
