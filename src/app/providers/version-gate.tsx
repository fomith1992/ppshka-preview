import React from 'react';
import deviceInfoModule from 'react-native-device-info';
import useAsync from 'react-use/lib/useAsync';
import { versionAPI } from 'src/api';

interface VersionGateProps {
  fallback?: React.ReactNode;
  children: React.ReactElement;
  invalidScreen: React.ReactElement;
  errorConnectServerScreen: React.ReactElement;
}

export function VersionGate({
  fallback,
  invalidScreen,
  children,
  errorConnectServerScreen,
}: VersionGateProps): React.ReactElement {
  const currentVersion = deviceInfoModule.getVersion();

  const needVersion = useAsync(async () => {
    return await versionAPI.getCurrentVersoin({ version: currentVersion }).catch((e) => {
      console.log(e);
    });
  }, [currentVersion]);

  if (needVersion.loading) {
    return <>{fallback}</>;
  }

  if (needVersion.error) {
    return <>{errorConnectServerScreen}</>;
  }

  if (needVersion.value?.data.need_update) {
    return <>{invalidScreen}</>;
  }
  return children;
}
