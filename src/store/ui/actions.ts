import { EUIActionTypes, TUIActions } from './types';

export const uiSetLoader = (value: boolean): TUIActions => ({
  type: EUIActionTypes.UI_SET_LOADER,
  payload: value,
});

export const uiSetFavorites = (value: number[]): TUIActions => ({
  type: EUIActionTypes.UI_SET_FAVORITE,
  payload: value,
});
