import { EUIActionTypes, IUIState, TUIActions } from './types';

import produce from 'immer';
import { Reducer } from 'redux';

const initialState: IUIState = {
  isShowingLoader: true,
  favorites: [],
};

const uiReducer: Reducer<IUIState, TUIActions> = (state = initialState, action) => {
  return produce(state, (draft: IUIState) => {
    switch (action.type) {
      case EUIActionTypes.UI_SET_LOADER:
        draft.isShowingLoader = action.payload;
        return;
      case EUIActionTypes.UI_SET_FAVORITE:
        draft.favorites = action.payload;
        return;

      default:
        return state;
    }
  });
};

export default uiReducer;
