export interface IUIState {
  isShowingLoader: boolean;
  favorites: number[];
}

export enum EUIActionTypes {
  UI_SET_LOADER = 'UI_SET_LOADER',
  UI_SET_FAVORITE = 'UI_SET_FAVORITE',
}

interface IUISetLoader {
  type: EUIActionTypes.UI_SET_LOADER;
  payload: boolean;
}

interface IUISetFavorite {
  type: EUIActionTypes.UI_SET_FAVORITE;
  payload: number[];
}

export type TUIActions = IUISetLoader | IUISetFavorite;
