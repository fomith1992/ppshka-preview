import { TPlanCategories } from 'src/api/meal-plan';
import { TCategories } from 'src/api/recipes';
import { TUserData } from 'src/api/user';
import { ESessionActionTypes, ISessionTokens, TSessionActions } from './types';

export const sessionSetTokens = (tokens: ISessionTokens): TSessionActions => ({
  type: ESessionActionTypes.SESSION_SET_TOKENS,
  payload: tokens,
});

export const setUserData = (data: TUserData): TSessionActions => ({
  type: ESessionActionTypes.SESSION_SET_USER,
  payload: data,
});

export const setRecipeCategories = (data: TCategories[]): TSessionActions => ({
  type: ESessionActionTypes.SESSION_SET_RECIPE_CATEGORIES,
  payload: data,
});

export const setPlanCategories = (data: TPlanCategories[]): TSessionActions => ({
  type: ESessionActionTypes.SESSION_SET_PLAN_CATEGORIES,
  payload: data,
});

export const setApiUrl = (url: string): TSessionActions => ({
  type: ESessionActionTypes.SESSION_SET_API_URL,
  payload: url,
});

export const sessionReset = (): TSessionActions => ({
  type: ESessionActionTypes.SESSION_RESET,
});
