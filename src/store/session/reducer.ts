import { ESessionActionTypes, ISessionState, TSessionActions } from './types';

import produce from 'immer';
import { Reducer } from 'redux';
import Config from 'react-native-config';

const initialState: ISessionState = {
  api: {
    url: Config.API_BACKEND,
  },
  tokens: {
    accessToken: '',
    refreshToken: '',
  },
  user: {
    id: null,
    name: null,
    activity: null,
    goal: null,
    user_photo: null,
    is_pro: false,
    b_day: '',
    height: null,
  },
  categories: [],
  planCategories: [],
};

const sessionReducer: Reducer<ISessionState, TSessionActions> = (state = initialState, action) => {
  return produce(state, (draft: ISessionState) => {
    switch (action.type) {
      case ESessionActionTypes.SESSION_SET_TOKENS:
        draft.tokens = action.payload;
        return;

      case ESessionActionTypes.SESSION_SET_USER:
        draft.user = action.payload;
        return;

      case ESessionActionTypes.SESSION_SET_API_URL:
        draft.api.url = action.payload;
        return;

      case ESessionActionTypes.SESSION_SET_RECIPE_CATEGORIES:
        draft.categories = action.payload;
        return;

      case ESessionActionTypes.SESSION_SET_PLAN_CATEGORIES:
        draft.planCategories = action.payload;
        return;

      case ESessionActionTypes.SESSION_RESET:
        return initialState;

      default:
        return state;
    }
  });
};

export default sessionReducer;
