import { RefreshTokenResponse } from 'react-native-auth0';

import { auth0 } from 'src/api/auth0';
import store, { AppState } from 'src/store';

import { uiSetLoader } from '../ui/actions';
import { TSessionActions } from './types';

import { ThunkDispatch } from 'redux-thunk';
import { auth, location, meelAPI, recipesAPI, userAPI, versionAPI } from 'src/api';
import { format } from 'date-fns';
import { storage } from 'src/utils/storage';
import { getSpareApiUrl } from 'src/api/base';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Config from 'react-native-config';
import { firebaseSetUser } from 'src/firebase/set-user-firebase';
import {
  sessionSetTokens,
  setApiUrl,
  setPlanCategories,
  setRecipeCategories,
  setUserData,
} from './actions.types';
import deviceInfoModule from 'react-native-device-info';

export const sessionBootstrap = () => {
  return async (dispatch: ThunkDispatch<AppState, null, TSessionActions>) => {
    let refreshToken: storage.RefreshToken;
    try {
      refreshToken = await storage.getRefreshToken();
      if (refreshToken.type === 'email') {
        await auth
          .refreshAccessToken({ refresh_token: refreshToken.refresh_token })
          .then(async ({ data }) => {
            if (data.data.refresh_token) {
              await storage.setRefreshToken({
                refresh_token: data.data.refresh_token,
                type: 'email',
              });
            }
            dispatch(
              sessionSetTokens({
                accessToken: data.data.access_token,
                refreshToken: data.data.refresh_token,
              }),
            );
            const isUpdateLocationDate = await storage.isUpdateLocationDate();
            if (!isUpdateLocationDate) {
              const timezone = format(new Date(), 'xx');
              await location.setTimeZone({ access_token: data.data.access_token, timezone });
              storage.setUpdateLocationDate();
            }
          });
        console.log(`Update tokens email: `, 'success');
      } else if (refreshToken.type === 'auth0') {
        const response = (await auth0.auth.refreshToken({
          refreshToken: refreshToken.refresh_token,
        })) as RefreshTokenResponse;
        if (response.refreshToken) {
          await storage.setRefreshToken({ refresh_token: response.refreshToken, type: 'auth0' });
        }
        await auth.refreshAccessTokenAuth0(response.accessToken).then(async ({ data }) => {
          dispatch(
            sessionSetTokens({
              accessToken: data.data.access_token,
              refreshToken: response.refreshToken
                ? response.refreshToken
                : (refreshToken.refresh_token as string),
            }),
          );
          const isUpdateLocationDate = await storage.isUpdateLocationDate();
          if (!isUpdateLocationDate) {
            const timezone = format(new Date(), 'xx');
            await location.setTimeZone({ access_token: data.data.access_token, timezone });
            storage.setUpdateLocationDate();
          }
        });
        console.log(`Update tokens auth0: `, 'success');
      }
    } catch (e) {
      console.log(`Update tokens: `, 'err', e);
    } finally {
      store.dispatch(uiSetLoader(false));
    }
  };
};

export const getUserData = (access_token: string) => {
  return async (dispatch: ThunkDispatch<AppState, null, TSessionActions>) => {
    try {
      await userAPI.userData(access_token).then(async ({ data }) => {
        if (data.data.name || data.data.user_photo) {
          firebaseSetUser(data.data.id?.toString() ?? null);
          dispatch(
            setUserData({
              id: data.data.id,
              name: data.data.name,
              user_photo: data.data.user_photo,
              activity: data.data.activity,
              goal: data.data.goal,
              is_pro: data.data.is_pro,
              b_day: data.data.b_day,
              height: data.data.height,
            }),
          );
        }
      });
    } catch (e) {
      console.log(`Update tokens: `, 'err', e);
    } finally {
      store.dispatch(uiSetLoader(false));
    }
  };
};

export const getCategoriesData = () => {
  return async (dispatch: ThunkDispatch<AppState, null, TSessionActions>) => {
    try {
      await recipesAPI.getCategories().then(async ({ data }) => {
        dispatch(
          setRecipeCategories([
            {
              id: undefined,
              name: 'Все рецепты',
              system_key: 'all',
            },
            ...data.data.data,
            {
              id: 99,
              name: 'Избранное',
              system_key: 'faves',
            },
          ]),
        );
      });
      await meelAPI.getCategoriesPlan().then(async ({ data }) => {
        dispatch(setPlanCategories(data.data.data.sort((a, b) => a.sort - b.sort)));
      });
    } catch (e) {
      console.log(`Update Categories: `, 'err', e);
    }
  };
};

export const setBaseAPI = () => {
  const currentVersion = deviceInfoModule.getVersion();

  return async (dispatch: ThunkDispatch<AppState, null, TSessionActions>) => {
    await versionAPI
      .getCurrentVersoin({ version: currentVersion })
      .then(() => AsyncStorage.setItem('APIURL', JSON.stringify(Config.API_BACKEND)))
      .catch(async () => {
        await getSpareApiUrl().then(async (data) => {
          dispatch(setApiUrl(data.data.spare_url));
          AsyncStorage.setItem('APIURL', JSON.stringify(data.data.spare_url));
        });
      });
  };
};
