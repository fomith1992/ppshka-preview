import { TPlanCategories } from 'src/api/meal-plan';
import { TCategories } from 'src/api/recipes';
import { TUserData } from 'src/api/user';

export interface ISessionTokens {
  accessToken: string;
  refreshToken: string;
}

export interface ISessionTokens {
  accessToken: string;
  refreshToken: string;
}

export interface ISessionState {
  api: {
    url: string;
  };
  tokens: ISessionTokens;
  user: TUserData;
  categories: TCategories[];
  planCategories: TPlanCategories[];
}

export enum ESessionActionTypes {
  SESSION_SET_TOKENS = 'SESSION_SET_TOKENS',
  SESSION_RESET = 'SESSION_RESET',
  SESSION_SET_USER = 'SESSION_SET_USER',
  SESSION_SET_API_URL = 'SESSION_SET_API_URL',
  SESSION_SET_RECIPE_CATEGORIES = 'SESSION_SET_RECIPE_CATEGORIES',
  SESSION_SET_PLAN_CATEGORIES = 'SESSION_SET_PLAN_CATEGORIES',
}

interface ISessionSetTokens {
  type: ESessionActionTypes.SESSION_SET_TOKENS;
  payload: ISessionTokens;
}

interface ISessionReset {
  type: ESessionActionTypes.SESSION_RESET;
}

interface ISessionSetUser {
  type: ESessionActionTypes.SESSION_SET_USER;
  payload: TUserData;
}

interface ISessionSetApiUrl {
  type: ESessionActionTypes.SESSION_SET_API_URL;
  payload: string;
}

interface ISessionSetRecipeCategoriesUrl {
  type: ESessionActionTypes.SESSION_SET_RECIPE_CATEGORIES;
  payload: TCategories[];
}

interface ISessionSetplanCategoriesUrl {
  type: ESessionActionTypes.SESSION_SET_PLAN_CATEGORIES;
  payload: TPlanCategories[];
}

export type TSessionActions =
  | ISessionReset
  | ISessionSetTokens
  | ISessionSetUser
  | ISessionSetApiUrl
  | ISessionSetplanCategoriesUrl
  | ISessionSetRecipeCategoriesUrl;
