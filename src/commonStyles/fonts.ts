import { getScaleSize } from 'src/utils/dimensions';

export const font12m = {
  fontFamily: 'TTNorms-Medium',
  fontSize: getScaleSize(12),
  lineHeight: getScaleSize(13.2),
};

export const font16m = {
  fontFamily: 'TTNorms-Medium',
  fontSize: getScaleSize(16),
  lineHeight: getScaleSize(17.6),
};

export const font50r = {
  fontFamily: 'TTNorms-Regular',
  fontSize: getScaleSize(50),
  lineHeight: getScaleSize(55),
};

export const font16r = {
  fontFamily: 'TTNorms-Regular',
  fontSize: getScaleSize(16),
  lineHeight: getScaleSize(24),
};

export const font12r = {
  fontFamily: 'TTNorms-Regular',
  fontSize: getScaleSize(12),
  lineHeight: getScaleSize(13.2),
};

export const font14m = {
  fontFamily: 'TTNorms-Medium',
  fontSize: getScaleSize(14),
  lineHeight: getScaleSize(15.4),
};

export const font14r = {
  fontFamily: 'TTNorms-Regular',
  fontSize: getScaleSize(14),
  lineHeight: getScaleSize(15.4),
};

export const font30m = {
  fontFamily: 'Montserrat-Medium',
  fontSize: getScaleSize(30),
  lineHeight: getScaleSize(33),
};

export const font35m = {
  fontFamily: 'TTNorms-Medium',
  fontSize: getScaleSize(35),
  lineHeight: getScaleSize(38.5),
};

export const font12b = {
  fontFamily: 'TTNorms-Bold',
  fontSize: getScaleSize(12),
  lineHeight: getScaleSize(14.16),
};

export const font20m = {
  fontSize: getScaleSize(20),
  lineHeight: getScaleSize(22),
  fontFamily: 'TTNorms-Medium',
};

export const font20r = {
  fontSize: getScaleSize(20),
  lineHeight: getScaleSize(22),
  fontFamily: 'TTNorms-Regular',
};

export const font10r = {
  fontSize: getScaleSize(10),
  lineHeight: getScaleSize(16),
  fontFamily: 'TTNorms-Regular',
};

export const font8m = {
  fontSize: getScaleSize(8),
  lineHeight: getScaleSize(8.8),
  fontFamily: 'TTNorms-Medium',
};
