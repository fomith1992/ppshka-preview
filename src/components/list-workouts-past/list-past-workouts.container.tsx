import { WorkoutsTitle } from '@screens/programs/styles';
import React from 'react';
import { useSelector } from 'react-redux';
import useAsync from 'react-use/lib/useAsync';
import { userAPI } from 'src/api';
import { AppState } from 'src/store';
import { ItemPastWorkput } from '../list-item-workouts-past/item-past-workout.container';

export function ListPastWorkouts(): React.ReactElement {
  const access_token = useSelector((state: AppState) => state.session.tokens.accessToken);
  const userPastWorkouts = useAsync(async () => {
    return await userAPI.getUserPastWorkouts(access_token).catch(console.log);
  }, [access_token]);
  if (userPastWorkouts.value == null || userPastWorkouts.value.data.data.length === 0) return <></>;
  return (
    <>
      <WorkoutsTitle>Прошедшие тренировки</WorkoutsTitle>
      {userPastWorkouts.value.data.data.reverse().map((value, _) => (
        <ItemPastWorkput
          key={value.calendar_id}
          calendar_id={value.calendar_id}
          data={{
            workoutId: value.workout_id,
            done: value.done,
            mark: value.mark,
            postpone_workout: value.postpone_workout,
          }}
        />
      ))}
    </>
  );
}
