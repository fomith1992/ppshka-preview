import { color } from '@shared/config/color';
import { styled } from '@shared/config/styled';
import { font } from '@shared/config/text';
import { TextInput } from 'react-native';
import { getScaleSize } from 'src/utils/dimensions';

function linesHeight(linesNumber: number | undefined): number | undefined {
  return linesNumber == null
    ? undefined
    : getScaleSize(
        12 * 2 + // padding
          2 + // border
          linesNumber * 20, // lines
      );
}

export const Input = styled(
  TextInput,
  (props: { minLinesNumber?: number; maxLinesNumber?: number }) =>
    ({
      ...font({ type: 'text2' }),
      color: color('black'),
      // padding does not work with multiline inputs for some reason =/
      paddingHorizontal: getScaleSize(12),
      paddingTop: getScaleSize(8),
      padding: getScaleSize(15),
      paddingBottom: getScaleSize(12),
      minHeight: linesHeight(props.minLinesNumber),
      maxHeight: linesHeight(props.maxLinesNumber),
      textAlignVertical:
        props.minLinesNumber != null && props.minLinesNumber > 1 ? 'top' : 'center',
      borderRadius: getScaleSize(15),
      borderColor: color('gray6'),
      borderWidth: 1,
      marginBottom: getScaleSize(10),
    } as const),
);
