import React from 'react';
import { Input } from './text-input.style';

export interface TextInputProps {
  value: string | undefined;
  onChange: (value: string) => void;
  onFocus?: () => void;
  onBlur?: () => void;
  error?: string | null;
  success?: string | null;
  active?: boolean;
  touched?: boolean;
  disabled?: boolean;

  placeholder?: string;
  maxLength?: number;
  label?: string;
  numberOfLines?: number | [number, number];
}

export function TextInput({
  value,
  onChange,
  onFocus,
  onBlur,
  disabled = false,
  placeholder,
  maxLength,
  numberOfLines = 1,
}: TextInputProps): React.ReactElement {
  const [minLinesNumber, maxLinesNumber] =
    typeof numberOfLines === 'number' ? [numberOfLines, numberOfLines] : numberOfLines;

  return (
    <Input
      value={value}
      onChangeText={onChange}
      onFocus={onFocus}
      onBlur={onBlur}
      editable={!disabled}
      maxLength={maxLength}
      placeholder={placeholder}
      multiline={maxLinesNumber > 1}
      minLinesNumber={minLinesNumber}
      maxLinesNumber={maxLinesNumber}
    />
  );
}
