import { color } from '@shared/config/color';
import { styled } from '@shared/config/styled';
import { font } from '@shared/config/text';
import { container } from '@shared/config/view';
import { Image, Text, TouchableOpacity, View } from 'react-native';
import { getScaleSize } from 'src/utils/dimensions';

export const Container = styled(View, {
  flexDirection: 'row',
  backgroundColor: color('white'),
  borderRadius: getScaleSize(10),
  overflow: 'hidden',
  marginBottom: getScaleSize(10),
});

export const ImageContainer = View;

export const MiniImage = styled(Image, {
  width: getScaleSize(50),
  height: getScaleSize(50),
});

export const InfoContainer = styled(View, {
  ...container('padding'),
  flex: 1,
  flexDirection: 'row',
  alignItems: 'center',
  justifyContent: 'space-between',
});

export const InfoDescriptionContainer = styled(View, {
  flexDirection: 'column',
});

export const Title = styled(Text, {
  ...font({ type: 'text2' }),
  color: color('black'),
});

export const StatusWork = styled(Text, {
  ...font({ type: 'text3' }),
  color: color('gray7'),
});

export const PostponeContainer = styled(TouchableOpacity, {});
