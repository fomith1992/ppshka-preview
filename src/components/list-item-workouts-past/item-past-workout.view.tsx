import { LinkButtonText } from '@screens/workout-details/styles';
import React from 'react';
import {
  Container,
  ImageContainer,
  InfoContainer,
  InfoDescriptionContainer,
  MiniImage,
  PostponeContainer,
  StatusWork,
  Title,
} from './item-past-workout.style';

export interface ItemPastWorkputProps {
  workoutId: number;
  done: boolean;
  mark?: number;
  postpone_workout: boolean;
}

type ItemListPastWorkputsViewProps = {
  title: string;
  url_image?: string;
  onPressPostponementButton: () => void;
} & Omit<ItemPastWorkputProps, 'workoutId'>;

export function ItemListPastWorkputsView({
  title,
  url_image,
  done,
  postpone_workout,
  onPressPostponementButton,
}: ItemListPastWorkputsViewProps): React.ReactElement {
  return (
    <Container>
      <ImageContainer>
        <MiniImage
          source={{
            uri: url_image,
          }}
        />
      </ImageContainer>
      <InfoContainer>
        <InfoDescriptionContainer>
          <Title>{title}</Title>
          <StatusWork>{done ? 'Выполнена' : 'Пропущена'}</StatusWork>
        </InfoDescriptionContainer>
        {postpone_workout ? (
          <PostponeContainer onPress={onPressPostponementButton}>
            <LinkButtonText>Перенести</LinkButtonText>
          </PostponeContainer>
        ) : null}
      </InfoContainer>
    </Container>
  );
}
