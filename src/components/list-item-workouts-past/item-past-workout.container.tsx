import { CommonActions, useNavigation } from '@react-navigation/native';
import { Bottom } from '@screens/recipes/ui/header.style';
import {
  BottomContainer,
  BottomDescription,
  LinkButtonContainer,
  LinkButtonText,
} from '@screens/workout-details/styles';
import React, { useState } from 'react';
import { Alert } from 'react-native';
import { useSelector } from 'react-redux';
import useAsync from 'react-use/lib/useAsync';
import useAsyncFn from 'react-use/lib/useAsyncFn';
import { userAPI, workoutAPI } from 'src/api';
import { AppState } from 'src/store';
import { getCacheContent } from 'src/utils/cache-content';
import Button from '../Button';
import { PopUpWindow } from '../pop-up-window/pop-up-window.view';
import { ItemListPastWorkputsView, ItemPastWorkputProps } from './item-past-workout.view';

export function ItemPastWorkput(props: {
  data: ItemPastWorkputProps;
  calendar_id: number;
}): React.ReactElement {
  const { data, calendar_id } = props;
  const { workoutId, done, mark, postpone_workout } = data;
  const navigation = useNavigation();
  const [modalVisible, setModalVisible] = useState<boolean>(false);
  const access_token = useSelector((state: AppState) => state.session.tokens.accessToken);
  const workout = useAsync(async () => {
    return await workoutAPI
      .getWorkoutById({
        access_token,
        id: workoutId,
      })
      .catch(console.log);
  }, [workoutId]);
  const [_, userPostponementWorkout] = useAsyncFn(
    async (rebuild: boolean) => {
      return await userAPI
        .userPostponementWorkout(access_token, calendar_id, rebuild)
        .then((data) => {
          if (data.data.status === 'ok') {
            navigation.dispatch(
              CommonActions.reset({
                index: 1,
                routes: [{ name: 'ProgramsList' }],
              }),
            );
          } else {
            Alert.alert('Ошибка переноса тренировки!');
          }
        })
        .catch(console.log);
    },
    [calendar_id],
  );
  function handlePressPostponementButton() {
    setModalVisible(true);
  }

  const photo = useAsync(async () => {
    return await getCacheContent({ url: workout.value?.data.data.url_image, format: 'png' });
  }, [workout.value?.data.data]);

  const workoutData = workout.value?.data.data;
  if (workout.loading || workoutData == null || photo.loading) return <></>;
  return (
    <>
      <ItemListPastWorkputsView
        title={workoutData.name}
        url_image={photo.value}
        done={done}
        mark={mark}
        postpone_workout={postpone_workout}
        onPressPostponementButton={handlePressPostponementButton}
      />
      <PopUpWindow modalVisible={modalVisible}>
        <BottomContainer>
          <BottomDescription>
            Перенести 1 тренировку, или перестроить календарь тренировок?
          </BottomDescription>
          <Bottom mb={24} />
          <Button
            uppercase={false}
            onPress={() => {
              userPostponementWorkout(true);
              setModalVisible(false);
            }}
            content="Перенести тренировку С перестроением графика тренировок"
          />
          <Bottom mb={12} />
          <Button
            uppercase={false}
            onPress={() => {
              userPostponementWorkout(false);
              setModalVisible(false);
            }}
            content="Перенести тренировку БЕЗ перестроения графика тренировок"
          />
          <Bottom mb={30} />
          <LinkButtonContainer onPress={() => setModalVisible(false)}>
            <LinkButtonText>Отмена</LinkButtonText>
          </LinkButtonContainer>
        </BottomContainer>
      </PopUpWindow>
    </>
  );
}
