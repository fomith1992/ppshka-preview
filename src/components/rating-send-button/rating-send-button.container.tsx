import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import useAsyncFn from 'react-use/lib/useAsyncFn';
import { feedbackAPI, recipesAPI } from 'src/api';
import { AppState } from 'src/store';
import { AddFeedbackProps, AddRateProps, RatingSendButtonView } from './rating-send-button.view';

export function RatingSendButton(props: {
  children?: React.ReactElement;
  recipeId: number;
  onGetRecipeRate: () => void;
}): React.ReactElement {
  const access_token = useSelector((state: AppState) => state.session.tokens.accessToken);
  const [rateId, setRateId] = useState<number | null>(null);

  const [, onAddRateToRecipe] = useAsyncFn(async ({ rate }: AddRateProps) => {
    try {
      const recipeFeedbackData = await recipesAPI.addFeedbackToRecipe({
        recipe_id: props.recipeId,
        rate,
        access_token,
      });
      if (recipeFeedbackData.data.status === 'error') {
        console.log('Error add feedback', recipeFeedbackData.data.message);
        throw recipeFeedbackData.data.message;
      }
      setRateId(recipeFeedbackData.data.data);
      props.onGetRecipeRate();
    } catch (error) {
      console.log('Error common add rate: ', error);
    }
  }, []);

  const [addFeedbackToRecipe, onAddFeedbackToRecipe] = useAsyncFn(
    async ({ text, photo }: AddFeedbackProps) => {
      try {
        if (rateId != null && text != null) {
          const feedbackData = await feedbackAPI.addAttachmentsToFeedback({
            feedbackId: rateId, // data и есть id нового отзыва
            text,
            photoPath: photo,
          });
          if (feedbackData.data.status === 'error') {
            console.log('Error add attachment to feedback', feedbackData.data.message);
            throw feedbackData.data.message;
          }
        }
      } catch (error) {
        console.log('Error common add feedback: ', error);
      }
    },
    [rateId],
  );

  return (
    <RatingSendButtonView
      children={props.children}
      loading={addFeedbackToRecipe.loading}
      onPressAddFeedback={onAddFeedbackToRecipe}
      onPressAddRate={onAddRateToRecipe}
      isRating={rateId != null}
    />
  );
}
