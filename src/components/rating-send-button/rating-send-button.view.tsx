import React, { useState } from 'react';
import { PopUpWindow } from 'src/components/pop-up-window/pop-up-window.view';
import { Rating } from 'react-native-ratings';
import {
  UploadFeedbackPhoto,
  FeedbackPhotoContainer,
  FeedbackPhotoText,
  HeaderText,
  RatingStartContainer,
  DeleteUploadFeedbackPhotoContainer,
  UploadFeedbackPhotoContainer,
  ContainerButtonLink,
  LinkButtonContainer,
  LinkButtonText,
} from './rating-send-button.style';
import { CircleButton } from '../Button/circle-button';
import ImageCropPicker from 'react-native-image-crop-picker';
import { CloseIcon8 } from '@shared/ui/icons/close.icon-8';
import { ActivityIndicator, LayoutAnimation, Platform, UIManager } from 'react-native';
import { theme } from '@shared/config/theme';
import { ButtonLink } from '../Button/button-link';
import Icon from '../Icon';
import { TextInput } from '../text-input/text-input';

export interface AddRateProps {
  rate: number;
}

export interface AddFeedbackProps {
  text: string;
  photo: string;
}

interface RatingSendButtonViewProps {
  loading: boolean;
  isRating: boolean;
  onPressAddFeedback: (data: AddFeedbackProps) => void;
  onPressAddRate: (data: AddRateProps) => void;
  children?: React.ReactElement;
}

export function RatingSendButtonView({
  loading,
  isRating,
  onPressAddFeedback,
  onPressAddRate,
  children,
}: RatingSendButtonViewProps): React.ReactElement {
  const [visibleModal, setVisibleModal] = useState<boolean>(false);
  const [feedbackValue, setFeedbackValue] = useState<number>(0);
  const [feedbackText, setFeedbackText] = useState<string>('');
  const [image, setImage] = useState<{ path: string; base64: string | null | undefined }>({
    path: '',
    base64: null,
  });

  if (Platform.OS === 'android' && UIManager.setLayoutAnimationEnabledExperimental) {
    UIManager.setLayoutAnimationEnabledExperimental(true);
  }
  LayoutAnimation.configureNext(LayoutAnimation.Presets.linear);

  const onUploadPhoto = () => {
    ImageCropPicker.openPicker({
      width: 400,
      height: 400,
      cropping: true,
      mediaType: 'photo',
      includeBase64: true,
    })
      .then(({ data, path }) => setImage({ path, base64: data }))
      .catch((e) => {
        console.log('Error set avatar', e);
      });
  };
  function handlePressAddFeedback() {
    onPressAddFeedback({
      text: feedbackText,
      photo: image.base64 ?? '',
    });
    setVisibleModal(false);
  }
  return (
    <>
      <LinkButtonContainer onPress={() => setVisibleModal(true)}>
        {children ? (
          children
        ) : (
          <>
            <LinkButtonText>Оставить отзыв</LinkButtonText>
            <Icon
              style={{ transform: [{ rotate: '180deg' }] }}
              name="arrowBack"
              size={12}
              color="primary"
            />
          </>
        )}
      </LinkButtonContainer>
      <PopUpWindow modalVisible={visibleModal}>
        <HeaderText>Ваш отзыв</HeaderText>
        <RatingStartContainer>
          <Rating
            ratingCount={5}
            minValue={1}
            startingValue={feedbackValue}
            jumpValue={1}
            style={{
              margin: 8,
              padding: 0,
            }}
            onFinishRating={(rate: number) => {
              setFeedbackValue(rate);
              onPressAddRate({ rate });
            }}
          />
        </RatingStartContainer>
        {isRating && (
          <>
            <TextInput
              value={feedbackText}
              onChange={setFeedbackText}
              numberOfLines={[3, 7]}
              maxLength={500}
              placeholder="Напишите Ваши впечатления"
            />
            {image.base64 ? (
              <UploadFeedbackPhotoContainer>
                <UploadFeedbackPhoto source={{ uri: image.path }}>
                  <DeleteUploadFeedbackPhotoContainer
                    onPress={() =>
                      setImage({
                        path: '',
                        base64: null,
                      })
                    }
                  >
                    <CloseIcon8 size={25} />
                  </DeleteUploadFeedbackPhotoContainer>
                </UploadFeedbackPhoto>
              </UploadFeedbackPhotoContainer>
            ) : (
              <FeedbackPhotoContainer onPress={onUploadPhoto}>
                <FeedbackPhotoText>Добавьте фотографию</FeedbackPhotoText>
              </FeedbackPhotoContainer>
            )}
          </>
        )}
        {loading ? (
          <ActivityIndicator size="large" color={theme.colors.primary} />
        ) : (
          <CircleButton uppercase content="Оставить отзыв" onPress={handlePressAddFeedback} />
        )}
        <ContainerButtonLink>
          <ButtonLink onPress={() => setVisibleModal(false)}>Отмена</ButtonLink>
        </ContainerButtonLink>
      </PopUpWindow>
    </>
  );
}
