import { color } from '@shared/config/color';
import { styled } from '@shared/config/styled';
import { font } from '@shared/config/text';
import { Text, TextInput, TouchableOpacity, View } from 'react-native';

import { getScaleSize } from 'src/utils/dimensions';
import PreLoadedImage from '../PreLoadedImage';

export const HeaderText = styled(Text, {
  ...font({ type: 'h2' }),
  color: color('gray10'),
  marginLeft: getScaleSize(5),
});

export const RatingStartContainer = styled(View, {
  marginVertical: getScaleSize(5),
});

export const FeedbackTextInput = styled(TextInput, {
  ...font({ type: 'text2' }),
  color: color('black'),
  paddingTop: getScaleSize(8),
  textAlignVertical: 'top',
  padding: getScaleSize(15),
  borderRadius: getScaleSize(15),
  borderColor: color('gray6'),
  borderWidth: 1,
  marginBottom: getScaleSize(10),
});

export const FeedbackPhotoContainer = styled(TouchableOpacity, {
  justifyContent: 'center',
  alignItems: 'center',
  flexDirection: 'row',
  borderColor: color('gray6'),
  borderWidth: 1,
  borderStyle: 'dashed',
  borderRadius: getScaleSize(15),
  marginBottom: getScaleSize(10),
  paddingVertical: getScaleSize(10),
});
export const FeedbackPhotoText = styled(Text, {
  ...font({ type: 'text1' }),
  color: color('gray6'),
  marginRight: getScaleSize(10),
});
export const UploadFeedbackPhotoContainer = styled(View, {});
export const UploadFeedbackPhoto = styled(PreLoadedImage, {
  aspectRatio: 1,
  height: 150,
  alignSelf: 'center',
  borderRadius: getScaleSize(15),
  marginBottom: getScaleSize(10),
});
export const DeleteUploadFeedbackPhotoContainer = styled(TouchableOpacity, {
  padding: getScaleSize(10),
  borderRadius: getScaleSize(15),
  right: 0,
  position: 'absolute',
});

export const ContainerButtonLink = styled(View, {
  alignItems: 'center',
  marginTop: getScaleSize(12),
});

export const LinkButtonContainer = styled(TouchableOpacity, {
  marginTop: getScaleSize(8),
  flexDirection: 'row',
  alignItems: 'center',
  justifyContent: 'center',
});

export const LinkButtonText = styled(Text, {
  ...font({ type: 'text2' }),
  color: color('primary'),
  textAlign: 'center',
  marginRight: getScaleSize(4),
});
