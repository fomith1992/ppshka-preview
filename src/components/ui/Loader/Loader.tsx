import React from 'react';
import { useSelector } from 'react-redux';
import { SplashScreen } from 'src/components/splash-screen/splash-screen.view';

import { AppState } from 'src/store';

const Loader = () => {
  const isShowingLoader = useSelector((state: AppState) => state.ui.isShowingLoader);

  if (!isShowingLoader) {
    return null;
  }

  return <SplashScreen />;
};

export default Loader;
