import { Modal } from '@shared/ui/modal';
import React from 'react';
import { ActivityIndicator, View } from 'react-native';
import { styles } from './styles';

interface LoaderModalProps {
  visible?: boolean;
}

export const LoaderModal = ({ visible = false }: LoaderModalProps) => {
  if (visible) {
    return (
      <Modal visible>
        <View style={styles.container}>
          <ActivityIndicator size="large" color={'#fff'} />
        </View>
      </Modal>
    );
  } else return null;
};
