import React from 'react';
import { ActivityIndicator, ColorValue, View } from 'react-native';
import { styles } from './styles';

interface LoaderModalProps {
  visible?: boolean;
  color?: ColorValue;
}

export const LoadingIndicator = ({ visible = false, color = '#fff' }: LoaderModalProps) => {
  if (visible) {
    return (
      <View style={styles.container}>
        <ActivityIndicator size="large" color={color} />
      </View>
    );
  } else return null;
};
