import React, { FC } from 'react';
import { ActivityIndicator, View } from 'react-native';

import { styles } from './styles';

interface IProps {
  color?: string;
  backgroundColor?: string;
}

const LoaderInternal: FC<IProps> = ({ color = 'black', backgroundColor = 'white' }) => (
  <View style={[styles.container, { backgroundColor }]}>
    <ActivityIndicator size="large" color={color} />
  </View>
);

export default LoaderInternal;
