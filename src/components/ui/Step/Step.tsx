import { gradientComponentsColors } from '@shared/ui/gradient-layout/constant';
import React, { FC } from 'react';
import { StyleProp, Text, ViewStyle } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

import { styles } from './styles';

interface IProps {
  value: number;
  style?: StyleProp<ViewStyle>;
}

const Step: FC<IProps> = ({ value, style }) => {
  return (
    <LinearGradient style={[styles.container, style]} colors={gradientComponentsColors}>
      <Text style={styles.text}>{'Шаг ' + value}</Text>
    </LinearGradient>
  );
};

export default Step;
