import { StyleSheet } from 'react-native';

import { font14m } from 'src/commonStyles/fonts';
import { getScaleSize } from 'src/utils/dimensions';

export const styles = StyleSheet.create({
  container: {
    padding: getScaleSize(10),
    backgroundColor: '#6034A8',
    borderRadius: getScaleSize(8),
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'flex-start',
  },
  text: {
    ...font14m,
    color: 'white',
  },
});
