import React, { useState } from 'react';
import { StyleProp, TouchableOpacity, ViewStyle } from 'react-native';
import { useSelector } from 'react-redux';
import { mock_user_avatar } from 'src/mock/user';
import { AppState } from 'src/store';

import { hitSlopParams } from 'src/utils/hit-slop-params';

import { AddPost } from '../add-post/add-post.view';
import {
  Avatar,
  Description,
  HeaderContainer,
  HeaderText,
  TitleContainer,
} from './header-light.style';

interface HeaderLightProps {
  style?: StyleProp<ViewStyle>;
}
export const HeaderLight = ({ style }: HeaderLightProps): React.ReactElement => {
  const user = useSelector((state: AppState) => state.session.user);
  const [modalVisible, setModalVisible] = useState<boolean>(false);

  return (
    <>
      <HeaderContainer style={style}>
        <Avatar source={{ uri: user.avatar ?? mock_user_avatar }} />
        <TitleContainer>
          <HeaderText>Привет, {user.name ?? 'Пользователь'}!</HeaderText>
          <TouchableOpacity onPress={() => setModalVisible(true)} hitSlop={hitSlopParams('L')}>
            <Description>Что нового сегодня у нас</Description>
          </TouchableOpacity>
        </TitleContainer>
      </HeaderContainer>
      <AddPost visible={modalVisible} onVisibleChange={() => setModalVisible(!modalVisible)} />
    </>
  );
};
