import { color } from '@shared/config/color';
import { styled } from '@shared/config/styled';
import { Platform, Text, TouchableOpacity, View } from 'react-native';
import { font12r, font20m } from 'src/commonStyles/fonts';
import PreLoadedImage from 'src/components/PreLoadedImage';
import { getScaleSize } from 'src/utils/dimensions';

export const HeaderContainer = styled(TouchableOpacity, {
  flexDirection: 'row',
  alignItems: 'center',
  paddingHorizontal: getScaleSize(15),
  paddingTop: getScaleSize(6),
  paddingBottom: getScaleSize(20),
  backgroundColor: color('white'),
  ...Platform.select({
    android: {
      elevation: 1,
    },
    ios: {
      shadowColor: color('black'),
      shadowOffset: {
        width: 0,
        height: 1,
      },
      shadowOpacity: 0.06,
      shadowRadius: 1,
    },
  }),
});

export const Avatar = styled(PreLoadedImage, {
  width: getScaleSize(40),
  height: getScaleSize(40),
  borderRadius: getScaleSize(8),
});

export const TitleContainer = styled(View, {
  marginLeft: getScaleSize(12),
});

export const HeaderText = styled(Text, {
  ...font20m,
});

export const Description = styled(Text, {
  ...font12r,
  color: color('gray7'),
  marginTop: getScaleSize(2),
});
