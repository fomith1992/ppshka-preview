import { Portal } from '@shared/ui/portal';
import React from 'react';
import { View, Image, StyleSheet } from 'react-native';
import { windowHeight, windowWidth } from 'src/utils/dimensions';

export const SplashScreen = () => {
  return (
    <Portal>
      <View style={styles.container}>
        <Image style={{ width: 300, height: 388 }} source={require('./assets/splash.gif')} />
      </View>
    </Portal>
  );
};

const styles = StyleSheet.create({
  container: {
    width: windowWidth,
    height: windowHeight,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F7EDF0',
  },
});
