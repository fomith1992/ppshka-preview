import { color } from '@shared/config/color';
import { styled } from '@shared/config/styled';
import { font } from '@shared/config/text';
import { Text, View } from 'react-native';
import { getScaleSize } from 'src/utils/dimensions';
import LinearGradient from 'react-native-linear-gradient';

export const CounterContainer = styled(LinearGradient, {
  borderRadius: getScaleSize(15),
  overflow: 'hidden',
});

export const Counter = styled(View, {
  margin: 1,
  flexDirection: 'row',
  alignItems: 'center',
  borderRadius: getScaleSize(15),
  overflow: 'hidden',
  backgroundColor: color('white'),
});

export const CounterBlock = styled(View, {
  alignItems: 'center',
});

export const CounterTab = styled(View, {
  width: getScaleSize(35),
  height: getScaleSize(25),
  justifyContent: 'center',
  alignItems: 'center',
});

export const CounterText = styled(Text, {
  ...font({ type: 'text2' }),
  color: color('gray7'),
});

export const CounterState = styled(Text, {
  ...font({ type: 'text3', weight: 'normal' }),
  color: color('gray10'),
});
