import React from 'react';
import { TouchableOpacity } from 'react-native';
import {
  CounterContainer,
  CounterTab,
  CounterText,
  CounterState,
  Counter,
} from './recipes-counter.style';
import { gradientComponentsColors } from '@shared/ui/gradient-layout/constant';
import { hitSlopParams } from 'src/utils/hit-slop-params';

interface RecipeCounterProps {
  count: number;
  setCount: (col: number) => void;
}

export const RecipeCounter = ({ count, setCount }: RecipeCounterProps): React.ReactElement => {
  return (
    <CounterContainer colors={gradientComponentsColors}>
      <Counter>
        <TouchableOpacity disabled={count === 1} onPress={() => setCount(count - 1)}>
          <CounterTab hitSlop={hitSlopParams('L')}>
            <CounterText>-</CounterText>
          </CounterTab>
        </TouchableOpacity>
        <CounterState>{count}</CounterState>
        <TouchableOpacity onPress={() => setCount(count + 1)}>
          <CounterTab>
            <CounterText>+</CounterText>
          </CounterTab>
        </TouchableOpacity>
      </Counter>
    </CounterContainer>
  );
};
