import { color } from '@shared/config/color';
import { styled } from '@shared/config/styled';
import { font } from '@shared/config/text';
import { container } from '@shared/config/view';
import { Platform, Text, TouchableHighlight, TouchableOpacity, View } from 'react-native';

import { getScaleSize, screenWidth } from 'src/utils/dimensions';
import PreLoadedImage from '../PreLoadedImage';

export const Container = styled(TouchableHighlight, {
  paddingBottom: getScaleSize(20),
  borderRadius: getScaleSize(15),
  backgroundColor: color('white'),
  overflow: 'hidden',
  ...Platform.select({
    android: {
      elevation: 2,
    },
    ios: {
      shadowColor: color('black'),
      shadowOffset: {
        width: 0,
        height: 1,
      },
      shadowOpacity: 0.06,
      shadowRadius: 1,
    },
  }),
});

export const ContainerIcon = styled(TouchableOpacity, {
  justifyContent: 'center',
  alignItems: 'center',
  padding: getScaleSize(7),
  backgroundColor: color('white'),
  borderRadius: getScaleSize(8),
  position: 'absolute',
  right: getScaleSize(10),
  top: getScaleSize(10),
});

export const ContainerBottomIcon = styled(TouchableOpacity, {
  justifyContent: 'center',
  alignItems: 'center',
  backgroundColor: color('white'),
  borderRadius: getScaleSize(8),
  position: 'absolute',
  padding: getScaleSize(5),
  right: getScaleSize(10),
  bottom: getScaleSize(25),
});

export const RecipeCardImage = styled(PreLoadedImage, {
  borderTopRightRadius: getScaleSize(12),
  borderTopLeftRadius: getScaleSize(12),
  height: ({ layout }) => (screenWidth - getScaleSize(layout.margin) * 4) * 0.733,
  width: ({ layout }) => screenWidth - getScaleSize(layout.margin) * 2,
  marginBottom: getScaleSize(16),
});

export const ContainerBurnTime = styled(View, {
  ...container('margin'),
  flexDirection: 'row',
  alignItems: 'center',
  justifyContent: 'space-between',
  marginBottom: getScaleSize(14),
});

export const BlurContainer = styled(View, {
  position: 'absolute',
  top: 0,
  bottom: 0,
  left: 0,
  right: 0,
  paddingTop: getScaleSize(20),
  backgroundColor: color('whiteOpacity80'),
});

export const BillingButoonContainer = styled(View, {
  ...container(),
  width: '50%',
});

export const TextBurn = styled(Text, {
  ...font({ type: 'text3', weight: 'normal' }),
  color: color('secondary'),
});

export const TextTime = styled(Text, {
  ...font({ type: 'text3', weight: 'normal' }),
  color: color('primary'),
});

export const TextCategory = styled(Text, {
  ...font({ type: 'text3', weight: 'normal' }),
  color: color('tertiary'),
});

export const Circle = styled(View, {
  width: getScaleSize(4),
  height: getScaleSize(4),
  borderRadius: getScaleSize(4) / 2,
  backgroundColor: color('gray5'),
});

export const TextTitle = styled(Text, {
  ...container('margin'),
  ...font({ type: 'text1', weight: 'normal' }),
  marginBottom: getScaleSize(2),
  color: color('gray10'),
});

export const TextSubtitle = styled(Text, {
  ...font({ type: 'text3' }),
  color: color('gray7'),
  ...container('margin'),
});

export const Separator = styled(View, {
  width: '100%',
  height: getScaleSize(1),
  backgroundColor: color('gray3'),
  marginVertical: getScaleSize(20),
});

export const InfoContainer = styled(View, {
  flexDirection: 'row',
  justifyContent: 'space-between',
  alignItems: 'center',
  ...container('margin'),
});

export const TextItemValue = styled(Text, {
  ...font({ type: 'text1', weight: 'normal' }),
  color: color('gray10'),
});

export const TextItemName = styled(Text, {
  ...font({ type: 'text3' }),
  color: color('gray7'),
  marginTop: getScaleSize(2),
});

export const ContainerComplexityIndicators = styled(View, {
  flexDirection: 'row',
  paddingVertical: getScaleSize(4),
});

export const ComplexityIndicator = styled(
  View,
  (props: { active: boolean }) =>
    ({
      width: getScaleSize(3),
      height: getScaleSize(15),
      marginRight: getScaleSize(5),
      backgroundColor: color(props.active ? 'secondary' : 'gray4'),
    } as const),
);
