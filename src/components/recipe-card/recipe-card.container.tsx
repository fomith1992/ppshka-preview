import React, { memo, useState } from 'react';
import { Alert, Image, View } from 'react-native';
import { CommonActions, useNavigation } from '@react-navigation/native';
import Icon from 'src/components/Icon';

import {
  Circle,
  ComplexityIndicator,
  Container,
  ContainerBurnTime,
  ContainerComplexityIndicators,
  ContainerIcon,
  InfoContainer,
  RecipeCardImage,
  Separator,
  TextBurn,
  TextCategory,
  TextItemName,
  TextItemValue,
  TextTime,
  TextTitle,
  BlurContainer,
  BillingButoonContainer,
  ContainerBottomIcon,
} from './recipe-card.style';
import { TCategories, TRecipe } from 'src/api/recipes';
import { favoritesAPI } from 'src/utils/storage';
import useAsyncFn from 'react-use/lib/useAsyncFn';
import { useSelector } from 'react-redux';
import { AppState } from 'src/store';
import { meelAPI, recipesAPI } from 'src/api';
import useAsync from 'react-use/lib/useAsync';
import { ReturnScreenRecipes } from '@screens/recipes/recipes.screen';
import Button from '../Button';
import { format } from 'date-fns';
import { theme } from '@shared/config/theme';
import { addRecipeToPlanEvent } from 'src/firebase/analytics/analytics-recipes-plan/add-recipe-to-plan-event';
import recipesInactive from '../../shared/ui/assets/recipes-plan-inactive.png';
import { SizeText } from '@screens/recipes-plan/recipes-plan.style';
import { expectDefined } from 'src/utils/expect-defined';
import { getCacheContent } from 'src/utils/cache-content';

interface RecipeCardProps {
  data: TRecipe;
  fallback?: boolean;
  date?: Date;
  category: TCategories;
  returnScreen?: ReturnScreenRecipes;
  openContextMenu: (data: { recipe_id: number; category: number }) => void;
}

const areEqual = () => {
  return false;
};

export const RecipeCard = memo(
  ({ data, category, date, returnScreen, openContextMenu }: RecipeCardProps) => {
    const access_token = useSelector((state: AppState) => state.session.tokens.accessToken);
    const planCategories = useSelector((state: AppState) => state.session.planCategories);
    const categories = useSelector((state: AppState) => state.session.categories);
    const faves = useSelector((state: AppState) => state.ui.favorites);

    const [favorite, setFavorite] = useState<boolean>(false);
    const navigation = useNavigation();

    const getCategoryId = () => {
      const defaultId = expectDefined(data.category_array.find((x) => x.default));
      if (category?.id != null && category.id !== 99) {
        return expectDefined(data.category_array.find((x) => x.id === category.id) ?? defaultId);
      } else {
        return defaultId;
      }
    };

    const [_x, addToRecipesPlan] = useAsyncFn(
      async (date?: Date) => {
        if (date != null) {
          return await meelAPI
            .addToMealPlan({
              access_token,
              recipe_id: data.id,
              date: format(date ?? new Date(), 'y-MM-dd'),
              category_id: getCategoryId().id,
            })
            .then(async () => {
              addRecipeToPlanEvent(data.id.toString());
              navigation.dispatch(
                CommonActions.reset({
                  index: 0,
                  routes: [
                    {
                      name: 'TabNavigator',
                      params: {
                        screen: returnScreen,
                        initial: false,
                        params: {
                          initial: false,
                          screen: returnScreen,
                          params: { date },
                        },
                      },
                    },
                  ],
                }),
              );
            })
            .catch(() =>
              Alert.alert('Произошла ошибка', 'Ошибка добавления рецепта в план питания'),
            );
        }
      },
      [date, returnScreen],
    );

    const [_, setToFavoriteStatus] = useAsyncFn(async () => {
      return await recipesAPI
        .addToFavorite({ access_token, recipe_id: data.id })
        .then(async () => {
          const isFavorite = await favoritesAPI.checkIsFaveReceipe(data.id);
          if (!isFavorite) {
            await favoritesAPI.setFavoriteReceipe(data.id);
          } else {
            await favoritesAPI.delFavoriteReceipe(data.id);
          }
          setFavorite(await favoritesAPI.checkIsFaveReceipe(data.id));
        })
        .catch(() => Alert.alert('Произошла ошибка'));
    }, [favorite]);

    useAsync(async () => {
      setFavorite(await favoritesAPI.checkIsFaveReceipe(data.id));
    }, [JSON.stringify(faves)]);

    const photo = useAsync(async () => {
      return await getCacheContent({ url: data.url_image, format: 'png' });
    }, [data]);

    return (
      <Container
        disabled={!data.has_access}
        underlayColor={theme.colors.gray4}
        onPress={() =>
          navigation.navigate('RecipeFull', {
            id: data.id,
            date,
            returnScreen,
            category: getCategoryId(),
          })
        }
      >
        <>
          <View>
            {/* need add visible data.url_gif to visible */}
            <RecipeCardImage source={{ uri: photo.value }} />
            <ContainerIcon onPress={setToFavoriteStatus}>
              <Icon name={!favorite ? 'favorites' : 'isFavorites'} color="primary" size={16} />
            </ContainerIcon>
            {planCategories.some((item) => item.id === getCategoryId().id) && (
              <ContainerBottomIcon
                onPress={() => {
                  if (date != null) {
                    addToRecipesPlan(date);
                  } else {
                    openContextMenu({
                      recipe_id: data.id,
                      category: getCategoryId().id,
                    });
                  }
                }}
              >
                <Image style={{ width: 20, height: 20 }} source={recipesInactive} />
              </ContainerBottomIcon>
            )}
          </View>
          <ContainerBurnTime>
            <Icon name="burn" />
            <TextBurn>{data.ccal} Ккал</TextBurn>
            <Circle />
            <Icon name="clock" />
            <TextTime>{data.serving_time} мин</TextTime>
            <Circle />
            <Icon color="tertiary" name="food" size={18} />
            <TextCategory>{categories.find((x) => x.id === getCategoryId().id)?.name}</TextCategory>
            <Circle />
            <Icon name="portionSize" />
            <SizeText>{data.cooking_size} г</SizeText>
          </ContainerBurnTime>
          <TextTitle>{data.name}</TextTitle>
          <Separator />
          <InfoContainer>
            <View>
              <TextItemValue>{data.proteins} г.</TextItemValue>
              <TextItemName>Белки</TextItemName>
            </View>
            <View>
              <TextItemValue>{data.carbonohydrates} г.</TextItemValue>
              <TextItemName>Углеводы</TextItemName>
            </View>
            <View>
              <TextItemValue>{data.fats} г.</TextItemValue>
              <TextItemName>Жиры</TextItemName>
            </View>
            <View>
              <ContainerComplexityIndicators>
                <ComplexityIndicator active={data.cooking_complexity >= 1} />
                <ComplexityIndicator active={data.cooking_complexity >= 2} />
                <ComplexityIndicator active={data.cooking_complexity >= 3} />
                <ComplexityIndicator active={data.cooking_complexity >= 4} />
                <ComplexityIndicator active={data.cooking_complexity >= 5} />
                <ComplexityIndicator active={data.cooking_complexity >= 6} />
                <ComplexityIndicator active={data.cooking_complexity >= 7} />
              </ContainerComplexityIndicators>
              <TextItemName>Сложность</TextItemName>
            </View>
          </InfoContainer>
          {!data.has_access && (
            <BlurContainer>
              <BillingButoonContainer>
                <Button
                  onPress={() => navigation.navigate('Promo')}
                  size="S"
                  content="Доступно в PRO"
                />
              </BillingButoonContainer>
            </BlurContainer>
          )}
        </>
      </Container>
    );
  },
  areEqual,
);
