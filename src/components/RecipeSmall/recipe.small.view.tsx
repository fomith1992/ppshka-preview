import { useNavigation } from '@react-navigation/core';
import { format, parse } from 'date-fns';
import React, { FC } from 'react';
import { StyleProp, View, ViewStyle } from 'react-native';
import { useSelector } from 'react-redux';

import { AppState } from 'src/store';
import Button from '../Button';
import {
  Added,
  BillingButonContainer,
  BlurContainer,
  Burn,
  Container,
  FlagsContainer,
  Image,
  Name,
  TitleContainer,
} from './recipe.small.style';

interface IProps {
  name: string;
  publish_date: string;
  burn: string;
  photoUri: string;
  style?: StyleProp<ViewStyle>;
  onPress: () => void;
}

const dateParse = (dateString: string): boolean => {
  const todayDate = format(new Date(), 'd');
  const incomingDate = format(new Date(parse(dateString, 'yyyy-MM-dd HH:mm:ss', new Date())), 'd');
  if (todayDate === incomingDate) {
    return true;
  } else return false;
};

const RecipeSmall: FC<IProps> = ({ style, name, burn, photoUri, publish_date, onPress }) => {
  const is_pro = useSelector((state: AppState) => state.session.user.is_pro);
  const navigation = useNavigation();

  return (
    <Container disabled={!is_pro} onPress={onPress} style={style}>
      <Image source={{ uri: photoUri }} />
      <TitleContainer>
        <Name>{name.length > 15 ? name.slice(0, 15) + '...' : name}</Name>
        <Burn>{burn}</Burn>
        <Burn>
          Добавлен: <Added>{dateParse(publish_date) ? 'Сегодня' : 'Вчера'}</Added>
        </Burn>
      </TitleContainer>
      <BlurContainer blur={!is_pro}>
        <FlagsContainer>
          <View />
          {is_pro ? (
            <View />
          ) : (
            <BillingButonContainer>
              <Button onPress={() => navigation.navigate('Promo')} size="S" content="PRO" />
            </BillingButonContainer>
          )}
        </FlagsContainer>
      </BlurContainer>
    </Container>
  );
};

export default RecipeSmall;
