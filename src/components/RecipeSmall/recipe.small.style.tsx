import { color } from '../../shared/config/color';
import { styled } from '../../shared/config/styled';
import { Text, TouchableOpacity, View } from 'react-native';
import { getScaleSize } from 'src/utils/dimensions';
import PreLoadedImage from '../PreLoadedImage';
import { font } from '@shared/config/text';

export const Container = styled(TouchableOpacity, {
  paddingRight: getScaleSize(15),
  flexDirection: 'row',
  backgroundColor: 'white',
  borderRadius: getScaleSize(15),
  marginBottom: getScaleSize(15),
  alignItems: 'center',
  overflow: 'hidden',
});

export const Image = styled(PreLoadedImage, {
  width: getScaleSize(90),
  height: getScaleSize(90),
  borderTopLeftRadius: getScaleSize(8),
  borderBottomLeftRadius: getScaleSize(8),
  marginRight: getScaleSize(10),
});

export const Name = styled(Text, {
  ...font({ type: 'text2', weight: 'normal' }),
  color: color('gray10'),
  marginBottom: getScaleSize(2),
});

export const Burn = styled(Text, {
  ...font({ type: 'text3' }),
  lineHeight: getScaleSize(16),
  color: color('gray7'),
  marginBottom: getScaleSize(4),
});

export const Added = styled(Text, {
  ...font({ type: 'text3' }),
  lineHeight: getScaleSize(16),
  color: color('tertiary'),
  marginBottom: getScaleSize(4),
});

export const BlurContainer = styled(
  View,
  (props: { blur: boolean }) =>
    ({
      position: 'absolute',
      top: 0,
      bottom: 0,
      left: 0,
      right: 0,
      backgroundColor: props.blur ? color('whiteOpacity80') : undefined,
    } as const),
);

export const BillingButonContainer = styled(View, {
  transform: [{ scale: 0.7 }],
});

export const FlagsContainer = styled(View, {
  position: 'absolute',
  flexDirection: 'row',
  width: '100%',
  justifyContent: 'space-between',
});

export const TitleContainer = styled(View, {});
