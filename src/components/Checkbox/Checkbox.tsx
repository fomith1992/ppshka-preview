import React, { Dispatch, FC, ReactNode, SetStateAction } from 'react';
import { StyleProp, TouchableOpacity, View, ViewStyle } from 'react-native';

import * as colors from 'src/commonStyles/colors';

import Icon from '../Icon';
import { styles } from './styles';

interface IProps {
  children: ReactNode;
  value: boolean;
  onChangeValue: Dispatch<SetStateAction<boolean>>;
  style?: StyleProp<ViewStyle>;
}

const Checkbox: FC<IProps> = ({ children, value, onChangeValue, style }) => {
  return (
    <TouchableOpacity
      style={[styles.container, style]}
      onPress={() => onChangeValue((prev) => !prev)}
    >
      <View
        style={[
          styles.checkbox,
          value ? { borderWidth: 0, backgroundColor: colors.purpleColor } : null,
        ]}
      >
        {value ? <Icon name="tick" /> : null}
      </View>
      {children}
    </TouchableOpacity>
  );
};

export default Checkbox;
