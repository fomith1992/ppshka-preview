import { StyleSheet } from 'react-native';

import { purpleColor } from 'src/commonStyles/colors';
import { getScaleSize } from 'src/utils/dimensions';

export const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  checkbox: {
    marginRight: getScaleSize(8),
    justifyContent: 'center',
    alignItems: 'center',
    width: getScaleSize(20),
    height: getScaleSize(20),
    borderRadius: 5,
    borderColor: purpleColor,
    borderWidth: 1,
  },
});
