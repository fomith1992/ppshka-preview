import React from 'react';
import { hitSlopParams } from 'src/utils/hit-slop-params';
import { ImageResizeView } from '../image-resize/image-resize.view';

import { AttachmentContainer, CloseButton, CloseSvg } from './add-post.style';

interface AttachmentProps {
  url: string;
  onDeleteImage: () => void;
}

export const Attachment = ({ url, onDeleteImage }: AttachmentProps): React.ReactElement => {
  return (
    <AttachmentContainer>
      <ImageResizeView border uri={url} />
      <CloseButton onPress={onDeleteImage} hitSlop={hitSlopParams('L')}>
        <CloseSvg />
      </CloseButton>
    </AttachmentContainer>
  );
};
