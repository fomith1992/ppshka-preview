import React, { useRef, useState } from 'react';
import { Animated, ScrollView, useWindowDimensions } from 'react-native';

import { Modal } from '@shared/ui/modal';

import { hitSlopParams } from 'src/utils/hit-slop-params';

import Button from '../Button';
import { ImageResizeView } from '../image-resize/image-resize.view';
import {
  AddImageButton,
  AddNewsContainer,
  AddPostTextInput,
  AttachmentsContainer,
  BottomButtonContainer,
  BottomContainer,
  GoBackButton,
  HeaderContainer,
  HLine,
  ImagesCloseButton,
  ImagesCloseSvg,
  ImageSvg,
  OverlayContainer,
  SelectImagesContainer,
  ShevronBack,
  Title,
  VLine,
} from './add-post.style';
import { Attachment } from './attachment.view';

interface AddPostProps {
  visible?: boolean;
  onVisibleChange: () => void;
}

const mockPhoto = 'https://www.gastronom.ru/binfiles/images/20170404/b826aef2.jpg';

export const AddPost = ({ visible = false, onVisibleChange }: AddPostProps): React.ReactElement => {
  const { width: windowWidth } = useWindowDimensions();
  const scrollX = useRef<ScrollView>(null);
  const scroll = useRef(new Animated.Value(0)).current;
  const [postText, setPostText] = useState<string>('');

  const localAttachments = [mockPhoto, mockPhoto, mockPhoto];
  const [attachments, setAttachments] = useState<string[]>(localAttachments);

  const moveBody = (index: number): void => {
    scrollX.current?.scrollTo({
      x: index * windowWidth,
      animated: true,
    });
  };

  const deleteImage = (idx: number) => {
    setAttachments(attachments.filter((_, index) => index !== idx));
  };

  return (
    <Modal animation="fade" visible={visible}>
      <ScrollView
        horizontal
        pagingEnabled
        scrollEnabled={false}
        showsHorizontalScrollIndicator={false}
        ref={scrollX}
        onScroll={Animated.event(
          [
            {
              nativeEvent: {
                contentOffset: {
                  x: scroll,
                },
              },
            },
          ],
          { useNativeDriver: false },
        )}
        scrollEventThrottle={1}
      >
        <OverlayContainer style={{ width: windowWidth }}>
          <AddNewsContainer>
            <HeaderContainer>
              <GoBackButton onPress={onVisibleChange} hitSlop={hitSlopParams('XL')}>
                <ShevronBack />
              </GoBackButton>
              <Title>Добавить новость</Title>
              <VLine />
              <AddImageButton onPress={() => moveBody(1)} hitSlop={hitSlopParams('XL')}>
                <ImageSvg />
              </AddImageButton>
            </HeaderContainer>
            <HLine />
            <AddPostTextInput
              placeholder="Расскажите, что у Вас нового..."
              value={postText}
              onChangeText={setPostText}
              maxLength={1000}
              multiline
            />
            {(postText.length > 0 || attachments.length > 0) && (
              <>
                <HLine />
                {attachments.length > 0 && (
                  <AttachmentsContainer>
                    {attachments.map((x, idx) => (
                      <Attachment
                        key={idx.toString()}
                        url={x}
                        onDeleteImage={() => deleteImage(idx)}
                      />
                    ))}
                  </AttachmentsContainer>
                )}
                <BottomButtonContainer>
                  <Button content="продолжить" />
                </BottomButtonContainer>
              </>
            )}
          </AddNewsContainer>
        </OverlayContainer>
        <OverlayContainer style={{ width: windowWidth }}>
          <SelectImagesContainer>
            <ImageResizeView uri={mockPhoto} />
            <ImagesCloseButton onPress={() => moveBody(0)} hitSlop={hitSlopParams('L')}>
              <ImagesCloseSvg />
            </ImagesCloseButton>
          </SelectImagesContainer>
          <BottomContainer>{/* container for images */}</BottomContainer>
        </OverlayContainer>
      </ScrollView>
    </Modal>
  );
};
