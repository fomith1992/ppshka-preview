import { color } from '@shared/config/color';
import { styled } from '@shared/config/styled';
import { blockShadow } from '@shared/ui/block-shadow/block-shadow';
import { ArrowBackIcon14 } from '@shared/ui/icons/arrow-back.icon-14';
import { CloseIcon8 } from '@shared/ui/icons/close.icon-8';
import { ImageIcon14 } from '@shared/ui/icons/image.icon-14';
import { TouchableOpacity, View, Text, TextInput } from 'react-native';
import { font14r, font20m } from 'src/commonStyles/fonts';
import { getScaleSize } from 'src/utils/dimensions';

export const OverlayContainer = styled(View, {
  flex: 1,
  backgroundColor: 'rgba(130, 130, 130, 0.6)',
});

export const AddNewsContainer = styled(View, {
  flex: 1,
  marginTop: getScaleSize(42),
  paddingTop: getScaleSize(24),
  paddingBottom: getScaleSize(40),
  backgroundColor: '#fff',
  borderTopLeftRadius: getScaleSize(15),
  borderTopRightRadius: getScaleSize(15),
  ...blockShadow(),
});

export const HeaderContainer = styled(View, {
  flexDirection: 'row',
  alignItems: 'center',
  marginHorizontal: getScaleSize(15),
  marginBottom: getScaleSize(20),
});

export const GoBackButton = styled(TouchableOpacity, {
  height: getScaleSize(30),
  width: getScaleSize(30),
  justifyContent: 'center',
  alignItems: 'center',
  marginRight: getScaleSize(12),
});

export const ShevronBack = styled(ArrowBackIcon14, {
  color: color('black'),
});

export const Title = styled(Text, {
  ...font20m,
  color: color('black'),
});

export const VLine = styled(View, {
  width: 1,
  height: getScaleSize(18),
  backgroundColor: color('gray5'),
  marginLeft: 'auto',
});

export const HLine = styled(View, {
  width: '100%',
  height: 1,
  backgroundColor: color('gray4'),
});

export const AddImageButton = styled(TouchableOpacity, {
  height: getScaleSize(30),
  width: getScaleSize(30),
  justifyContent: 'center',
  alignItems: 'center',
  marginLeft: getScaleSize(12),
});

export const ImageSvg = styled(ImageIcon14, {
  color: color('black'),
});

export const AddPostTextInput = styled(TextInput, {
  ...font14r,
  color: color('black'),
  textAlignVertical: 'top',
  height: getScaleSize(200),
  paddingHorizontal: getScaleSize(15),
  paddingTop: getScaleSize(20),
});

export const BottomButtonContainer = styled(View, {
  paddingHorizontal: getScaleSize(15),
  marginTop: getScaleSize(10),
});

export const BottomButtonFlexContainer = styled(View, {
  flex: 1,
  paddingHorizontal: getScaleSize(15),
  marginTop: getScaleSize(10),
});

export const AttachmentsContainer = styled(View, {
  paddingHorizontal: getScaleSize(15),
  marginTop: getScaleSize(20),
  flexDirection: 'row',
});

export const AttachmentContainer = styled(View, {
  width: getScaleSize(80),
  height: getScaleSize(90),
  marginRight: getScaleSize(8),
});

export const CloseButton = styled(TouchableOpacity, {
  width: getScaleSize(20),
  height: getScaleSize(20),
  position: 'absolute',
  right: getScaleSize(8),
  top: getScaleSize(8),
  backgroundColor: color('gray8'),
  justifyContent: 'center',
  alignItems: 'center',
  borderRadius: getScaleSize(5),
});

export const CloseSvg = styled(CloseIcon8, {
  color: color('white'),
});

export const SelectImagesContainer = styled(View, {
  flex: 1,
  backgroundColor: color('white'),
});

export const ImagesCloseButton = styled(TouchableOpacity, {
  width: getScaleSize(30),
  height: getScaleSize(30),
  position: 'absolute',
  left: getScaleSize(15),
  top: getScaleSize(50),
  backgroundColor: color('white'),
  justifyContent: 'center',
  alignItems: 'center',
  borderRadius: getScaleSize(8),
});

export const ImagesCloseSvg = styled(ArrowBackIcon14, {
  color: color('black'),
});

export const BottomContainer = styled(View, {
  /* top: getScaleSize(-35), */
  height: '55%',
  /* flex: 1, */
  backgroundColor: color('white'),
  borderTopLeftRadius: getScaleSize(15),
  borderTopRightRadius: getScaleSize(15),
  ...blockShadow(),
});
