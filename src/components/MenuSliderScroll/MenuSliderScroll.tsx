import React, { FC } from 'react';
import { LayoutAnimation, StyleProp, Text, TouchableOpacity, View, ViewStyle } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';

import { styles } from './styles';

interface IProps {
  style?: StyleProp<ViewStyle>;
  values: string[];
  selectedValue: string;
  setSelectedValue: (value: string) => void;
}

const MenuSliderScroll: FC<IProps> = ({ style, selectedValue, values, setSelectedValue }) => {
  return (
    <ScrollView
      horizontal
      style={style}
      contentContainerStyle={styles.container}
      showsHorizontalScrollIndicator={false}
    >
      <View style={styles.containerContent}>
        {values.map((value) => (
          <TouchableOpacity
            key={value}
            onPress={() => {
              setSelectedValue(value);
              LayoutAnimation.configureNext({
                ...LayoutAnimation.Presets.easeInEaseOut,
                duration: 100,
              });
            }}
            style={[
              styles.containerItem,
              selectedValue === value ? styles.containerSelectedItem : null,
            ]}
          >
            <Text
              style={[styles.textMenu, selectedValue === value ? styles.textSelectedItem : null]}
            >
              {value}
            </Text>
          </TouchableOpacity>
        ))}
      </View>
    </ScrollView>
  );
};

export default MenuSliderScroll;
