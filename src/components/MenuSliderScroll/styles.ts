import { StyleSheet } from 'react-native';

import { font14m, font14r } from 'src/commonStyles/fonts';
import { getScaleSize } from 'src/utils/dimensions';

export const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 5,
  },
  containerContent: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  containerItem: {
    paddingBottom: getScaleSize(10),
    paddingHorizontal: getScaleSize(5),
    marginHorizontal: 10,
  },
  containerSelectedItem: {
    borderBottomWidth: getScaleSize(1.5),
    borderColor: '#6034A8',
  },
  textMenu: {
    ...font14r,
    color: '#8C8C8C',
  },
  textSelectedItem: {
    ...font14m,
    color: '#262626',
  },
});
