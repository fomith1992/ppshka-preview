import { StyleSheet } from 'react-native';

import { getScaleSize } from '../../utils/dimensions';

export const styles = StyleSheet.create({
  imageCircle: {
    width: getScaleSize(30),
    height: getScaleSize(30),
    borderRadius: getScaleSize(30) / 2,
  },
  imageSquare: {
    width: getScaleSize(40),
    height: getScaleSize(40),
    borderRadius: getScaleSize(8),
  },
});
