import React, { FC } from 'react';
import { StyleProp, ViewStyle } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';

import { useNavigation } from '@react-navigation/native';

import PreLoadedImage from 'src/components/PreLoadedImage';

import { styles } from './styles';
import { mock_user_avatar } from 'src/mock/user';
import { useSelector } from 'react-redux';
import { AppState } from 'src/store';

interface IProps {
  style?: StyleProp<ViewStyle>;
  type?: 'square' | 'circle';
}

const ProfileIcon: FC<IProps> = ({ style, type = 'circle' }) => {
  const navigation = useNavigation();
  const userAvatar = useSelector((state: AppState) => state.session.user.user_photo);

  const onGoProfile = () => {
    navigation.navigate('Profile');
  };

  return (
    <TouchableOpacity style={style} onPress={onGoProfile}>
      <PreLoadedImage
        source={{
          uri: userAvatar ?? mock_user_avatar,
        }}
        style={type === 'circle' ? styles.imageCircle : styles.imageSquare}
      />
    </TouchableOpacity>
  );
};

export default ProfileIcon;
