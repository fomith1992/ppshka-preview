import React from 'react';
import { StyleSheet, View } from 'react-native';

import * as UI from './news-small.style';

type NewsSmallProps = {
  photo?: string;
  title?: string;
};

const fallbackPhoto = 'https://m.buro247.ru/images/2021/04/1619013418943542.jpg';

export const NewsSmallView = ({
  photo = fallbackPhoto,
  title = 'Мы обновили приложение. Новые фишки',
}: NewsSmallProps) => {
  return (
    <UI.Card>
      <UI.Image source={{ uri: photo }} />
      <View style={StyleSheet.absoluteFill}>
        <UI.LinearGradient
          colors={['rgba(0, 0, 0, 0)', '#000000']}
          start={{ x: 0, y: -0.2 }}
          end={{ x: 0, y: 1.3 }}
        />
        <UI.Title>{title}</UI.Title>
      </View>
    </UI.Card>
  );
};
