import { StyleSheet, Text, View } from 'react-native';
import LibLinearGradient from 'react-native-linear-gradient';

import { color } from '@shared/config/color';
import { styled } from '@shared/config/styled';

import PreLoadedImage from 'src/components/PreLoadedImage';
import { getScaleSize } from 'src/utils/dimensions';

import { font12m } from '../../commonStyles/fonts';

const CardStyle = {
  width: getScaleSize(120),
  height: getScaleSize(136),
  borderRadius: getScaleSize(15),
};

export const Card = styled(View, CardStyle);

export const Image = styled(PreLoadedImage, CardStyle);

export const LinearGradient = styled(LibLinearGradient, [StyleSheet.absoluteFill, CardStyle]);

export const Title = styled(Text, {
  ...font12m,
  position: 'absolute',
  left: getScaleSize(8),
  right: getScaleSize(8),
  bottom: getScaleSize(10),
  color: color('gray1'),
});
