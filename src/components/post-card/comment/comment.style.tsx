import { color } from '@shared/config/color';
import { styled } from '@shared/config/styled';
import { LikeIcon16 } from '@shared/ui/icons/like.icon-16';
import { TouchableOpacity } from 'react-native';
import { Text, View } from 'react-native';
import { font12m, font12r, font14m } from 'src/commonStyles/fonts';
import PreLoadedImage from 'src/components/PreLoadedImage';
import { getScaleSize } from 'src/utils/dimensions';

export const CommentContainer = styled(View, {
  flexDirection: 'row',
  alignItems: 'flex-start',
  marginTop: getScaleSize(16),
  marginBottom: getScaleSize(16),
});

export const Avatar = styled(
  PreLoadedImage,
  (props: { isReply: boolean }) =>
    ({
      width: getScaleSize(props.isReply ? 20 : 28),
      height: getScaleSize(props.isReply ? 20 : 28),
      borderRadius: getScaleSize(props.isReply ? 5 : 8),
    } as const),
);

export const BodyContainer = styled(View, {
  flex: 1,
  marginLeft: getScaleSize(12),
});

export const Title = styled(Text, {
  ...font14m,
});

export const Body = styled(Text, {
  ...font12r,
  marginTop: getScaleSize(2),
  fontSize: getScaleSize(13),
  lineHeight: getScaleSize(19),
});

export const Date = styled(Text, {
  ...font12r,
  marginTop: getScaleSize(2),
  fontSize: getScaleSize(13),
  lineHeight: getScaleSize(19),
  color: color('gray7'),
});

export const BottomContainer = styled(View, {
  flexDirection: 'row',
  alignItems: 'center',
  marginTop: getScaleSize(6),
});

export const ReplyButton = styled(TouchableOpacity, {
  marginLeft: getScaleSize(20),
});

export const ReplyButtonText = styled(Text, {
  ...font12m,
  marginTop: getScaleSize(2),
  color: color('gray7'),
});

export const LikeButton = styled(TouchableOpacity, {
  alignItems: 'center',
  flexDirection: 'row',
  marginRight: getScaleSize(16),
  marginLeft: 'auto',
});

export const LikeSvg = styled(
  LikeIcon16,
  (props: { isLiked: boolean }) =>
    ({
      color: color(props.isLiked ? 'secondary' : 'gray9'),
    } as const),
);

export const LikesCount = styled(
  Text,
  (props: { isPressed: boolean }) =>
    ({
      ...font12m,
      color: color(props.isPressed ? 'primary' : 'gray9'),
      lineHeight: getScaleSize(16),
      textAlign: 'center',
      marginLeft: getScaleSize(8),
    } as const),
);
