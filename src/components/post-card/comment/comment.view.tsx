import React from 'react';

import { hitSlopParams } from 'src/utils/hit-slop-params';

import { PostCardViewItem } from '../post-card.view';
import {
  Avatar,
  Body,
  BodyContainer,
  BottomContainer,
  CommentContainer,
  Date,
  LikeButton,
  LikesCount,
  LikeSvg,
  ReplyButton,
  ReplyButtonText,
  Title,
} from './comment.style';

interface CommentProps {
  data: PostCardViewItem;
  isReply?: boolean;
  replies?: React.ReactNode;
}

export const Comment = ({ data, isReply = false, replies }: CommentProps): React.ReactElement => {
  return (
    <CommentContainer>
      <Avatar isReply={isReply} source={{ uri: data.userData.avatar }} />
      <BodyContainer>
        <Title>
          {data.userData.firstName} {data.userData.lastName}
        </Title>
        <Body>{data.body}</Body>
        <BottomContainer>
          <Date>Сегодня 10:30</Date>
          <ReplyButton>
            <ReplyButtonText>Ответить</ReplyButtonText>
          </ReplyButton>
          <LikeButton hitSlop={hitSlopParams('L')}>
            <LikeSvg isLiked={data.isLiked} />
            <LikesCount isPressed={false}>{data.likesCount > 0 ? data.likesCount : ''}</LikesCount>
          </LikeButton>
        </BottomContainer>
        {replies}
      </BodyContainer>
    </CommentContainer>
  );
};
