import React from 'react';
import { TagText } from './tags.style';

export interface TagProps {
  tag?: string;
}

export const Tag = ({ tag }: TagProps): React.ReactElement => {
  if (!tag) {
    return <></>;
  }
  return <TagText>{tag}</TagText>;
};
