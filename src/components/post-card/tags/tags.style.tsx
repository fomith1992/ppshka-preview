import { color } from '@shared/config/color';
import { styled } from '@shared/config/styled';
import { Text } from 'react-native';
import { font10r } from 'src/commonStyles/fonts';
import { getScaleSize } from 'src/utils/dimensions';

export const TagText = styled(Text, {
  ...font10r,
  marginLeft: getScaleSize(8),
  paddingHorizontal: getScaleSize(5),
  paddingVertical: getScaleSize(3),
  backgroundColor: color('primary'),
  color: color('white'),
  borderRadius: getScaleSize(5),
});
