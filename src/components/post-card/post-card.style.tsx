import { Platform, StyleSheet, Text, TouchableOpacity, View } from 'react-native';

import { color } from '@shared/config/color';
import { styled } from '@shared/config/styled';
import { ArrowBackIcon14 } from '@shared/ui/icons/arrow-back.icon-14';
import { BookmarkIcon16 } from '@shared/ui/icons/bookmark.icon-16';
import { LikeIcon16 } from '@shared/ui/icons/like.icon-16';
import { MessageCloudIcon16 } from '@shared/ui/icons/message-cloud.icon-16';

import { font12m, font12r, font14r, font16m } from 'src/commonStyles/fonts';
import { getScaleSize } from 'src/utils/dimensions';

import PreLoadedImage from '../PreLoadedImage';

export const PostCardContainer = styled(
  View,
  (props: { full: boolean }) =>
    ({
      paddingHorizontal: getScaleSize(20),
      paddingTop: getScaleSize(20),
      backgroundColor: color('white'),
      paddingBottom: getScaleSize(20),
      marginBottom: !props.full ? getScaleSize(16) : undefined,
      borderRadius: !props.full ? getScaleSize(15) : undefined,
      ...Platform.select({
        android: {
          elevation: 1,
        },
        ios: {
          shadowColor: color('black'),
          shadowOffset: {
            width: 0,
            height: 1,
          },
          shadowOpacity: 0.06,
          shadowRadius: 1,
        },
      }),
    } as const),
);

export const HeaderContainer = styled(TouchableOpacity, {
  flexDirection: 'row',
  alignItems: 'center',
  marginBottom: getScaleSize(16),
});

export const DescriptionContainer = styled(View, {
  marginLeft: getScaleSize(12),
});

export const TitleContainer = styled(View, {
  flexDirection: 'row',
  alignItems: 'center',
});

export const Avatar = styled(PreLoadedImage, {
  width: getScaleSize(40),
  height: getScaleSize(40),
  borderRadius: getScaleSize(8),
});

export const Title = styled(Text, {
  ...font16m,
});

export const Description = styled(Text, {
  ...font12r,
});

export const BodyText = styled(Text, {
  ...font14r,
  lineHeight: getScaleSize(21),
  marginTop: getScaleSize(12),
});

export const PostButtonsContainer = styled(View, {
  flexDirection: 'row',
  alignItems: 'center',
  marginTop: getScaleSize(12),
});

export const DotsContainer = styled(View, {
  ...StyleSheet.absoluteFillObject,
  justifyContent: 'center',
  alignItems: 'center',
});

export const CommentButton = styled(TouchableOpacity, {
  alignItems: 'center',
  flexDirection: 'row',
  marginRight: getScaleSize(16),
});

export const LikeSvg = styled(
  LikeIcon16,
  (props: { isLiked: boolean }) =>
    ({
      color: color(props.isLiked ? 'secondary' : 'gray9'),
    } as const),
);

export const CommentSvg = styled(MessageCloudIcon16, {
  color: color('gray9'),
});

export const BookmarkSvg = styled(BookmarkIcon16, {
  color: color('gray9'),
  right: 0,
});

export const PostButtonCount = styled(
  Text,
  (props: { isPressed: boolean }) =>
    ({
      ...font12m,
      color: color(props.isPressed ? 'primary' : 'gray9'),
      lineHeight: getScaleSize(16),
      textAlign: 'center',
      marginLeft: getScaleSize(8),
    } as const),
);

export const Spring = styled(View, {
  flex: 1,
});

export const GoBackButton = styled(TouchableOpacity, {
  height: getScaleSize(40),
  width: getScaleSize(40),
  justifyContent: 'center',
  alignItems: 'center',
  marginRight: getScaleSize(15),
  borderWidth: 1,
  borderRadius: getScaleSize(8),
  borderColor: color('gray4'),
});

export const ShevronBack = styled(ArrowBackIcon14, {
  color: color('black'),
});

export const HLine = styled(View, {
  width: '100%',
  height: 1,
  backgroundColor: color('gray3'),
  marginTop: getScaleSize(16),
});
