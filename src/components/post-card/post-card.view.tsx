import React, { useState } from 'react';
import { StyleProp, TouchableOpacity, ViewStyle } from 'react-native';

import { hitSlopParams } from 'src/utils/hit-slop-params';

import { Slider } from '../slider';
import { SwiperDots } from '../slider/dots';
import { Comment } from './comment/comment.view';
import {
  Avatar,
  BodyText,
  BookmarkSvg,
  CommentButton,
  CommentSvg,
  Description,
  DescriptionContainer,
  DotsContainer,
  GoBackButton,
  HeaderContainer,
  HLine,
  LikeSvg,
  PostButtonCount,
  PostButtonsContainer,
  PostCardContainer,
  ShevronBack,
  Spring,
  Title,
  TitleContainer,
} from './post-card.style';
import { Tag } from './tags/tags.view';

export interface PostCardViewItem {
  userData: {
    firstName: string;
    lastName: string;
    specialization: string;
    avatar?: string;
    tag?: string;
  };
  likesCount: number;
  isLiked: boolean;
  commentsCount: number;
  attachments: string[];
  body: string;
}

export interface PostCardViewProps extends PostCardViewItem {
  onPostPressed: () => void;
  onGoBack: () => void;
  full: boolean;
  comments: PostCardViewItem[];
  style?: StyleProp<ViewStyle>;
}

export const PostCardView = ({
  userData,
  attachments,
  body,
  commentsCount,
  isLiked,
  likesCount,
  full,
  comments,
  onPostPressed,
  onGoBack,
  style,
}: PostCardViewProps): React.ReactElement => {
  const openFullPost = () => {
    onPostPressed();
  };

  const [dotIndex, setDotIndex] = useState(0);

  return (
    <PostCardContainer full={full} style={style}>
      <HeaderContainer>
        {full && (
          <GoBackButton onPress={onGoBack}>
            <ShevronBack />
          </GoBackButton>
        )}
        <Avatar source={{ uri: userData.avatar }} />
        <DescriptionContainer>
          <TitleContainer>
            <Title>
              {userData.firstName} {userData.lastName}
            </Title>
            <Tag tag={userData.tag} />
          </TitleContainer>
          <Description>{userData.specialization}</Description>
        </DescriptionContainer>
      </HeaderContainer>
      <Slider items={attachments} onPress={onPostPressed} onIndexChanged={setDotIndex} />
      <PostButtonsContainer>
        <CommentButton hitSlop={hitSlopParams('L')}>
          <LikeSvg isLiked={isLiked} />
          <PostButtonCount isPressed={false}>{likesCount > 0 ? likesCount : ''}</PostButtonCount>
        </CommentButton>
        <CommentButton hitSlop={hitSlopParams('L')}>
          <CommentSvg />
          <PostButtonCount isPressed={false}>
            {commentsCount > 0 ? commentsCount : ''}
          </PostButtonCount>
        </CommentButton>
        {attachments.length > 1 && (
          <DotsContainer>
            <SwiperDots activeIndex={dotIndex} itemsCount={attachments.length} />
          </DotsContainer>
        )}
        <Spring />
        <TouchableOpacity hitSlop={hitSlopParams('L')}>
          <BookmarkSvg />
        </TouchableOpacity>
      </PostButtonsContainer>
      <TouchableOpacity activeOpacity={0.8} onPress={openFullPost}>
        <BodyText>{body}</BodyText>
      </TouchableOpacity>
      {full && (
        <>
          <HLine />
          {comments.map((item, idx) => (
            <Comment
              key={'item' + idx.toString()}
              replies={[
                <Comment key={'1'} isReply data={item} />,
                <Comment key={'2'} isReply data={item} />,
                <Comment key={'3'} isReply data={item} />,
              ]}
              data={item}
            />
          ))}
        </>
      )}
    </PostCardContainer>
  );
};
