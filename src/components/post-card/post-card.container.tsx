import React from 'react';
import { StyleProp, ViewStyle } from 'react-native';

import { useNavigation } from '@react-navigation/native';

import { PostCardView, PostCardViewItem } from './post-card.view';

interface PostCardProps {
  full?: boolean;
  style?: StyleProp<ViewStyle>;
}

const mockData: PostCardViewItem = {
  userData: {
    firstName: 'Екатерина',
    lastName: 'Пантелеева',
    specialization: 'Тренер',
    avatar: 'https://www.gastronom.ru/binfiles/images/20170404/b826aef2.jpg',
    tag: 'PRO',
  },
  attachments: [
    'https://www.gastronom.ru/binfiles/images/20170404/b826aef2.jpg',
    'https://www.gastronom.ru/binfiles/images/20201208/b42ff775.jpg',
    'https://www.gastronom.ru/binfiles/images/20191227/bf8527c2.jpg',
  ],
  commentsCount: 12311,
  body: 'Развлекательный контент – неотъемлемый блок в любой нише. Да, любой. Даже если вам (или заказчику) кажется, что у вас серьезная тема. В любой тематике покупатели – люди',
  isLiked: false,
  likesCount: 2231,
};

export const PostCard = ({ full = false, style = {} }: PostCardProps): React.ReactElement => {
  const navigation = useNavigation();
  return (
    <PostCardView
      full={full}
      comments={[mockData]}
      attachments={mockData.attachments}
      body={mockData.body}
      commentsCount={mockData.commentsCount}
      isLiked={mockData.isLiked}
      likesCount={mockData.likesCount}
      userData={mockData.userData}
      onPostPressed={() => navigation.navigate('FullPost')}
      onGoBack={navigation.goBack}
      style={style}
    />
  );
};
