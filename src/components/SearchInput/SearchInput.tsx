import React, { FC } from 'react';
import { TextInput, View } from 'react-native';

import Icon from 'src/components/Icon';
import { getScaleSize } from 'src/utils/dimensions';

import { styles } from './styles';

interface IProps {
  value: string;
  onChangeText: (text: string) => void;
  height?: number;
}

const SearchInput: FC<IProps> = ({ value, onChangeText, height = 34 }) => {
  return (
    <View style={styles.container}>
      <Icon name="search" style={styles.icon} size={height === 34 ? undefined : 14} />
      <TextInput
        value={value}
        onChangeText={onChangeText}
        style={[styles.input, { height: getScaleSize(height) }]}
        placeholder="Поиск рецепта"
        placeholderTextColor="#8C8C8C"
        selectionColor={'black'}
      />
    </View>
  );
};

export default SearchInput;
