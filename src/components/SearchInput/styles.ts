import { StyleSheet } from 'react-native';

import { font12r } from 'src/commonStyles/fonts';
import { getScaleSize } from 'src/utils/dimensions';

export const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    backgroundColor: '#F5F5F5',
    borderRadius: getScaleSize(5),
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: getScaleSize(13),
    flex: 1,
  },
  input: {
    padding: 0,
    flex: 1,
    ...font12r,
  },
  icon: {
    marginRight: getScaleSize(10),
  },
});
