import React from 'react';

import { Container, Dot } from './styles';

type SwiperDotsProps = {
  activeIndex: number;
  itemsCount: number;
};

export const SwiperDots = ({ activeIndex, itemsCount }: SwiperDotsProps) => {
  return (
    <Container>
      {Array.from({ length: itemsCount }, (_, idx) => (
        <Dot key={idx} isActive={idx === activeIndex} />
      ))}
    </Container>
  );
};
