import React, { memo } from 'react';
import { Image, TouchableOpacity } from 'react-native';
import Swiper from 'react-native-swiper';

import { getScaleSize } from 'src/utils/dimensions';

type SliderProps = {
  items: string[];
  onPress?: () => void;
  onIndexChanged?: (index: number) => void;
};

export const Slider = memo(({ items, onPress, onIndexChanged }: SliderProps) => {
  return (
    <Swiper
      height={220}
      removeClippedSubviews={false}
      onIndexChanged={onIndexChanged}
      showsPagination={false}
      showsButtons={false}
    >
      {items.map((uri) => (
        <TouchableOpacity activeOpacity={0.8} onPress={onPress}>
          <Image
            source={{ uri }}
            style={{ height: '100%', width: '100%', borderRadius: getScaleSize(15) }}
          />
        </TouchableOpacity>
      ))}
    </Swiper>
  );
});
