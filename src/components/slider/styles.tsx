import { View } from 'react-native';

import { color } from '@shared/config/color';
import { styled } from '@shared/config/styled';

import { getScaleSize } from 'src/utils/dimensions';

export const Dot = styled(
  View,
  ({ isActive }: { isActive: boolean }) =>
    ({
      backgroundColor: isActive ? color('primary') : color('gray5'),
      width: getScaleSize(5),
      height: getScaleSize(5),
      borderRadius: getScaleSize(3),
      marginHorizontal: getScaleSize(4),
    } as const),
);

export const Container = styled(View, {
  flexDirection: 'row',
});
