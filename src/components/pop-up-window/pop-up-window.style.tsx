import { color } from '@shared/config/color';
import { styled } from '@shared/config/styled';
import { View } from 'react-native';
import { getScaleSize, screenWidth } from 'src/utils/dimensions';

export const OverlayContainer = styled(View, {
  flex: 1,
  backgroundColor: 'rgba(130, 130, 130, 0.6)',
  justifyContent: 'center',
  alignItems: 'center',
});

export const PopUpContainer = styled(View, {
  width: screenWidth - getScaleSize(30),
  marginHorizontal: getScaleSize(15),
  paddingHorizontal: getScaleSize(15),
  paddingVertical: getScaleSize(20),
  backgroundColor: color('white'),
  borderRadius: getScaleSize(12),
});
