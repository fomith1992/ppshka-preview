import { Modal } from '@shared/ui/modal';
import React from 'react';
import { KeyboardAvoidingView } from 'react-native';
import { OverlayContainer, PopUpContainer } from './pop-up-window.style';

interface PopUpWindowProps {
  modalVisible: boolean;
  children: React.ReactNode;
}

export const PopUpWindow = ({ modalVisible, children }: PopUpWindowProps) => {
  return (
    <Modal animation="fade" visible={modalVisible}>
      <KeyboardAvoidingView style={{ flex: 1 }} behavior={'padding'}>
        <OverlayContainer>
          <PopUpContainer>{children}</PopUpContainer>
        </OverlayContainer>
      </KeyboardAvoidingView>
    </Modal>
  );
};
