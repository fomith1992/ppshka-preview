import { useNavigation } from '@react-navigation/native';
import React from 'react';
import { WorkoutCardView } from './program-card.view';

interface ProgramCardProps {
  id: number;
  title: string;
  description: string;
  duration: number;
  photoUri: string;
}

export function ProgramCard(data: ProgramCardProps): React.ReactElement {
  const navigation = useNavigation();
  return (
    <WorkoutCardView
      {...data}
      onPress={() => navigation.navigate('ProgramDetails', { id: data.id })}
    />
  );
}
