import React from 'react';

import Icon from 'src/components/Icon';

import {
  Container,
  ContentContainer,
  Description,
  ImageWorkout,
  TimeContainer,
  TimeText,
  Title,
} from './program-card.style';

interface WorkoutCardViewProps {
  title: string;
  description: string;
  duration: number;
  photoUri: string;
  onPress: () => void;
}

export function WorkoutCardView({
  title,
  description,
  duration,
  photoUri,
  onPress,
}: WorkoutCardViewProps): React.ReactElement {
  return (
    <Container onPress={onPress} disabled={!onPress}>
      <ImageWorkout source={{ uri: photoUri }} />
      <ContentContainer>
        <TimeContainer>
          <Icon name="clock" size={16} />
          <TimeText>{duration} дней</TimeText>
        </TimeContainer>
        <Title>{title}</Title>
        <Description numberOfLines={4}>{description}</Description>
      </ContentContainer>
    </Container>
  );
}
