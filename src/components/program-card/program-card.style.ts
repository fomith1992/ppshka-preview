import { color } from '@shared/config/color';
import { styled } from '@shared/config/styled';
import { font } from '@shared/config/text';
import { Text, TouchableOpacity, View } from 'react-native';
import { Image } from 'react-native';

import { getScaleSize } from 'src/utils/dimensions';

export const Container = styled(TouchableOpacity, {
  backgroundColor: '#fff',
  borderRadius: getScaleSize(12),
  padding: getScaleSize(10),
});

export const ImageWorkout = styled(Image, {
  aspectRatio: 3 / 2,
  borderRadius: getScaleSize(12),
});

export const ContentContainer = styled(View, {
  marginTop: getScaleSize(10),
});

export const TimeContainer = styled(View, {
  flexDirection: 'row',
  marginBottom: getScaleSize(8),
  alignItems: 'center',
});

export const TimeText = styled(Text, {
  ...font({ type: 'text3' }),
  color: color('primary'),
  marginLeft: getScaleSize(5),
});

export const Title = styled(Text, {
  ...font({ type: 'text2', weight: 'bold' }),
  color: color('gray9'),
});

export const Description = styled(Text, {
  ...font({ type: 'text3' }),
  color: color('gray7'),
});
