import React from 'react';
import { LinkButtonContainer, LinkButtonText } from './button-link.style';

interface ButtonLinkProps {
  onPress: () => void;
  children: React.ReactNode;
}

export function ButtonLink({ onPress, children }: ButtonLinkProps): React.ReactElement {
  return (
    <LinkButtonContainer onPress={onPress}>
      <LinkButtonText>{children}</LinkButtonText>
    </LinkButtonContainer>
  );
}
