import { color } from '@shared/config/color';
import { styled } from '@shared/config/styled';
import { font } from '@shared/config/text';
import { Text, TouchableOpacity } from 'react-native';

export const LinkButtonContainer = TouchableOpacity;

export const LinkButtonText = styled(Text, {
  ...font({ type: 'text2' }),
  color: color('gray10'),
});
