import { color } from '@shared/config/color';
import { styled } from '@shared/config/styled';
import { font } from '@shared/config/text';
import { Text, TouchableOpacity } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

import { getScaleSize } from 'src/utils/dimensions';

export const ButtonContainerBackground = styled(
  TouchableOpacity,
  (props: { active: boolean; disabled: boolean }) =>
    ({
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      borderRadius: getScaleSize(15),
      paddingHorizontal: getScaleSize(20),
      backgroundColor: props.active ? color('white') : 'transparent',
    } as const),
);

export const GradientContainer = styled(
  LinearGradient,
  (props: { size?: 'S' | 'M' }) =>
    ({
      height: getScaleSize(props.size === 'S' ? 35 : 55),
      borderRadius: getScaleSize(30),
      padding: 1,
    } as const),
);

export const Title = styled(
  Text,
  (props: { borderGradient?: boolean; uppercase?: boolean }) =>
    ({
      ...font({ type: 'text3', weight: 'bold' }),
      color: color(props.borderGradient ? 'gray10' : 'white'),
      textTransform: props.uppercase ? 'uppercase' : undefined,
      textAlignVertical: 'center',
      textAlign: 'center',
    } as const),
);
