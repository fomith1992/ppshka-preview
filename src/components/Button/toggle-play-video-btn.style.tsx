import { styled } from '@shared/config/styled';
import { font } from '@shared/config/text';
import { Text, TouchableOpacity } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

import { getScaleSize } from 'src/utils/dimensions';

export const TouchableContainer = styled(TouchableOpacity, {
  padding: getScaleSize(15),
  justifyContent: 'center',
  alignItems: 'center',
  borderRadius: getScaleSize(30),
  overflow: 'hidden',
});

export const GradientContainer = styled(LinearGradient, {
  borderRadius: getScaleSize(30),
});

export const Title = styled(Text, {
  ...font({ type: 'text3', weight: 'bold' }),
  color: '#fff',
  textTransform: 'uppercase',
});
