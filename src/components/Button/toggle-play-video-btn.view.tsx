import { gradientComponentsColors } from '@shared/ui/gradient-layout/constant';
import React from 'react';
import { GradientContainer, TouchableContainer } from './toggle-play-video-btn.style';
import { IButtonProps } from './types';

export function TogglePlayVideoButton({
  onPress,
  content,
  disabled,
}: IButtonProps): React.ReactElement {
  return (
    <GradientContainer colors={gradientComponentsColors}>
      <TouchableContainer disabled={disabled ?? false} onPress={onPress}>
        {content}
      </TouchableContainer>
    </GradientContainer>
  );
}
