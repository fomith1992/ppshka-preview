export interface IButtonProps {
  content: React.ReactNode;
  disabled?: boolean;
  onPress?: () => void;
  size?: 'S' | 'M';
  borderGradient?: boolean;
  uppercase?: boolean;
}
