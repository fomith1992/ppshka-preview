import { gradientComponentsColors, transparentsColors } from '@shared/ui/gradient-layout/constant';
import React, { FC } from 'react';

import { GradientContainer, Title, ButtonContainerBackground } from './styles';
import { IButtonProps } from './types';

export const CircleButton: FC<IButtonProps> = ({
  onPress,
  content,
  disabled = false,
  size = 'M',
  borderGradient = false,
  uppercase = false,
}) => {
  return (
    <GradientContainer
      size={size}
      colors={disabled ? transparentsColors : gradientComponentsColors}
    >
      <ButtonContainerBackground
        active={borderGradient}
        disabled={disabled ?? false}
        onPress={onPress}
      >
        <Title uppercase={uppercase} borderGradient={borderGradient || disabled}>
          {content}
        </Title>
      </ButtonContainerBackground>
    </GradientContainer>
  );
};
