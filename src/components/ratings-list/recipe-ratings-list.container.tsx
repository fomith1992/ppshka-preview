import React, { useState } from 'react';
import { Alert } from 'react-native';
import { useSelector } from 'react-redux';
import useAsync from 'react-use/lib/useAsync';
import { feedbackAPI } from 'src/api';
import { AppState } from 'src/store';
import { RatingsListView, RecipeFeedback } from './ratings-list.view';

interface RecipeRatingsListProps {
  id: number;
}

export function RecipeRatingsList({ id }: RecipeRatingsListProps): React.ReactElement {
  const access_token = useSelector((state: AppState) => state.session.tokens.accessToken);
  const [serverCurrentPage, setServerCurrentPage] = useState(0);
  const [feedbacksData, setFeedbacksData] = useState<RecipeFeedback[]>([]);
  const [hasMoreToLoad, setHasMoreToLoad] = useState<boolean>(true);

  const feedbackApi = useAsync(async () => {
    return await feedbackAPI
      .recipeFeedbacksById({
        recipe_id: id,
        page: serverCurrentPage,
        access_token,
      })
      .then(async ({ data: { data } }) => {
        setHasMoreToLoad(data.data.length > 0);
        setFeedbacksData(feedbacksData.concat(data.data));
      })
      .catch((e) => {
        console.log('Recipes: ', e);
        Alert.alert('Произошла ошибка');
      });
  }, [serverCurrentPage]);

  return (
    <RatingsListView
      id={id}
      data={feedbacksData}
      loading={feedbackApi.loading}
      setNextPage={() => {
        setServerCurrentPage(serverCurrentPage + 1);
      }}
      hasMoreToLoad={hasMoreToLoad}
    />
  );
}
