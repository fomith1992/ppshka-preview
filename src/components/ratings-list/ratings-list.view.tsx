import { theme } from '@shared/config/theme';
import { gradientComponentsColors } from '@shared/ui/gradient-layout/constant';
import React, { useState } from 'react';
import { ActivityIndicator, FlatList, NativeScrollEvent, NativeSyntheticEvent } from 'react-native';
import { Feedback } from 'src/api/feedback';
import { screenHeight, screenWidth } from 'src/utils/dimensions';
import { RecipeRatingCard } from '../rating-card/recipe-rating-card.container';
import { RatingSendButton } from '../rating-send-button/rating-send-button.container';
import { RatingItemCard } from './rating-item-card/rating-item-card.vew';
import {
  ButtonTitle,
  Container,
  EmptyComponentContainer,
  EmptyComponentText,
  GradientContainer,
  RateButtonContainer,
  SpringBottom,
} from './ratings-list.style';

export type RecipeFeedback = Feedback<{ recipe_id: number }>;

interface RatingsListViewProps {
  data: RecipeFeedback[];
  loading: boolean;
  setNextPage: () => void;
  hasMoreToLoad: boolean;
  id: number;
}

export function RatingsListView({
  data,
  loading,
  hasMoreToLoad,
  setNextPage,
  id,
}: RatingsListViewProps): React.ReactElement {
  const [buttonVisible, setButtonVisible] = useState(false);
  const handleOnScroll = (event: NativeSyntheticEvent<NativeScrollEvent>) => {
    if (event.nativeEvent.contentOffset.y > screenHeight * 0.8) {
      setButtonVisible(true);
    } else {
      setButtonVisible(false);
    }
  };
  return (
    <Container>
      <FlatList<RecipeFeedback>
        contentContainerStyle={{ position: 'absolute' }}
        onEndReached={hasMoreToLoad ? setNextPage : null}
        onEndReachedThreshold={0.3}
        onScroll={handleOnScroll}
        data={data}
        ListHeaderComponentStyle={{ paddingTop: (screenWidth / 4) * 5 - 16 }}
        ListHeaderComponent={<RecipeRatingCard id={id} small />}
        renderItem={({ item }) => (
          <RatingItemCard
            userName={item.user_name}
            userAvatarUrl={item.user_photo.length > 0 ? item.user_photo : undefined}
            comment={item.comment}
            createdAt={item.normal_add_date}
            rate={item.rate}
            imageUri={item.photo_file_path}
          />
        )}
        keyExtractor={(_, index) => index.toString()}
        showsVerticalScrollIndicator={false}
        ListFooterComponent={() => (
          <>
            {loading && data.length !== 0 && (
              <ActivityIndicator size="large" color={theme.colors.white} />
            )}
            <SpringBottom />
          </>
        )}
        ListEmptyComponent={() => (
          <>
            {loading ? (
              <ActivityIndicator size="large" color={theme.colors.white} />
            ) : (
              <EmptyComponentContainer>
                <EmptyComponentText>Пока нет отзывов</EmptyComponentText>
              </EmptyComponentContainer>
            )}
          </>
        )}
      />
      <RateButtonContainer style={{ bottom: buttonVisible ? 100 : -100 }}>
        <RatingSendButton
          onGetRecipeRate={() => console.log('need update feedbacks state')}
          recipeId={id}
        >
          <GradientContainer colors={gradientComponentsColors}>
            <ButtonTitle>оставить отзыв</ButtonTitle>
          </GradientContainer>
        </RatingSendButton>
      </RateButtonContainer>
    </Container>
  );
}
