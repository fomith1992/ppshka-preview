import { theme } from '@shared/config/theme';
import React from 'react';
import { Rating } from 'react-native-ratings';
import {
  BoxContainer,
  CommentText,
  Container,
  CreatedAtText,
  FeedbackImage,
  HeaderContainer,
  UserAvatar,
  UserDescriptionContainer,
  UserInfoContainer,
  UserNameText,
} from './rating-item-card.style';
import mock_user_avatar from '../../../shared/ui/assets/no-avatar.png';

interface RatingItemCardProps {
  userAvatarUrl?: string;
  userName: string;
  createdAt: string;
  comment: string;
  rate: number;
  imageUri?: string;
}

export function RatingItemCard({
  userAvatarUrl,
  userName,
  createdAt,
  comment,
  rate,
  imageUri,
}: RatingItemCardProps): React.ReactElement {
  const HEIGHT_STARS_ROW = 15;
  return (
    <BoxContainer>
      <Container>
        <HeaderContainer>
          <UserInfoContainer>
            <UserAvatar
              source={
                userAvatarUrl
                  ? {
                      uri: userAvatarUrl,
                    }
                  : mock_user_avatar
              }
            />
            <UserDescriptionContainer>
              <UserNameText>
                {userName === '' || userName == null ? 'No Name' : userName}
              </UserNameText>
              <CreatedAtText>{createdAt}</CreatedAtText>
            </UserDescriptionContainer>
          </UserInfoContainer>
          <Rating
            type="custom"
            ratingColor={theme.colors.gradientColor2}
            tintColor={theme.colors.white}
            ratingBackgroundColor={theme.colors.gray6}
            ratingCount={5}
            startingValue={rate}
            imageSize={HEIGHT_STARS_ROW}
            readonly={true}
            style={{
              margin: 0,
              padding: 0,
            }}
          />
        </HeaderContainer>
        <CommentText>{comment}</CommentText>
        {imageUri == null ? (
          <></>
        ) : (
          <FeedbackImage
            source={{
              uri: imageUri,
            }}
          />
        )}
      </Container>
    </BoxContainer>
  );
}
