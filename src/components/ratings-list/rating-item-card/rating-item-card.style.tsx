import { color } from '@shared/config/color';
import { styled } from '@shared/config/styled';
import { font } from '@shared/config/text';
import { container } from '@shared/config/view';
import { blockShadow } from '@shared/ui/block-shadow/block-shadow';
import { Text, View } from 'react-native';
import PreLoadedImage from 'src/components/PreLoadedImage';

import { getScaleSize, screenWidth } from 'src/utils/dimensions';

export const BoxContainer = styled(View, {
  width: screenWidth,
  paddingTop: getScaleSize(16),
  backgroundColor: color('gray4'),
});

export const Container = styled(View, {
  width: screenWidth,
  borderRadius: getScaleSize(12),
  /* paddingVertical: getScaleSize(20), */
  paddingTop: getScaleSize(20),
  backgroundColor: color('white'),
  ...blockShadow(),
});

export const HeaderContainer = styled(View, {
  flexDirection: 'row',
  justifyContent: 'space-between',
  alignItems: 'center',
  marginRight: getScaleSize(16),
});

export const CreatedAtText = styled(Text, {
  ...font({ type: 'text2', weight: 'light' }),
  color: color('gray7'),
});

export const UserInfoContainer = styled(View, {
  ...container('margin'),
  flexDirection: 'row',
  marginVertical: getScaleSize(5),
  alignItems: 'center',
});

export const UserAvatar = styled(PreLoadedImage, {
  width: getScaleSize(40),
  height: getScaleSize(40),
  borderRadius: getScaleSize(8),
});

export const UserNameText = styled(Text, {
  ...font({ type: 'text1' }),
  color: color('gray10'),
});

export const UserDescriptionContainer = styled(View, {
  marginLeft: getScaleSize(10),
});

export const CommentText = styled(Text, {
  ...font({ type: 'text2' }),
  color: color('gray9'),
  ...container('margin'),
  marginBottom: getScaleSize(10),
});

export const FeedbackImage = styled(PreLoadedImage, {
  borderBottomRightRadius: getScaleSize(12),
  borderBottomLeftRadius: getScaleSize(12),
  height: ({ layout }) => (screenWidth - getScaleSize(layout.margin) * 2) * 0.56,
  width: screenWidth,
});
