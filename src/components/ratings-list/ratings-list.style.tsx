import { color } from '@shared/config/color';
import { styled } from '@shared/config/styled';
import { font } from '@shared/config/text';
import { container } from '@shared/config/view';
import { Text, View } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

import { getScaleSize } from 'src/utils/dimensions';

export const Container = styled(View, {
  flex: 1,
});

export const SpringBottom = styled(View, {
  height: getScaleSize(90),
});

export const RateButtonContainer = styled(View, {
  ...container(),
  position: 'absolute',
  left: 0,
  right: 0,
});

export const EmptyComponentContainer = styled(View, {
  flex: 1,
  justifyContent: 'center',
  alignItems: 'center',
});

export const EmptyComponentText = styled(Text, {
  ...font({ type: 'text1' }),
  color: color('white'),
  fontSize: getScaleSize(16),
});

export const ButtonTitle = styled(Text, {
  ...font({ type: 'text3', weight: 'bold' }),
  color: color('white'),
  textTransform: 'uppercase',
  textAlignVertical: 'center',
  textAlign: 'center',
});

export const GradientContainer = styled(LinearGradient, {
  flex: 1,
  height: getScaleSize(55),
  borderRadius: getScaleSize(30),
  alignItems: 'center',
  justifyContent: 'center',
});
