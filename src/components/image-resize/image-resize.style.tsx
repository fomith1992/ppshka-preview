import { styled } from '@shared/config/styled';
import { View, Image } from 'react-native';
import { getScaleSize } from 'src/utils/dimensions';

interface ImageResizeContainerProps {
  aspectRatio: number;
  border: boolean;
}

export const ImageResizeContainer = styled(
  View,
  ({ aspectRatio, border }: ImageResizeContainerProps) =>
    ({
      justifyContent: 'center',
      alignItems: 'center',
      width: '100%',
      aspectRatio,
      borderRadius: border ? getScaleSize(12) : undefined,
      overflow: 'hidden',
    } as const),
);

interface ImageResizeProps {
  aspectRatio: number;
}

export const ImageResize = styled(Image, ({ aspectRatio }: ImageResizeProps) => ({
  height: '100%',
  aspectRatio,
}));

export const OverlayContainer = styled(View, {
  position: 'absolute',
  flex: 1,
});
