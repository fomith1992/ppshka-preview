import React from 'react';
import Animated, { Extrapolate } from 'react-native-reanimated';

import { BottomSheetBackdrop, BottomSheetBackdropProps } from '@gorhom/bottom-sheet';

export const AntiFlickerBackdrop = ({ animatedIndex, ...rest }: BottomSheetBackdropProps) => {
  const clampedAnimatedIndex = Animated.interpolateNode(animatedIndex, {
    inputRange: [0.7, 1],
    outputRange: [0, 0.5],
    extrapolateLeft: Extrapolate.CLAMP,
  });

  return <BottomSheetBackdrop animatedIndex={clampedAnimatedIndex} {...rest} />;
};
