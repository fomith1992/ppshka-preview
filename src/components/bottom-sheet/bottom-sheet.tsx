import React, { RefObject, useCallback, useEffect, useMemo, useState } from 'react';
import { BackHandler, Platform, StyleSheet, View } from 'react-native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';

import BottomSheet, { BottomSheetView } from '@gorhom/bottom-sheet';

import { AntiFlickerBackdrop } from './antiflicker-backdrop';
import { OPEN_ANIMATION_DURATION } from './constants';
import { CustomHandle } from './handle';
import { theme } from '@shared/config/theme';

interface BottomSheetGorhomProps {
  sheetRef: RefObject<BottomSheet>;
  children: React.ReactNode;
}

export const BottomSheetGorhom = (props: BottomSheetGorhomProps) => {
  const [contentHeight, setContentHeight] = useState(0);

  const { bottom: safeBottomArea } = useSafeAreaInsets();

  const snapPoints = useMemo(() => [-400, contentHeight], [contentHeight]);

  const [curIndex, setIndex] = useState(0);

  const isIos = Platform.OS === 'ios';

  const handleOnLayout = useCallback(
    ({
      nativeEvent: {
        layout: { height },
      },
    }) => {
      setContentHeight(height);
    },
    [],
  );

  const contentContainerStyle = useMemo(
    () => ({
      paddingBottom: isIos ? 0 : safeBottomArea,
      backgroundColor: theme.colors.gray3,
    }),
    [safeBottomArea, isIos],
  );

  useEffect(() => {
    const nativeBackPress = () => {
      props.sheetRef.current?.close();
      return curIndex === 1;
    };

    BackHandler.addEventListener('hardwareBackPress', nativeBackPress);

    return () => {
      BackHandler.removeEventListener('hardwareBackPress', nativeBackPress);
    };
  }, [props.sheetRef, curIndex]);

  return (
    <View style={StyleSheet.absoluteFill} pointerEvents="box-none">
      <BottomSheet
        ref={props.sheetRef}
        snapPoints={snapPoints}
        handleComponent={CustomHandle}
        backgroundComponent={null}
        backdropComponent={AntiFlickerBackdrop}
        onAnimate={(_, toIndex) => setIndex(toIndex)}
        animationDuration={OPEN_ANIMATION_DURATION}
      >
        <BottomSheetView style={contentContainerStyle} onLayout={handleOnLayout}>
          {props.children}
        </BottomSheetView>
      </BottomSheet>
    </View>
  );
};
