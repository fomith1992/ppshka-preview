import React from 'react';

import { BottomSheetHandleProps } from '@gorhom/bottom-sheet';
import { View } from 'react-native';
import { styled } from '@shared/config/styled';
import { color } from '@shared/config/color';
import { getScaleSize } from 'src/utils/dimensions';

export const CustomHandle: React.FC<BottomSheetHandleProps> = () => {
  return (
    <Container>
      <Line />
    </Container>
  );
};

export const Container = styled(View, {
  top: 1,
  height: getScaleSize(36),
  flexShrink: 0,
  borderTopLeftRadius: getScaleSize(32),
  borderTopRightRadius: getScaleSize(32),
  backgroundColor: color('white'),
  alignItems: 'center',
  justifyContent: 'center',
  flexDirection: 'row',
});

export const Line = styled(View, {
  height: getScaleSize(4),
  width: getScaleSize(115),
  borderRadius: getScaleSize(4),
  backgroundColor: color('gray4'),
});
