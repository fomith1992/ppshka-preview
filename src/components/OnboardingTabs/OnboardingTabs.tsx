import React, { FC, ReactNode, useEffect, useMemo, useState } from 'react';
import { StyleProp, View, ViewStyle } from 'react-native';

import { styles } from './styles';

interface IProps {
  activeValue: number;
  totalValues: number;
  style?: StyleProp<ViewStyle>;
}

const OnboardingTabs: FC<IProps> = ({ activeValue, totalValues, style }) => {
  const [tabs, setTabs] = useState<ReactNode[]>([]);
  const containerStyle = useMemo(() => [styles.container, style], [style]);

  useEffect(() => {
    const tabsToCalc: ReactNode[] = [];
    for (let i = 0; i < totalValues; i++) {
      const style = [styles.tab, i === activeValue - 1 ? styles.activeTab : null];
      tabsToCalc.push(<View key={i} style={style} />);
    }

    setTabs(tabsToCalc);
  }, [totalValues, activeValue]);

  return <View style={containerStyle}>{tabs.map((tab) => tab)}</View>;
};

export default OnboardingTabs;
