import { StyleSheet } from 'react-native';

import { purpleColor } from 'src/commonStyles/colors';
import { getScaleSize } from 'src/utils/dimensions';

export const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  tab: {
    flex: 1,
    height: getScaleSize(3),
    borderRadius: getScaleSize(5),
    backgroundColor: '#F0F0F0',
    marginHorizontal: getScaleSize(8),
  },
  activeTab: {
    backgroundColor: purpleColor,
  },
});
