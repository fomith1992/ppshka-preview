import { useNavigation } from '@react-navigation/core';
import { Modal } from '@shared/ui/modal';
import { format, add } from 'date-fns';
import { ru } from 'date-fns/locale';
import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import { AppState } from 'src/store';
import { getScaleSize } from 'src/utils/dimensions';
import { isEqualDay } from 'src/utils/date';
import Button from '../Button';
import Icon from '../Icon';
import {
  CalendarScroll,
  DayItemContainer,
  DateText,
  DayContainer,
  DayOfWeek,
  CalendarContainer,
  OverflowContainer,
  ModalBlock,
  ButtonBack,
  ButtonBackText,
  ButtonsContainer,
  PopUpTitle,
  PopUpDescription,
  GradientContainer,
} from './calendar.style';
import { gradientComponentsColors } from '@shared/ui/gradient-layout/constant';
import { theme } from '@shared/config/theme';

const dataLength = Array.from({ length: 10 }, (_, idx) => add(Date.now(), { days: idx }));

interface DayItemProps {
  active?: boolean;
  item: Date;
  setSelectedDate: (item: Date) => void;
}

const DayItem = ({ item, active = false, setSelectedDate }: DayItemProps): React.ReactElement => {
  const dayOfWeek = format(item, 'cccccc', {
    locale: ru,
  })
    .split('')
    .map((item, idx) => (idx === 0 ? item.toLocaleUpperCase() : item))
    .join('');
  const dateToday = format(item, 'd', {
    locale: ru,
  });
  return (
    <DayItemContainer>
      <DayOfWeek>{dayOfWeek}</DayOfWeek>
      <DayContainer onPress={() => setSelectedDate(item)} active={active}>
        <DateText active={false}>{dateToday}</DateText>
      </DayContainer>
    </DayItemContainer>
  );
};

export const DayItemGradient = ({
  item,
  active = false,
  setSelectedDate,
}: DayItemProps): React.ReactElement => {
  const dayOfWeek = format(item, 'cccccc', {
    locale: ru,
  })
    .split('')
    .map((item, idx) => (idx === 0 ? item.toLocaleUpperCase() : item))
    .join('');
  const dateToday = format(item, 'd', {
    locale: ru,
  });
  return (
    <DayItemContainer>
      <DayOfWeek>{dayOfWeek}</DayOfWeek>

      <DayContainer onPress={() => setSelectedDate(item)} active={active}>
        <GradientContainer
          colors={active ? gradientComponentsColors : [theme.colors.white, theme.colors.white]}
        >
          <DateText active={active}>{dateToday}</DateText>
        </GradientContainer>
      </DayContainer>
    </DayItemContainer>
  );
};

interface CalendarProps {
  selectedDate: Date;
  setSelectedDate: (date: Date) => void;
  gradient?: boolean;
}

interface AvailableInPropopUpProps {
  popUpVisible: boolean;
  setPopUpVisible: (popUpVisible: boolean) => void;
}

export const AvailableInPropopUp = ({
  popUpVisible,
  setPopUpVisible,
}: AvailableInPropopUpProps) => {
  const navigation = useNavigation();
  return (
    <Modal visible={popUpVisible}>
      <OverflowContainer>
        <ModalBlock>
          <PopUpTitle>Доступно в Pro</PopUpTitle>
          <PopUpDescription>
            Этот функционал доступен только при подписке на один из наших тарфиных планов
          </PopUpDescription>
          <ButtonsContainer>
            <Button
              uppercase
              content="Выбрать тариф"
              onPress={() => {
                setPopUpVisible(false);
                navigation.navigate('Promo');
              }}
            />
            <ButtonBack onPress={() => setPopUpVisible(false)}>
              <Icon name="arrowBack" size={12} color="primary" />
              <ButtonBackText>Назад</ButtonBackText>
            </ButtonBack>
          </ButtonsContainer>
        </ModalBlock>
      </OverflowContainer>
    </Modal>
  );
};

export const Calendar = ({
  selectedDate,
  setSelectedDate,
  gradient = false,
}: CalendarProps): React.ReactElement => {
  const user = useSelector((state: AppState) => state.session.user);
  const [popUpVisible, setPopUpVisible] = useState(false);

  const setDate = (date: Date) => {
    if (!user.is_pro && date > add(Date.now(), { days: 1 })) {
      setPopUpVisible(true);
    } else {
      setSelectedDate(date);
    }
  };

  return (
    <CalendarContainer>
      <CalendarScroll
        contentContainerStyle={{ paddingHorizontal: getScaleSize(6) }}
        horizontal
        showsHorizontalScrollIndicator={false}
      >
        {dataLength.map((item) =>
          gradient ? (
            <DayItemGradient
              key={item.toString()}
              active={isEqualDay(selectedDate, item)}
              item={item}
              setSelectedDate={setDate}
            />
          ) : (
            <DayItem
              key={item.toString()}
              active={isEqualDay(selectedDate, item)}
              item={item}
              setSelectedDate={setDate}
            />
          ),
        )}
      </CalendarScroll>
      <AvailableInPropopUp popUpVisible={popUpVisible} setPopUpVisible={setPopUpVisible} />
    </CalendarContainer>
  );
};
