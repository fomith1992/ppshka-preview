import { color } from '@shared/config/color';
import { styled } from '@shared/config/styled';
import { font } from '@shared/config/text';
import { theme } from '@shared/config/theme';
import { container } from '@shared/config/view';
import { blockShadow } from '@shared/ui/block-shadow/block-shadow';
import { TouchableOpacity } from 'react-native';
import { ScrollView, Text, View } from 'react-native';
import { font12m } from 'src/commonStyles/fonts';
import { getScaleSize, screenWidth, windowWidth } from 'src/utils/dimensions';
import LinearGradient from 'react-native-linear-gradient';

export const CalendarContainer = styled(View, {
  height: (windowWidth - getScaleSize(12)) / 7 + 2, // 2 - borderWidth
});

export const CalendarScroll = ScrollView;

export const DayContainer = styled(
  TouchableOpacity,
  (props: { active: boolean }) =>
    ({
      justifyContent: 'center',
      alignItems: 'center',
      width: (windowWidth - getScaleSize(12)) / 7 - getScaleSize(18),
      height: (windowWidth - getScaleSize(12)) / 7 - getScaleSize(18),
      marginHorizontal: getScaleSize(9),
      borderRadius: getScaleSize(8),
      borderWidth: props.active ? undefined : 1,
      borderColor: !props.active ? color('gray4') : undefined,
      backgroundColor: props.active ? color('white') : 'rgba(255, 255, 255, 0.5)',
      overflow: 'hidden',
    } as const),
);

export const DayItemContainer = styled(View, {
  alignItems: 'center',
});

export const GradientContainer = styled(LinearGradient, {
  flex: 1,
  position: 'absolute',
  top: 0,
  bottom: 0,
  left: 0,
  right: 0,
  justifyContent: 'center',
  alignItems: 'center',
});

export const DayOfWeek = styled(Text, {
  ...font12m,
  lineHeight: getScaleSize(18),
  marginBottom: getScaleSize(2),
  color: color('gray6'),
});

export const DateText = styled(
  Text,
  (props: { active: boolean }) =>
    ({
      ...font12m,
      lineHeight: getScaleSize(18),
      marginBottom: getScaleSize(2),
      color: color(props.active ? 'white' : 'gray9'),
    } as const),
);

export const ButtonBackText = styled(Text, {
  ...font({ type: 'text3' }),
  color: color('primary'),
  marginLeft: getScaleSize(8),
});

export const PopUpTitle = styled(Text, {
  ...font({ type: 'h2' }),
  color: color('primary'),
});

export const PopUpDescription = styled(Text, {
  marginTop: getScaleSize(8),
  ...font({ type: 'text2' }),
  color: color('gray10'),
  textAlign: 'center',
});

export const OverflowContainer = styled(View, {
  flex: 1,
  alignItems: 'center',
  justifyContent: 'center',
  backgroundColor: color('grayOpacity50'),
});

export const ButtonsContainer = styled(View, {
  marginTop: getScaleSize(24),
});

export const ButtonBack = styled(TouchableOpacity, {
  paddingVertical: getScaleSize(18),
  flexDirection: 'row',
  alignItems: 'center',
  justifyContent: 'center',
});

export const ModalBlock = styled(View, {
  ...container(),
  ...container('padding'),
  alignItems: 'center',
  width: screenWidth - theme.layout.margin * 2,
  paddingVertical: getScaleSize(20),
  borderRadius: 12,
  backgroundColor: color('white'),
  ...blockShadow(),
});
