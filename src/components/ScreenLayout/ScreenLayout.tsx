import React, { FC } from 'react';
import { StatusBar, StyleProp, View, ViewStyle } from 'react-native';
import { Edge, SafeAreaView } from 'react-native-safe-area-context';

import { styles } from './styles';

interface IProps {
  safeArea?: boolean;
  wrapperStyle?: StyleProp<ViewStyle>;
  withBottomTabs?: boolean;
  edges?: ReadonlyArray<Edge>;
  style?: StyleProp<ViewStyle>;
}

const ScreenLayout: FC<IProps> = ({ safeArea = true, wrapperStyle, edges, style, children }) => {
  const contentContainerStyle = [{ flexGrow: 1 }, style];

  return (
    <View style={[{ flexGrow: 1 }, wrapperStyle]}>
      <StatusBar barStyle={'dark-content'} />
      {safeArea ? (
        <SafeAreaView edges={edges} style={styles.flex1}>
          <View style={contentContainerStyle}>{children}</View>
        </SafeAreaView>
      ) : (
        <View style={contentContainerStyle}>{children}</View>
      )}
    </View>
  );
};

export default ScreenLayout;
