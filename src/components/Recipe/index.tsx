import React from 'react';
import { StyleProp, Text, TouchableOpacity, View, ViewStyle } from 'react-native';

import { useNavigation } from '@react-navigation/native';

import Icon from 'src/components/Icon';
import PreLoadedImage from 'src/components/PreLoadedImage';

import { styles } from './styles';

interface RecipeProps {
  data: unknown;
  photo: string;
  style?: StyleProp<ViewStyle>;
}

export const Recipe = ({ photo, style = {} }: RecipeProps) => {
  const navigation = useNavigation();

  return (
    <TouchableOpacity
      style={[styles.container, style]}
      onPress={() => navigation.navigate('RecipeDetails')}
    >
      <PreLoadedImage source={{ uri: photo }} style={styles.image} />
      <View style={styles.containerInfo}>
        <View style={styles.containerTime}>
          <Icon name="clock" />
          <Text style={styles.textTime}>25 мин</Text>
        </View>
        <Text style={styles.textTitle}>Кокосовое балманже с ягодным соусом</Text>
        <Text style={styles.textSubTitle}>Размер порции: 250г</Text>
        <View style={styles.containerAdditionalInfoWithMargin}>
          <View style={styles.containerFirstItem}>
            <Text style={styles.textInfo}>15 г.</Text>
            <Text style={styles.textNameInfo}>Белки</Text>
          </View>
          <View>
            <Text style={styles.textInfo}>45 г.</Text>
            <Text style={styles.textNameInfo}>Углеводы</Text>
          </View>
        </View>
        <View style={styles.containerAdditionalInfo}>
          <View style={styles.containerFirstItem}>
            <Text style={styles.textInfo}>22 г.</Text>
            <Text style={styles.textNameInfo}>Жиры</Text>
          </View>
          <View>
            <Text style={styles.textInfo}>220 г.</Text>
            <Text style={styles.textNameInfo}>Ккал</Text>
          </View>
        </View>
      </View>
    </TouchableOpacity>
  );
};
