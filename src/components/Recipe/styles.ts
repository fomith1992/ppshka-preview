import { StyleSheet } from 'react-native';

import { font12r, font14m } from 'src/commonStyles/fonts';

import { purpleColor } from '../../commonStyles/colors';
import { font12m, font16m } from '../../commonStyles/fonts';
import { getScaleSize } from '../../utils/dimensions';

export const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    backgroundColor: 'white',
    borderRadius: getScaleSize(12),
  },
  containerInfo: {
    paddingTop: getScaleSize(20),
    paddingBottom: getScaleSize(25),
    paddingHorizontal: getScaleSize(20),
    flex: 1,
    zIndex: 1,
  },
  image: {
    width: getScaleSize(148),
    borderTopLeftRadius: getScaleSize(12),
    borderBottomLeftRadius: getScaleSize(12),
  },
  containerTime: {
    flexDirection: 'row',
    marginBottom: getScaleSize(15),
    alignItems: 'center',
  },
  textTime: {
    ...font12m,
    color: purpleColor,
    marginLeft: getScaleSize(5),
  },
  textTitle: {
    ...font16m,
    color: '#262626',
    marginBottom: getScaleSize(2),
    flex: 1,
    flexWrap: 'wrap',
  },
  textSubTitle: {
    ...font12r,
    color: '#8C8C8C',
    marginBottom: getScaleSize(20),
  },
  containerAdditionalInfo: {
    flexDirection: 'row',
  },
  containerAdditionalInfoWithMargin: {
    flexDirection: 'row',
    marginBottom: getScaleSize(23),
  },
  textInfo: {
    ...font14m,
    color: '#262626',
    marginBottom: getScaleSize(2),
  },
  textNameInfo: {
    ...font12r,
    color: '#8C8C8C',
  },
  containerFirstItem: {
    marginRight: getScaleSize(20),
  },
});
