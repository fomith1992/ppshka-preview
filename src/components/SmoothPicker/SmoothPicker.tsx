import React, { useMemo, useRef } from 'react';
import {
  Animated,
  FlatList,
  NativeScrollEvent,
  NativeSyntheticEvent,
  Text,
  View,
} from 'react-native';

import { windowWidth } from 'src/utils/dimensions';

import { styles } from './styles';

const weight = Array.from({ length: 240 }, (_, i) => String(i + 10));
const height = Array.from({ length: 140 }, (_, i) => String(i + 90));

const itemWidth = windowWidth / 2 - 20;
const containerPadding = windowWidth / 2 - itemWidth / 2;

interface SmoothPickerProps {
  onSetCurrentWeight: (currentValue: string) => void;
  type: 'weight' | 'height';
}

const SmoothPicker = ({ onSetCurrentWeight, type }: SmoothPickerProps) => {
  const scrollX = useRef(new Animated.Value(1)).current;
  const flatListRef = useRef<FlatList>(null);

  const numbers = type === 'height' ? height : weight;

  // Intended to determine if the user has started scrolling
  const isDraggedRef = useRef<boolean>(false);

  const onScrollStops = (event: NativeSyntheticEvent<NativeScrollEvent>) => {
    // Checking if the user initiated scroll prevents looping as scrollToOffset also triggers onScrollStops
    if (isDraggedRef.current) {
      const position = event.nativeEvent.contentOffset.x;
      const count = Math.floor(position / itemWidth);
      const whole = count * itemWidth;
      const add = position - whole > itemWidth / 2 ? 1 : 0;
      flatListRef.current?.scrollToOffset({
        offset: (count + add) * itemWidth || 1,
        animated: true,
      });
      isDraggedRef.current = false;
    }
  };

  const renderItem = useMemo(
    () =>
      ({ item, index }: { item: string; index: number }) => {
        // const onPress = () => {
        //   // onItemPress(item.id);
        // };
        const inputRange =
          index === 0
            ? [0, 0, itemWidth / 2, itemWidth]
            : [
                itemWidth / 2 + itemWidth * (index - 1),
                itemWidth + itemWidth * (index - 1),
                (itemWidth / 2) * 3 + itemWidth * (index - 1),
                itemWidth * 2 + itemWidth * (index - 1),
              ];

        const itemCoef = scrollX.interpolate({
          inputRange,
          outputRange: [0.5, 1, 0.5, 0.5],
          extrapolate: 'clamp',
        });
        if ((itemCoef as any).__getValue() > 0.7) {
          onSetCurrentWeight(item);
        }
        return (
          <Animated.View
            style={[
              styles.item,
              {
                width: itemWidth,
                transform: [
                  {
                    scale: itemCoef,
                  },
                ],
              },
            ]}
          >
            <Text style={styles.textChoice}>
              {item} {type === 'height' ? 'см' : 'кг'}
            </Text>
          </Animated.View>
        );
      },
    [scrollX], // eslint-disable-line react-hooks/exhaustive-deps
  );

  const onMomentumScrollEnd = (event: NativeSyntheticEvent<NativeScrollEvent>) => {
    onScrollStops(event);
  };

  const onScrollEndDrag = (event: NativeSyntheticEvent<NativeScrollEvent>) => {
    if (event.nativeEvent.velocity?.x === 0) {
      onScrollStops(event);
    }
  };

  const onScrollBeginDrag = () => {
    isDraggedRef.current = true;
  };
  // Value should never be 0 to prevent wrong interpolation when the scale of the first element can become 0
  const onScroll = (event: NativeSyntheticEvent<NativeScrollEvent>) =>
    scrollX.setValue(event.nativeEvent.contentOffset.x || 1);

  return (
    <View style={styles.zoomingScroll}>
      <Animated.FlatList
        ref={flatListRef}
        showsHorizontalScrollIndicator={false}
        contentContainerStyle={{ paddingHorizontal: containerPadding }}
        bounces={false}
        overScrollMode={'never'}
        data={numbers}
        renderItem={renderItem}
        onMomentumScrollEnd={onMomentumScrollEnd}
        onScrollEndDrag={onScrollEndDrag}
        onScrollBeginDrag={onScrollBeginDrag}
        horizontal
        keyExtractor={(_, idx) => idx.toString()}
        onScroll={onScroll}
        initialNumToRender={100}
        contentOffset={{ x: itemWidth * (type === 'height' ? 80 : 60), y: 0 }}
      />
      <View style={styles.containerDashes}>
        <View style={styles.smallDash} />
        <View style={styles.dash} />
        <View style={styles.smallDash} />
        <View style={styles.smallDash} />
        <View style={styles.smallDash} />
        <View style={styles.smallDash} />
        <View style={styles.mainDash} />
        <View style={styles.smallDash} />
        <View style={styles.smallDash} />
        <View style={styles.smallDash} />
        <View style={styles.smallDash} />
        <View style={styles.dash} />
        <View style={styles.smallDash} />
      </View>
    </View>
  );
};

export default SmoothPicker;
