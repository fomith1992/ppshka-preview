import { StyleSheet } from 'react-native';

import { purpleColor } from 'src/commonStyles/colors';
import { font50r } from 'src/commonStyles/fonts';
import { getScaleSize } from 'src/utils/dimensions';

export const styles = StyleSheet.create({
  zoomingScroll: {
    width: '100%',
    height: 201,
    alignItems: 'center',
    justifyContent: 'center',

    // borderWidth: 1,
    // borderColor: 'pink',
  },
  smallDash: {
    width: getScaleSize(2),
    height: getScaleSize(15),
    backgroundColor: '#D9D9D9',
  },
  dash: {
    width: getScaleSize(2),
    height: getScaleSize(30),
    backgroundColor: '#D9D9D9',
  },
  containerDashes: {
    position: 'absolute',
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'space-between',
    width: '100%',
    zIndex: -1,
  },
  textChoice: {
    color: '#434343',
    ...font50r,
  },
  mainDash: {
    width: getScaleSize(3),
    height: getScaleSize(67),
    backgroundColor: purpleColor,
  },
  item: {
    width: 140,
    height: 201,
    alignItems: 'center',
    // justifyContent: 'center',
    // borderWidth: 1,
    // borderColor: 'red',
  },
  itemTextContainer: {
    position: 'absolute',
    bottom: 0,
  },
  itemText: {
    lineHeight: 32,
    color: 'red',
  },
});
