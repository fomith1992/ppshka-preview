import { StyleSheet } from 'react-native';

import { font14m, font14r } from 'src/commonStyles/fonts';
import { getScaleSize } from 'src/utils/dimensions';

export const styles = StyleSheet.create({
  container: {},
  containerContent: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  containerItem: {
    paddingBottom: getScaleSize(10),
    marginHorizontal: getScaleSize(15),
  },
  containerSelectedItem: {
    borderBottomWidth: getScaleSize(1.5),
    borderColor: '#fff',
  },
  textMenu: {
    ...font14r,
    color: '#fff',
  },
  textSelectedItem: {
    ...font14m,
    color: '#fff',
  },
});
