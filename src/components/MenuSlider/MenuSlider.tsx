import React from 'react';
import {
  FlatList,
  LayoutAnimation,
  StyleProp,
  Text,
  TouchableOpacity,
  View,
  ViewStyle,
} from 'react-native';
import { useSelector } from 'react-redux';
import { AppState } from 'src/store';
import { hitSlopParams } from 'src/utils/hit-slop-params';

import { styles } from './styles';

interface IProps {
  style?: StyleProp<ViewStyle>;
  selectedValue?: number;
  setSelectedValue: (value?: number) => void;
}

const MenuSlider = ({ style, selectedValue, setSelectedValue }: IProps): React.ReactElement => {
  const categories = useSelector((state: AppState) => state.session.categories);

  return (
    <View style={[styles.container, style]}>
      <View style={styles.containerContent}>
        <FlatList
          data={categories}
          horizontal
          keyExtractor={(_, idx) => idx.toString()}
          showsHorizontalScrollIndicator={false}
          renderItem={({ item }) => (
            <TouchableOpacity
              hitSlop={hitSlopParams('XL')}
              onPress={() => {
                setSelectedValue(item.id);
                LayoutAnimation.configureNext({
                  ...LayoutAnimation.Presets.easeInEaseOut,
                  duration: 100,
                });
              }}
              style={[
                styles.containerItem,
                selectedValue === item.id ? styles.containerSelectedItem : null,
              ]}
            >
              <Text
                style={[
                  styles.textMenu,
                  selectedValue === item.id ? styles.textSelectedItem : null,
                ]}
              >
                {item.name}
              </Text>
            </TouchableOpacity>
          )}
        />
      </View>
    </View>
  );
};

export default MenuSlider;
