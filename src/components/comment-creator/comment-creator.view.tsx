import React, { useState } from 'react';
import { Text } from 'react-native';

import { theme } from '../../shared/config/theme';
import {
  Avatar,
  CommentCreatorContainer,
  CommentInput,
  InputContainer,
  SendCommentButton,
  SendImg,
} from './comment-creator.style';

import * as yup from 'yup';

const mockPhoto = 'https://www.gastronom.ru/binfiles/images/20170404/b826aef2.jpg';

const commentMaxLength = 1000;

const formSchema = yup
  .object({
    commentText: yup.string().max(commentMaxLength).required(),
  })
  .required();

export type AddCommentFormData = yup.InferType<typeof formSchema>;

export interface CommentTextInputProps {
  value: string | undefined;
  onChange: (value: string) => void;
  placeholder?: string;
}

const minLinesNumber = 1;
const maxLinesNumber = 5;

const CommentTextInput = ({ value, onChange, placeholder }: CommentTextInputProps) => {
  const butifyText = value?.split(/(\s)/g).map((item, i) => {
    if (/[A-zA-яё]+/gi.test(item) && i === 0) {
      return (
        <Text key={i} style={{ color: theme.colors.primary }}>
          {item}
        </Text>
      );
    }

    return item;
  });

  return (
    <CommentInput
      maxLength={commentMaxLength}
      onChangeText={onChange}
      placeholder={placeholder}
      multiline
      minLinesNumber={minLinesNumber}
      maxLinesNumber={maxLinesNumber}
    >
      <Text>{butifyText}</Text>
    </CommentInput>
  );
};

function AddCommentButton(props: { valid: boolean; onSubmit: () => void }): React.ReactElement {
  const { valid, onSubmit } = props;
  return (
    <SendCommentButton valid={valid} disabled={!valid} onPress={onSubmit}>
      <SendImg />
    </SendCommentButton>
  );
}

export interface CommentCreatorViewProps {
  onAddComment: () => void;
}

export interface ReplyInfoProps {
  author: {
    firstName: string;
  };
}

export function CommentCreatorView({ onAddComment }: CommentCreatorViewProps): React.ReactElement {
  const [commentText, setCommentText] = useState<string>();
  return (
    <CommentCreatorContainer>
      <InputContainer>
        <Avatar source={{ uri: mockPhoto }} />
        <CommentTextInput
          value={commentText}
          onChange={setCommentText}
          placeholder="Добавьте комментарий..."
        />
        <AddCommentButton onSubmit={onAddComment} valid={!!commentText && commentText.length > 0} />
      </InputContainer>
    </CommentCreatorContainer>
  );
}
