import { color } from '@shared/config/color';
import { LazyStyleProp, styled } from '@shared/config/styled';
import { ArrowRightIcon18 } from '@shared/ui/icons/arrow-right.icon-18';
import { TouchableOpacity, TextInput, View } from 'react-native';
import { font12r } from 'src/commonStyles/fonts';
import { getScaleSize } from 'src/utils/dimensions';
import PreLoadedImage from '../PreLoadedImage';

function linesHeight(linesNumber: number | undefined): LazyStyleProp<number | undefined> {
  return linesNumber == null
    ? undefined
    : getScaleSize(
        10 * 2 + // padding
          2 + // border
          linesNumber * 17, // lines
      );
}

export const CommentInput = styled(
  TextInput,
  (props: { minLinesNumber: number; maxLinesNumber: number }) =>
    ({
      flex: 1,
      ...font12r,
      lineHeight: getScaleSize(18),
      /* height: getScaleSize(18), */
      borderRadius: 8,
      borderWidth: 1,
      borderColor: color('gray5'),
      paddingVertical: 0,
      paddingHorizontal: getScaleSize(12),
      marginHorizontal: getScaleSize(12),
      minHeight: linesHeight(props.minLinesNumber),
      maxHeight: linesHeight(props.maxLinesNumber),
      backgroundColor: color('white'),
    } as const),
);

export const CommentCreatorContainer = styled(View, {
  paddingHorizontal: getScaleSize(15),
  backgroundColor: color('white'),
});

export const InputContainer = styled(View, {
  flexDirection: 'row',
  alignItems: 'flex-end',
  backgroundColor: color('white'),
});

export const SendImg = styled(ArrowRightIcon18, {
  color: color('gray6'),
});

export const SendCommentButton = styled(
  TouchableOpacity,
  (props: { valid: boolean }) =>
    ({
      height: getScaleSize(40),
      width: getScaleSize(40),
      justifyContent: 'center',
      alignItems: 'center',
      borderWidth: 1,
      borderRadius: getScaleSize(8),
      borderColor: props.valid ? color('primary') : color('gray5'),
      backgroundColor: color(props.valid ? 'primary' : 'white'),
    } as const),
);

export const Avatar = styled(PreLoadedImage, {
  height: getScaleSize(40),
  width: getScaleSize(40),
  borderRadius: getScaleSize(8),
});
