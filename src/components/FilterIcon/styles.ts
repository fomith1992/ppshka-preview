import { StyleSheet } from 'react-native';

import { getScaleSize } from '../../utils/dimensions';

export const styles = StyleSheet.create({
  container: {
    borderRadius: getScaleSize(5),
    backgroundColor: '#F5F5F5',
    width: getScaleSize(34),
    height: getScaleSize(34),
    justifyContent: 'center',
    alignItems: 'center',
  },
});
