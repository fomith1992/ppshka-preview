import React, { FC } from 'react';
import { StyleProp, ViewStyle } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';

import Icon from 'src/components/Icon';

import { styles } from './styles';

interface IProps {
  style?: StyleProp<ViewStyle>;
}

const FilterIcon: FC<IProps> = ({ style = {} }) => {
  return (
    <TouchableOpacity style={[styles.container, style]}>
      <Icon name="filter" />
    </TouchableOpacity>
  );
};

export default FilterIcon;
