import { color } from '@shared/config/color';
import { styled } from '@shared/config/styled';
import { font } from '@shared/config/text';
import { TouchableOpacity, View, Text } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

import { getScaleSize } from 'src/utils/dimensions';

import PreLoadedImage from '../PreLoadedImage';

export const Container = styled(TouchableOpacity, {
  width: getScaleSize(120),
  height: getScaleSize(136),
  borderRadius: getScaleSize(15),
  overflow: 'hidden',
});

export const Background = styled(PreLoadedImage, {
  flex: 1,
});

export const Layout = styled(LinearGradient, {
  position: 'absolute',
  width: '100%',
  height: '100%',
});

export const ContentContainer = styled(View, {
  padding: getScaleSize(8),
  paddingBottom: getScaleSize(10),
  justifyContent: 'space-between',
  flex: 1,
});

export const TitleContainer = styled(View, {
  flexDirection: 'row',
  justifyContent: 'space-between',
});

export const LabelGradientContainer = styled(LinearGradient, {
  flexDirection: 'row',
  alignItems: 'center',
  padding: 5,
  borderRadius: getScaleSize(7),
});

export const ProLabelText = styled(Text, {
  ...font({ type: 'text5' }),
  color: color('white'),
});

export const LabelContainer = styled(View, {
  flexDirection: 'row',
  alignItems: 'center',
  padding: 5,
  backgroundColor: '#fff',
  borderRadius: getScaleSize(7),
});

export const NewLabelText = styled(Text, {
  ...font({ type: 'text5' }),
  color: color('secondary'),
});

export const DateText = styled(Text, {
  ...font({ type: 'text5' }),
  color: color('gray10'),
  marginLeft: getScaleSize(5),
});

export const TitleText = styled(Text, {
  ...font({ type: 'text3' }),
  color: color('white'),
});
