import { useNavigation } from '@react-navigation/native';
import React from 'react';
import { useSelector } from 'react-redux';
import useAsync from 'react-use/lib/useAsync';
import { AppState } from 'src/store';
import { getCacheContent } from 'src/utils/cache-content';
import { WorkoutCardSmallView } from './workout-small.view';

interface WorkoutCardDataProps {
  id: number;
  calendar_id: number;
  dateStart: string;
  name: string;
  duration: number;
  photoUri: string;
  isAvailableOnFree: boolean;
  isToday: boolean;
}

interface WorkoutCardProps {
  data: WorkoutCardDataProps;
}

export function WorkoutSmallCard({ data }: WorkoutCardProps): React.ReactElement {
  const navigation = useNavigation();
  const user = useSelector((state: AppState) => state.session.user);
  const is_pro = user.is_pro;

  const photo = useAsync(async () => {
    return await getCacheContent({ url: data.photoUri, format: 'png' });
  }, [data]);

  return (
    <WorkoutCardSmallView
      {...data}
      photoUri={photo.value}
      photoLoading={photo.loading}
      disabled={!data.isToday || (!data.isAvailableOnFree && !is_pro)}
      availableOnSubscribe={data.isAvailableOnFree || is_pro}
      onPress={() => {
        if (!data.isAvailableOnFree && !is_pro) {
          return navigation.navigate('Promo', {});
        }
        if (data.isToday) {
          return navigation.navigate('WorkoutDetails', {
            id: data.id,
            dateStart: data.dateStart,
            calendar_id: data.calendar_id,
          });
        }
      }}
    />
  );
}
