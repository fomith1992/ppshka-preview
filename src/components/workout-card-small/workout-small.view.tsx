import { gradientLayoutColors } from '@shared/ui/gradient-layout/constant';
import React from 'react';

import Icon from 'src/components/Icon';
import { LoadingIndicator } from '../ui/Loader/loading-indicator';
// import { format } from 'date-fns';
// import ruLocale from 'date-fns/locale/ru';

import {
  Background,
  Container,
  ContentContainer,
  LabelContainer,
  DateText,
  Layout,
  TitleContainer,
  TitleText,
  LabelGradientContainer,
  ProLabelText,
} from './workout-small.style';

export interface WorkoutSmallViewProps {
  name: string;
  duration: number;
  photoUri?: string;
  onPress?: () => void;
  availableOnSubscribe: boolean;
  photoLoading: boolean;
  disabled?: boolean;
}

export function WorkoutCardSmallView({
  onPress,
  name,
  duration,
  photoUri,
  photoLoading,
  availableOnSubscribe = true,
  disabled = false,
}: WorkoutSmallViewProps): React.ReactElement {
  return (
    <Container onPress={onPress}>
      <LoadingIndicator visible={photoLoading} />
      {!photoLoading && <Background source={{ uri: photoUri }} />}
      <Layout colors={disabled ? ['#d9d9d9cc', '#d9d9d9cc'] : ['rgba(0, 0, 0, 0)', '#000000']}>
        <ContentContainer>
          <TitleContainer>
            <LabelContainer>
              <Icon name="clock" size={10} color="gray10" />
              <DateText>{duration} мин</DateText>
              {/* <DateText>{format(duration, 'dd MMM', { locale: ruLocale })}</DateText> */}
            </LabelContainer>
            {availableOnSubscribe ? null : (
              <LabelGradientContainer colors={gradientLayoutColors}>
                <ProLabelText>PRO</ProLabelText>
              </LabelGradientContainer>
            )}
          </TitleContainer>
          <TitleText>{availableOnSubscribe ? name : 'Доступно в PRO'}</TitleText>
        </ContentContainer>
      </Layout>
    </Container>
  );
}
