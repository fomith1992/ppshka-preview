import { TabContainer, TabTitle, TabDescription } from './tabs.style';
import React from 'react';

interface TabInfoProps {
  title: string;
  description: string;
}

export function TabInfo({ title, description }: TabInfoProps): React.ReactElement {
  return (
    <TabContainer>
      <TabTitle>{title}</TabTitle>
      <TabDescription>{description}</TabDescription>
    </TabContainer>
  );
}
