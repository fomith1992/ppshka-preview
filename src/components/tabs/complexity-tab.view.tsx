import {
  TabContainer,
  TabDescription,
  TabComplexityContainer,
  TabItemComplexity,
} from './tabs.style';
import React from 'react';

interface TabComplexityProps {
  current: number;
  total: number;
  description: string;
}

export function TabComplexity({
  current,
  total,
  description,
}: TabComplexityProps): React.ReactElement {
  return (
    <TabContainer>
      <TabComplexityContainer>
        {[...Array(total).keys()].map((value, index) => (
          <TabItemComplexity key={index} isActive={value < current} />
        ))}
      </TabComplexityContainer>
      <TabDescription>{description}</TabDescription>
    </TabContainer>
  );
}
