import { color } from '@shared/config/color';
import { styled } from '@shared/config/styled';
import { font } from '@shared/config/text';
import { Text, View } from 'react-native';

import { getScaleSize } from 'src/utils/dimensions';

export const TabsInfoContainer = styled(View, {
  flexDirection: 'row',
  justifyContent: 'space-between',
});

export const TabContainer = View;

export const TabTitle = styled(Text, {
  ...font({ type: 'text1' }),
});
export const TabDescription = styled(Text, {
  ...font({ type: 'text3', weight: 'light' }),
  color: color('gray7'),
});

export const TabComplexityContainer = styled(View, {
  marginTop: getScaleSize(6),
  flexDirection: 'row',
  marginBottom: getScaleSize(4),
});
export const TabItemComplexity = styled(
  View,
  (props: { isActive: boolean }) =>
    ({
      width: getScaleSize(3),
      height: getScaleSize(15),
      marginRight: getScaleSize(5),
      backgroundColor: props.isActive ? color('secondary') : color('gray4'),
    } as const),
);
