import React from 'react';
import { Rating } from 'react-native-ratings';
import * as Progress from 'react-native-progress';
import {
  AddFeedbackButtonContainerSmall,
  BoxContainer,
  ContainerSmall,
  DescriptionText,
  FeedbacksCountTextSmall,
  ProgressBarContainer,
  RaitingContainerSmall,
  RatingListEnumContainer,
  RatingListEnumItemContainer,
  RatingText,
  RatingTextContainer,
  StarsContainer,
  Title,
  TitleContainer,
} from './rating-card.style';
import { RatingSendButton } from '../rating-send-button/rating-send-button.container';
import { theme } from '@shared/config/theme';
import { toCaseCount } from 'src/utils/to-case-count';

interface ItemStarsRatingProps {
  number: number;
  countPercentFeedback: number;
}

export const HEIGHT_STARS_ROW = 9;

function ItemStarsRating({
  number,
  countPercentFeedback,
}: ItemStarsRatingProps): React.ReactElement {
  return (
    <RatingListEnumItemContainer>
      <StarsContainer starsCount={number}>
        <Rating
          type="custom"
          ratingCount={number}
          ratingColor={number < 3 ? theme.colors.gray6 : theme.colors.gradientColor2}
          startingValue={number}
          imageSize={HEIGHT_STARS_ROW}
          readonly={true}
          style={{
            margin: 0,
            padding: 0,
          }}
        />
      </StarsContainer>
      <ProgressBarContainer>
        <Progress.Bar
          progress={countPercentFeedback}
          height={HEIGHT_STARS_ROW - 7}
          width={null}
          color={theme.colors.gradientColor2}
          unfilledColor="rgba(244, 244, 244, 1)"
          borderWidth={0}
        />
      </ProgressBarContainer>
    </RatingListEnumItemContainer>
  );
}

interface RatingCardViewProps {
  recipeId: number;
  rating: number;
  percentFeedbackMap: ItemStarsRatingProps[];
  countFeedBack: number;
  onGetRecipeRate: () => void;
  numberOfFeedbacks: number;
}

export function RecipeRatingCardSmall({
  recipeId,
  rating,
  percentFeedbackMap,
  countFeedBack,
  onGetRecipeRate,
  numberOfFeedbacks,
}: RatingCardViewProps): React.ReactElement {
  return (
    <BoxContainer>
      <ContainerSmall>
        <TitleContainer>
          <Title>Отзывы</Title>
          <FeedbacksCountTextSmall>
            {`${numberOfFeedbacks} ${toCaseCount(
              ['отзыв', 'отзыва', 'отзывов'],
              numberOfFeedbacks,
            )}`}
          </FeedbacksCountTextSmall>
        </TitleContainer>
        <RaitingContainerSmall>
          <RatingTextContainer>
            <RatingText>{rating === 0 ? '--' : rating.toFixed(1)}</RatingText>
          </RatingTextContainer>
          <RatingListEnumContainer>
            {percentFeedbackMap.map((value) => (
              <ItemStarsRating
                key={value.number}
                number={value.number}
                countPercentFeedback={
                  countFeedBack === 0 ? 0 : value.countPercentFeedback / countFeedBack
                }
              />
            ))}
          </RatingListEnumContainer>
        </RaitingContainerSmall>
        {rating === 0 && <DescriptionText>Пока слишком мало оценок</DescriptionText>}
        <AddFeedbackButtonContainerSmall>
          <RatingSendButton onGetRecipeRate={onGetRecipeRate} recipeId={recipeId} />
        </AddFeedbackButtonContainerSmall>
      </ContainerSmall>
    </BoxContainer>
  );
}
