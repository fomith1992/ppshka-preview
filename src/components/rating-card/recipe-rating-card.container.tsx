import { useNavigation } from '@react-navigation/core';
import React, { useEffect, useState } from 'react';
import { Alert } from 'react-native';
import { useSelector } from 'react-redux';
import useAsync from 'react-use/lib/useAsync';
import useAsyncFn from 'react-use/lib/useAsyncFn';
import { feedbackAPI } from 'src/api';
import { getRecipe, TRecipe } from 'src/api/recipes';
import { AppState } from 'src/store';
import { RecipeRatingCardSmall } from './rating-card-small.view';
import { RatingCardView } from './rating-card.view';

interface RecipeRatingCardProps {
  id: number;
  image?: string;
  small?: boolean;
}

export function RecipeRatingCard({
  id,
  image,
  small = false,
}: RecipeRatingCardProps): React.ReactElement {
  const access_token = useSelector((state: AppState) => state.session.tokens.accessToken);
  const navigator = useNavigation();
  const [rate, setRate] = useState<TRecipe | null>(null);

  const [, onGetRecipeRate] = useAsyncFn(async () => {
    return getRecipe({
      access_token,
      id,
    })
      .then((rate) => setRate(rate.data.data.data))
      .catch((err) => console.log('Error getRecipe: ', err));
  }, []);

  useEffect(() => {
    onGetRecipeRate();
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  const feedbackApi = useAsync(async () => {
    return await feedbackAPI
      .recipeFeedbacksById({
        recipe_id: id,
        page: 1,
        access_token,
        pagination_limit: 10000,
      })
      .catch((e) => {
        console.log('Recipe feedbacks: ', e);
        Alert.alert('Произошла ошибка');
      });
  }, []);

  if (rate == null) return <></>;
  const feedbacksFotos =
    feedbackApi.value?.data.data.data.filter((item) => item.photo_file_path) ?? [];
  const recipeArr = Object.entries(rate.rate_result);

  if (!small) {
    return (
      <RatingCardView
        feedbacksFotos={feedbacksFotos}
        numberOfFeedbacks={feedbackApi.value?.data.data.data.length ?? 0}
        onGetRecipeRate={onGetRecipeRate}
        recipeId={id}
        onPressAllFeedbackButton={() => navigator.navigate('RecipeFeedbacks', { id, image })}
        rating={rate.rate}
        percentFeedbackMap={recipeArr.reverse().map(([key, value]) => ({
          number: parseFloat(key),
          countPercentFeedback: value,
        }))}
        countFeedBack={recipeArr.reduce((acc, [, value]) => {
          return acc + value;
        }, 0)}
      />
    );
  } else {
    return (
      <RecipeRatingCardSmall
        numberOfFeedbacks={feedbackApi.value?.data.data.data.length ?? 0}
        onGetRecipeRate={onGetRecipeRate}
        recipeId={id}
        rating={rate.rate}
        percentFeedbackMap={recipeArr.reverse().map(([key, value]) => ({
          number: parseFloat(key),
          countPercentFeedback: value,
        }))}
        countFeedBack={recipeArr.reduce((acc, [, value]) => {
          return acc + value;
        }, 0)}
      />
    );
  }
}
