import React from 'react';
import { Rating } from 'react-native-ratings';
import * as Progress from 'react-native-progress';
import {
  AddFeedbackButtonContainer,
  AllFeedbackButtonContainer,
  Container,
  DescriptionText,
  FeedbacksCountText,
  ImagesContainer,
  LinkButtonContainer,
  LinkButtonText,
  MiniImage,
  ProgressBarContainer,
  RaitingContainer,
  RatingListEnumContainer,
  RatingListEnumItemContainer,
  RatingText,
  StarsContainer,
  Title,
} from './rating-card.style';
import { RatingSendButton } from '../rating-send-button/rating-send-button.container';
import { theme } from '@shared/config/theme';
import { toCaseCount } from 'src/utils/to-case-count';
import { Feedback } from 'src/api/feedback';

interface ItemStarsRatingProps {
  number: number;
  countPercentFeedback: number;
}

export const HEIGHT_STARS_ROW = 9;

function ItemStarsRating({
  number,
  countPercentFeedback,
}: ItemStarsRatingProps): React.ReactElement {
  return (
    <RatingListEnumItemContainer>
      <StarsContainer starsCount={number}>
        <Rating
          type="custom"
          ratingCount={number}
          ratingColor={number < 3 ? theme.colors.gray6 : theme.colors.gradientColor2}
          startingValue={number}
          imageSize={HEIGHT_STARS_ROW}
          readonly={true}
          style={{
            margin: 0,
            padding: 0,
          }}
        />
      </StarsContainer>
      <ProgressBarContainer>
        <Progress.Bar
          progress={countPercentFeedback}
          height={HEIGHT_STARS_ROW - 7}
          width={null}
          color={theme.colors.gradientColor2}
          unfilledColor="rgba(244, 244, 244, 1)"
          borderWidth={0}
        />
      </ProgressBarContainer>
    </RatingListEnumItemContainer>
  );
}

interface RatingCardViewProps {
  recipeId: number;
  rating: number;
  percentFeedbackMap: ItemStarsRatingProps[];
  countFeedBack: number;
  onPressAllFeedbackButton: () => void;
  onGetRecipeRate: () => void;
  feedbacksFotos: Feedback<{ recipe_id: number }>[];
  numberOfFeedbacks: number;
}

export function RatingCardView({
  recipeId,
  rating,
  percentFeedbackMap,
  countFeedBack,
  onPressAllFeedbackButton,
  onGetRecipeRate,
  feedbacksFotos,
  numberOfFeedbacks,
}: RatingCardViewProps): React.ReactElement {
  return (
    <Container>
      <AllFeedbackButtonContainer>
        <Title>Отзывы</Title>
        <LinkButtonContainer onPress={onPressAllFeedbackButton}>
          <LinkButtonText>Читать все</LinkButtonText>
        </LinkButtonContainer>
      </AllFeedbackButtonContainer>
      <RaitingContainer>
        <RatingText>{rating === 0 ? '--' : rating.toFixed(1)}</RatingText>
        <RatingListEnumContainer>
          {percentFeedbackMap.map((value) => (
            <ItemStarsRating
              key={value.number}
              number={value.number}
              countPercentFeedback={
                countFeedBack === 0 ? 0 : value.countPercentFeedback / countFeedBack
              }
            />
          ))}
        </RatingListEnumContainer>
      </RaitingContainer>
      {rating === 0 && <DescriptionText>Пока слишком мало оценок</DescriptionText>}
      <FeedbacksCountText>
        {`${numberOfFeedbacks} ${toCaseCount(['отзыв', 'отзыва', 'отзывов'], numberOfFeedbacks)}`}
      </FeedbacksCountText>
      <ImagesContainer
        style={{ justifyContent: feedbacksFotos.length > 2 ? 'space-between' : 'flex-start' }}
      >
        {feedbacksFotos.map(
          (item, idx) =>
            idx < 3 && (
              <MiniImage key={item.photo_file_path} source={{ uri: item.photo_file_path }} />
            ),
        )}
      </ImagesContainer>
      <AddFeedbackButtonContainer>
        <RatingSendButton onGetRecipeRate={onGetRecipeRate} recipeId={recipeId} />
      </AddFeedbackButtonContainer>
    </Container>
  );
}
