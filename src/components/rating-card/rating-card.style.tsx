import { color } from '@shared/config/color';
import { styled } from '@shared/config/styled';
import { font } from '@shared/config/text';
import { container } from '@shared/config/view';
import { Text, TouchableOpacity, TouchableWithoutFeedback, View } from 'react-native';
import FastImage from 'react-native-fast-image';

import { getScaleSize, screenWidth } from 'src/utils/dimensions';

export const Container = styled(View, {
  width: screenWidth,
  ...container('padding'),
});

export const ContainerSmall = styled(View, {
  ...container('padding'),
  paddingVertical: getScaleSize(22),
  backgroundColor: color('white'),
  borderRadius: getScaleSize(20),
});

export const BoxContainer = styled(View, {
  backgroundColor: color('gray4'),
  borderTopLeftRadius: getScaleSize(20),
  borderTopRightRadius: getScaleSize(20),
});

export const RaitingContainer = styled(View, {
  flexDirection: 'row',
  alignItems: 'center',
});

export const RaitingContainerSmall = styled(View, {
  flexDirection: 'row',
  alignItems: 'center',
  marginBottom: getScaleSize(22),
});

export const RatingTextContainer = View;

export const RatingText = styled(Text, {
  ...font({ type: 'h1', weight: 'bold' }),
  fontSize: getScaleSize(42),
  lineHeight: getScaleSize(45),
  fontWeight: '700',
  color: color('gradientColor2'),
  marginRight: getScaleSize(10),
});

export const RatingListEnumContainer = styled(View, {
  flex: 1,
  flexDirection: 'column',
});

export const RatingListEnumItemContainer = styled(View, {
  flexDirection: 'row',
  alignItems: 'center',
});

export const StarsContainer = styled(
  View,
  (props: { starsCount: number }) =>
    ({
      marginLeft: getScaleSize(5) + (5 - props.starsCount) * 9,
      justifyContent: 'flex-end',
    } as const),
);

export const ProgressBarContainer = styled(View, {
  flex: 1,
  marginLeft: getScaleSize(5),
});

export const AllFeedbackButtonContainer = styled(View, {
  flexDirection: 'row',
  alignItems: 'center',
  justifyContent: 'space-between',
  marginBottom: getScaleSize(8),
});

export const AddFeedbackButtonContainer = styled(TouchableWithoutFeedback, {
  alignItems: 'center',
});

export const AddFeedbackButtonContainerSmall = styled(TouchableWithoutFeedback, {
  alignItems: 'center',
});

export const DescriptionText = styled(Text, {
  ...font({ type: 'text3', weight: 'light' }),
  color: color('gray6'),
  textAlign: 'center',
});

export const LinkButtonContainer = TouchableOpacity;

export const LinkButtonText = styled(Text, {
  ...font({ type: 'text2' }),
  fontWeight: '600',
  color: color('gradientColor2'),
});

export const FeedbacksCountText = styled(Text, {
  ...font({ type: 'text4' }),
  lineHeight: getScaleSize(21),
  color: color('gray6'),
  marginVertical: getScaleSize(8),
});

export const Title = styled(Text, {
  ...font({ type: 'text3' }),
  color: color('gray10'),
  textTransform: 'uppercase',
});

export const TitleContainer = styled(View, {
  flexDirection: 'row',
  justifyContent: 'space-between',
  marginBottom: getScaleSize(16),
});

export const ImagesContainer = styled(View, {
  flexDirection: 'row',
  justifyContent: 'space-between',
  marginBottom: getScaleSize(16),
});

export const MiniImage = styled(FastImage, {
  width: screenWidth * 0.3,
  height: screenWidth * 0.2,
  borderRadius: getScaleSize(4),
  marginHorizontal: getScaleSize(1),
  overflow: 'hidden',
});

export const FeedbacksCountTextSmall = styled(Text, {
  ...font({ type: 'text4' }),
  color: color('gray6'),
});
