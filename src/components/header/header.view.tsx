import React from 'react';
import { HeaderContainer, HeaderTitle } from './header.style';

interface HeaderProps {
  title: string;
  inGradientHeader?: boolean;
  children?: React.ReactNode;
}

export function Header({
  title,
  children,
  inGradientHeader = false,
}: HeaderProps): React.ReactElement {
  return (
    <HeaderContainer inGradientHeader={inGradientHeader}>
      <HeaderTitle>{title}</HeaderTitle>
      {children}
    </HeaderContainer>
  );
}
