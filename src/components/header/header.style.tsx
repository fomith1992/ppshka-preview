import { color } from '@shared/config/color';
import { styled } from '@shared/config/styled';
import { font } from '@shared/config/text';
import { blockShadow } from '@shared/ui/block-shadow/block-shadow';
import { Text, View } from 'react-native';
import { getScaleSize } from 'src/utils/dimensions';

export const HeaderContainer = styled(View, (props: { inGradientHeader?: boolean }) => ({
  paddingTop: getScaleSize(props.inGradientHeader ? 50 : 6),
  paddingBottom: getScaleSize(24),
  ...blockShadow(),
}));
export const HeaderTitle = styled(Text, {
  ...font({ type: 'h1' }),
  color: color('white'),
  marginHorizontal: getScaleSize(15),
  marginBottom: getScaleSize(12),
});
