import React from 'react';
import useAsync from 'react-use/lib/useAsync';
import { getCacheContent } from 'src/utils/cache-content';
import {
  BoldDurationText,
  Container,
  DurationText,
  InfoContainer,
  MiniImage,
  Title,
} from './chosen-program-card.style';

interface ChosenProgramCardViewProps {
  title: string;
  uri: string;
  rest: number;
}

export function ChosenProgramCardView({
  title,
  uri,
  rest,
}: ChosenProgramCardViewProps): React.ReactElement {
  const photo = useAsync(async () => {
    return await getCacheContent({ url: uri, format: 'png' });
  }, [uri]);

  return (
    <Container>
      <MiniImage
        source={{
          uri: photo.value,
        }}
      />
      <InfoContainer>
        <Title>{title}</Title>
        <DurationText>
          Осталось тренировок: <BoldDurationText>{rest.toString()}</BoldDurationText>
        </DurationText>
      </InfoContainer>
    </Container>
  );
}
