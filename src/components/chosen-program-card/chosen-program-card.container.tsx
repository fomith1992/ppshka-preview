import React from 'react';
import { useSelector } from 'react-redux';
import useAsync from 'react-use/lib/useAsync';
import { userAPI, workoutAPI } from 'src/api';
import { AppState } from 'src/store';
import { BaseContainer } from './chosen-program-card.style';
import { ChosenProgramCardView } from './chosen-program-card.view';

export function ChosenProgramCard(props: { header: React.ReactNode }): React.ReactElement | null {
  const access_token = useSelector((state: AppState) => state.session.tokens.accessToken);
  const userPrograms = useAsync(async () => {
    return await userAPI.getUserPrograms(access_token, 0).catch(console.log);
  }, []);
  const workouts = useAsync(async () => {
    return await workoutAPI.getWorkoutsCalendarList({ access_token }).catch(console.log);
  }, []);
  if (userPrograms.loading || workouts.loading) return null;
  const program = userPrograms.value?.data.data.data[0];
  if (program == null) return null;
  return (
    <BaseContainer>
      {props.header}
      <ChosenProgramCardView
        title={program.name}
        uri={program.url_image}
        rest={workouts.value?.data.data.length ?? 0}
      />
    </BaseContainer>
  );
}
