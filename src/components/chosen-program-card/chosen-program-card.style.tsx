import { color } from '@shared/config/color';
import { styled } from '@shared/config/styled';
import { font } from '@shared/config/text';
import { container } from '@shared/config/view';
import { Text, View } from 'react-native';
import FastImage from 'react-native-fast-image';
import { getScaleSize } from 'src/utils/dimensions';

export const BaseContainer = View;

export const Container = styled(View, {
  paddingRight: getScaleSize(15),
  flexDirection: 'row',
  backgroundColor: 'white',
  borderRadius: getScaleSize(15),
  marginBottom: getScaleSize(30),
  alignItems: 'center',
  overflow: 'hidden',
});

export const MiniImage = styled(FastImage, {
  width: getScaleSize(100),
  height: getScaleSize(100),
});

export const InfoContainer = styled(View, {
  ...container('padding'),
});

export const Title = styled(Text, {
  ...font({ type: 'text1' }),
  color: color('primary'),
});

export const DurationText = styled(Text, {
  ...font({ type: 'text2' }),
  color: color('gray7'),
});

export const BoldDurationText = styled(Text, {
  ...font({ type: 'text2', weight: 'bold' }),
  color: color('gray7'),
});
