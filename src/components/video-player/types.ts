import { StyleProp, View } from 'react-native';
import { VideoProperties } from 'react-native-video';

export interface VideoProps extends VideoProperties {
  toggleResizeModeOnFullscreen?: boolean;
  controlAnimationTiming?: number;
  doubleTapTime?: number;
  controlTimeout?: number;
  scrubbing?: number;
  showOnStart?: boolean;
  videoStyle?: StyleProp<View>;
  seekColor?: string;
  tapAnywhereToPause?: boolean;

  disableFullscreen?: boolean;
  disablePlayPause?: boolean;
  disableSeekbar?: boolean;
  disableVolume?: boolean;
  disableTimer?: boolean;
  disableBack?: boolean;
}
