import { StyleSheet, View } from 'react-native';

import { styled } from '@shared/config/styled';
import { theme } from '@shared/config/theme';

import { PlayIcon67 } from '../../shared/ui/icons/play.icon-67';

export const Container = styled(View, {
  position: 'relative',
  zIndex: 0,
});

export const Overlay = styled(View, {
  ...StyleSheet.absoluteFillObject,
  zIndex: 0,
});

export const PlayIcon = styled(PlayIcon67, { color: theme.colors.primary });

export const IconContainer = styled(View, {
  ...StyleSheet.absoluteFillObject,
  justifyContent: 'center',
  alignItems: 'center',
  zIndex: 0,
});
