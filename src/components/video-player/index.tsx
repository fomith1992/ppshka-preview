import React, { useState } from 'react';
import { TouchableOpacity, TouchableWithoutFeedback } from 'react-native';
import VideoPlayerWC from 'react-native-video-controls/VideoPlayer';
import { screenWidth } from 'src/utils/dimensions';
import PreLoadedImage from '../PreLoadedImage';

import * as UI from './styles';
import { VideoProps } from './types';

export const VideoPlayer: React.FC<VideoProps> = ({ paused, ...props }) => {
  const [isPlay, setIsPlay] = useState(!paused);
  const [isStop, setIsStop] = useState(false);
  const [currentTime, setCurrentTime] = useState<number | undefined>(undefined);

  const handlePlay = () => {
    setIsPlay(true);
    setIsStop(false);
    setCurrentTime(undefined);
  };
  const handlePause = () => setIsPlay(false);

  const handleEnd = () => {
    if (props.repeat) {
      setCurrentTime(0);
      handlePlay();
    } else {
      setIsStop(true);
      handlePause();
      setCurrentTime(0);
    }
  };
  const togglePlay = () => (isPlay ? handlePause() : handlePlay());

  return (
    <TouchableWithoutFeedback onPress={togglePlay}>
      <UI.Container>
        <VideoPlayerWC
          controls={false}
          onPlay={handlePlay}
          onPause={handlePause}
          onStop={handlePause}
          paused={!isPlay}
          onEnd={handleEnd}
          currentTime={currentTime}
          {...props}
        />
        <UI.Overlay>
          {isStop && (
            <PreLoadedImage
              style={{ width: screenWidth, height: (screenWidth / 4) * 5 }}
              source={{ uri: props.poster }}
            />
          )}
        </UI.Overlay>
        {props.source && !isPlay && (
          <UI.IconContainer>
            <TouchableOpacity onPress={togglePlay}>
              <UI.PlayIcon />
            </TouchableOpacity>
          </UI.IconContainer>
        )}
      </UI.Container>
    </TouchableWithoutFeedback>
  );
};
