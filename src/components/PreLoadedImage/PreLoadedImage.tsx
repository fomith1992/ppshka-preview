import React, { FC } from 'react';
import { StyleProp } from 'react-native';
import FastImage, { FastImageProps, ImageStyle } from 'react-native-fast-image';

interface IProps extends FastImageProps {
  style?: StyleProp<ImageStyle>;
}

const PreLoadedImage: FC<IProps> = ({ style, children, ...rest }) => {
  return (
    <FastImage style={style} onError={() => console.log('error fast image')} {...rest}>
      {children}
    </FastImage>
  );
};

export default PreLoadedImage;
