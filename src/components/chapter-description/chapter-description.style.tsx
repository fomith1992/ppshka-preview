import { color } from '@shared/config/color';
import { styled } from '@shared/config/styled';
import { font } from '@shared/config/text';
import { theme } from '@shared/config/theme';
import { container } from '@shared/config/view';
import { Platform, ScrollView, Text, View } from 'react-native';
import { getScaleSize, screenHeight, windowWidth } from 'src/utils/dimensions';

export const PortalBlurContainer = styled(View, {
  flex: 1,
  justifyContent: 'center',
  alignItems: 'center',
  backgroundColor: color('grayOpacity50'),
});

export const PopUpContainer = styled(ScrollView, {
  width: windowWidth - theme.layout.margin * 2,
  maxHeight: screenHeight * 0.5,
  ...container('padding'),
  paddingVertical: getScaleSize(15),
  backgroundColor: color('white'),
  borderRadius: getScaleSize(16),
  ...Platform.select({
    android: {
      elevation: 2,
    },
    ios: {
      shadowColor: color('black'),
      shadowOffset: {
        width: 0,
        height: 1,
      },
      shadowOpacity: 0.06,
      shadowRadius: 1,
    },
  }),
});

export const PortalDescription = styled(Text, {
  ...font({ type: 'text2' }),
  marginTop: getScaleSize(15),
  marginBottom: getScaleSize(15),
});

export const Spring = styled(Text, {
  flex: 1,
});
