import AsyncStorage from '@react-native-async-storage/async-storage';
import { Bottom, Row } from '@screens/recipes/ui/header.style';
import { Modal } from '@shared/ui/modal';
import React, { useEffect, useState } from 'react';
import { View } from 'react-native';
import { useSelector } from 'react-redux';
import useAsync from 'react-use/lib/useAsync';
import { userAPI } from 'src/api';
import { AppState } from 'src/store';
import { CloseButton, CloseSvg } from '../add-post/add-post.style';
import Button from '../Button';
import {
  PopUpContainer,
  PortalBlurContainer,
  PortalDescription,
  Spring,
} from './chapter-description.style';

export type TChapter = 'plain' | 'recipe' | 'dashboard' | 'training' | 'profile';

interface ChapterDescriptionProps {
  chapter: TChapter;
}

export const ChapterDescription = ({ chapter }: ChapterDescriptionProps): React.ReactElement => {
  const [needVisible, setNeedVisible] = useState<boolean>(false);
  const access_token = useSelector((state: AppState) => state.session.tokens.accessToken);
  useEffect(() => {
    AsyncStorage.getItem(chapter, (err, result) => {
      if (err != null) {
        console.log('AsyncStorage err', err);
      }
      if (result !== undefined) {
        setNeedVisible(JSON.parse(result) ?? true);
      }
    });
  }, [needVisible, chapter]);

  const handleClosePopUp = () => {
    AsyncStorage.setItem(chapter, JSON.stringify(false));
    setNeedVisible(false);
  };

  const recipesApi = useAsync(async () => {
    if (needVisible) {
      return await userAPI.getChapterDescription({ access_token, chapter });
    } else return null;
  }, [needVisible, chapter]);

  if (!needVisible) return <></>;

  return (
    <Modal>
      <PortalBlurContainer>
        <PopUpContainer
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{ flexGrow: 1 }}
        >
          <Row spaceBeetween>
            <View style={{ height: 20 }} />
            <CloseButton onPress={handleClosePopUp}>
              <CloseSvg />
            </CloseButton>
          </Row>
          <PortalDescription>{recipesApi.value?.data.data}</PortalDescription>
          <Spring />
          <Button content="понятно" uppercase onPress={handleClosePopUp} />
          <Bottom mb={30} />
        </PopUpContainer>
      </PortalBlurContainer>
    </Modal>
  );
};
