import { StyleSheet } from 'react-native';

import { theme } from '@shared/config/theme';

import { font14r } from 'src/commonStyles/fonts';
import { getScaleSize } from 'src/utils/dimensions';

export const styles = StyleSheet.create({
  container: {
    borderBottomWidth: 1,
    borderBottomColor: '#F0F0F0',
    justifyContent: 'center',
  },
  input: {
    height: getScaleSize(57),
    paddingRight: getScaleSize(20),
    color: 'black',
    ...font14r,
  },
  containerEye: {
    position: 'absolute',
    alignSelf: 'flex-end',
  },
  subTitle: {
    ...font14r,
    lineHeight: getScaleSize(17),
    color: theme.colors.secondary,
    marginTop: getScaleSize(8),
  },
});
