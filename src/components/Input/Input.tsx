import React, { FC, useState } from 'react';
import { StyleProp, Text, TouchableOpacity, View, ViewStyle } from 'react-native';
import { TextInput } from 'react-native-gesture-handler';

import Icon from 'src/components/Icon';
import { hitSlopParams } from 'src/utils/hit-slop-params';

import { styles } from './styles';

interface IProps {
  value: string | undefined;
  onChange: (text: string) => void;
  placeholder: string;
  isPassword?: boolean;
  style?: StyleProp<ViewStyle>;
  error?: string | null;
}

const Input: FC<IProps> = ({ value = '', onChange, isPassword, placeholder, style, error }) => {
  const [isShowingPassword, setIsShowingPassword] = useState(false);

  const onToggleShowingPassword = () => {
    setIsShowingPassword((prev) => !prev);
  };

  return (
    <>
      <View style={style}>
        <View style={styles.container}>
          <TextInput
            value={value}
            onChangeText={onChange}
            style={styles.input}
            textContentType={isPassword ? 'password' : 'none'}
            secureTextEntry={isPassword && !isShowingPassword}
            placeholder={placeholder}
            placeholderTextColor={'#8C8C8C'}
            ref={(ref) => ref && ref.setNativeProps({ style: { fontFamily: 'TTNorms-Regular' } })}
          />
          {isPassword ? (
            <TouchableOpacity
              style={styles.containerEye}
              onPress={onToggleShowingPassword}
              hitSlop={hitSlopParams('XL')}
            >
              <Icon name={isShowingPassword ? 'eyeShow' : 'eyeHide'} />
            </TouchableOpacity>
          ) : null}
        </View>
        {error && <Text style={styles.subTitle}>{error}</Text>}
      </View>
    </>
  );
};

export default Input;
