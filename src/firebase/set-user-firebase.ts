import analytics from '@react-native-firebase/analytics';
import crashlytics from '@react-native-firebase/crashlytics';

export function firebaseSetUser(userId: string | null): void {
  if (userId != null) {
    crashlytics()
      .setUserId(userId)
      .catch((err) => {
        console.error(`[Firebase Crashlytics] Failed to set user with user id:`, userId, err);
      });
  }
  analytics()
    .setUserId(userId)
    .catch((err) => {
      console.error(`[Firebase Analytics] Failed to set user with user id:`, userId, err);
    });
}
