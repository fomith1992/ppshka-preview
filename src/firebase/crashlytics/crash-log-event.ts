import crashlytics from '@react-native-firebase/crashlytics';

export function firebaseCrashLog(message: string): void {
  crashlytics().log(message);
}
