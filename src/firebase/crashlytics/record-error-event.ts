import crashlytics from '@react-native-firebase/crashlytics';

export function firebaseRecordError(error: Error): void {
  crashlytics().recordError(error);
}
