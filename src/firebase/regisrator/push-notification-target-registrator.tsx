/* eslint-disable react-hooks/rules-of-hooks */
import messaging from '@react-native-firebase/messaging';
import React, { useMemo } from 'react';
import { Platform } from 'react-native';
import { getUniqueId } from 'react-native-device-info';
import useObservable from 'react-use/lib/useObservable';
import * as rx from 'rxjs';
import * as $ from 'rxjs/operators';
import { useBehaviorSubject } from '../../hooks/use-behavior-subject';
import { useSelector } from 'react-redux';
import { AppState } from 'src/store';
import { registerNotificationTarget } from 'src/api/notifications';

const os = Platform.select({
  android: 'ANDROID',
  ios: 'IOS',
} as const);

const deviceId = getUniqueId();

const initialToken$ = rx
  .defer(async () => await messaging().getToken())
  .pipe(
    $.retryWhen((errors) =>
      errors.pipe(
        $.tap((error) => {
          console.error('[FCM] Failed to get FCM token, retrying in 5 seconds...', error);
        }),
        $.delay(5000),
      ),
    ),
  );

const tokenRefreshed$ = rx.fromEventPattern<string>(
  (handler) => messaging().onTokenRefresh(handler),
  (_, off) => off(),
);

const token$ = tokenRefreshed$.pipe(
  $.map((token) => rx.of(token)),
  $.startWith(initialToken$),
  $.switchAll(),
);

export function PushNotificationTargetRegistrator(): React.ReactElement {
  if (os == null || deviceId === 'unknown') {
    // works only for iOS and Android with known deviceId
    return <></>;
  }
  const access_token = useSelector((state: AppState) => state.session.tokens.accessToken);
  const user$ = useBehaviorSubject(access_token);

  const registration$ = useMemo(
    () =>
      rx.zip(user$, token$).pipe(
        $.switchMap(([user, token]) =>
          user == null
            ? []
            : rx
                .defer(
                  async () =>
                    await registerNotificationTarget({
                      deviceId,
                      os,
                      token,
                      access_token,
                    }),
                )
                .pipe(
                  $.tap((value) => {
                    const result = value.data;
                    if (result.status === 'error') {
                      console.error('Notification target registration error', value);
                      throw new Error('Notification target registration error');
                    }
                  }),
                  $.retryWhen((errors) =>
                    errors.pipe(
                      $.tap((error) => {
                        console.error(
                          'Failed to register notification target, retrying in 5 seconds...',
                          error,
                        );
                      }),
                      $.delay(5000),
                    ),
                  ),
                  $.tap((_) => {
                    console.log('Notification target registered');
                  }),
                ),
        ),
      ),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [user$, token$, access_token],
  );

  useObservable(registration$);

  return <></>;
}
