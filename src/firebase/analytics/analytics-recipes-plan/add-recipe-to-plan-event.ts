import { firebaseLogEvent } from '../common/firebase-log-event';

export function addRecipeToPlanEvent(recipeId: string | null): void {
  firebaseLogEvent('recipe_plan_add_recipe', {
    recipeId,
  });
}
