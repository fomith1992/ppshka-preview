import { firebaseLogEvent } from '../common/firebase-log-event';

export function delRecipeToPlanEvent(recipeId: string | null): void {
  firebaseLogEvent('recipe_plan_del_recipe', {
    recipeId,
  });
}
