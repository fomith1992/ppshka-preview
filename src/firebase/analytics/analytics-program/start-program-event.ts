import { firebaseLogEvent } from '../common/firebase-log-event';

export function startProgramEvent(programId: string): void {
  firebaseLogEvent('program_start', {
    programId,
  });
}
