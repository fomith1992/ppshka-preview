import { firebaseLogEvent } from '../common/firebase-log-event';

export function clearBasketEvent(): void {
  firebaseLogEvent('clear_food_basket', {});
}
