import { firebaseLogEvent } from '../common/firebase-log-event';

export function addRecipeToBasketEvent(recipeId: string | null): void {
  firebaseLogEvent('food_basket_add_recipe', {
    recipeId,
  });
}
