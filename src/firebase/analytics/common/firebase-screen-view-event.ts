import analytics from '@react-native-firebase/analytics';

export function firebaseScreenViewEvent(
  previousRouteName: string | undefined,
  currentRouteName: string | undefined,
): void {
  if (currentRouteName !== undefined && currentRouteName !== previousRouteName) {
    analytics()
      .logScreenView({
        screen_class: currentRouteName,
        screen_name: currentRouteName,
      })
      .catch((err) => {
        console.error(
          `[Firebase Analytics] Failed to log screen view event for ${currentRouteName}`,
          err,
        );
      });
  }
}
