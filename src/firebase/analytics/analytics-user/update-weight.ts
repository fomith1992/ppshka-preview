import { firebaseLogEvent } from '../common/firebase-log-event';

export function updateWeightEvent(userId: string | null): void {
  firebaseLogEvent('update_weight', {
    userId,
  });
}

export function updateWeightTargetEvent(userId: string | null): void {
  firebaseLogEvent('update_weight_target', {
    userId,
  });
}
