import { firebaseLogEvent } from '../common/firebase-log-event';

export function updateNameEvent(userId: string | null): void {
  firebaseLogEvent('update_name', {
    userId,
  });
}
