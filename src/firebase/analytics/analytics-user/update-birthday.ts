import { firebaseLogEvent } from '../common/firebase-log-event';

export function updateBirthdayEvent(userId: string | null): void {
  firebaseLogEvent('update_birthday', {
    userId,
  });
}
