import { firebaseLogEvent } from '../common/firebase-log-event';

export function updateMeasurementsEvent(userId: string | null): void {
  firebaseLogEvent('update_measurements', {
    userId,
  });
}
