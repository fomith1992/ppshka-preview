import { firebaseLogEvent } from '../common/firebase-log-event';

export function updateAccountType(userId: string | null, productId: string): void {
  firebaseLogEvent('buy_pro', {
    userId,
    productId,
  });
}
