import analytics from '@react-native-firebase/analytics';

export function signUpEvent(method: 'email' | 'facebook'): void {
  analytics()
    .logSignUp({ method })
    .catch((err) => {
      console.error(`[Firebase Analytics] Failed to log signup with method:`, method, err);
    });
}
