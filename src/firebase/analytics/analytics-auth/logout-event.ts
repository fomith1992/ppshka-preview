import { firebaseLogEvent } from '../common/firebase-log-event';

export function logoutEvent(): void {
  firebaseLogEvent('user_logout', {});
}
