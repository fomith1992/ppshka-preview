import analytics from '@react-native-firebase/analytics';

export function signInEvent(method: 'email' | 'facebook'): void {
  analytics()
    .logLogin({ method })
    .catch((err) => {
      console.error(`[Firebase Analytics] Failed to log signup with method:`, method, err);
    });
}
