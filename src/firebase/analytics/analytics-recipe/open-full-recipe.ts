import { firebaseLogEvent } from '../common/firebase-log-event';

export function openFullRecipeEvent(recipeId: string | null): void {
  firebaseLogEvent('open_full_recipe', {
    recipeId,
  });
}
