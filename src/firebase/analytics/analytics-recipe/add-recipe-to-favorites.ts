import { firebaseLogEvent } from '../common/firebase-log-event';

export function addRecipeToFavorites(recipeId: string | null): void {
  firebaseLogEvent('add_recipe_to_favorites', {
    recipeId,
  });
}
