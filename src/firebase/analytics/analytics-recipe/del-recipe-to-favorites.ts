import { firebaseLogEvent } from '../common/firebase-log-event';

export function delRecipeToFavorites(recipeId: string | null): void {
  firebaseLogEvent('del_recipe_to_favorites', {
    recipeId,
  });
}
