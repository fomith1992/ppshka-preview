import { firebaseLogEvent } from '../common/firebase-log-event';

export function finishWorkoutEvent(workoutId: string): void {
  firebaseLogEvent('workout_finish', {
    workoutId,
  });
}
