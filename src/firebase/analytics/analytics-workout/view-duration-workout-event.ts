import { firebaseLogEvent } from '../common/firebase-log-event';

export function viewDurationWorkoutEvent(workoutId: string, duration: number): void {
  firebaseLogEvent('workout_view_duration', {
    workoutId,
    duration,
  });
}
