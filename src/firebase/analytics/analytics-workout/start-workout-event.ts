import { firebaseLogEvent } from '../common/firebase-log-event';

export function startWorkoutEvent(workoutId: string): void {
  firebaseLogEvent('workout_start', {
    workoutId,
  });
}
