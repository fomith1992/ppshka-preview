import { Platform, TextStyle } from 'react-native';
import { getScaleSize } from 'src/utils/dimensions';
import { LazyStyle } from './styled';

type FontFamily = 'Montserrat';

type FontType = 'h1' | 'h2' | 'text1' | 'text2' | 'text3' | 'text4' | 'text5';

type FontWeight = 'normal' | 'light' | 'bold';

interface FontProps {
  type: FontType;
  weight?: FontWeight;
}

const fontFamily = (type: FontType): FontFamily => {
  switch (type) {
    case 'h1':
    case 'h2':
    case 'text1':
    case 'text2':
    case 'text3':
    case 'text4':
    case 'text5':
      return 'Montserrat';
  }
};

const fontLineHeight = (type: FontType): number => {
  switch (type) {
    case 'h1':
      return 33;
    case 'h2':
      return 27;
    case 'text1':
      return 24;
    case 'text2':
      return 21;
    case 'text3':
      return 18;
    case 'text4':
      return 16;
    case 'text5':
      return 9;
  }
};

const getFontSize = (type: FontType): number => {
  switch (type) {
    case 'h1':
      return 30;
    case 'h2':
      return 20;
    case 'text1':
      return 16;
    case 'text2':
      return 14;
    case 'text3':
      return 12;
    case 'text4':
      return 10;
    case 'text5':
      return 8;
  }
};

const exactFontWeight = (relativeWeight: FontWeight): number => {
  switch (relativeWeight) {
    case 'bold':
      return 600;
    case 'normal':
      return 500;
    case 'light':
      return 400;
  }
};

const fontWeightToFileNameFontWeight = (weight: number): string => {
  switch (weight) {
    case 100:
      return 'Thin';
    case 200:
      return 'ExtraLight';
    case 300:
      return 'Light';
    case 400:
    default:
      return 'Regular';
    case 500:
      return 'Medium';
    case 600:
      return 'SemiBold';
    case 700:
      return 'Bold';
    case 800:
      return 'ExtraBold';
    case 900:
      return 'Black';
  }
};

const fontWeightTextStyleFontWeight = (weight: number): TextStyle['fontWeight'] => {
  switch (weight) {
    case 100:
      return '100';
    case 200:
      return '200';
    case 300:
      return '300';
    case 400:
    default:
      return '400';
    case 500:
      return '500';
    case 600:
      return '600';
    case 700:
      return '700';
    case 800:
      return '800';
    case 900:
      return '900';
  }
};

/**
 * fontSize: h1: 30 , h2: 20 , text1: 16 , text2: 14 , text3: 12 , text4: 10 , text5: 8
 *
 * lineHeight: h1: 33 , h2: 27 , text1: 24 , text2: 21 , text3: 18 , text4: 16 , text5: 9
 */
export function font({ type, weight = 'normal' }: FontProps): LazyStyle<TextStyle> {
  const fontFamilyName = fontFamily(type);
  const fontWeight = exactFontWeight(weight);
  const fontSize = getScaleSize(getFontSize(type));
  const lineHeight = getScaleSize(fontLineHeight(type));
  return Platform.OS === 'android'
    ? {
        fontFamily: `${fontFamilyName.replace(' ', '')}-${fontWeightToFileNameFontWeight(
          fontWeight,
        )}`,
        fontSize,
        lineHeight,
      }
    : {
        fontFamily: fontFamilyName,
        fontWeight: fontWeightTextStyleFontWeight(fontWeight),
        fontSize,
        lineHeight,
      };
}
