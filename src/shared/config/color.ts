import { LazyStyleProp } from './styled';
import { TTheme } from './theme';

export type ColorType = keyof TTheme['colors'];

export function color(name: ColorType): LazyStyleProp<string> {
  return ({ colors }) => colors[name];
}
