import { createContext, useContext } from 'react';
import { getScaleSize } from 'src/utils/dimensions';

export type TTheme = typeof theme;
export type TColorType = keyof TTheme['colors'];

export const theme = {
  colors: {
    white: '#fff',

    whiteOpacity: '#FFFFFF80',
    whiteOpacity80: '#ffffffc3',
    whiteOpacity50: '#ffffff80',
    whiteOpacity20: '#ffffff33',

    grayOpacity50: '#bfbfbf80',
    black: '#000',

    primary: '#6034A8',
    secondary: '#EF4923',
    grayBackground: '#FAFAFA',

    tertiary: '#5FA834',

    gray1: '#ffffff',
    gray2: '#fafafafa',
    gray3: '#f5f5f5',
    gray4: '#f0f0f0',
    gray5: '#d9d9d9',
    gray6: '#bfbfbf',
    gray7: '#8c8c8c',
    gray8: '#595959',
    gray9: '#434343',
    gray10: '#262626',

    defaultIconColor: '#d9d9d9', // gray5
    transparent: '#ffffff0',

    weightColorYellow: '#ffd700',
    weightColorOrange: '#ff9900',

    gradientColor1: '#F06163',
    gradientColor2: '#BD2B98',
    gradientColor3: '#6034A8',
  },

  layout: {
    margin: getScaleSize(16),
    gutter: 8,
  },

  size: {
    scale: 1, // assuming baseline=16
  },
};

const context = createContext(theme);

export const ThemeProvider = context.Provider;
export const useTheme = (): TTheme => useContext(context);
