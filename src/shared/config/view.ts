import { ViewStyle } from 'react-native';
import { getScaleSize } from 'src/utils/dimensions';
import { LazyStyle } from './styled';

export function container(type: 'margin' | 'padding' = 'margin'): LazyStyle<ViewStyle> {
  switch (type) {
    case 'margin':
      return {
        marginHorizontal: ({ layout }) => getScaleSize(layout.margin),
      };
    case 'padding':
      return {
        paddingHorizontal: ({ layout }) => getScaleSize(layout.margin),
      };
  }
}
