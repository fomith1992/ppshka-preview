/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/ban-types */

import React from 'react';
import { StyleSheet } from 'react-native';

import { TTheme, useTheme } from './theme';

import _ from 'lodash';

export type LazyStyleProp<T> = T | ((theme: TTheme) => T);

export type LazyStyle<S> = {
  [P in keyof S]: LazyStyleProp<S[P]>;
};

function isEager<T>(style: LazyStyle<T>): style is T {
  return Object.values(style).every((prop) => !(prop instanceof Function));
}

export function lift<T>(style: LazyStyle<T>): LazyStyleProp<T> {
  if (isEager(style)) {
    return style;
  } else {
    return (theme) => _.mapValues(style, (val) => (val instanceof Function ? val(theme) : val));
  }
}

export function materialize<T>(theme: TTheme, style: LazyStyleProp<T>): T {
  return style instanceof Function ? style(theme) : style;
}

type StyleOf<T extends React.ComponentType<any>> = React.ComponentPropsWithoutRef<T> extends {
  style?: infer S;
}
  ? S
  : never;

export function styled<T extends React.ComponentType<any>, A extends {} = {}>(
  Component: T,
  ...styles: Array<LazyStyle<StyleOf<T>> | ((props: A) => LazyStyle<StyleOf<T>>)>
): React.ComponentType<React.ComponentPropsWithRef<T> & A> {
  return React.forwardRef((props: any, ref: any) => {
    const theme = useTheme();
    const compiled = styles.map((style) =>
      materialize(theme, lift(style instanceof Function ? style(props) : style)),
    );
    return <Component {...props} ref={ref} style={StyleSheet.compose(compiled, props.style)} />;
  }) as any;
}

export function variants<K extends string, V extends string, I, O>(
  key: K,
  variants: (input: I) => Record<V, O>,
): (input: I & Record<K, V>) => O;
export function variants<K extends string, V extends string, O>(
  key: K,
  variants: Record<V, O>,
): (input: Record<K, V>) => O;
export function variants<K extends string, V extends string, I, O>(
  key: K,
  variants: ((input: Record<K, V> & I) => Record<V, O>) | Record<V, O>,
): (input: Record<K, V> & I) => O {
  return variants instanceof Function
    ? (input) => variants(input)[input[key]]
    : (input) => variants[input[key]];
}

export function useMaterialized<T>(style: LazyStyleProp<T>): T {
  const theme = useTheme();
  return materialize(theme, style);
}
