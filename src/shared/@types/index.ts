export interface ViewToken<T = unknown> {
  item: T;
  key: string;
  index: number | null;
  isViewable: boolean;
  section?: unknown;
}
