declare module 'react-native-immersive' {
  export namespace Immersive {
    function on(): void;

    function off(): void;

    function setImmersive(isOn: boolean): void;

    function getImmersive(): unknown; // do not always match actual display state

    /**
     * The Immersive State do not last forever (SYSTEM_UI_FLAG_IMMERSIVE_STICKY is not sticky enough).
     * The Immersive State will get reset when:
     *
     *   coming back to active AppState
     *   after Keyboard opening
     *   after Alert opening
     *   after Modal opening
     *
     *  To restore the Immersive State, add an additional listener.
     *
     *   // listener for Immersive State recover
     *   const restoreImmersive = () => {
     *    __DEV__ && console.warn('Immersive State Changed!')
     *    Immersive.on()
     *  }
     *   Immersive.addImmersiveListener(restoreImmersive)
     *   Immersive.removeImmersiveListener(restoreImmersive)
     */
    function addImmersiveListener(listener: (data: unknown) => void): void;

    /**
     * Remove a handler by passing the handler
     */
    function removeImmersiveListener(listener: (data: unknown) => void): void;
  }
}
