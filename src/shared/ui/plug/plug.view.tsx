import React from 'react';

import { Container, Title } from './plug.style';

export function Plug(props: { part: string }): React.ReactElement {
  return (
    <Container>
      <Title>{`Раздел "${props.part}" недоступен в бета-тестировании`}</Title>
    </Container>
  );
}
