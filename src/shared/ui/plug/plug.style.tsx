import { color } from '@shared/config/color';
import { styled } from '@shared/config/styled';
import { font } from '@shared/config/text';
import { Text, View } from 'react-native';

export const Container = styled(View, {
  position: 'absolute',
  height: '100%',
  width: '100%',
  flex: 1,
  justifyContent: 'center',
  backgroundColor: color('white'),
});

export const Title = styled(Text, {
  ...font({ type: 'h2' }),
  color: color('gray9'),
  textAlign: 'center',
});
