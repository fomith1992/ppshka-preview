export const gradientComponentsColors = ['#F06163', '#BD2B98', '#6034A8'];
export const gradientLayoutColors = ['#6034A8', '#BD2B98', '#F06163'];
export const transparentsColors = ['#d9d9d9', '#d9d9d9'];
