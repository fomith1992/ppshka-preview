import { styled } from '@shared/config/styled';
import { View } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

export const Container = styled(View, {
  position: 'absolute',
  width: '100%',
  height: '100%',
});

export const GradientLayout = styled(
  LinearGradient,
  (props: { center: boolean }) =>
    ({
      flex: 1,
      justifyContent: props.center ? 'center' : undefined,
      alignItems: props.center ? 'center' : undefined,
    } as const),
);
