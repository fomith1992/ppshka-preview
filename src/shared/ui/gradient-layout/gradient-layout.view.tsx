import React from 'react';
import { gradientLayoutColors, gradientComponentsColors } from './constant';
import { Container, GradientLayout } from './gradient-layout.style';

interface GradientLayoutViewProps {
  children?: React.ReactNode;
  horizontal?: boolean;
  reverseColor?: boolean;
  center?: boolean;
}

export function GradientLayoutView({
  children,
  horizontal = false,
  center = false,
  reverseColor = false,
}: GradientLayoutViewProps): React.ReactElement {
  return (
    <Container>
      <GradientLayout
        start={horizontal ? { x: 1, y: 0 } : undefined}
        end={horizontal ? { x: 0, y: 0 } : undefined}
        colors={reverseColor ? gradientComponentsColors : gradientLayoutColors}
        center={center}
      >
        {children}
      </GradientLayout>
    </Container>
  );
}
