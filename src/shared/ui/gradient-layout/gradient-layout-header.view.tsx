import React from 'react';
import { gradientLayoutColors } from './constant';
import { Container, GradientLayout } from './gradient-layout-header.style';

interface GradientLayoutHeaderViewProps {
  children?: React.ReactNode;
  colors?: string[];
}

export function GradientLayoutHeaderView({
  children,
  colors,
}: GradientLayoutHeaderViewProps): React.ReactElement {
  return (
    <Container>
      <GradientLayout colors={colors ?? gradientLayoutColors}>{children}</GradientLayout>
    </Container>
  );
}
