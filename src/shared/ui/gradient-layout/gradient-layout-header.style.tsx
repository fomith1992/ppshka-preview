import { styled } from '@shared/config/styled';
import { View } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { getScaleSize } from 'src/utils/dimensions';

export const Container = styled(View, {
  borderBottomEndRadius: getScaleSize(50),
  borderBottomStartRadius: getScaleSize(50),
  overflow: 'hidden',
});

export const GradientLayout = styled(LinearGradient, {
  paddingTop: getScaleSize(44),
  paddingBottom: getScaleSize(44),
});
