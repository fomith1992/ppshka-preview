import { color } from '@shared/config/color';
import { Platform } from 'react-native';

export const blockShadow = () => {
  return Platform.select({
    android: {
      elevation: 2,
    },
    ios: {
      shadowColor: color('black'),
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowOpacity: 0.06,
      shadowRadius: 1,
    },
  });
};
