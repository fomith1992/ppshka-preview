import React from 'react';
import { StyleSheet } from 'react-native';
import Svg, { Path } from 'react-native-svg';

import { theme } from '@shared/config/theme';

import { getScaleSize } from 'src/utils/dimensions';

import { IconComponentProps } from './icons.config';

export const GymIcon30 = ({ style, size = 16, color, ...rest }: IconComponentProps) => {
  const { width = size, height = size, color: styleColor } = StyleSheet.flatten(style) ?? {};
  const pickColor = styleColor || color || theme.colors.defaultIconColor;

  return (
    <Svg
      width={getScaleSize(Number(width))}
      height={getScaleSize(Number(height))}
      viewBox="0 0 16 16"
      fill="none"
      {...rest}
    >
      <Path
        d="M14.6787 4.06238H14.369C14.2439 4.06238 14.123 4.08022 14.0083 4.11284C13.8914 3.503 13.3542 3.04069 12.7108 3.04069H12.4012C11.6726 3.04069 11.0798 3.63347 11.0798 4.36203V6.49478H4.92019V4.10081C4.92019 3.37222 4.32741 2.77947 3.59884 2.77947H3.28919C2.64578 2.77947 2.10862 3.24178 1.99175 3.85163C1.877 3.819 1.75609 3.80116 1.63103 3.80116H1.32137C0.592781 3.80116 0 4.39388 0 5.1225V10.6163C0 11.3449 0.592781 11.9376 1.32134 11.9376H1.631C1.75606 11.9376 1.87697 11.9198 1.99172 11.8871C2.10862 12.497 2.64578 12.9593 3.28916 12.9593H3.59881C4.32741 12.9593 4.92016 12.3665 4.92016 11.6379V9.71913H11.0797V11.8992C11.0797 12.6278 11.6725 13.2205 12.4011 13.2205H12.7108C13.3542 13.2205 13.8914 12.7582 14.0082 12.1484C14.123 12.181 14.2439 12.1989 14.3689 12.1989H14.6786C15.4072 12.1989 16 11.6061 16 10.8775V5.38372C16 4.65513 15.4072 4.06238 14.6787 4.06238ZM11.0798 8.73466H4.92019V7.47925H11.0798L11.0798 8.73466Z"
        fill={pickColor}
      />
    </Svg>
  );
};
