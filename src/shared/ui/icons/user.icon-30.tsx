import React from 'react';
import { StyleSheet } from 'react-native';
import Svg, { Path } from 'react-native-svg';

import { theme } from '@shared/config/theme';

import { getScaleSize } from 'src/utils/dimensions';

import { IconComponentProps } from './icons.config';

export const UserIcon30 = ({ style, size = 30, color, ...rest }: IconComponentProps) => {
  const { width = size, height = size, color: styleColor } = StyleSheet.flatten(style) ?? {};
  const pickColor = styleColor || color || theme.colors.defaultIconColor;

  return (
    <Svg
      width={getScaleSize(Number(width))}
      height={getScaleSize(Number(height))}
      viewBox="0 0 31 30"
      fill="none"
      {...rest}
    >
      <Path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M15.5 17.1356C9.68279 17.1356 5.00002 21.8391 5.00002 27.6H2.90002C2.90002 20.6228 8.55948 15 15.5 15C22.4406 15 28.1 20.6228 28.1 27.6H26C26 21.8391 21.3173 17.1356 15.5 17.1356Z"
        fill={pickColor}
      />
      <Path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M15.7101 16.9221C19.6534 16.9221 22.8501 13.6712 22.8501 9.66104C22.8501 5.65089 19.6534 2.40002 15.7101 2.40002C11.7668 2.40002 8.57007 5.65089 8.57007 9.66104C8.57007 13.6712 11.7668 16.9221 15.7101 16.9221ZM15.7101 14.7866C18.4936 14.7866 20.7501 12.4918 20.7501 9.66114C20.7501 6.83044 18.4936 4.53571 15.7101 4.53571C12.9266 4.53571 10.6701 6.83044 10.6701 9.66114C10.6701 12.4918 12.9266 14.7866 15.7101 14.7866Z"
        fill={pickColor}
      />
    </Svg>
  );
};
