import React from 'react';
import { StyleSheet } from 'react-native';
import Svg, { Path } from 'react-native-svg';

import { theme } from '@shared/config/theme';

import { getScaleSize } from 'src/utils/dimensions';

import { IconComponentProps } from './icons.config';

export const EditIcon14 = ({ style, size = 14, color, ...rest }: IconComponentProps) => {
  const { width = size, height = size, color: styleColor } = StyleSheet.flatten(style) ?? {};
  const pickColor = styleColor || color || theme.colors.defaultIconColor;

  return (
    <Svg
      width={getScaleSize(Number(width))}
      height={getScaleSize(Number(height))}
      viewBox="0 0 14 14"
      fill="none"
      {...rest}
    >
      <Path
        d="M11.0833 7.02693C10.7607 7.02693 10.5 7.2883 10.5 7.61023V12.2769C10.5 12.5983 10.2386 12.8602 9.9167 12.8602H1.75C1.42796 12.8602 1.1667 12.5983 1.1667 12.2769V4.11023C1.1667 3.78883 1.42796 3.52693 1.75 3.52693H6.4167C6.73927 3.52693 7 3.26556 7 2.94363C7 2.6216 6.73927 2.36023 6.4167 2.36023H1.75C0.785172 2.36023 0 3.1454 0 4.11023V12.2769C0 13.2418 0.785172 14.0269 1.75 14.0269H9.9167C10.8815 14.0269 11.6667 13.2418 11.6667 12.2769V7.61023C11.6667 7.28766 11.4059 7.02693 11.0833 7.02693Z"
        fill={pickColor}
      />
      <Path
        d="M5.46929 6.49557C5.42848 6.53638 5.40103 6.58829 5.38939 6.64426L4.97699 8.707C4.95777 8.8026 4.9881 8.90118 5.05689 8.97061C5.11232 9.02605 5.18698 9.05574 5.26346 9.05574C5.28204 9.05574 5.30138 9.05403 5.3206 9.04997L7.38271 8.63757C7.43985 8.62582 7.49176 8.59848 7.53203 8.55757L12.1474 3.94225L10.0853 1.88025L5.46929 6.49557Z"
        fill={pickColor}
      />
      <Path
        d="M13.573 0.453922C13.0043 -0.114849 12.0791 -0.114849 11.5109 0.453922L10.7036 1.2612L12.7657 3.32331L13.573 2.51592C13.8484 2.2412 14 1.87484 14 1.48519C14 1.09554 13.8484 0.729176 13.573 0.453922Z"
        fill={pickColor}
      />
    </Svg>
  );
};
