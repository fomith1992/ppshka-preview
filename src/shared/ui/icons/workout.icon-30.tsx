import React from 'react';
import { StyleSheet } from 'react-native';
import Svg, { Path } from 'react-native-svg';

import { theme } from '@shared/config/theme';

import { getScaleSize } from 'src/utils/dimensions';

import { IconComponentProps } from './icons.config';

export const WorkoutIcon30 = ({ style, size = 30, color, ...rest }: IconComponentProps) => {
  const { width = size, height = size, color: styleColor } = StyleSheet.flatten(style) ?? {};
  const pickColor = styleColor || color || theme.colors.defaultIconColor;

  return (
    <Svg
      width={getScaleSize(Number(width))}
      height={getScaleSize(Number(height))}
      viewBox="0 0 31 30"
      fill="none"
      {...rest}
    >
      <Path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M7.67178 2.59265C5.16523 2.59265 3.15453 4.66427 3.22932 7.16971L3.70472 23.0956C3.77641 25.4975 5.74422 27.4075 8.14718 27.4075H22.853C25.256 27.4075 27.2238 25.4975 27.2955 23.0956L27.7709 7.16971C27.8457 4.66427 25.835 2.59265 23.3284 2.59265H7.67178ZM8.01329 4.4445C6.33616 4.4445 4.99334 5.83527 5.05215 7.51137L5.58496 22.6966C5.64093 24.2915 6.95014 25.5556 8.5461 25.5556H22.4541C24.05 25.5556 25.3593 24.2915 25.4152 22.6966L25.948 7.51137C26.0069 5.83527 24.664 4.4445 22.9869 4.4445H8.01329Z"
        fill={pickColor}
      />
      <Path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M8.31345 13.7048C8.67536 9.96108 11.7548 7.03711 15.5001 7.03711C19.2454 7.03711 22.3249 9.96108 22.6868 13.7048C22.7261 14.112 22.3907 14.4445 21.9816 14.4445H9.01862C8.60952 14.4445 8.27408 14.112 8.31345 13.7048ZM10.1481 12.593C10.3753 10.3115 12.6847 8.51859 15.5001 8.51859C18.3155 8.51859 20.6249 10.3115 20.8521 12.593C20.8724 12.7965 20.7047 12.963 20.5001 12.963H10.5001C10.2956 12.963 10.1278 12.7965 10.1481 12.593Z"
        fill={pickColor}
      />
    </Svg>
  );
};
