import React from 'react';
import { StyleSheet } from 'react-native';
import Svg, { Path } from 'react-native-svg';

import { theme } from '@shared/config/theme';

import { getScaleSize } from 'src/utils/dimensions';

import { IconComponentProps } from './icons.config';

export const ArrowBackIcon14 = ({ style, size = 14, color, ...rest }: IconComponentProps) => {
  const { width = size, height = size, color: styleColor } = StyleSheet.flatten(style) ?? {};
  const pickColor = styleColor || color || theme.colors.defaultIconColor;

  return (
    <Svg
      width={getScaleSize(Number(width))}
      height={getScaleSize(Number(height))}
      viewBox="0 0 14 14"
      fill="none"
      {...rest}
    >
      <Path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M9.84873 2.13172C9.59422 1.84709 9.15716 1.82266 8.87253 2.07717L4.17452 6.27792C3.79496 6.61731 3.7896 7.20963 4.16295 7.55584L8.86327 11.9143C9.14325 12.1739 9.58069 12.1574 9.84031 11.8774C10.0999 11.5975 10.0834 11.16 9.80344 10.9004L5.52067 6.92912L9.79418 3.10793C10.0788 2.85342 10.1032 2.41636 9.84873 2.13172Z"
        fill={pickColor}
      />
    </Svg>
  );
};
