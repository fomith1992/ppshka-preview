import React from 'react';
import { StyleSheet } from 'react-native';
import Svg, { Path } from 'react-native-svg';

import { theme } from '@shared/config/theme';

import { getScaleSize } from 'src/utils/dimensions';

import { IconComponentProps } from './icons.config';

export const BagIcon18 = ({ style, size = 18, color, ...rest }: IconComponentProps) => {
  const { width = size, height = size, color: styleColor } = StyleSheet.flatten(style) ?? {};
  const pickColor = styleColor || color || theme.colors.gray7;

  return (
    <Svg
      width={getScaleSize(Number(width))}
      height={getScaleSize(Number(height))}
      viewBox="0 0 18 18"
      fill="none"
      {...rest}
    >
      <Path
        d="M15.7477 5.01188C15.7213 4.72219 15.4794 4.5 15.1875 4.5H12.375V3.375C12.375 1.51369 10.8619 0 9 0C7.13869 0 5.625 1.51369 5.625 3.375V4.5H2.8125C2.52112 4.5 2.27869 4.72219 2.25225 5.01188L1.12725 17.3869C1.11319 17.5438 1.1655 17.7002 1.27237 17.8166C1.37869 17.9336 1.52944 18 1.6875 18H16.3125C16.4706 18 16.6213 17.9336 16.7276 17.8166C16.8334 17.7002 16.8868 17.5444 16.8727 17.3869L15.7477 5.01188ZM6.75 3.375C6.75 2.13412 7.75969 1.125 9 1.125C10.2403 1.125 11.25 2.13412 11.25 3.375V4.5H6.75V3.375ZM2.304 16.875L3.32606 5.625H5.625V6.90581C5.28975 7.101 5.0625 7.45988 5.0625 7.875C5.0625 8.496 5.5665 9 6.1875 9C6.8085 9 7.3125 8.496 7.3125 7.875C7.3125 7.45988 7.08525 7.101 6.75 6.90581V5.625H11.25V6.90581C10.9147 7.101 10.6875 7.45931 10.6875 7.875C10.6875 8.496 11.1915 9 11.8125 9C12.4335 9 12.9375 8.496 12.9375 7.875C12.9375 7.45988 12.7108 7.101 12.375 6.90581V5.625H14.6739L15.696 16.875H2.304Z"
        fill={pickColor}
      />
    </Svg>
  );
};
