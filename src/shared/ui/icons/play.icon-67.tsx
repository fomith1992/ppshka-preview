import React from 'react';
import { StyleSheet } from 'react-native';
import Svg, { Circle, Path } from 'react-native-svg';

import { theme } from '@shared/config/theme';

import { getScaleSize } from 'src/utils/dimensions';

import { IconComponentProps } from './icons.config';

export const PlayIcon67 = ({ style, size = 67, color, ...rest }: IconComponentProps) => {
  const { width = size, height = size, color: styleColor } = StyleSheet.flatten(style) ?? {};
  const pickColor = styleColor || color || theme.colors.defaultIconColor;

  return (
    <Svg
      width={getScaleSize(Number(width))}
      height={getScaleSize(Number(height))}
      viewBox="0 0 67 67"
      fill="none"
      {...rest}
    >
      <Circle cx="33.5" cy="33.5" r="33.5" fill="white" />
      <Path
        d="M40 28.3039C44 30.6132 46 31.7679 46 33.5C46 35.232 44 36.3868 40 38.6962L34.75 41.7272C30.75 44.0366 28.75 45.1913 27.25 44.3253C25.75 43.4593 25.75 41.1499 25.75 36.5311L25.75 30.4689C25.75 25.8501 25.75 23.5407 27.25 22.6747C28.75 21.8087 30.75 22.9634 34.75 25.2728L40 28.3039Z"
        fill={pickColor}
      />
    </Svg>
  );
};
