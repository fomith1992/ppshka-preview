import React from 'react';
import { StyleSheet } from 'react-native';
import Svg, { Path } from 'react-native-svg';

import { theme } from '@shared/config/theme';

import { getScaleSize } from 'src/utils/dimensions';

import { IconComponentProps } from './icons.config';

export const FavoritesIcon30 = ({ style, size = 30, color, ...rest }: IconComponentProps) => {
  const { width = size, height = size, color: styleColor } = StyleSheet.flatten(style) ?? {};
  const pickColor = styleColor || color || theme.colors.defaultIconColor;

  return (
    <Svg
      width={getScaleSize(Number(width))}
      height={getScaleSize(Number(height))}
      viewBox="0 0 31 30"
      fill="none"
      {...rest}
    >
      <Path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M9.38902 2.59253C6.93443 2.59253 4.94458 4.57367 4.94458 7.01754V26.6688C4.94458 27.2297 5.54861 27.5853 6.04228 27.315L15.5001 22.1363L24.958 27.315C25.4517 27.5853 26.0557 27.2297 26.0557 26.6688V7.01754C26.0557 4.57368 24.0658 2.59253 21.6112 2.59253H9.38902ZM6.79643 24.0922V7.38629C6.79643 5.75705 8.123 4.43629 9.75939 4.43629H21.2409C22.8773 4.43629 24.2038 5.75705 24.2038 7.38629V24.0922C24.2038 24.3731 23.901 24.5508 23.6542 24.4149L15.8587 20.1213C15.6356 19.9984 15.3647 19.9984 15.1416 20.1213L7.34608 24.4149C7.09923 24.5508 6.79643 24.3731 6.79643 24.0922Z"
        fill={pickColor}
      />
    </Svg>
  );
};
