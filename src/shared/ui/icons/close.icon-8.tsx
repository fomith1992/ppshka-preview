import React from 'react';
import { StyleSheet } from 'react-native';
import Svg, { Path } from 'react-native-svg';

import { theme } from '@shared/config/theme';

import { getScaleSize } from 'src/utils/dimensions';

import { IconComponentProps } from './icons.config';

export const CloseIcon8 = ({ style, size = 8, color, ...rest }: IconComponentProps) => {
  const { width = size, height = size, color: styleColor } = StyleSheet.flatten(style) ?? {};
  const pickColor = styleColor || color || theme.colors.defaultIconColor;

  return (
    <Svg
      width={getScaleSize(Number(width))}
      height={getScaleSize(Number(height))}
      viewBox="0 0 8 8"
      fill="none"
      {...rest}
    >
      <Path
        d="M6.8808 0.490669C7.05436 0.317103 7.33577 0.317103 7.50934 0.490669C7.6829 0.664235 7.6829 0.945642 7.50934 1.11921L1.11919 7.50935C0.94562 7.68292 0.664213 7.68292 0.490646 7.50935C0.31708 7.33579 0.31708 7.05438 0.490646 6.88082L6.8808 0.490669Z"
        fill={pickColor}
      />
      <Path
        d="M0.490539 1.11922C0.316973 0.94565 0.316973 0.664244 0.490539 0.490677C0.664106 0.317111 0.945513 0.317111 1.11908 0.490677L7.50923 6.88083C7.6828 7.05439 7.6828 7.3358 7.50923 7.50937C7.33567 7.68293 7.05426 7.68293 6.88069 7.50937L0.490539 1.11922Z"
        fill={pickColor}
      />
    </Svg>
  );
};
