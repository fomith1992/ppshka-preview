import React from 'react';
import { StyleProp, ViewProps, ViewStyle } from 'react-native';

export interface IconComponentProps extends ViewProps {
  size?: number;
  color?: string;
  style?: StyleProp<
    ViewStyle & {
      color?: string;
    }
  >;
}

export type IconComponent = React.ComponentType<IconComponentProps>;
