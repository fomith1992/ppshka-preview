import React from 'react';
import { StyleSheet } from 'react-native';
import Svg, { Path } from 'react-native-svg';

import { theme } from '@shared/config/theme';

import { getScaleSize } from 'src/utils/dimensions';

import { IconComponentProps } from './icons.config';

export const ImageIcon14 = ({ style, size = 14, color, ...rest }: IconComponentProps) => {
  const { width = size, height = size, color: styleColor } = StyleSheet.flatten(style) ?? {};
  const pickColor = styleColor || color || theme.colors.defaultIconColor;

  return (
    <Svg
      width={getScaleSize(Number(width))}
      height={getScaleSize(Number(height))}
      viewBox="0 0 14 14"
      fill="none"
      {...rest}
    >
      <Path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M0.734419 0.734419C0 1.46884 0 2.65087 0 5.01493V8.98507C0 11.3491 0 12.5312 0.734419 13.2656C1.46884 14 2.65087 14 5.01493 14H8.98507C11.3491 14 12.5312 14 13.2656 13.2656C14 12.5312 14 11.3491 14 8.98507V5.01493C14 2.65087 14 1.46884 13.2656 0.734419C12.5312 0 11.3491 0 8.98507 0H5.01493C2.65087 0 1.46884 0 0.734419 0.734419ZM1.53439 1.53439C1.04478 2.024 1.04478 2.81202 1.04478 4.38806V9.61194C1.04478 11.188 1.04478 11.976 1.53439 12.4656C2.024 12.9552 2.81202 12.9552 4.38806 12.9552H9.61194C11.188 12.9552 11.976 12.9552 12.4656 12.4656C12.9552 11.976 12.9552 11.188 12.9552 9.61194V4.38806C12.9552 2.81202 12.9552 2.024 12.4656 1.53439C11.976 1.04478 11.188 1.04478 9.61194 1.04478H4.38806C2.81202 1.04478 2.024 1.04478 1.53439 1.53439Z"
        fill={pickColor}
      />
      <Path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M4.17921 5.85075C4.64082 5.85075 5.01503 5.47655 5.01503 5.01493C5.01503 4.55332 4.64082 4.17911 4.17921 4.17911C3.7176 4.17911 3.34339 4.55332 3.34339 5.01493C3.34339 5.47655 3.7176 5.85075 4.17921 5.85075ZM4.17921 6.68658C5.10243 6.68658 5.85085 5.93816 5.85085 5.01493C5.85085 4.09171 5.10243 3.34329 4.17921 3.34329C3.25599 3.34329 2.50757 4.09171 2.50757 5.01493C2.50757 5.93816 3.25599 6.68658 4.17921 6.68658Z"
        fill={pickColor}
      />
      <Path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M10.8757 7.20463L13.026 9.11565L13.7201 8.3347L11.494 6.35638C11.1142 6.01887 10.535 6.04409 10.186 6.41333L7.14396 9.63174L5.59618 8.46209C5.31521 8.24976 4.93914 8.21224 4.62175 8.36487L0.400391 10.3949L0.853185 11.3365L5.01712 9.33404L6.5887 10.5217C6.97112 10.8107 7.50971 10.7658 7.83897 10.4174L10.8757 7.20463Z"
        fill={pickColor}
      />
    </Svg>
  );
};
