import React from 'react';
import { StyleSheet } from 'react-native';
import Svg, { Path } from 'react-native-svg';

import { theme } from '@shared/config/theme';

import { getScaleSize } from 'src/utils/dimensions';

import { IconComponentProps } from './icons.config';

export const LogOutIcon18 = ({ style, size = 18, color, ...rest }: IconComponentProps) => {
  const { width = size, height = size, color: styleColor } = StyleSheet.flatten(style) ?? {};
  const pickColor = styleColor || color || theme.colors.defaultIconColor;

  return (
    <Svg
      width={getScaleSize(Number(width))}
      height={getScaleSize(Number(height))}
      viewBox="0 0 18 18"
      fill="none"
      {...rest}
    >
      <Path
        fill-rule="evenodd"
        clip-rule="evenodd"
        d="M15.8326 7.16504C15.4946 7.16504 15.2207 7.41084 15.2207 7.71406V11.5572C15.2207 12.608 15.2194 13.3408 15.1365 13.8937C15.0561 14.4309 14.9088 14.7153 14.6831 14.9179C14.4573 15.1205 14.1403 15.2526 13.5416 15.3248C12.9254 15.3992 12.1086 15.4003 10.9376 15.4003H7.06161C5.89082 15.4003 5.07429 15.3992 4.45819 15.3248C3.85972 15.2526 3.54275 15.1205 3.31694 14.9179C3.09115 14.7153 2.94393 14.4309 2.86345 13.8937C2.7806 13.3408 2.7793 12.608 2.7793 11.5572V7.71406C2.7793 7.41084 2.50535 7.16504 2.16743 7.16504C1.8295 7.16504 1.55555 7.41084 1.55555 7.71406V11.5974C1.55554 12.5985 1.55552 13.4054 1.65062 14.04C1.74935 14.6989 1.96056 15.2537 2.45162 15.6943C2.94266 16.1349 3.56087 16.3245 4.2951 16.4131C5.00227 16.4984 5.90138 16.4984 7.01683 16.4984H10.9823C12.0981 16.4984 12.9974 16.4984 13.7047 16.4131C14.439 16.3245 15.0573 16.135 15.5484 15.6944C16.0394 15.2537 16.2507 14.6989 16.3494 14.04C16.4445 13.4054 16.4445 12.5985 16.4444 11.5974L16.4444 7.71406C16.4444 7.41084 16.1705 7.16504 15.8326 7.16504Z"
        fill={pickColor}
      />
      <Path
        fill-rule="evenodd"
        clip-rule="evenodd"
        d="M8.88888 3.1095C8.52069 3.1095 8.22222 3.40797 8.22222 3.77616V10.8873C8.22222 11.2555 8.52069 11.5539 8.88888 11.5539C9.25707 11.5539 9.55555 11.2555 9.55555 10.8873V3.77616C9.55555 3.40797 9.25707 3.1095 8.88888 3.1095Z"
        fill={pickColor}
      />
      <Path
        fill-rule="evenodd"
        clip-rule="evenodd"
        d="M8.41141 1.86656C8.15446 2.13026 8.15992 2.55233 8.42362 2.80929L11.5347 5.84087C11.7984 6.09783 12.2205 6.09236 12.4775 5.82866C12.7344 5.56497 12.729 5.14289 12.4653 4.88593L9.35414 1.85435C9.09045 1.5974 8.66837 1.60286 8.41141 1.86656Z"
        fill={pickColor}
      />
      <Path
        fill-rule="evenodd"
        clip-rule="evenodd"
        d="M9.3687 1.86894C9.11308 1.60395 8.69104 1.59635 8.42604 1.85198L5.28335 4.88356C5.01836 5.13919 5.01076 5.56123 5.26638 5.82622C5.52201 6.09121 5.94405 6.09881 6.20904 5.84318L9.35174 2.8116C9.61673 2.55597 9.62432 2.13393 9.3687 1.86894Z"
        fill={pickColor}
      />
    </Svg>
  );
};
