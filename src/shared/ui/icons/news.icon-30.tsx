import React from 'react';
import { StyleSheet } from 'react-native';
import Svg, { Path } from 'react-native-svg';

import { theme } from '@shared/config/theme';

import { getScaleSize } from 'src/utils/dimensions';

import { IconComponentProps } from './icons.config';

export const NewsIcon30 = ({ style, size = 30, color, ...rest }: IconComponentProps) => {
  const { width = size, height = size, color: styleColor } = StyleSheet.flatten(style) ?? {};
  const pickColor = styleColor || color || theme.colors.defaultIconColor;

  return (
    <Svg
      width={getScaleSize(Number(width))}
      height={getScaleSize(Number(height))}
      viewBox="0 0 31 30"
      fill="none"
      {...rest}
    >
      <Path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M7.53697 2.59253C5.08237 2.59253 3.09253 4.58237 3.09253 7.03697V22.9629C3.09253 25.4175 5.08237 27.4073 7.53697 27.4073H23.4629C25.9175 27.4073 27.9073 25.4175 27.9073 22.9629V7.03697C27.9073 4.58237 25.9175 2.59253 23.4629 2.59253H7.53697ZM7.90734 4.44438C6.27094 4.44438 4.94438 5.77094 4.94438 7.40734V22.5925C4.94438 24.2289 6.27094 25.5555 7.90734 25.5555H23.0925C24.7289 25.5555 26.0555 24.2289 26.0555 22.5925V7.40734C26.0555 5.77094 24.7289 4.44438 23.0925 4.44438H7.90734Z"
        fill={pickColor}
      />
      <Path
        d="M8.64807 10.1851C8.64807 9.67371 9.06262 9.25916 9.574 9.25916H21.4258C21.9372 9.25916 22.3518 9.67371 22.3518 10.1851C22.3518 10.6965 21.9372 11.111 21.4258 11.111H9.574C9.06262 11.111 8.64807 10.6965 8.64807 10.1851Z"
        fill={pickColor}
      />
      <Path
        d="M8.64807 14.9999C8.64807 14.4885 9.06262 14.074 9.574 14.074H15.4999C16.0113 14.074 16.4258 14.4885 16.4258 14.9999C16.4258 15.5113 16.0113 15.9258 15.4999 15.9258H9.574C9.06262 15.9258 8.64807 15.5113 8.64807 14.9999Z"
        fill={pickColor}
      />
      <Path
        d="M8.64807 19.8148C8.64807 19.3035 9.06262 18.8889 9.574 18.8889H18.8333C19.3446 18.8889 19.7592 19.3035 19.7592 19.8148C19.7592 20.3262 19.3446 20.7408 18.8333 20.7408H9.574C9.06262 20.7408 8.64807 20.3262 8.64807 19.8148Z"
        fill={pickColor}
      />
    </Svg>
  );
};
