import React from 'react';
import { StyleSheet } from 'react-native';
import Svg, { Path } from 'react-native-svg';

import { theme } from '@shared/config/theme';

import { getScaleSize } from 'src/utils/dimensions';

import { IconComponentProps } from './icons.config';

export const PlayIcon23 = ({ style, size = 25, color, ...rest }: IconComponentProps) => {
  const { width = size, height = size, color: styleColor } = StyleSheet.flatten(style) ?? {};
  const pickColor = styleColor || color || theme.colors.defaultIconColor;

  return (
    <Svg
      width={getScaleSize(Number(width))}
      height={getScaleSize(Number(height))}
      viewBox="0 0 23 23"
      {...rest}
    >
      <Path
        d="M15 6.304c4 2.31 6 3.464 6 5.196 0 1.732-2 2.887-6 5.196l-5.25 3.031c-4 2.31-6 3.464-7.5 2.598-1.5-.866-1.5-3.175-1.5-7.794V8.47c0-4.619 0-6.928 1.5-7.794 1.5-.866 3.5.288 7.5 2.598L15 6.303z"
        fill={pickColor}
      />
    </Svg>
  );
};
