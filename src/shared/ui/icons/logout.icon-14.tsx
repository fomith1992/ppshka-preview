import React from 'react';
import { StyleSheet } from 'react-native';
import Svg, { Path } from 'react-native-svg';

import { theme } from '@shared/config/theme';

import { getScaleSize } from 'src/utils/dimensions';

import { IconComponentProps } from './icons.config';

export const LogOutIcon14 = ({ style, size = 14, color, ...rest }: IconComponentProps) => {
  const { width = size, height = size, color: styleColor } = StyleSheet.flatten(style) ?? {};
  const pickColor = styleColor || color || theme.colors.defaultIconColor;

  return (
    <Svg
      width={getScaleSize(Number(width))}
      height={getScaleSize(Number(height))}
      viewBox="0 0 14 14"
      fill="none"
      {...rest}
    >
      <Path
        d="M6.97676 12.8139H1.74418C1.42324 12.8139 1.16279 12.5534 1.16279 12.2325V1.7674C1.16279 1.44646 1.42327 1.18601 1.74418 1.18601H6.97676C7.29827 1.18601 7.55814 0.926139 7.55814 0.604631C7.55814 0.283123 7.29827 0.0231934 6.97676 0.0231934H1.74418C0.782551 0.0231934 0 0.805772 0 1.7674V12.2325C0 13.1941 0.782551 13.9767 1.74418 13.9767H6.97676C7.29827 13.9767 7.55814 13.7168 7.55814 13.3953C7.55814 13.0738 7.29827 12.8139 6.97676 12.8139Z"
        fill={pickColor}
      />
      <Path
        d="M13.8267 6.58606L10.2919 3.09768C10.0639 2.87209 9.69535 2.87502 9.46977 3.1035C9.24418 3.33199 9.24651 3.70001 9.47559 3.92559L12.0017 6.4186H5.23255C4.91104 6.4186 4.65117 6.67848 4.65117 6.99999C4.65117 7.32149 4.91104 7.5814 5.23255 7.5814H12.0017L9.47559 10.0744C9.24653 10.3 9.24478 10.668 9.46977 10.8965C9.58371 11.0116 9.73372 11.0697 9.88372 11.0697C10.0314 11.0697 10.1791 11.0139 10.2919 10.9023L13.8267 7.41392C13.9372 7.30462 14 7.15576 14 6.99996C14 6.84421 13.9378 6.69595 13.8267 6.58606Z"
        fill={pickColor}
      />
    </Svg>
  );
};
