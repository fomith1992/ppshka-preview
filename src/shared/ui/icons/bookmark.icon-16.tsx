import React from 'react';
import { StyleSheet } from 'react-native';
import Svg, { Path } from 'react-native-svg';

import { theme } from '@shared/config/theme';

import { getScaleSize } from 'src/utils/dimensions';

import { IconComponentProps } from './icons.config';

export const BookmarkIcon16 = ({ style, size = 16, color, ...rest }: IconComponentProps) => {
  const { width = size, height = size, color: styleColor } = StyleSheet.flatten(style) ?? {};
  const pickColor = styleColor || color || theme.colors.defaultIconColor;

  return (
    <Svg
      width={getScaleSize(Number(width))}
      height={getScaleSize(Number(height))}
      viewBox="0 0 16 16"
      fill="none"
      {...rest}
    >
      <Path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M4.74077 1.38271C3.43165 1.38271 2.3704 2.43932 2.3704 3.74271V14.2234C2.3704 14.5225 2.69254 14.7122 2.95583 14.568L8.00003 11.8061L13.0442 14.568C13.3075 14.7122 13.6297 14.5225 13.6297 14.2234V3.74271C13.6297 2.43932 12.5684 1.38271 11.2593 1.38271H4.74077ZM3.35805 12.8492V3.93938C3.35805 3.07045 4.06555 2.36604 4.9383 2.36604H11.0618C11.9345 2.36604 12.642 3.07045 12.642 3.93938V12.8492C12.642 12.999 12.4805 13.0938 12.3489 13.0213L8.19126 10.7314C8.07227 10.6659 7.92778 10.6659 7.80879 10.7314L3.6512 13.0213C3.51954 13.0938 3.35805 12.999 3.35805 12.8492Z"
        fill={pickColor}
      />
    </Svg>
  );
};
