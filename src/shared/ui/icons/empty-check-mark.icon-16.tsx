import React from 'react';
import { StyleSheet } from 'react-native';
import Svg, { Path } from 'react-native-svg';

import { theme } from '@shared/config/theme';

import { getScaleSize } from 'src/utils/dimensions';

import { IconComponentProps } from './icons.config';

export const CheckMarkEmpty16 = ({ style, size = 16, color, ...rest }: IconComponentProps) => {
  const { width = size, height = size, color: styleColor } = StyleSheet.flatten(style) ?? {};
  const pickColor = styleColor || color || theme.colors.defaultIconColor;

  return (
    <Svg
      width={getScaleSize(Number(width))}
      height={getScaleSize(Number(height))}
      viewBox="0 0 16 16"
      fill="none"
      {...rest}
    >
      <Path
        d="M16 8C16 12.4183 12.4183 16 8 16C3.58172 16 0 12.4183 0 8C0 3.58172 3.58172 0 8 0C12.4183 0 16 3.58172 16 8ZM1.31636 8C1.31636 11.6913 4.30873 14.6836 8 14.6836C11.6913 14.6836 14.6836 11.6913 14.6836 8C14.6836 4.30873 11.6913 1.31636 8 1.31636C4.30873 1.31636 1.31636 4.30873 1.31636 8Z"
        fill={pickColor}
      />
    </Svg>
  );
};
