import React from 'react';
import { StyleSheet } from 'react-native';
import Svg, { Path } from 'react-native-svg';

import { theme } from '@shared/config/theme';

import { getScaleSize } from 'src/utils/dimensions';

import { IconComponentProps } from './icons.config';

export const MessageCloudIcon16 = ({ style, size = 16, color, ...rest }: IconComponentProps) => {
  const { width = size, height = size, color: styleColor } = StyleSheet.flatten(style) ?? {};
  const pickColor = styleColor || color || theme.colors.defaultIconColor;

  return (
    <Svg
      width={getScaleSize(Number(width))}
      height={getScaleSize(Number(height))}
      viewBox="0 0 16 16"
      fill="none"
      {...rest}
    >
      <Path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M3.09729 1.58025C1.82308 1.58025 0.790131 2.6415 0.790131 3.95062V14.0248C0.790131 14.3197 1.09317 14.5106 1.34898 14.377L3.66181 13.1684C3.98584 12.9991 4.34423 12.9108 4.70776 12.9108H12.9027C14.1769 12.9108 15.2099 11.8495 15.2099 10.5404V3.95062C15.2099 2.6415 14.1769 1.58025 12.9027 1.58025H3.09729ZM3.28955 2.55702C2.44008 2.55702 1.75145 3.26452 1.75145 4.13726V12.7752C1.75145 12.9247 1.9069 13.02 2.03499 12.949L3.5245 12.1235C3.74888 11.9991 3.99982 11.934 4.25475 11.934H9.05746H12.7105C13.5599 11.934 14.2486 11.2265 14.2486 10.3538V4.13726C14.2486 3.26452 13.5599 2.55702 12.7105 2.55702H3.28955Z"
        fill={pickColor}
      />
      <Path
        d="M4.44315 4.99894C4.44315 4.72922 4.65834 4.51056 4.9238 4.51056H11.0762C11.3417 4.51056 11.5569 4.72922 11.5569 4.99894C11.5569 5.26867 11.3417 5.48733 11.0762 5.48733H4.9238C4.65834 5.48733 4.44315 5.26867 4.44315 4.99894Z"
        fill={pickColor}
      />
      <Path
        d="M4.44315 7.14784C4.44315 6.87811 4.65834 6.65945 4.9238 6.65945H8.00002C8.26548 6.65945 8.48068 6.87811 8.48068 7.14784C8.48068 7.41757 8.26548 7.63623 8.00002 7.63623H4.9238C4.65834 7.63623 4.44315 7.41757 4.44315 7.14784Z"
        fill={pickColor}
      />
      <Path
        d="M4.44315 9.29675C4.44315 9.02702 4.65834 8.80836 4.9238 8.80836H9.73039C9.99585 8.80836 10.211 9.02702 10.211 9.29675C10.211 9.56648 9.99585 9.78513 9.73039 9.78513H4.9238C4.65834 9.78513 4.44315 9.56648 4.44315 9.29675Z"
        fill={pickColor}
      />
    </Svg>
  );
};
