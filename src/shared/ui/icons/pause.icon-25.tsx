import React from 'react';
import { StyleSheet } from 'react-native';
import Svg, { Path } from 'react-native-svg';

import { theme } from '@shared/config/theme';

import { getScaleSize } from 'src/utils/dimensions';

import { IconComponentProps } from './icons.config';

export const PauseIcon25 = ({ style, size = 25, color, ...rest }: IconComponentProps) => {
  const { width = size, height = size, color: styleColor } = StyleSheet.flatten(style) ?? {};
  const pickColor = styleColor || color || theme.colors.defaultIconColor;

  return (
    <Svg
      width={getScaleSize(Number(width))}
      height={getScaleSize(Number(height))}
      viewBox="0 0 25 25"
      fill="none"
      {...rest}
    >
      <Path
        d="M9.448 21.518a3.482 3.482 0 01-6.965 0V3.482a3.482 3.482 0 016.965 0v18.036zM22.516 21.518a3.482 3.482 0 01-6.964 0V3.482a3.483 3.483 0 016.964 0v18.036z"
        fill={pickColor}
      />
    </Svg>
  );
};
