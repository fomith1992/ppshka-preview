import React from 'react';
import { StyleSheet } from 'react-native';
import Svg, { Path } from 'react-native-svg';

import { theme } from '@shared/config/theme';

import { getScaleSize } from 'src/utils/dimensions';

import { IconComponentProps } from './icons.config';

export const DashBoardIcon30 = ({ style, size = 30, color, ...rest }: IconComponentProps) => {
  const { width = size, height = size, color: styleColor } = StyleSheet.flatten(style) ?? {};
  const pickColor = styleColor || color || theme.colors.defaultIconColor;

  return (
    <Svg
      width={getScaleSize(Number(width))}
      height={getScaleSize(Number(height))}
      viewBox="0 0 31 30"
      fill="none"
      {...rest}
    >
      <Path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M4.39428 3.8944C3.09253 5.19615 3.09253 7.29128 3.09253 11.4815V18.5186C3.09253 22.7088 3.09253 24.804 4.39428 26.1057C5.69602 27.4075 7.79116 27.4075 11.9814 27.4075H19.0185C23.2087 27.4075 25.3038 27.4075 26.6056 26.1057C27.9073 24.804 27.9073 22.7088 27.9073 18.5186V11.4815C27.9073 7.29128 27.9073 5.19615 26.6056 3.8944C25.3038 2.59265 23.2087 2.59265 19.0185 2.59265H11.9814C7.79116 2.59265 5.69602 2.59265 4.39428 3.8944ZM5.81221 5.31234C4.94438 6.18017 4.94438 7.57692 4.94438 10.3704V19.6297C4.94438 22.4232 4.94438 23.82 5.81221 24.6878C6.68004 25.5556 8.0768 25.5556 10.8703 25.5556H20.1296C22.9231 25.5556 24.3198 25.5556 25.1877 24.6878C26.0555 23.82 26.0555 22.4232 26.0555 19.6297V10.3704C26.0555 7.57692 26.0555 6.18017 25.1877 5.31234C24.3198 4.4445 22.9231 4.4445 20.1296 4.4445H10.8703C8.0768 4.4445 6.68004 4.4445 5.81221 5.31234Z"
        fill={pickColor}
      />
      <Path
        d="M14.7164 9.67251C14.7164 9.23981 15.0672 8.88904 15.4999 8.88904C15.9326 8.88904 16.2834 9.23981 16.2834 9.67251V20.3278C16.2834 20.7605 15.9326 21.1113 15.4999 21.1113C15.0672 21.1113 14.7164 20.7605 14.7164 20.3278V9.67251Z"
        fill={pickColor}
      />
      <Path
        d="M10.1723 15.7837C9.73957 15.7837 9.38879 15.4329 9.38879 15.0002C9.38879 14.5675 9.73957 14.2167 10.1723 14.2167L20.8275 14.2167C21.2602 14.2167 21.611 14.5675 21.611 15.0002C21.611 15.4329 21.2602 15.7837 20.8275 15.7837L10.1723 15.7837Z"
        fill={pickColor}
      />
    </Svg>
  );
};
