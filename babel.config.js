const COMMON_PLUGINS = [
  [
    'module-resolver',
    {
      cwd: 'babelrc',
      root: ['.'],
      extensions: ['.js', '.ts', '.tsx', '.ios.js', '.android.js'],
      alias: require('./aliases.js'),
    },
  ],
];

module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins: [...COMMON_PLUGINS, 'react-native-reanimated/plugin'],
  env: {
    production: {
      plugins: [...COMMON_PLUGINS, 'transform-remove-console'],
    },
  },
};
